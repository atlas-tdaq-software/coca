"""File-like object representing read-only file in CASTOR using RFIO library.

This software was developed for the ATLAS project.  If you use all or
part of it, please give an appropriate acknowledgment.
"""
from __future__ import annotations

import ctypes
import errno
import logging
import os


class CastorFile:
    """File-like object for CASTOR files."""

    def __init__(self, name: str):

        self.name = str(name)
        self.closed = True

        self.so = ctypes.CDLL('libshift.so')
        self.serrno = ctypes.c_int.in_dll(self.so, 'serrno')

        self.fd = self.so.rfio_open64(self.name, os.O_RDONLY | os.O_LARGEFILE, 0x644)
        logging.debug("CastorFile: fd=%d name=%s", self.fd, self.name)
        if self.fd < 0:
            raise IOError(self.serrno.value, os.strerror(self.serrno.value), self.name)

        self.closed = False

    def __del__(self) -> None:
        self.close()

    def close(self) -> None:
        if not self.closed:
            if self.so.rfio_close(self.fd) < 0:
                raise IOError(self.serrno.value, os.strerror(self.serrno.value), self.name)
            self.fd = None
            self.closed = True

    def flush(self) -> None:
        pass

    def fileno(self) -> int:
        return self.fd

    def isatty(self) -> bool:
        return False

    def next(self) -> None:
        raise IOError(errno.ENOSYS, os.strerror(errno.ENOSYS), self.name)

    def read(self, size: int | None = None) -> bytes:
        logging.debug("castor file read: size=%s", size)
        if size:
            buf = ctypes.create_string_buffer(size)
            self.so.rfio_read.argtypes = [ctypes.c_int, ctypes.c_void_p, ctypes.c_int]
            ret_size = self.so.rfio_read(self.fd, buf, size)
            logging.debug("castor file read: ret_size=%s", ret_size)
            if ret_size < 0:
                raise IOError(self.serrno.value, os.strerror(self.serrno.value), self.name)
            return buf[:ret_size]  # type: ignore
        else:
            res = []
            while True:
                nx = self.read(1024 * 1024)
                if nx:
                    res.append(nx)
                else:
                    break
            return b"".join(res)

    def readline(self, size: int | None = None) -> None:
        raise IOError(errno.ENOSYS, os.strerror(errno.ENOSYS), self.name)

    def readlines(self, size: int | None = None) -> None:
        raise IOError(errno.ENOSYS, os.strerror(errno.ENOSYS), self.name)

    def seek(self, offset: int, whence: int = os.SEEK_SET) -> None:
        logging.debug("castor file seek: offset=%s whence=%s", offset, whence)
        self.so.rfio_lseek64.restype = ctypes.c_longlong
        off = self.so.rfio_lseek64(self.fd, ctypes.c_longlong(offset), whence)
        # it seems there is a bugin castor when whence is SEEK_END then
        # it does not work unless we call SEEK_CUR too
        off = self.so.rfio_lseek64(self.fd, ctypes.c_longlong(0), os.SEEK_CUR)
        logging.debug("castor file seek: new offset=%s", off)

    def tell(self) -> int:
        self.so.rfio_lseek64.restype = ctypes.c_longlong
        off = self.so.rfio_lseek64(self.fd, ctypes.c_longlong(0), os.SEEK_CUR)
        logging.debug("castor file tell: offset=%s", off)
        return off
