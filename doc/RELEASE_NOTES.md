# coca

- Tag `coca-03-16-00`
- Add support for location overrides via metadata config.
- Add `coca-cli` script which will be main CLI for coca in the future.
- Tag `coca-03-15-12`
- Fix stringification of MD5 checksum.
- Tag `coca-03-15-11`
- Fix the location of `py.typed` in `CMakeLists.txt`
- Tag `coca-03-15-10`
- Replace deprecated filesystem path method.
- Tag `coca-03-15-09`
- Add type annotations for Python modules.

## tdaq-11-02-01

## tdaq-11-02-00

- Tag `coca-03-15-08`
- Fix syntax in `coca-migrate-3-to-4.sql`
- Tag `coca-03-15-07`
- Add missing include file for gcc 13
- Tag `coca-03-15-06`
- Enable space cleanup across multiple servers/releases (ADHI-4902)

## tdaq-10-00-00

- Tag `coca-03-15-05`
- Add support for shema version 4 (ADHI-4852)
- Tag `coca-03-15-03` (should make as a patch into tdaq-09-04-00)
- Fix for Python wrapper RemoteFile.size method to return integer
- Tag `coca-03-15-02` (should make as a patch into tdaq-09-04-00)
- Fix handling of NULL FILE_SIZE in DBStat.

## tdaq-09-04-00

- Tag `coca-03-15-00`
- Saving exact file size and checksum in database, some operations do checks
  for consistency. The code still supports all known versions of schema.
- Tag `coca-03-14-00`
- Improve cache space management across multiple instances of coca_server,
  now they can cleanup files left from other instances (older releases).
- Tag `coca-03-13-00`
- Some fixes for handling of removed archives (we never removed archives yet).
- Add unit tests for database classes (running on SQLite schema).
- Add code to support schema versions up to 3.
- Tag `coca-03-12-10`
- Add schema migration scripts for versions up to 3 (for Oracle only).

## tdaq-09-03-00

- Tag `coca-03-12-09`.
