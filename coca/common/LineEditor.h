#ifndef COCA_LINEEDITOR_H
#define COCA_LINEEDITOR_H

//-----------------
// C/C++ Headers --
//-----------------
#include <regex>
#include <string>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq::coca {

/// @addtogroup coca_client

/**
 *  @ingroup coca_client
 *
 *  @brief Class providing tools for editing strings using JSON-encoded rules.
 *
 *  @author Andy Salnikov
 */
class LineEditor {
public:

    // Rule class is exposed for testing only.
    class Rule {
    public:
        Rule(const std::regex& match, const std::string fmt, bool keep_original, bool discard, bool stop);
        // Edit the line, return resulting lines and a flag which tells whether
        // so stop further processing with remaining rules.
        // Returned vector may be empty, if rule is configured to discard
        // matching lines. If there is no match then original line returned
        // without change.
        std::pair<std::vector<std::string>, bool> apply(const std::string& line) const;
    private:
        std::regex m_match;   // Regular expression for matching the lines.
        std::string m_fmt;    // Format string to use for replacement.
        bool m_keep_original; // If true then original line is preserved.
        bool m_discard;       // If true then discard the line completely.
        bool m_stop;          // If true then processing stops after this rule.
    };

    /// @brief  Construct editing rules from a JSON-encoded string.
    /// @param rules JSON string, must be array of rule objects.
    /// @throws LineEditorRulesError in case rule string is not valid.
    explicit LineEditor(const std::string& rules);

    // Access set of rules, for testing only.
    const std::vector<Rule>& rules() const { return m_rules; }

    /// @brief Edit lines
    /// @param lines Sequence of lines to edit.
    /// @return Sequence of lines after editing.
    std::vector<std::string> apply(const std::vector<std::string>& lines) const;

private:

    std::vector<Rule> m_rules;

};

} // namespace daq::coca

#endif // COCA_LINEEDITOR_H
