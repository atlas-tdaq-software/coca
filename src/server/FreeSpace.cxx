//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Description:
//      Class FreeSpace...
//
// Author List:
//      Andy Salnikov
//
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/server/FreeSpace.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <ctime>
#include <boost/filesystem.hpp>
#include <boost/thread/mutex.hpp>
#include <stdexcept>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/FileCheck.h"
#include "coca/common/FileSystem.h"
#include "coca/server/CachedFile.h"
#include "coca/server/DBServer.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::coca;
namespace fs = boost::filesystem;

namespace {

std::vector<CachedFile>
filter_device_id(std::vector<CachedFile>&& files, ::dev_t dev_id, FileSystemABC& filesystem)
{
    std::vector<CachedFile> filtered;
    for (auto&& file: files) {
        try {
            if (filesystem.device_id(file.serverPath().string()) == dev_id) {
                filtered.push_back(std::move(file));
            }
        } catch (std::exception const& exc) {
            // complain but continue, it may mean someone deleted a file
            ERS_LOG("Failed to determine filesystem device ID for file \"" << file.serverPath() << "\"");
        }
    }
    return filtered;
}

void
deleteOneFile(DBServer& db, const CachedFile& file, FileSystemABC& filesystem)
{
    try {

        // Delete from disk
        const fs::path& serverPath = file.serverPath();
        if (filesystem.is_regular_file(serverPath)) {
            // check that disk size is consistent with database before removing it
            if (file.checksum().empty()) {
                // If checksum is empty then this is likely a file which was
                // created with old schema which only had MB precision
                FileCheck::checkMB(serverPath.string(), file.size(), filesystem);
            } else {
                FileCheck::check(serverPath.string(), file.size(), file.checksum(), filesystem);
            }
            filesystem.remove(serverPath);
            ERS_LOG("file removed from cache: " << serverPath);
        } else {
            ERS_LOG("file not found any more in cache: " << serverPath);
        }

        // Delete from database
        db.removeFromCache(file.dataset(), file.relPath().string());
        ERS_LOG("file removed from database: " << file.relPath());

    } catch (const ers::Issue &ex) {
        ers::warning(ex);
    }
}

// As we operate on global state (filesystem)
// we also need global mutex here to protect it.
boost::mutex g_mutex;

}

namespace daq {
namespace coca {

FreeSpace::FreeSpace(DBServer& db, unsigned freePercentGoal, FileSystemABC& filesystem)
    : m_db(db)
    , m_freePercentGoal(freePercentGoal)
    , m_fs(filesystem)
{
}

bool FreeSpace::makeAvailable(Size goal, const std::string& dir)
{
    // Directory must exist
    try {
        m_fs.create_dirs(dir);
    } catch (const FileSystem::Exception& ex) {
        throw FreeSpaceIssues::FileSystemError(ERS_HERE, "failed to create directory", ex);
    }

    time_t now = time(0);

    // get current space and set goals
    auto space = m_fs.space(dir);

    Size percentGoal(space.capacity * m_freePercentGoal / 100);
    Size explicitGoal = goal;
    // Usually explicit goal is reasonably small, bit if the explicit goal is
    // very hight make sure that large goal is at least as large
    Size largeGoal = std::max(percentGoal, explicitGoal);
    ERS_LOG("Disk space total=" << space.capacity/(1024*1024) << "MB, available="
                    << space.available/(1024*1024) << "MB, free goal="
                    << largeGoal.megabytes() << "MB, free minimum=" << goal.megabytes() << "MB");

    // First pass is not to delete anything but check if the files are on disk, if not there then
    // unregister it from database (in case someone cleaned cache manually)
    bool const sameServer = false;  // allow deleting files from other servers on the same host
    for (const auto& file: m_db.filesToDelete(sameServer)) {
        try {
            if (m_fs.status(file.serverPath()).type() == fs::file_not_found) {
                // Delete from database
                ERS_LOG("file not found in cache, will remove from database: " << file.serverPath());
                m_db.removeFromCache(file.dataset(), file.relPath().string());
            }
        } catch (const ers::Issue &ex) {
            ers::warning(ex);
        }
    }

    // First check, maybe we have enough already
    if (Size(space.available) >= largeGoal)
        return true;

    // we only want to delete files that are on the same filesystem
    ::dev_t dir_fs_id;
    try {
        dir_fs_id = m_fs.device_id(dir);
    } catch (std::exception const& exc) {
        ERS_LOG("Failed to determine filesystem device ID for directory \"" << dir << "\"");
        return false;
    }

    // protect the rest with global lock so that multiple threads
    // do not try to delete the same file
    boost::mutex::scoped_lock lock(g_mutex);

    // get the files that can be deleted, filter out files from different filesystem
    auto files = ::filter_device_id(std::move(m_db.filesToDelete(sameServer)), dir_fs_id, m_fs);
    if (files.empty()) {
        // no files to delete? OK if there is enough space already.
        return Size(space.available) >= explicitGoal;
    }

    // sort by cost in _descending_ order
    std::sort(files.begin(), files.end(), [now](const CachedFile& lhs, const CachedFile& rhs) {
        return rhs.cacheCost(now) < lhs.cacheCost(now);
    });

    // First try to delete only expired files to try to reach largeGoal
    for (const auto& file: files) {
        if (file.expired(now)) {
            ::deleteOneFile(m_db, file, m_fs);
            if (Size(m_fs.space(dir).available) >= largeGoal)
                return true;
        }
    }

    if (Size(m_fs.space(dir).available) >= explicitGoal)
        return true;

    ERS_LOG("Cannot free " << explicitGoal.megabytes() << "MB by deleting expired files only, "
            "will remove some non-expired files");

    // still not enough space, try to remove non-expired files to reach explicit goal
    files = ::filter_device_id(std::move(m_db.filesToDelete(sameServer)), dir_fs_id, m_fs);
    std::sort(files.begin(), files.end(), [now](const CachedFile& lhs, const CachedFile& rhs) {
        return rhs.cacheCost(now) < lhs.cacheCost(now);
    });

    for (const auto& file: files) {
        ::deleteOneFile(m_db, file, m_fs);
        if (Size(m_fs.space(dir).available) >= explicitGoal)
            return true;
    }

    ERS_LOG("Cannot free explicitly requested " << explicitGoal.megabytes() << "MB of disk space");
    return false;
}

} // namespace coca
} // namespace daq
