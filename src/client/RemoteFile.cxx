//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/client/RemoteFile.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>
#include <iterator>
#include <iostream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace coca {

//----------------
// Constructors --
//----------------
RemoteFile::RemoteFile (const std::string& dataset,
                        const std::string& relPath,
                        const FileLocations& locations,
                        const std::string& archive,
                        Size size,
                        const std::string& checksum)
    : m_dataset(dataset)
    , m_relPath(relPath)
    , m_locations(locations)
    , m_archive(archive)
    , m_size(size)
    , m_checksum(checksum)
{
}

//--------------
// Destructor --
//--------------
RemoteFile::~RemoteFile () throw ()
{
}

/// Standard stream insertion operator
std::ostream&
operator<<(std::ostream& str, const RemoteFile& file)
{
    str << "Dataset: " << file.dataset() << '\n'
        << "RelPath: " << file.relPath() << '\n'
        << "Size: " << file.size() << '\n'
        << "Checksum: " << file.checksum() << '\n';

    const RemoteFile::FileLocations& locations = file.locations();
    std::copy(locations.begin(), locations.end(), std::ostream_iterator<FileLocation>(str, "\n"));

    str << "Archive: "<< file.archive() << '\n';

    return str;

}

} // namespace coca
} // namespace daq
