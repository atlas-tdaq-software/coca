#ifndef COCA_SIZE_H
#define COCA_SIZE_H

#include <iostream>
#include <cstdint>

namespace daq::coca {


/// @addtogroup coca_common

/**
 * @ingroup coca_common
 *
 * @brief Helper class for safer handling of file sizes.
 */
class Size {
public:

    using value_type = uint64_t;

    Size() = default;

    /// Construct from exact number of bytes
    explicit Size(value_type bytes) : m_bytes(bytes) {}

    Size(const Size& size) = default;
    Size& operator=(const Size& size) = default;

    // construct from a number of megabytes
    static Size MB(value_type MBytes) {
        return Size(MBytes * 1024 * 1024);
    }

    value_type bytes() const { return m_bytes; }

    /**
     * Returns size divided by the number of bytes in Megabyte, except
     * that for non-zero sizes below 1MB it always returns 1.
     */
    value_type megabytes() const {
        auto sizeMB = m_bytes / 1048576;
        if (sizeMB == 0 && m_bytes > 0) {
            sizeMB = 1;
        }
        return sizeMB;
    }

    bool operator<(const Size& other) const { return m_bytes < other.m_bytes; }
    bool operator>(const Size& other) const { return m_bytes > other.m_bytes; }
    bool operator<=(const Size& other) const { return m_bytes <= other.m_bytes; }
    bool operator>=(const Size& other) const { return m_bytes >= other.m_bytes; }
    bool operator==(const Size& other) const { return m_bytes == other.m_bytes; }
    bool operator!=(const Size& other) const { return m_bytes != other.m_bytes; }

    Size& operator+=(const Size& other) { m_bytes += other.m_bytes; return *this; }

private:

    value_type m_bytes = 0;

};

inline
std::ostream&
operator<<(std::ostream& out, const Size& size) {
    return out << size.bytes();
}

inline Size operator+(const Size& lhs, const Size& rhs) { return Size(lhs.bytes() + rhs.bytes()); }
inline Size operator*(const Size& lhs, unsigned rhs) { return Size(lhs.bytes() * rhs); }
inline Size operator*(unsigned lhs, const Size& rhs) { return Size(rhs.bytes() * lhs); }

} // namespace daq::coca

#endif // COCA_SIZE_H
