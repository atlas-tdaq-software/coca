//-----------------
// C/C++ Headers --
//-----------------
#include <ctime>
#include <boost/program_options.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/server/CachedFile.h"
#include "coca/server/DBServer.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::coca;
namespace po = boost::program_options;
using namespace std::chrono;

int
main(int argc, char **argv)
try {
    po::options_description cmdline_options("Available options");
    cmdline_options.add_options()
        ("help,h", "print usage and exit")
        ("conn-str,c", po::value<std::string>()->required(), "Database connection string")
        ("server-host,s", po::value<std::string>()->default_value("pc-tdq-mon-01.cern.ch"), "Name of the server host")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, cmdline_options), vm);

    if (vm.count("help")) {
        std::cout << "Dump CoCa cached file costs.\n\n"
                << "Usage: " << argv[0] << " [options]\n\n"
                << cmdline_options << "\n";
        return 0;
    }
    po::notify(vm);

    std::string connStr = vm["conn-str"].as<std::string>();
    std::string serverIPC = "pc-tdq-mon-01";
    std::string serverHost = vm["server-host"].as<std::string>();
    std::string topDir = "/data/coca/cache";
    std::string cdrHost = "pc-tdq-mon-01";
    std::string cdrDirOnCDRHost = "/data/coca/cdr";
    std::string archive_dir = "/eos";
    DBServer db(connStr, serverIPC, serverHost, topDir, cdrHost, cdrDirOnCDRHost, archive_dir);

    auto files = db.filesToDelete(false);
    time_t now = time(0);
    std::sort(files.begin(), files.end(), [now](const CachedFile& lhs, const CachedFile& rhs) {
        return rhs.cacheCost(now) < lhs.cacheCost(now);
    });

    for (auto&& file: files) {
        auto t = file.registeredAt();
        struct tm tm;
        localtime_r(&t, &tm);
        char buf[64];
        strftime(buf, 64, "%Y-%m-%d %H:%M:%S%z", &tm);
        std::cout
            << "cost=" << file.cacheCost(now)
            << " size=" << file.size()
            << " expired=" << (file.expired(now) ? "yes" : "no")
            << " path=" << file.relPath()
            << " dataset=" << file.dataset()
            << " created=" << buf
            << "\n";
    }

    return 0;

} catch (const po::error& ex) {
    std::cerr << ex.what() << "\n"
            "Use --help option for usage information.\n";
    return 1;
} catch (const std::exception& ex) {
    std::cerr << ex.what() << "\n";
    return 1;
}
