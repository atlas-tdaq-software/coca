//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class System...
//
// Author List:
//      Andrey Salnikov,32 2-A22,+41227678094,
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/common/System.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <vector>
#include <unistd.h>
#include <grp.h>
#include <sys/types.h>
#include <sys/stat.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

    /**
     *  @class SystemIssues::getgrnamError
     *  @brief Exception generated when getgrp() call returns an error
     */
    ERS_DECLARE_ISSUE(errors, NoSuchGroupError,
            "group " << name << " does not exist", ((std::string) name))

    /**
     *  @class SystemIssues::InsufficientBufferSize
     *  @brief Exception generated when getgrp() buffer is too small
     */
    ERS_DECLARE_ISSUE(errors, InsufficientBufferSize,
            "buffer size too small in call to " << name << " (only " << size
            << " bytes); please increase and recompile",
            ((std::string) name) ((size_t) size))

    /**
     *  @class SystemIssues::LibcCallError
     *  @brief Exception generated for errors in standard library calls
     */
    ERS_DECLARE_ISSUE(errors, LibcCallError,
            "libc call `" << libccall << "' failed with error: "
            << ::strerror(errnum), ((std::string) libccall) ((int) errnum))

    // Singleton instance
    std::unique_ptr<daq::coca::System> _instance;

}

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

namespace daq {
namespace coca {


void
System::initialize(const std::string& group, unsigned umask, bool setgid)
{
    ::_instance.reset(new System(group, umask, setgid));
}

void
System::initialize(const coca_dal::UnixProperties& properties)
{
    ::_instance.reset(new System(properties.get_NewDirGroup(),
                                 properties.get_ProcessUmask(),
                                 properties.get_NewDirHasSetGid()));
}

System&
System::instance()
{
    if (not ::_instance.get()) {
        throw SystemIssues::NotInitialized(ERS_HERE);
    }
    return *::_instance;
}

System::System(const std::string& group, unsigned umask, bool setgid)
    : m_umask(umask)
    , m_newDirGroup(group)
    , m_newDirGroupId(-1)
    , m_newDirHasSetGid(setgid)

{
    // new umask
    ::umask(m_umask);

    // check group name and get group id
    if (not m_newDirGroup.empty()) {
        struct group data;
        struct group *check = NULL;
        const size_t bufSize = 8*1024*1024;
        std::vector<char> buf(bufSize);
        int rc = getgrnam_r (m_newDirGroup.c_str(), &data, buf.data(), bufSize, &check);
        if (check != &data) {
            if (rc == ERANGE) {
                ::errors::InsufficientBufferSize err(ERS_HERE, "getgrnam_r", bufSize);
                throw SystemIssues::GroupNotFound(ERS_HERE, m_newDirGroup, err);
            } else if (rc) {
                ::errors::LibcCallError err(ERS_HERE, "getgrnam", rc);
                throw SystemIssues::GroupNotFound(ERS_HERE, m_newDirGroup, err);
            } else {
                ::errors::NoSuchGroupError err(ERS_HERE, m_newDirGroup);
                throw SystemIssues::GroupNotFound(ERS_HERE, m_newDirGroup, err);
            }
        }
        m_newDirGroupId = data.gr_gid;
        ERS_DEBUG (3, "group: " << data.gr_name << " (" << data.gr_gid << ")");
    }
}

/// Dump configuration info to a stream
void
System::print (std::ostream& out) const
{
    // debug printout
    out << "UNIX properties:"
        "\n process umask set to: 0"
        << std::oct << m_umask << std::dec << ";"
        "\n new directories " << (m_newDirHasSetGid ? "WILL" : "will NOT")
        << " have the setgid bit enabled;"
        "\n new directories will belong to group: " << m_newDirGroup
        << " (" << m_newDirGroupId << ")";
}

} // namespace coca
} // namespace daq
