
#include "coca/common/Dataset.h"

#include <iostream>

namespace daq {
namespace coca {

Dataset::Dataset::Dataset (const std::string& name,
                            unsigned quotaMB,
                            unsigned ttlSec,
                            Size minArchiveSize,
                            Size maxArchiveSize,
                            unsigned maxDelaySec,
                            BundlePolicy bundlePolicy)
    : m_name(name)
    , m_quotaMB(quotaMB)
    , m_ttlSec(ttlSec)
    , m_minArchiveSize(minArchiveSize)
    , m_maxArchiveSize(maxArchiveSize)
    , m_maxDelaySec(maxDelaySec)
    , m_bundlePolicy(bundlePolicy)
{
}


std::ostream&
operator<<(std::ostream& str, Dataset::BundlePolicy pol)
{
    const char* polstr = "";
    switch(pol) {
    case Dataset::SingleFile:
        polstr = "SingleFile";
        break;
    case Dataset::ZipBundle:
        polstr = "ZipBundle";
        break;
    case Dataset::ZipBundleDir:
        polstr = "ZipBundle";
        break;
    }
    return str << polstr;
}

std::istream&
operator>>(std::istream& str, Dataset::BundlePolicy &pol)
{
    std::string polstr;
    str >> polstr;
    if (polstr == "SingleFile") {
        pol = Dataset::SingleFile;
    } else if (polstr == "ZipBundle") {
        pol = Dataset::ZipBundle;
    } else if (polstr == "ZipBundleDir") {
        pol = Dataset::ZipBundleDir;
    } else {
        // say conversion failed
        str.setstate(std::ios_base::failbit);
    }
    return str;
}

}
}
