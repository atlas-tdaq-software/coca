#ifndef COCA_DBSTAT_H
#define COCA_DBSTAT_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <map>
#include <memory>
#include <chrono>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/client/StatFiles.h"
#include "coca/common/DBBase.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_client

/**
 *  @ingroup coca_client
 *
 *  @brief Class providing statistics-gathering API for CoCa database.
 *  
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class DBStat {
public:

    enum Interval { Hour, Day, Month, Year };
    
    typedef std::chrono::system_clock::time_point time_point;
    
    /**
     *  @brief Constructor which takes CORAL connection string.
     *  
     *  @param[in] connStr  CORAL connection string.
     */
    explicit DBStat(const std::string& connStr = DBBase::defConnStr()) ;

    // non-copyable
    DBStat(const DBStat&) = delete;
    DBStat& operator=(const DBStat&) = delete;

    // Destructor
    ~DBStat() throw() ;
    
    /**
     *  @brief Returns per-time interval statistics
     *  
     *  @param[out] stat  Returned statistics, mapping with the begin time of 
     *                  the interval as a key.
     *  @param[in] begin  Start of the time interval
     *  @param[in] end    End of the time interval
     *  @param[in] interval  Interval period
     *  
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException  Thrown in case of the database access errors. 
     */
    void intervalStat(std::map<time_point, StatFiles>& stat,
                      const time_point& begin,
                      const time_point& end,
                      Interval interval);
    
    /**
     *  @brief Returns per-dataset statistics
     *  
     *  @param[out] stat  Returned statistics, mapping with the name of dataset as a key.
     *  @param[in] cached If true then information about cached files only will be 
     *                  returned, otherwise all files.
     *  
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException  Thrown in case of the database access errors. 
     */
    void datasetStat(std::map<std::string, StatFiles>& stat, bool cached);
  
protected:

private:
    
    std::unique_ptr<DBBase> m_impl;

};

} // namespace coca
} // namespace daq

#endif // COCA_DBSTAT_H
