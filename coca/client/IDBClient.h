#ifndef COCA_IDBCLIENT_H
#define COCA_IDBCLIENT_H

//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>
#include <memory>
#include <string>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/client/RemoteArchive.h"
#include "coca/client/RemoteFile.h"
#include "coca/common/Metadata.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_client

/**
 *  @ingroup coca_client
 *
 *  @brief Abstract interface for CoCa database.
 */
class IDBClient {
public:

    typedef std::chrono::system_clock::time_point time_point;

    /// Enumeration of possible file locations, used by some methods to
    /// limit the list of returned files or archives.
    enum Location {
        LocCache = 0x1,
        LocArchive = 0x2,
        LocAll = LocCache | LocArchive
    };

    IDBClient() = default;

    // non-copyable
    IDBClient(const IDBClient&) = delete;
    IDBClient& operator=(const IDBClient&) = delete;

    // Destructor
    virtual ~IDBClient() = default;

    /**
     *  @brief Get the list of all datasets.
     *
     *  @return  list of strings.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException  Thrown in case of the database access errors.
     */
    virtual std::vector<std::string> datasets() = 0;

    /**
     *  @brief Get the list of archives.
     *
     *  Finds the list of archive names which satisfy selection
     *  criteria set by the method arguments.
     *
     *  @param[in] dataset   Name of dataset, if empty means any dataset.
     *  @param[in] archive   Name of the archive, if empty means any archive.
     *                       Archive name can contain wildcard characters (*?) to match
     *                       multiple names.
     *  @param[in] since     Earliest archive time, if missing any time allowed.
     *  @param[in] until     Latest archive time, if missing any time allowed.
     *  @return List of archive objects.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw DBClientIssues::DatasetNotFound Thrown if dataset does not exist
     *  @throw Issues::CoralException  Thrown in case of the database access errors.
     */
    virtual std::vector<RemoteArchive> archives(const std::string& dataset = std::string(),
                                                const std::string& archive = std::string(),
                                                const time_point& since = time_point(),
                                                const time_point& until = time_point()) = 0;

    /**
     *  @brief Get the list of archives.
     *
     *  Finds the list of archive names which satisfy selection criteria set
     *  by the method arguments. Difference from previous method is that one needs
     *  to specify max number of archives to be returned instead of starting time.
     *  Archives returned will be newest archives created by coca, but one can
     *  also specify optional number of latest archives to skip.
     *
     *  @param[in] dataset   Name of dataset, if empty means any dataset.
     *  @param[in] archive   Name of the archive, if empty means any archive.
     *                       Archive name can contain wildcard characters (*?) to match
     *                       multiple names.
     *  @param[in] count     Max. number of archives to return, newest archives are returned.
     *  @param[in] skip      Number of archives to skip.
     *  @return List of archive objects.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw DBClientIssues::DatasetNotFound Thrown if dataset does not exist
     *  @throw Issues::CoralException  Thrown in case of the database access errors.
     */
    virtual std::vector<RemoteArchive> archives(const std::string& dataset,
                                                const std::string& archive,
                                                unsigned count,
                                                unsigned skip = 0) = 0;

    /**
     *  @brief Get the number of files in archive.
     *
     *  Returns total number of files which satisfy selection
     *  criteria set by the method arguments.
     *
     *  @param[in] dataset   Name of dataset, if empty means any dataset.
     *  @param[in] archive   Name of the archive, if empty means any archive.
     *                       Archive name can contain wildcard characters (*?) to match
     *                       multiple names.
     *  @param[in] file      Name of the file, if empty means any file.
     *                       File name can contain wildcard characters (*?) to match
     *                       multiple names.
     *  @return Total number of files matching any of the above selection.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw DBClientIssues::DatasetNotFound Thrown if dataset does not exist
     *  @throw Issues::CoralException  Thrown in case of the database access errors.
     */
    virtual unsigned countFiles(const std::string& dataset = std::string(),
                                const std::string& archive = std::string(),
                                const std::string& file = std::string()) = 0;

    /**
     *  @brief Get the list of files.
     *
     *  Finds the list of files which satisfy selection
     *  criteria set by the method arguments.
     *
     *  @param[in] dataset   Name of dataset, if empty means any dataset.
     *  @param[in] archive   Name of the archive, if empty means any archive.
     *                       Archive name can contain wildcard characters (*?) to match
     *                       multiple names.
     *  @param[in] file      Name of the file, if empty means any file.
     *                       file name can contain wildcard characters (*?) to match
     *                       multiple names.
     *  @param[in] since     Earliest file registration time, if missing any time allowed.
     *  @param[in] until     Latest file registration time, if missing any time allowed.
     *  @return List of file objects.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw DBClientIssues::DatasetNotFound Thrown if dataset does not exist
     *  @throw DBClientIssues::FileNotFound Thrown if \p file is not empty but
     *         no files were found matching a name.
     *  @throw Issues::CoralException  Thrown in case of the database access errors.
     */
    virtual std::vector<RemoteFile> files(const std::string& dataset = std::string(),
                                          const std::string& archive = std::string(),
                                          const std::string& file = std::string(),
                                          const time_point& since = time_point(),
                                          const time_point& until = time_point()) = 0;

    /**
     *  @brief Get the list of files.
     *
     *  Finds the list of files which satisfy selection criteria set by the
     *  method arguments. Difference from previous method is that one needs
     *  to specify max number of files to be returned instead of starting time.
     *  Files returned will be newest files registered in coca, but one can
     *  also specify optional number of files to skip.
     *
     *  @param[in] dataset   Name of dataset, if empty means any dataset.
     *  @param[in] archive   Name of the archive, if empty means any archive.
     *                       Archive name can contain wildcard characters (*?) to match
     *                       multiple names.
     *  @param[in] file      Name of the file, if empty means any file.
     *                       File name can contain wildcard characters (*?) to match
     *                       multiple names.
     *  @param[in] count     Max. number of file to return, latest files are returned.
     *  @param[in] skip      Number of file to skip.
     *  @return List of file objects.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw DBClientIssues::DatasetNotFound Thrown if dataset does not exist
     *  @throw Issues::CoralException  Thrown in case of the database access errors.
     */
    virtual std::vector<RemoteFile> files(const std::string& dataset,
                                          const std::string& archive,
                                          const std::string& file,
                                          unsigned count,
                                          unsigned skip = 0) = 0;

    /**
     *  @brief Get the list of known file locations.
     *
     *  Finds and returns the list of all known file locations for a given
     *  dataset and file name. File location is returned as a string,
     *  starts with the "root://host/path" for file in coca cache, "rfio:/..."
     *  for file in CASTOR, or "root://eosatlas/..." for file in EOS.
     *  It is not guaranteed that file can be open from the cache location
     *  even if that location is returned, the file can be purged from cache
     *  at any moment. For CASTOR/EOS locations, if the file is in archive then
     *  location will look like "root://archive-path.zip#file-name.ext"; if the
     *  file is stored individually then location looks like "root://file-path.ext".
     *
     *  Locations returned are limited by location argument, if LocCache is
     *  specified then only Point1 cache location is returned (this is likely
     *  not accessible outside P1), if LocArchive is specified then locations
     *  in CASTOR/EOS archives are returned, if LocAll is specified then all
     *  possible location are returned.
     *
     *  @param[in] file      Name of the file, non-empty string
     *  @param[in] dataset   Name of dataset, possibly empty.
     *  @param[in] location  Select list of locations to search for files.
     *  @return              Vector of file location strings.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException  Thrown in case of the database access errors.
     */
    virtual std::vector<std::string> fileLocations(const std::string& file,
                                                   const std::string& dataset,
                                                   Location location) = 0;

    /**
     *  @brief Get the list of known file locations.
     *
     *  This method is equivalent to fileLocations(file, dataset, LocAll).
     *
     *  @param[in] file      Name of the file, non-empty string
     *  @param[in] dataset   Name of dataset, possibly empty.
     *  @return              Vector of file location strings.
     *
     *  @throw Issues::CoralException  Thrown in case of the database access errors.
     */
    virtual std::vector<std::string> fileLocations(const std::string& file,
                                                   const std::string& dataset) = 0;

    /**
     * @brief Return object providing metadata access.
     */
    virtual Metadata metadata() = 0;

};

}} // namespace daq::coca

#endif // COCA_IDBCLIENT_H
