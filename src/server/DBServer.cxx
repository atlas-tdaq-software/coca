//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Description:
//      Class DBServer...
//
// Author List:
//      Andy Salnikov
//
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/server/DBServer.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <map>
#include <boost/thread/locks.hpp>
#include <boost/filesystem.hpp>
#include <CoralBase/Attribute.h>
#include <CoralBase/AttributeList.h>
#include <CoralBase/TimeStamp.h>
#include <RelationalAccess/ConnectionServiceException.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISessionProxy.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/SchemaException.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/coral-helpers.h"
#include "coca/common/ArchiveName.h"
#include "coca/common/Dataset.h"
#include "coca/common/DBBase.h"
#include "coca/common/IncomingFile.h"
#include "coca/common/Issues.h"
#include "coca/common/Transaction.h"
#include "coca/common/TransientFile.h"
#include "coca/server/CachedFile.h"
#include "coca/server/ServerInfo.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace std::chrono;
using namespace daq::coca;
namespace fs = boost::filesystem;

//              ----------------------------------------
//              -- Public Function Member Definitions --
//              ----------------------------------------

/*
 * DBServer
 */

DBServer::DBServer (const std::string& connStr,
                    const std::string& serverIPC,
                    const std::string& serverHost,
                    const std::string& topDir,
                    const std::string& cdrHost,
                    const std::string& cdrDirOnCDRHost,
                    const std::string& archive_dir)
    : m_impl(new DBBase(connStr, true))
    , m_serverIPC(serverIPC)
    , m_serverHost(serverHost)
    , m_topDir(topDir)
    , m_cdrHost(cdrHost)
    , m_cdrDirOnCDRHost(cdrDirOnCDRHost)
    , m_archive_dir(archive_dir)
    , m_serverInfo()
    , m_mutex()
{
}

DBServer::~DBServer ()
    throw ()
{
}

std::string
DBServer::addNewFile(const IncomingFile& file, const Dataset& dataset)
try {

    boost::lock_guard<boost::mutex> lock(m_mutex);

    ERS_DEBUG (3, file);
    auto session = m_impl->session();
    unsigned dsId = 0;
    {
        Transaction tran(session, Transaction::Update);
        dsId = m_impl->insStr (dataset.name(), tran);
    }

    auto addsize = file.size();
    // checked by server.cxx
    ERS_ASSERT (addsize <= dataset.maxArchiveSize());
    // find open archive
    unsigned archive = 0;
    std::string name;
    Size totsize;
    const bool singlefile = ArchiveName::isEligible4SingleFile (file, dataset);
    if (singlefile) {

        {
            // generate new archive ID for this separate file
            Transaction tran(session, Transaction::Update);
            archive = m_impl->nextId ("DATASET", dsId, tran);
        }
        std::string fname (file.relPath());
        name = ArchiveName::archiveName (dataset, fname);

        newArchive(dsId, archive, name, dataset.ttlSec(), dataset.maxDelaySec());

    } else {

        bool op_ar = findOpenArchive (dsId, archive, name, totsize);
        // close if necessary
        if (op_ar && (totsize + addsize > dataset.maxArchiveSize()
                      || totsize >= dataset.minArchiveSize()))
        {
            closeArchive (dsId, archive);
            op_ar = false;
        }
        if (!op_ar) {
            ERS_DEBUG (0, "no open archives for dataset " << dataset.name());
            {
                // Generate new archive ID for next archive
                Transaction tran(session, Transaction::Update);
                archive = m_impl->nextId ("DATASET", dsId, tran);
            }
            name = ArchiveName::archiveName (dataset, archive);

            newArchive(dsId, archive, name, dataset.ttlSec(), dataset.maxDelaySec());

            totsize = Size(0);
        }
    }

    // BIG transaction, abort if something throws
    Transaction tran(session, Transaction::Update, Transaction::Abort);
    this->updateArchiveSize (dsId, archive, addsize, tran);
    this->addFile (dsId, archive, file, tran);
    tran.commit();

    // close if necessary
    if (singlefile) {
        // mark as closed, size/checksum are those of original file
        closeArchive(dsId, archive, file.size(), file.checksum());
    } else if (totsize + addsize > dataset.minArchiveSize()) {
        // mark as closed, size/checksum calculated from archive contents
        closeArchive(dsId, archive);
    }
    return name;

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

/*
 * Get list of files to be retrieved from clients
 */

std::vector<TransientFile>
DBServer::filesToRetrieve ()
try {

    boost::lock_guard<boost::mutex> lock(m_mutex);

    std::vector<TransientFile> files;

    const ServerInfo& serverInfo = this->serverInfo();

    // open transaction
    Transaction tran(m_impl->session());

    // new query
    std::unique_ptr<coral::IQuery> query (tran.session().nominalSchema().newQuery());

    // What
    query->addToTableList(m_impl->archiveTableName(), "AR");
    query->addToTableList(m_impl->cacheTableName(), "CA");

    coral_helpers::defineOutput<std::string> (*query, "CA.REL_PATH", "REL_PATH");
    coral_helpers::defineOutput<uint32_t> (*query, "CA.SIZE_MB", "SIZE_MB");
    coral_helpers::defineOutput<uint32_t> (*query, "AR.DIR_SERVER", "DIR_SERVER");
    coral_helpers::defineOutput<uint32_t> (*query, "CA.CLIENT_HOST", "CLIENT_HOST");
    coral_helpers::defineOutput<uint32_t> (*query, "CA.DIR_CLIENT", "DIR_CLIENT");
    coral_helpers::defineOutput<uint32_t> (*query, "CA.DATASET", "DATASET");
    if (m_impl->schemaVersion() >= 3) {
        coral_helpers::defineOutput<Size> (*query, "CA.SIZE_BYTES", "SIZE_BYTES");
        coral_helpers::defineOutput<std::string> (*query, "CA.CHECKSUM", "CHECKSUM");
    }

    // selection
    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "SERVER_IPC", serverInfo.getServerIPC());
    coral_helpers::addAttr(bv, "NO", false);
    coral_helpers::addAttr(bv, "YES", true);

    std::string where = "CA.DATASET = AR.DATASET"
        " AND CA.ARCHIVE = AR.ARCHIVE"
        " AND AR.SERVER_IPC = :SERVER_IPC"
        " AND AR.ON_CDR = :NO"
        " AND CA.ON_CLIENT = :YES"
        " AND CA.IN_CACHE = :NO";
    query->setCondition (where, bv);

    // Return closed archives first, for them SIGN(AR.OPEN_IF_0) will
    // give 1 so we sort in descending order on that expression
    if (m_impl->technology() == "sqlite") {
        // SQLite complains that it has no SIGN function even though
        // documentation says it exists.
        query->addToOrderList ("CASE AR.OPEN_IF_0 WHEN 0 THEN 0 ELSE 1 END DESC");
    } else {
        query->addToOrderList ("SIGN(AR.OPEN_IF_0) DESC");
    }
    query->addToOrderList ("CA.REGISTERED_AT");

    // execute
    coral::ICursor &cursor = query->execute();
    while (cursor.next()) {
        const coral::AttributeList &row = cursor.currentRow();
        Size size = Size::MB(row[1].data<uint32_t>());
        std::string checksum;
        if (m_impl->schemaVersion() >= 3) {
            size = coral_helpers::getAttr(row[6], size);
            checksum = coral_helpers::getAttr(row[7], checksum);
        }
        TransientFile file (m_impl->getStr (row[3].data<uint32_t>(), tran),
                            m_impl->getStr (row[4].data<uint32_t>(), tran),
                            row[0].data<std::string>(),
                            m_impl->getStr (row[2].data<uint32_t>(), tran),
                            m_impl->getStr (row[5].data<uint32_t>(), tran),
                            size,
                            checksum);
        files.push_back (file);
    }

    return files;

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}


/*
 * Get list of files that can be deleted
 */

std::vector<CachedFile>
DBServer::filesToDelete(bool sameServer)
try {

    boost::lock_guard<boost::mutex> lock(m_mutex);
    std::vector<CachedFile> files;

    const ServerInfo& serverInfo = this->serverInfo();

    // open transaction
    Transaction tran(m_impl->session());

    // new query
    std::unique_ptr<coral::IQuery> query (tran.session().nominalSchema().newQuery());

    // What
    query->addToTableList(m_impl->archiveTableName(), "AR");
    query->addToTableList(m_impl->cacheTableName(), "CA");

    coral_helpers::defineOutput<std::string> (*query, "CA.REL_PATH", "REL_PATH");
    coral_helpers::defineOutput<uint32_t> (*query, "CA.SIZE_MB", "SIZE_MB");
    coral_helpers::defineOutput<uint32_t> (*query, "AR.DIR_SERVER", "DIR_SERVER");
    coral_helpers::defineOutput<uint32_t> (*query, "CA.DATASET", "DATASET");
    coral_helpers::defineOutput<int32_t> (*query, "CA.PRIO", "PRIO");
    coral_helpers::defineOutput<coral::TimeStamp> (*query, "CA.REGISTERED_AT", "REGISTERED_AT");
    coral_helpers::defineOutput<coral::TimeStamp> (*query, "CA.TRY2KEEP_TIL", "TRY2KEEP_TIL");
    coral_helpers::defineOutput<uint32_t> (*query, "AR.SERVER_IPC", "SERVER_IPC");
    if (m_impl->schemaVersion() >= 3) {
        coral_helpers::defineOutput<Size> (*query, "CA.SIZE_BYTES", "SIZE_BYTES");
        coral_helpers::defineOutput<std::string> (*query, "CA.CHECKSUM", "CHECKSUM");
    }

    // selection
    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "SERVER_HOST", serverInfo.getServerHost());
    coral_helpers::addAttr(bv, "YES", true);
    coral_helpers::addAttr(bv, "ZERO", uint32_t(0));

    std::string where = "CA.DATASET = AR.DATASET"
        " AND CA.ARCHIVE = AR.ARCHIVE"
        " AND AR.SERVER_HOST = :SERVER_HOST"

        // ON_CDR => closed archive; this request is meant only
        // to use the index in the ARCHIVES table
        " AND AR.OPEN_IF_0 != :ZERO"

        " AND AR.ON_CDR = :YES"
        " AND CA.IN_CACHE = :YES";
    if (sameServer) {
        coral_helpers::addAttr(bv, "SERVER_IPC", serverInfo.getServerIPC());
        where += " AND AR.SERVER_IPC = :SERVER_IPC";
    }
    query->setCondition (where, bv);

    // execute
    coral::ICursor &cursor = query->execute();
    while (cursor.next()) {
        const coral::AttributeList &row = cursor.currentRow();

        const std::string& fileName = row[0].data<std::string>();
        Size size = Size::MB(row[1].data<uint32_t>());
        const std::string& dirServer = m_impl->getStr(row[2].data<uint32_t>(), tran);
        const std::string& dataset = m_impl->getStr(row[3].data<uint32_t>(), tran);
        const std::string& serverIPC = m_impl->getStr(row[7].data<uint32_t>(), tran);
        int prio = row[4].data<int32_t>();
        const coral::TimeStamp& regAt = row[5].data<coral::TimeStamp>();
        const coral::TimeStamp& keepTill = row[6].data<coral::TimeStamp>();
        std::string checksum;
        if (m_impl->schemaVersion() >= 3) {
            size = coral_helpers::getAttr(row[8], size);
            checksum = coral_helpers::getAttr(row[9], checksum);
        }

        const unsigned nsec_in_sec = 1000000000;
        CachedFile file(dirServer, fileName, dataset, serverIPC, size, prio,
                       regAt.total_nanoseconds()/nsec_in_sec,
                       keepTill.total_nanoseconds()/nsec_in_sec,
                       checksum);
        files.push_back(file);
    }

    return files;

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

/*
 * The file has been copied into the cache
 */
void
DBServer::addToCache (const std::string &dataset,
                      const std::string &relPath,
                      Size size)
try {

    boost::lock_guard<boost::mutex> lock(m_mutex);

    auto session = m_impl->session();
    unsigned dsId = 0;
    {
        Transaction tran(session, Transaction::Update);
        dsId = m_impl->insStr (dataset, tran);
    }
    unsigned archive = findArchiveId (dataset, relPath);
    Transaction tran(session, Transaction::Update);
    {
        // Set cached flag for the file
        coral::AttributeList bv;
        coral_helpers::addAttr(bv, "IN_CACHE", true);
        coral_helpers::addAttr(bv, "IN_CACHE_AT",  coral::TimeStamp::now());
        coral_helpers::addAttr(bv, "DATASET", dsId);
        coral_helpers::addAttr(bv, "REL_PATH", relPath);
        std::string upd ("IN_CACHE = :IN_CACHE, IN_CACHE_AT = :IN_CACHE_AT");
        std::string where ("DATASET = :DATASET AND REL_PATH = :REL_PATH");

        // get table handle
        coral::ITable& table = m_impl->cacheTable(tran);

        // do update
        long updated = table.dataEditor().updateRows (upd, where, bv);
        ERS_ASSERT (updated == 1);
        ERS_LOG ("file in cache: " << dataset << ':' << relPath);
    }
    {
        // update size of cached files for archive
        coral::AttributeList bv;
        coral_helpers::addAttr(bv, "ADDSIZE_MB", uint32_t(size.megabytes()));
        coral_helpers::addAttr(bv, "DATASET", dsId);
        coral_helpers::addAttr(bv, "ARCHIVE", archive);
        std::string upd ("TOTSIZE_MB_CACHED = TOTSIZE_MB_CACHED + :ADDSIZE_MB");
        std::string where ("DATASET = :DATASET AND ARCHIVE = :ARCHIVE");

        // get table handle
        coral::ITable& table = m_impl->archiveTable(tran);

        // do update
        long updated = table.dataEditor().updateRows (upd, where, bv);
        ERS_ASSERT (updated == 1);
        ERS_DEBUG (2, "added " << size << " bytes to archive: "
                  << dataset << ':' << archive);
    }

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}


/*
 * The file has been removed from the cache
 */

void
DBServer::removeFromCache (const std::string &dataset,
                           const std::string &relPath)
try {

    boost::lock_guard<boost::mutex> lock(m_mutex);

    auto session = m_impl->session();
    unsigned dsId = 0;
    {
        Transaction tran(session, Transaction::Update);
        dsId = m_impl->insStr (dataset, tran);
    }

    Transaction tran(session, Transaction::Update);

    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "IN_CACHE", false);
    coral_helpers::addAttr(bv, "DATASET", dsId);
    coral_helpers::addAttr(bv, "REL_PATH", relPath);
    std::string upd ("IN_CACHE = :IN_CACHE");
    std::string where ("DATASET = :DATASET AND REL_PATH = :REL_PATH");

    // get table handle
    coral::ITable& table = m_impl->cacheTable(tran);

    // do update
    long updated = table.dataEditor().updateRows (upd, where, bv);
    ERS_ASSERT (updated == 1);
    ERS_LOG ("file removed from cache: " << dataset << ':' << relPath);

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

/*
 * The file was removed from the client machine
 */

void
DBServer::removeFromClient (const std::string &dataset,
                            const std::string &relPath)
try {

    boost::lock_guard<boost::mutex> lock(m_mutex);

    auto session = m_impl->session();
    unsigned dsId = 0;
    {
        Transaction tran(session, Transaction::Update);
        dsId = m_impl->insStr (dataset, tran);
    }

    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "NO", false);
    coral_helpers::addAttr(bv, "DATASET", dsId);
    coral_helpers::addAttr(bv, "REL_PATH", relPath);
    std::string upd ("ON_CLIENT = :NO");
    std::string where ("DATASET = :DATASET AND REL_PATH = :REL_PATH");

    Transaction tran(session, Transaction::Update);

    // get table handle
    coral::ITable& table = m_impl->cacheTable(tran);

    // do update
    long updated = table.dataEditor().updateRows (upd, where, bv);
    ERS_ASSERT (updated == 1);
    ERS_LOG ("file not on client: " << dataset << ':' << relPath);

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

/*
 * Get list of files to zip into an archive
 */

std::map<std::string, std::vector<LocalFile> >
DBServer::archivesToCDR()
try {

    std::map<std::string, std::vector<LocalFile> > archive2files;
    std::set<std::string> archives2skip;

    boost::lock_guard<boost::mutex> lock(m_mutex);

    const ServerInfo& serverInfo = this->serverInfo();

    // open transaction
    Transaction tran(m_impl->session());

    // new query
    std::unique_ptr<coral::IQuery> query (tran.session().nominalSchema().newQuery());

    // What
    query->addToTableList(m_impl->archiveTableName(), "AR");
    query->addToTableList(m_impl->cacheTableName(), "CA");

    coral_helpers::defineOutput<std::string> (*query, "AR.NAME", "NAME");
    coral_helpers::defineOutput<std::string> (*query, "CA.REL_PATH", "REL_PATH");
    coral_helpers::defineOutput<uint32_t> (*query, "CA.SIZE_MB", "SIZE_MB");
    coral_helpers::defineOutput<uint32_t> (*query, "AR.DIR_SERVER", "DIR_SERVER");
    coral_helpers::defineOutput<uint32_t> (*query, "AR.DATASET", "DATASET");
    coral_helpers::defineOutput<bool> (*query, "CA.IN_CACHE", "IN_CACHE");
    if (m_impl->schemaVersion() >= 3) {
        coral_helpers::defineOutput<Size> (*query, "CA.SIZE_BYTES", "SIZE_BYTES");
        coral_helpers::defineOutput<std::string> (*query, "CA.CHECKSUM", "CHECKSUM");
    }

    // selection
    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "SERVER_IPC", serverInfo.getServerIPC());
    coral_helpers::addAttr(bv, "NO", false);
    coral_helpers::addAttr(bv, "ZERO", uint32_t(0));
    std::string where = "CA.DATASET = AR.DATASET"
        " AND CA.ARCHIVE = AR.ARCHIVE"
        " AND AR.SERVER_IPC = :SERVER_IPC"
        " AND AR.OPEN_IF_0 != :ZERO"
        " AND AR.ON_CDR = :NO"
        " AND AR.REMOVED = :NO";
    query->setCondition (where, bv);

    query->addToOrderList ("AR.TOTSIZE_MB DESC");
    query->addToOrderList ("AR.CLOSED_AT");
    query->addToOrderList ("AR.ARCHIVE");

    // execute
    coral::ICursor &cursor = query->execute();
    while (cursor.next()) {

        const coral::AttributeList &row = cursor.currentRow();
        const std::string& arStr(row[0].data<std::string>());

        // skip archives with missing files
        if (archives2skip.count(arStr) > 0) continue;
        if (not row[5].data<bool>()) {
            archives2skip.insert(arStr);
            continue;
        }

        const uint32_t& dsId(row[4].data<uint32_t>());
        const std::string& datasetStr = m_impl->getStr (dsId, tran);
        Size size = Size::MB(row[2].data<uint32_t>());
        std::string checksum;
        if (m_impl->schemaVersion() >= 3) {
            size = coral_helpers::getAttr(row[6], size);
            checksum = coral_helpers::getAttr(row[7], checksum);
        }

        LocalFile file (m_impl->getStr (row[3].data<uint32_t>(), tran),
                        row[1].data<std::string>(),
                        datasetStr,
                        size,
                        checksum);

        archive2files[arStr].push_back(file);
    }

    // remove archives from a skip list
    for (std::set<std::string>::const_iterator it = archives2skip.begin(); it != archives2skip.end(); ++ it) {
        archive2files.erase(*it);
    }

    return archive2files;

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

/*
 *  Update archive metadata.
 */
void
DBServer::setArchiveMeta(const std::string& archive, Size size, std::string const& checksum)
{
    if (m_impl->schemaVersion() < 3) {
        // only makes sense for schema version >= 3
        return;
    }

    boost::lock_guard<boost::mutex> lock(m_mutex);

    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "NAME", archive);
    coral_helpers::addAttr(bv, "ARCHIVE_SIZE", size);
    std::string upd = "ARCHIVE_SIZE_BYTES = :ARCHIVE_SIZE";
    if (not checksum.empty()) {
        coral_helpers::addAttr(bv, "CHECKSUM", checksum);
        upd += ", CHECKSUM = :CHECKSUM";
    }
    std::string where ("NAME = :NAME");

    // start transaction
    Transaction tran(m_impl->session(), Transaction::Update);

    // get table handle
    coral::ITable& table = m_impl->archiveTable(tran);

    // run query
    long updated = table.dataEditor().updateRows (upd, where, bv);
    ERS_ASSERT (updated == 1);
    ERS_LOG("updated archive metadata: " << archive << " size=" << size << " checksum=" << checksum);
}

/*
 * Archive was created
 */

void
DBServer::setArchiveOnCDR (const std::string& archive)
try {

    boost::lock_guard<boost::mutex> lock(m_mutex);

    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "YES", true);
    coral_helpers::addAttr(bv, "ON_CDR_AT",  coral::TimeStamp::now());
    coral_helpers::addAttr(bv, "NAME", archive);
    std::string upd ("ON_CDR = :YES, ON_CDR_AT = :ON_CDR_AT");
    std::string where ("NAME = :NAME");

    Transaction tran(m_impl->session(), Transaction::Update);

    // get table handle
    coral::ITable& table = m_impl->archiveTable(tran);

    // do update
    long updated = table.dataEditor().updateRows (upd, where, bv);
    ERS_ASSERT (updated == 1);
    ERS_LOG ("archive copied to CDR staging directory: " << archive);

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

/*
 * Space exhausted: close all archives
 */

void
DBServer::closeAllArchives ()
try {

    boost::lock_guard<boost::mutex> lock(m_mutex);

    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "CLOSED_AT", coral::TimeStamp::now());
    coral_helpers::addAttr(bv, "ZERO", uint32_t(0));
    std::string upd ("OPEN_IF_0 = ARCHIVE, CLOSED_AT = :CLOSED_AT");
    std::string where ("OPEN_IF_0 = :ZERO");

    Transaction tran(m_impl->session(), Transaction::Update);

    // get table handle
    coral::ITable& table = m_impl->archiveTable(tran);

    // do update
#ifndef ERS_NO_DEBUG
    long updated =
#endif
    table.dataEditor().updateRows (upd, where, bv);
    ERS_DEBUG (2, "closed " << updated << " archives");

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

void
DBServer::closeOldArchives ()
try {

    boost::lock_guard<boost::mutex> lock(m_mutex);

    // selection
    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "NOW", coral::TimeStamp::now());
    std::string where = "OPEN_IF_0 = 0 AND :NOW > CLOSE_AT";
    std::string upd = "OPEN_IF_0 = ARCHIVE, CLOSED_AT = :NOW";

    // start transaction
    Transaction tran(m_impl->session(), Transaction::Update);

    // get table handle
    coral::ITable& table = m_impl->archiveTable(tran);

    // run query
    long updated = table.dataEditor().updateRows (upd, where, bv);
    if ( updated ) {
            ERS_LOG ("closed old archives: " << updated);
    }

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

std::vector<std::string>
DBServer::archivesNotArchived ()
try {

    boost::lock_guard<boost::mutex> lock(m_mutex);

    std::vector<std::string> archivePaths;

    const ServerInfo& serverInfo = this->serverInfo();

    // open transaction
    Transaction tran(m_impl->session());

    // get table handle
    coral::ITable& table = m_impl->archiveTable(tran);

    // new query
    std::unique_ptr<coral::IQuery> query (table.newQuery());

    coral_helpers::defineOutput<std::string> (*query, "CDR_PATH");

    // selection
    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "SERVER_IPC", serverInfo.getServerIPC());
    coral_helpers::addAttr(bv, "YES", true);
    coral_helpers::addAttr(bv, "NO", false);
    std::string where = "SERVER_IPC = :SERVER_IPC"
        " AND ON_CDR = :YES AND STORED = :NO AND REMOVED = :NO";
    query->setCondition (where, bv);

    // execute
    coral::ICursor &cursor = query->execute();
    while (cursor.next()) {
        const coral::AttributeList &row = cursor.currentRow();
        const std::string &archive_path (row[0].data<std::string>());
        archivePaths.push_back (archive_path);
    }

    return archivePaths;

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

void
DBServer::setArchived (const std::string &archivePath)
try {

    boost::lock_guard<boost::mutex> lock(m_mutex);

    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "YES", true);
    coral_helpers::addAttr(bv, "STORED_AT", coral::TimeStamp::now());
    coral_helpers::addAttr(bv, "CDR_PATH", archivePath);
    std::string upd ("STORED = :YES, STORED_AT = :STORED_AT");
    std::string where ("CDR_PATH = :CDR_PATH");

    Transaction tran(m_impl->session(), Transaction::Update);

    // get table handle
    coral::ITable& table = m_impl->archiveTable(tran);

    // do update
    long updated = 0;
    updated = table.dataEditor().updateRows (upd, where, bv);
    ERS_ASSERT (updated == 1);
    ERS_LOG ("archive is at external storage (EOS): " << archivePath);

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

/*
 *  Mark archive as removed.
 */
void
DBServer::setRemoved(const std::string& archive)
try {

    boost::lock_guard<boost::mutex> lock(m_mutex);

    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "YES", true);
    coral_helpers::addAttr(bv, "REMOVED_AT", coral::TimeStamp::now());
    coral_helpers::addAttr(bv, "ARCHIVE", archive);
    std::string upd ("REMOVED = :YES, REMOVED_AT = :REMOVED_AT");
    std::string where ("NAME = :ARCHIVE");

    Transaction tran(m_impl->session(), Transaction::Update);

    // get table handle
    coral::ITable& table = m_impl->archiveTable(tran);

    // do update
    long updated = 0;
    updated = table.dataEditor().updateRows (upd, where, bv);
    ERS_ASSERT (updated == 1);
    ERS_LOG ("archive was removed: " << archive);

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}


/*
 * Find archive for registered file
 */
unsigned
DBServer::findArchiveId (const std::string &dataset,
                         const std::string &relPath)
{
    // no lock here, can only call it from other methods when lock is acquired

    auto session = m_impl->session();

    // selection
    coral::AttributeList bv;
    {
        Transaction tran(session, Transaction::Update);
        coral_helpers::addAttr(bv, "DATASET", m_impl->insStr (dataset, tran));
    }
    coral_helpers::addAttr(bv, "REL_PATH", relPath);
    std::string where ("DATASET = :DATASET AND REL_PATH = :REL_PATH");

    // open transaction
    Transaction tran(session, Transaction::Read);

    // get table handle
    coral::ITable& table = m_impl->cacheTable(tran);

    // new query
    std::unique_ptr<coral::IQuery> query (table.newQuery());

    coral_helpers::defineOutput<uint32_t> (*query, "ARCHIVE");
    query->setCondition (where, bv);

    // execute
    coral::ICursor &cursor = query->execute();
    if (! cursor.next()) {
        throw Issues::NoSuchRecord (ERS_HERE, dataset + ":" + relPath);
    }
    const coral::AttributeList &row = cursor.currentRow();

    unsigned archive = row[0].data<uint32_t>();
    ERS_ASSERT (! cursor.next());

    return archive;
}

/*
 * Close archives with max delay missed
 */
void
DBServer::closeArchive (unsigned dataset, unsigned archive,
                        Size archiveSize, std::string const& checksum)
{
    // no lock here, can only call it from other methods when lock is acquired

    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "CLOSED_AT", coral::TimeStamp::now());
    coral_helpers::addAttr(bv, "DATASET", dataset);
    coral_helpers::addAttr(bv, "ARCHIVE", archive);
    std::string upd ("OPEN_IF_0 = ARCHIVE, CLOSED_AT = :CLOSED_AT");
    if (m_impl->schemaVersion() >= 3) {
        if (archiveSize != Size(0)) {
            coral_helpers::addAttr(bv, "ARCHIVE_SIZE", archiveSize);
            upd += ", ARCHIVE_SIZE_BYTES = :ARCHIVE_SIZE";
        }
        if (not checksum.empty()) {
            coral_helpers::addAttr(bv, "CHECKSUM", checksum);
            upd += ", CHECKSUM = :CHECKSUM";
        }
    }
    std::string where ("DATASET = :DATASET AND ARCHIVE = :ARCHIVE");

    // start transaction
    Transaction tran(m_impl->session(), Transaction::Update);

    // get table handle
    coral::ITable& table = m_impl->archiveTable(tran);

    // run query
    long updated = table.dataEditor().updateRows (upd, where, bv);
    ERS_ASSERT (updated == 1);
    ERS_LOG ("closed archive: " << dataset << ':' << archive);

}

/*
 * Find (and close) archive for new registration
 */

void
DBServer::newArchive (unsigned dataset,
                      unsigned archive,
                      const std::string &name,
                      unsigned ttl_s,
                      unsigned max_delay_s)
try {

    // no lock here, can only call it from other methods when lock is acquired

    const ServerInfo& serverInfo = this->serverInfo();

    // start transaction
    Transaction tran(m_impl->session(), Transaction::Update);

    coral::AttributeList ins;
    coral_helpers::addAttr(ins, "DATASET", dataset);
    coral_helpers::addAttr(ins, "ARCHIVE", archive);
    coral_helpers::addAttr(ins, "NAME", name);

    system_clock::time_point now = system_clock::now();
    coral::TimeStamp::ValueType cts_tmp = nanoseconds(now.time_since_epoch()).count();
    coral_helpers::addAttr(ins, "BOOKED_AT", coral::TimeStamp(cts_tmp));

    system_clock::time_point close_at = now + seconds(max_delay_s);
    cts_tmp = nanoseconds(close_at.time_since_epoch()).count();
    coral_helpers::addAttr(ins, "CLOSE_AT", coral::TimeStamp(cts_tmp));

    system_clock::time_point delete_at = now + seconds(ttl_s);
    cts_tmp = nanoseconds(delete_at.time_since_epoch()).count();
    coral_helpers::addAttr(ins, "DELETE_AT", coral::TimeStamp(cts_tmp));

    coral_helpers::addAttr(ins, "SERVER_IPC", serverInfo.getServerIPC());
    coral_helpers::addAttr(ins, "SERVER_HOST", serverInfo.getServerHost());
    fs::path dir_server = serverInfo.getTopDir();
    dir_server /= m_impl->getStr (dataset, tran);
    coral_helpers::addAttr(ins, "DIR_SERVER", m_impl->insStr(dir_server.string(), tran));

    coral_helpers::addAttr(ins, "TOTSIZE_MB", uint32_t(0));
    coral_helpers::addAttr(ins, "TOTSIZE_MB_CACHED", uint32_t(0));
    if (m_impl->schemaVersion() >= 3) {
        coral_helpers::addAttr(ins, "TOTSIZE_BYTES", Size(0));
        coral_helpers::addAttr(ins, "ARCHIVE_SIZE_BYTES", Size(0));
    }

    coral_helpers::addAttr(ins, "OPEN_IF_0", uint32_t(0));

    coral_helpers::addAttr<coral::TimeStamp>(ins, "CLOSED_AT");
    coral_helpers::addAttr(ins, "ON_CDR", false);
    coral_helpers::addAttr<coral::TimeStamp>(ins, "ON_CDR_AT");
    coral_helpers::addAttr(ins, "CDR_HOST", serverInfo.getCDRHost());
    fs::path cdr_path = serverInfo.getCDRDirOnCDRHost();
    cdr_path /= name;
    coral_helpers::addAttr(ins, "CDR_PATH", cdr_path.string());
    coral_helpers::addAttr(ins, "STORED", false);
    coral_helpers::addAttr<coral::TimeStamp>(ins, "STORED_AT");
    fs::path stor_path = serverInfo.getArchiveDir();
    stor_path /= name;
    coral_helpers::addAttr(ins, "STOR_PATH", stor_path.string());
    if (m_impl->schemaVersion() < 2) {
        // starting with version 2 DDM* columns has default values, and will be
        // dropped completely in some later version
        coral_helpers::addAttr(ins, "DDM", false);
        coral_helpers::addAttr<coral::TimeStamp>(ins, "DDM_AT");
        coral_helpers::addAttr<std::string>(ins, "DDM_GUID");
        coral_helpers::addAttr<std::string>(ins, "DDM_LFN");
    }
    coral_helpers::addAttr(ins, "REMOVED", false);
    coral_helpers::addAttr<coral::TimeStamp>(ins, "REMOVED_AT");

    // get table handle
    coral::ITable& table = m_impl->archiveTable(tran);

    // run query
    table.dataEditor().insertRow (ins);
    ERS_LOG ("new archive: " << name);

} catch (const coral::DuplicateEntryInUniqueKeyException &ex) {
    throw Issues::FileAlreadyRegistered(ERS_HERE, name, ex);
}

bool
DBServer::findOpenArchive (unsigned dataset,
                           unsigned& archive,
                           std::string& name,
                           Size& totsize)
{

    // no lock here, can only call it from other methods when lock is acquired

    const ServerInfo& serverInfo = this->serverInfo();

    // selection
    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "DATASET", dataset);
    coral_helpers::addAttr(bv, "SERVER_IPC", serverInfo.getServerIPC());
    coral_helpers::addAttr(bv, "ZERO", uint32_t(0));
    std::string where ("DATASET = :DATASET AND SERVER_IPC = :SERVER_IPC AND OPEN_IF_0 = :ZERO");

    // start transaction
    Transaction tran(m_impl->session(), Transaction::Read);

    // get table handle
    coral::ITable& table = m_impl->archiveTable(tran);

    // new query
    std::unique_ptr<coral::IQuery> query (table.newQuery());
    coral_helpers::defineOutput<uint32_t> (*query, "ARCHIVE");
    coral_helpers::defineOutput<std::string> (*query, "NAME");
    coral_helpers::defineOutput<uint32_t> (*query, "TOTSIZE_MB");
    if (m_impl->schemaVersion() >= 3) {
        coral_helpers::defineOutput<Size> (*query, "TOTSIZE_BYTES");
    }
    query->setCondition (where, bv);

    // execute
    coral::ICursor &cursor = query->execute();
    if (! cursor.next()) {
        return false;
    }

    // get values
    const coral::AttributeList &row = cursor.currentRow();
    archive = row[0].data<uint32_t>();
    name = row[1].data<std::string>();
    Size size = Size::MB(row[2].data<uint32_t>());
    if (m_impl->schemaVersion() >= 3) {
        size = coral_helpers::getAttr(row[3], size);
    }
    totsize = size;
    ERS_ASSERT (! cursor.next());

    return true;

}

void
DBServer::updateArchiveSize (unsigned dataset,
                             unsigned archive,
                             Size addsize,
                             Transaction& tran)
{

    // no lock here, can only call it from other methods when lock is acquired

    coral::AttributeList bv;
    coral_helpers::addAttr(bv, "ADDSIZE_MB", uint32_t(addsize.megabytes()));
    coral_helpers::addAttr(bv, "DATASET", dataset);
    coral_helpers::addAttr(bv, "ARCHIVE", archive);
    std::string upd ("TOTSIZE_MB = TOTSIZE_MB + :ADDSIZE_MB");
    if (m_impl->schemaVersion() >= 3) {
        coral_helpers::addAttr(bv, "ADDSIZE", addsize);
        upd += ", TOTSIZE_BYTES = TOTSIZE_BYTES + :ADDSIZE";
    }
    std::string where ("DATASET = :DATASET AND ARCHIVE = :ARCHIVE");

    // get table handle
    coral::ITable& table = m_impl->archiveTable(tran);

    long updated = table.dataEditor().updateRows (upd, where, bv);
    ERS_ASSERT (updated == 1);
    ERS_DEBUG (2, "added " << addsize << " MB to archive "
              << dataset << ':' << archive);

}

void
DBServer::addFile (unsigned dataset,
                   unsigned archive,
                   const IncomingFile& file,
                   Transaction& tran)
try {

    // no lock here, can only call it from other methods when lock is acquired

    coral::AttributeList ins;
    coral_helpers::addAttr(ins, "DATASET", dataset);
    coral_helpers::addAttr(ins, "REL_PATH", file.relPath());
    coral_helpers::addAttr(ins, "SIZE_MB", uint32_t(file.size().megabytes()));
    coral_helpers::addAttr(ins, "CLIENT_HOST", m_impl->insStr (file.host(), tran));
    coral_helpers::addAttr(ins, "DIR_CLIENT", m_impl->insStr(file.clientDir(), tran));
    coral_helpers::addAttr(ins, "PRIO", file.prio());

    system_clock::time_point try2keep_til = system_clock::now() + seconds(file.dtcSec());
    coral::TimeStamp::ValueType cts_tmp = nanoseconds(try2keep_til.time_since_epoch()).count();
    coral_helpers::addAttr(ins, "TRY2KEEP_TIL", coral::TimeStamp (cts_tmp));

    coral_helpers::addAttr(ins, "REGISTERED_AT", coral::TimeStamp::now());
    coral_helpers::addAttr(ins, "ARCHIVE", archive);
    coral_helpers::addAttr(ins, "ON_CLIENT", true);
    coral_helpers::addAttr(ins, "IN_CACHE", false);
    coral_helpers::addAttr<coral::TimeStamp>(ins, "IN_CACHE_AT");
    coral_helpers::addAttr(ins, "REMOVED", false);

    if (m_impl->schemaVersion() >= 3) {
        coral_helpers::addAttr(ins, "SIZE_BYTES", file.size());
        if (not file.checksum().empty()) {
            coral_helpers::addAttr(ins, "CHECKSUM", file.checksum());
        }
    }

    // get table handle
    coral::ITable& table = m_impl->cacheTable(tran);

    table.dataEditor().insertRow (ins);
    ERS_LOG ("new file: " << file.relPath() << " ("
             << dataset << ':' << archive << ")");

} catch (const coral::DuplicateEntryInUniqueKeyException &ex) {
    ERS_DEBUG (0, ex.what());
    throw Issues::FileAlreadyRegistered (ERS_HERE, file.relPath());
}

const ServerInfo&
DBServer::serverInfo()
{
    // no lock here, can only call it from other methods when lock is acquired

    if (not m_serverInfo.get()) {
        m_serverInfo.reset( new ServerInfo(m_impl->session(), m_impl->s2i(), m_serverIPC,
            m_serverHost, m_topDir, m_cdrHost, m_cdrDirOnCDRHost, m_archive_dir) );
    }
    return *m_serverInfo;
}
