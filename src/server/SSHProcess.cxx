//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class SSHProcess...
//
// Author List:
//      Andy Salnikov
//
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/server/SSHProcess.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>
#include <iterator>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

namespace daq {
namespace coca {

SSHProcess::SSHProcess (const std::string& machine,
                        const std::vector<std::string>& commandLine,
                        const std::string& name)
    : m_name (name)
    , m_process(0)
    , m_machine(machine)
{
    const char* sshCmd[] = { "/usr/bin/ssh",  "-n",
            "-o", "PasswordAuthentication=no",
            "-o", "StrictHostKeyChecking=no" };
    const int sshCmdSize = sizeof sshCmd / sizeof sshCmd[0];

    // build command line for ssh
    std::vector<std::string> cmdline;

    // reserve enough space, command line plus sshg command line  plus host name
    cmdline.reserve(commandLine.size() + sshCmdSize + 1);

    // copy all arguments
    std::copy(sshCmd, sshCmd+sshCmdSize, std::back_inserter(cmdline));
    cmdline.push_back (m_machine);
    std::copy(commandLine.begin(), commandLine.end(), std::back_inserter(cmdline));
    
    m_process = new Process (cmdline, m_name);
}

SSHProcess::~SSHProcess ()
    throw ()
{
    ERS_ASSERT (m_process);
    delete m_process;
}

bool
SSHProcess::isRunning (int &exit_code) const
{
    ERS_ASSERT (m_process);
    if (m_process->isRunning (exit_code)) {
        return true;
    }
    if (exit_code == 255) {
        // man ssh, 255 means error in SSH itself
        throw ProcessIssues::SSHFailure (ERS_HERE, m_name, m_process->pid());
    }
    return false;
}

bool
SSHProcess::isRunning () const
{
    int exit_code = -1;
    if (isRunning (exit_code)) {
        return true;
    }
    if (exit_code != 0) {
        throw ProcessIssues::BadExitCodeRemote (ERS_HERE, m_name, m_process->pid(),
                        exit_code, m_machine);
    }
    return false;
}

void
SSHProcess::terminate ()
{
    ERS_ASSERT (m_process);
    m_process->terminate();
}

void
SSHProcess::waitTimeout (int& exit_code, 
    const std::chrono::milliseconds& time2Wait,
    const std::chrono::milliseconds& pollTime)
{
    ERS_ASSERT (m_process);
    m_process->waitTimeout(exit_code, time2Wait, pollTime);
    if (exit_code == 255) {
        // man ssh, 255 means error in SSH itself
        throw ProcessIssues::SSHFailure (ERS_HERE, m_name, m_process->pid());
    }
}

void
SSHProcess::waitTimeout (const std::chrono::milliseconds& time2Wait,
    const std::chrono::milliseconds& pollTime)
{
    int exit_code = -1;
    waitTimeout (exit_code, time2Wait, pollTime);
    if (exit_code != 0) {
        throw ProcessIssues::BadExitCodeRemote (ERS_HERE, m_name, m_process->pid(),
                        exit_code, m_machine);
    }
}

void
SSHProcess::removeRemote (const std::string &machine, const std::string &path)
{
    std::vector<std::string> cmdline;
    cmdline.push_back ("rm");
    cmdline.push_back ("-f");
    cmdline.push_back (path);
    SSHProcess ssh (machine, cmdline, "RemoveRemote");
    // Wait one minute max
    ssh.waitTimeout (std::chrono::minutes(1));
}

} // namespace coca
} // namespace daq
