//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/server/NotifyFlag.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/thread/condition.hpp>
#include <boost/thread/mutex.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace coca {

class NotifyFlag::NotifyFlagImpl {
public:

    /**
     *  Constructor takes the name of the flag, name is arbitrary string and
     *  is used for  identification only when debugging output is produced.
     */
    NotifyFlagImpl(const std::string& name) : m_name(name), m_ready(false) {}

    /**
     *  @brief Send notification to a partner thread.
     */
    void notify() {
        ERS_DEBUG (0, "NotifyFlag(" << m_name << ")::notify");
        boost::mutex::scoped_lock lock(m_mutex);
        m_ready = true;
        m_cond.notify_one();
    }

    /**
     *  @brief Wait for notification to arrive.
     *
     *  If timeout expires before notification is received then false is returned,
     *  otherwise returns true.
     */
    bool wait(const boost::posix_time::time_duration& timeout) {
        boost::mutex::scoped_lock lock(m_mutex);
        if (m_ready) {
            ERS_DEBUG (0, "NotifyFlag(" << m_name << ")::wait - flag already set");
            m_ready = false;
            return true;
        }
        // not ready yet, then wait
        m_cond.timed_wait(lock, timeout);
        ERS_DEBUG (0, "NotifyFlag(" << m_name << ")::wait - flag: " << (m_ready ? "true" : "false"));
        bool res = m_ready;
        m_ready = false;
        return res;
    }

protected:

private:

    std::string m_name;
    bool m_ready;
    boost::mutex m_mutex;
    boost::condition m_cond;

};



//----------------
// Constructors --
//----------------
NotifyFlag::NotifyFlag(const std::string& name)
    : m_impl(std::make_shared<NotifyFlagImpl>(name))
{
}

//--------------
// Destructor --
//--------------
NotifyFlag::~NotifyFlag()
{
}

// Send notification to a partner thread.
void
NotifyFlag::notify()
{
    m_impl->notify();
}

// Wait for notification to arrive.
bool
NotifyFlag::wait(const boost::posix_time::time_duration& timeout)
{
    return m_impl->wait(timeout);
}

} // namespace coca
} // namespace daq
