//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/common/ArchiveName.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iomanip>
#include <boost/lexical_cast.hpp>
#include <time.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::coca;

namespace {

    ERS_DECLARE_ISSUE(error, BundlePolicyError,
            "ArchiveName: unexpected bundling policy " << policy,
            ((Dataset::BundlePolicy)policy) )

    // returns true relative path contains directory
    template <typename File>
    bool hasSubdir(const File& file)
    {
        return file.relPath().find ('/') != std::string::npos;
    }

    // return current year as integern number
    int currentYear()
    {
      time_t t = time(0);
      struct tm tm;
      localtime_r(&t, &tm);
      return tm.tm_year+1900;
    }

}

//              ----------------------------------------
//              -- Public Function Member Definitions --
//              ----------------------------------------

/**
 *  Returns true if the file is eligible to be saved as a single file without
 *  packing it into archive. File size must be larger than minimum size.
 */
bool
ArchiveName::isEligible4SingleFile (const IncomingFile& file, const Dataset& dataset)
{
    switch (dataset.bundlePolicy()) {
    case Dataset::SingleFile:
        // with single-file policy any file name or size should do
        return true;
    case Dataset::ZipBundle:
        // bundle small files or files which go into sub-directories
        return not ::hasSubdir(file) and file.size() > dataset.minArchiveSize();
    case Dataset::ZipBundleDir:
        // bundle small files only
        return file.size() > dataset.minArchiveSize();
    }
    throw error::BundlePolicyError(ERS_HERE, dataset.bundlePolicy());
}

/**
 *  Generate archive name out of dataset name and file name
 */
std::string
ArchiveName::archiveName (const Dataset& dataset, const std::string &fileName)
{
    switch (dataset.bundlePolicy()) {
    case Dataset::SingleFile:
    case Dataset::ZipBundleDir:
        // put it as it is in dataset subdirectory
        return boost::lexical_cast<std::string>(::currentYear()) + "/" + dataset.name() + "/" + fileName;
    case Dataset::ZipBundle:
        // without sub-directories add a prefix to make file name unique
        return "coca_" + dataset.name() + "_" + fileName;
    }
    throw error::BundlePolicyError(ERS_HERE, dataset.bundlePolicy());
}

/**
 *  Generate archive name out of dataset name and archive number
 */
std::string
ArchiveName::archiveName (const Dataset& dataset, unsigned archive)
{
    std::ostringstream buf;
    buf << "coca_" << dataset.name() << "_" << std::setw (6) << std::setfill('0')
        << archive << ".zip";

    switch (dataset.bundlePolicy()) {
    case Dataset::SingleFile:
    case Dataset::ZipBundleDir:
        // put it as it is in dataset subdirectory
        return boost::lexical_cast<std::string>(::currentYear()) + "/" + dataset.name() + "/" + buf.str();
    case Dataset::ZipBundle:
        // without sub-directories add a prefix to make file name unique
        return buf.str();
    }
    throw error::BundlePolicyError(ERS_HERE, dataset.bundlePolicy());
}

/**
 *  Returns true if the list of files is eligible to be saved
 *  as a single file (see isEligible4SingleFile()) and archive name
 *  matches the one generated from dataset and file name.
 */
bool
ArchiveName::wasEligible4SingleFile (const std::vector<LocalFile>& files,
                                     const std::string& dataset,
                                     const std::string& archive)
{
    if (files.size() != 1) return false;

    const std::string& fileName = files.front().relPath();

    // try various ways of generating single-file archive name corresponding
    // to the code above, if one of them matches stored archive name then we are good

    std::string test;

    test = boost::lexical_cast<std::string>(::currentYear()) + "/" + dataset + "/" + fileName;
    if (archive == test) return true;

    // try previous year too, this might happen if we run on Jan 1st
    test = boost::lexical_cast<std::string>(::currentYear()-1) + "/" + dataset + "/" + fileName;
    if (archive == test) return true;

    std::string test1 = "coca_" + dataset + "_" + fileName;

    test = test1;
    if (archive == test) return true;

    test = boost::lexical_cast<std::string>(::currentYear()) + "/" + dataset + "/" + test1;
    if (archive == test) return true;

    test = boost::lexical_cast<std::string>(::currentYear()-1) + "/" + dataset + "/" + test1;
    if (archive == test) return true;

    return false;
}
