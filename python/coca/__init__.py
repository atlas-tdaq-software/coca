"""Wrapper module for libpycoca, see help('libpycoca') for details"""

import sys

if sys.platform == 'linux2':
    # on Linux with g++ one needs RTLD_GLOBAL for dlopen
    # which Python does not set by default
    import DLFCN
    flags = sys.getdlopenflags()
    sys.setdlopenflags(flags | DLFCN.RTLD_GLOBAL)
    from libpycoca import *  # noqa: F401, F403
    sys.setdlopenflags(flags)
    del flags
    del DLFCN
else:
    from libpycoca import *  # noqa: F401, F403
