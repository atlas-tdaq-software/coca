#ifndef COCA_REMOTEARCHIVE_H
#define COCA_REMOTEARCHIVE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <vector>
#include <iosfwd>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/client/FileLocation.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_client

/**
 *  @ingroup coca_client
 *
 *  @brief Class containing complete information about remote CoCa archive.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class RemoteArchive  {
public:

    typedef std::vector<FileLocation> FileLocations;

    /**
     *  @brief Constructor
     */
    RemoteArchive (const std::string& dataset,
                   const std::string& name,
                   const FileLocations& locations,
                   const std::string& archivePath) ;

    // Destructor
    ~RemoteArchive () throw ();

    /// Returns dataset name.
    const std::string& dataset () const { return m_dataset; }

    /// Returns archive name.
    const std::string& name() const  { return m_name; }

    /**
     *  @brief  Returns list of the file locations.
     *
     *  This list is typically empty, but may contain single entry if
     *  archive is still in CDR directory on server host.
     */
    const FileLocations& locations() const { return m_locations; }

    /// Returns archive path name.
    const std::string& archivePath() const  { return m_archivePath; }

protected:

private:

    // Data members
    std::string m_dataset;
    std::string m_name;
    FileLocations m_locations;
    std::string m_archivePath;
};

/// Standard stream insertion operator
std::ostream&
operator<<(std::ostream &str, const RemoteArchive& file);

} // namespace coca
} // namespace daq

#endif // COCA_REMOTEARCHIVE_H
