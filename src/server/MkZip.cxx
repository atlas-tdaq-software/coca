//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Description:
//      Class MkZip...
//
// Author List:
//      Andy Salnikov
//
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/server/MkZip.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <boost/filesystem.hpp>
#include <boost/date_time.hpp>
#include <boost/thread.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/ArchiveName.h"
#include "coca/common/FileSystem.h"
#include "coca/common/Issues.h"
#include "coca/server/DBServer.h"
#include "coca/server/FreeSpace.h"
#include "coca/server/Process.h"
#include "ers/ers.h"
#include "owl/semaphore.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace fs = boost::filesystem;
namespace ptime = boost::posix_time;

namespace {

    ERS_DECLARE_ISSUE(error, ChDirError,
            "MkZip: Cannot change to directory '" << dir << "': errno=" << errno << " " << strerror(errno),
            ((std::string)dir) )

    ERS_DECLARE_ISSUE(error, MissingCacheFile,
            "MkZip: File is missing from cache: '" << file << "'", ((std::string)file) )

    ERS_DECLARE_ISSUE(error, RetryExceeded,
            "MkZip: Retry count exceeded limit (" << maxRetries << "): '" << path << "'",
            ((std::string) path)((int) maxRetries) )

    ERS_DECLARE_ISSUE(error, UnhandledException,
            "MkZip: Unhandled '" << name << "' exception was thrown",
            ((const char *)name) )

    ERS_DECLARE_ISSUE (error, SpaceExausted,
            "an archive cannot be created because the filesystem hosting the CoCa CDR"
            " directory (" << dir << ") is full; furthermore, no file can be deleted"
            " from cache as none of them has yet been copied to CDR. Please, free"
            " some space by hand in that filesystem (at least " << sizeMB << " Mbytes)",
            ((std::string) dir) ((unsigned) sizeMB))

    ERS_DECLARE_ISSUE(message, Interrupted,
            "thread " << thread_name  << " interrupted while " << action,
            ((std::string) thread_name) ((std::string) action))

    ERS_DECLARE_ISSUE(message, ErrorWithRetry,
            "MkZip: Generic error happened, will retry operation again.",)

}

namespace daq {
namespace coca {

MkZip::MkZip(DBServer& db,
        const std::string& dirCDR,
        const NotifyFlag& notifyUp,
        const NotifyFlag& notifyDown,
        const FreeSpace& freeSpace,
        OWLSemaphore& semaphore,
        FileSystemABC& filesystem)
    : m_db (db)
    , m_dirCDR(dirCDR)
    , m_notifyUp(notifyUp)
    , m_notifyDown(notifyDown)
    , m_freeSpace(freeSpace)
    , m_semaphore(semaphore)
    , m_filesystem(filesystem)
    , m_retries(2)
    , m_attemptCounts()
{
}

void
MkZip::copyFile (const std::string &file, const std::string &archive)
{
    std::vector<std::string> cmdline;
    cmdline.push_back ("/bin/cp");
    cmdline.push_back ("-p");
    cmdline.push_back (file);
    cmdline.push_back (archive);
    Process cp (cmdline, "cp");
    while (cp.isRunning()) {
        try {
            boost::this_thread::sleep(boost::posix_time::seconds(1));
        } catch(const boost::thread_interrupted& ex) {
            // terminate process, complain, and rethrow
            cp.terminate ();
            ers::warning(::message::Interrupted (ERS_HERE, "MkZip",
                    "copying file " + file + " to " + archive));
            throw;
        }
    }
    ERS_LOG ("copied: " << file << " -> " << archive);
}

void
MkZip::mkZip (const std::vector<LocalFile> &files, const std::string &archive)
{
    std::vector<std::string> cmdline;
    cmdline.push_back ("/usr/bin/zip");
    cmdline.push_back ("-q");
    cmdline.push_back ("-0");
    cmdline.push_back (archive);
    std::string dir_server;
    for (std::vector<LocalFile>::const_iterator it = files.begin(); it != files.end(); ++ it) {
        if (dir_server.empty()) {
            dir_server = it->serverDir();
        } else {
            ERS_ASSERT (dir_server == it->serverDir());
        }
        cmdline.push_back ("./" + it->relPath());
    }
    if ( chdir (dir_server.c_str()) < 0 ) {
        ::error::ChDirError err(ERS_HERE, dir_server);
        ers::warning(err);
        throw err;
    }
    Process zip (cmdline, "zip");
    while (zip.isRunning()) {
        try {
            boost::this_thread::sleep(boost::posix_time::seconds(1));
        } catch(const boost::thread_interrupted& ex) {
            // terminate process, complain, and rethrow
            zip.terminate ();
            ers::warning(::message::Interrupted (ERS_HERE, "MkZip",
                    "writing archive " + archive));
            throw;
        }
    }
    ERS_LOG ("new archive on CDR: " << archive);
}

void
MkZip::operator()()
try {

    // first time around do not wait
    ptime::time_duration wait_time = ptime::seconds(0);
    while (not boost::this_thread::interruption_requested()) {

        if (wait_time.total_seconds()) {
            m_notifyUp.wait(wait_time);
        }
        wait_time = ptime::minutes(3);

        try {

            ERS_DEBUG (0, "MkZip: new iteration");
            m_db.closeOldArchives();

            // give it a chance to break out of the loop
            boost::this_thread::interruption_point();

            typedef std::map<std::string, std::vector<LocalFile> > ArchiveMap;
            ArchiveMap amap = m_db.archivesToCDR();

            // give it a chance to break out of the loop
            boost::this_thread::interruption_point();

            for (ArchiveMap::const_iterator it = amap.begin(); it != amap.end(); ++ it ) {

                const std::string& archive = it->first;
                const std::vector<LocalFile>& files = it->second;
                if (files.empty()) {
                    // this should not happen, just ignore it
                    continue;
                }
                const std::string& dataset = files.front().dataset();

                // check that all files do exist in cache
                bool missing = false;
                for (const LocalFile& file: files) {
                    const std::string& path = file.serverPath();
                    if (::access(path.c_str(), R_OK) != 0) {

                        error::MissingCacheFile missIssue(ERS_HERE, path);
                        ers::warning(missIssue);
                        missing = true;

                        // m_retries is the number of _additional_ re-tries beyond first attempt,
                        // it can be set to negative number for infinite retry
                        if (m_retries >= 0) {
                            // Count failures, ignore file if too many failures.
                            DatasetAndRelPath key(file.dataset(), file.relPath());
                            if (++ m_attemptCounts[key] > m_retries) {
                                ers::error(error::RetryExceeded(ERS_HERE, file.relPath(), m_retries, missIssue));

                                // mark it as missing from cache, should stop retries
                                m_db.removeFromCache(file.dataset(), file.relPath());

                                // also clean up the count map to avoid wasting memory
                                m_attemptCounts.erase(key);
                            } else {
                                // don't wait whole cycle, retry sooner
                                 wait_time = ptime::minutes(1);
                            }
                        }
                    }
                }
                if (missing) continue;

                // create directories on CDR
                fs::path path = fs::path(m_dirCDR) / archive;
                fs::path writing_dir = (fs::path(m_dirCDR) / "writing") / dataset;
                FileSystem::create_dirs(path.parent_path().string());
                FileSystem::create_dirs(writing_dir.string());

                // free space
                Size needed_space;
                for (std::vector<LocalFile>::const_iterator it = files.begin();
                     it != files.end(); ++ it) {
                    needed_space += it->size();
                }

                try {

                    // TODO: this code implies that writing_dir is on the same filesystem with the
                    // cache directory if they are different then we can accidentally delete
                    // cache content without helping much archives directory
                    if (! m_freeSpace.makeAvailable(2*needed_space, writing_dir.string())) {
                        ers::error(::error::SpaceExausted(ERS_HERE, writing_dir.string(), needed_space.megabytes()));
                    } else {
                        fs::path writing = writing_dir / path.filename();
                        if (ArchiveName::wasEligible4SingleFile (files, dataset, archive)) {
                            const LocalFile &file = *(files.begin());
                            copyFile (file.serverPath(), writing.string());
                        } else {
                            mkZip (files, writing.string());

                            // update archive size/checksum
                            auto size = m_filesystem.file_size(writing);
                            auto checksum = m_filesystem.checksum(writing);
                            m_db.setArchiveMeta(archive, size, checksum);
                        }

                        // move from temporary CDR directory to staging area
                        m_filesystem.rename(writing, path);

                        // lock mutex, update database and signal other guy
                        m_db.setArchiveOnCDR (archive);
                        m_notifyDown.notify();
                    }

                } catch (const ers::Issue &ex) {

                    // some trouble, print a message and skip to the next file
                    ers::error(::message::ErrorWithRetry(ERS_HERE, ex));

                }

            }

        } catch (const Issues::CoralException& ex) {

            // This can potentially be any CORAL problem, but most likely this is
            // due to connectivity issues, retry again later
            ers::error(Issues::WaitForDatabase(ERS_HERE, ex));
            wait_time = ptime::minutes(1);

        } catch (const ers::Issue &ex) {

            // Some other errors, just generate an error message and hope
            // it's going to be noticed and be fixed eventually. Our main job
            // is to receive new data, archiving can be delayed, so do not die here.
            ers::error(::message::ErrorWithRetry(ERS_HERE, ex));

        }

    }

    m_semaphore.post();

} catch (const boost::thread_interrupted& ex) {
    // we just stop happily
    m_semaphore.post();
} catch( ers::Issue & ex ) {
    ers::fatal(::error::UnhandledException(ERS_HERE, ex.get_class_name(), ex));
    m_semaphore.post();
} catch (const std::exception &ex) {
    ers::fatal(::error::UnhandledException(ERS_HERE, "standard", ex));
    m_semaphore.post();
} catch (...) {
    ers::fatal(::error::UnhandledException(ERS_HERE, "unknown"));
    m_semaphore.post();
}

} // namespace coca
} // namespace daq
