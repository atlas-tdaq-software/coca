
#define BOOST_TEST_MODULE coca_test_checksum
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include <fstream>

#include "coca/common/Checksum.h"
#include "coca_test_util.h"

using namespace daq::coca;
using namespace daq::coca::test;
namespace fs = boost::filesystem;


std::string const data = "Digest this";

BOOST_AUTO_TEST_CASE(test_md5_string)
{
    auto checksum = Checksum::md5_string(data);
    BOOST_TEST(checksum == "md5:8DBD68CA892ADB12DA89C4503471410D");
}

BOOST_FIXTURE_TEST_CASE(test_md5_file, TmpDirFixture)
{
    std::string path = temp_folder + "/data.dat";
    std::ofstream f(path);
    f.write(data.data(), data.size());
    f.close();

    auto checksum = Checksum::md5_file(path);
    BOOST_TEST(checksum == "md5:8DBD68CA892ADB12DA89C4503471410D");
}
