
#include <errno.h>
#include <fstream>
#include <memory>
#include <stdlib.h>
#include <string.h>
#include <boost/filesystem.hpp>

#include <sqlite3.h>
#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/ITransaction.h"

#include "coca/common/DBBase.h"
#include "coca/common/Dataset.h"
#include "coca/common/IncomingFile.h"
#include "coca/common/Issues.h"
#include "coca/common/Transaction.h"
#include "coca/server/DBServer.h"
#include "coca_test_mockfs.h"


namespace daq {
namespace coca {
namespace test {

/*
 * Fixture that creates and deletes a temporary directory.
 */
struct TmpDirFixture {
    TmpDirFixture() {
        const char* tmpdir = getenv("TMPDIR");
        if (tmpdir == nullptr) {
            tmpdir = "/tmp";
        }
        std::string templ_str = tmpdir;
        templ_str += "/coca_test_dbschema_XXXXXX";
        auto ptr = mkdtemp(templ_str.data());  // data() is null-terminated since C++11
        if (ptr == nullptr) {
            throw std::runtime_error(strerror(errno));
        }
        temp_folder = ptr;
    }

    ~TmpDirFixture() {
        // delete folder with all stuff in it, ignore errors
        boost::system::error_code ec;
        if (getenv("COCA_UNIT_TEST_KEEP_TMPDIR") == nullptr) {
            boost::filesystem::remove_all(boost::filesystem::path(temp_folder), ec);
        }
    }

    std::string temp_folder;
};

/**
 * Test fixture that creates an empty SQLite database.
 */
struct EmptyDBFixture: TmpDirFixture {

    EmptyDBFixture() : TmpDirFixture() {
        std::string const uri = "sqlite_file:" + temp_folder + "/coca_test_db.sqlite3";
        coral::ConnectionService conn_serv;
        session.reset(conn_serv.connect(uri, coral::Update));
    }

    std::shared_ptr<coral::ISessionProxy> session;
};

/**
 * Test fixture that can create database with COCA schema.
 */
struct DBFixture: TmpDirFixture {

    /*
     * Create database with CoCa schema of a given version.
     *
     * This makes new SQLite database, because CORAL cannot create an exact
     * schema that we need we have to apply fixup scripts, their location must
     * be passes on a command line as a first argument (after "--").
     */
    std::string makeSchema(int version = -1) {
        std::string const dbpath = temp_folder + "/coca_test_db1.sqlite3";
        std::string const uri = "sqlite_file:" + dbpath;
        daq::coca::DBBase::createSchema(uri, version);

        if (version > 100) {
            // large version numbers are used for some test
            return uri;
        }

        // location of fixup script has to be passed on command line
        BOOST_TEST_REQUIRE(boost::unit_test::framework::master_test_suite().argc == 2);

        if (version < 0) {
            version = 0;
        }
        std::string const fixup_folder = boost::unit_test::framework::master_test_suite().argv[1];
        std::string const fixup_script = fixup_folder + "/coca-sqlite-fixup-" + std::to_string(version) + ".sql";

        // We could use `sqlite3` CLI to run it but PATH is not usually set
        // right but we can use C API to do the same.

        BOOST_TEST_MESSAGE("Reading fixup SQL file: " << fixup_script);
        std::string sql;
        std::ifstream sqlstream(fixup_script);
        BOOST_REQUIRE(bool(sqlstream));
        std::getline(sqlstream, sql, '\0');

        BOOST_TEST_MESSAGE("Opening sqlite database connection");
        sqlite3 *pDb = nullptr;
        auto rc = sqlite3_open(dbpath.c_str(), &pDb);
        BOOST_TEST_REQUIRE(rc == SQLITE_OK);

        BOOST_TEST_MESSAGE("Executing fixulp sql");
        rc = sqlite3_exec(pDb, sql.c_str(), nullptr, nullptr, nullptr);
        BOOST_TEST_REQUIRE(rc == SQLITE_OK);

        rc = sqlite3_close(pDb);
        BOOST_TEST_REQUIRE(rc == SQLITE_OK);

        return uri;
    }
};

/**
 * Test fixture (mixin) that populates database with some data.
 */
template <typename F>
struct PopulateBDFixture: F {

    std::vector<std::string> dataset_names = {"dataset1", "dataset2"};
    unsigned files_per_dataset = 10;
    unsigned cacheDtc = 3600;  // default value for DTC

    unsigned expectedNumFiles(int nDatasets=-1) const {
        if (nDatasets < 0) nDatasets = dataset_names.size();
        return nDatasets * files_per_dataset;
    }

    // generate file name from dataset name and file sequence number
    std::string file_name(std::string const& dataset_name, std::string const& name, unsigned i) const {
        return dataset_name + "-" + name + "-" + std::to_string(i) + ".root";
    }

    // Make Dataset instance from dataset name
    Dataset dataset(std::string const& dataset_name) const {
        std::string name = dataset_name;
        unsigned quotaMB = 1;
        unsigned ttlSec = 3600;
        Size minArchiveSize = Size::MB(1);
        Size maxArchiveSize = Size::MB(1000000);
        unsigned maxDelaySec = 300;
        Dataset::BundlePolicy bundlePolicy = Dataset::SingleFile;
        return daq::coca::Dataset(name, quotaMB, ttlSec, minArchiveSize, maxArchiveSize, maxDelaySec, bundlePolicy);
    }

    // Make an Incoming file
    IncomingFile incoming(std::string const& relPath) const {
        std::string host = "incoming-host";
        std::string clientDir = "/client/dir";
        Size size = Size::MB(1);
        int prio = 10;
        unsigned dtcSec = cacheDtc;
        // Use file name as file data
        auto checksum = Checksum::md5_string(relPath);
        BOOST_TEST_MESSAGE("Incoming file " << relPath);
        return IncomingFile(host, clientDir, relPath, size, prio, dtcSec, checksum);
    }

    // Populate database with some data
    void populateDB(DBServer& db, std::string const& filename, MockFileSystem& fs, std::string const& topCacheDir) const {

        for (auto dataset_name: dataset_names) {
            auto ds = this->dataset(dataset_name);
            // add a bunch of files
            for (unsigned i = 0; i < files_per_dataset; ++ i) {
                auto fname = file_name(dataset_name, filename, i);
                auto file = this->incoming(fname);
                db.addNewFile(file, ds);
                db.removeFromClient(dataset_name, fname);
                boost::filesystem::path cache_path = topCacheDir;
                cache_path /= dataset_name;
                cache_path /= fname;
                fs.add_file(cache_path, file.size(), file.checksum());
                db.addToCache(dataset_name, fname, file.size());
            }
        }

        // send them all to CDR
        for (auto&& archive: db.archivesToCDR()) {
            db.setArchiveOnCDR(archive.first);
        }
        BOOST_TEST(db.archivesToCDR().empty());

        // mark all as archived
        for (auto&& archive: db.archivesNotArchived()) {
            db.setArchived(archive);
        }
        BOOST_TEST(db.archivesNotArchived().empty());
    }
};

}}} // namespace daq::coca::test
