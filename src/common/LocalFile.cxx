//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class LocalFile...
//
// Author List:
//      Andy Salnikov
//
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/common/LocalFile.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/FileSystem.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

namespace daq {
namespace coca {

//----------------
// Constructors --
//----------------
LocalFile::LocalFile (const std::string& serverDir,
        const std::string& relPath,
        const std::string& dataset,
        Size size,
        const std::string& checksum)
    : m_serverDir(serverDir)
    , m_relPath(relPath)
    , m_dataset(dataset)
    , m_size(size)
    , m_checksum(checksum)
{
}

//--------------
// Destructor --
//--------------
LocalFile::~LocalFile () throw ()
{
}

/// Returns path name on coca server.
std::string
LocalFile::serverPath () const
{
    return FileSystem::join(m_serverDir, m_relPath);
}

/// Stream insertion operator to print object contents
std::ostream&
operator<<(std::ostream& out, const LocalFile& f)
{
    out << "RelPath(" << f.relPath() << ") Size(" << f.size() << ")"
        << " DirServer(" << f.serverDir() << ") Dataset(" << f.dataset() << ")";
    return out;
}


} // namespace coca
} // namespace daq
