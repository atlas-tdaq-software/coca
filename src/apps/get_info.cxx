// $Id$
// $Name$


//-----------------
// C/C++ Headers --
//-----------------
#include <chrono>
#include <time.h>
#include <boost/program_options.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/coca.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq;
namespace po = boost::program_options;
using namespace std::chrono;

namespace {

    ERS_DECLARE_ISSUE (Issues, CommandLineSyntax,
     "bad command line syntax: " << error, ((std::string) error))

    ERS_DECLARE_ISSUE (Issues, BadTimeFormat,
     "wrong time format: " << error, ((std::string) error))

    ERS_DECLARE_ISSUE(Issues, UncaughtException, "Uncaught exception, terminating.", )

    // Parse formatted time, throws if badly formatted
    system_clock::time_point parseTime(const std::string& str)
    {
        struct ::tm buf;
        // First clear the result structure.
        memset (&buf, 0, sizeof (struct ::tm));

        const char* tmp = ::strptime (str.c_str(), "%F", &buf);
        if (tmp == NULL or *tmp != 0) {
            tmp = ::strptime (str.c_str(), "%F %T", &buf);
        }
        if (tmp == NULL or *tmp != 0) {
            throw Issues::BadTimeFormat (ERS_HERE, str);
        }

        time_t out = ::mktime (&buf);
        if (out == (::time_t) (-1)) {
            throw Issues::BadTimeFormat (ERS_HERE, str);
        }
        return system_clock::from_time_t(out);

    }

}

int
main(int argc, char **argv)
try {
    /*
     * command line
     */

    po::options_description dataopt("Data specification options");
    dataopt.add_options()
            ("dataset,d", po::value<std::string>()/*->value_name("STRING")*/->default_value(std::string()), "specify dataset name")
            ("archive,a", po::value<std::string>()/*->value_name("STRING")*/->default_value(std::string()), "specify archive name")
            ("file,f", po::value<std::string>()/*->value_name("PATH")*/->default_value(std::string()), "file relative path")
            ("list,l", po::value<std::string>()/*->value_name("datasets|archives|files|locations")*/,
                    "data to display, one of 'datasets', 'archives', 'files', 'locations' (default depends on other options)")
            ;

    po::options_description filteropt("Data filtering options");
    filteropt.add_options()
            ("since,s", po::value<std::string>()/*->value_name("TIME")*/,
                    "start search from this date/time (format: \"yyyy-mm-dd hh:mm:ss\" or \"yyyy-mm-dd\")")
            ("until,u", po::value<std::string>()/*->value_name("TIME")*/,
                    "end search at this date/time (default: now)")
            ("number,n", po::value<unsigned>()/*->value_name("NUMBER")*/,
                    "limit number of returned records, only specified number of latest records will be returned."
                    " This option is not compatible with --since option."
                    " If none of --number or --since options is specified then 100 latest records are returned")
            ("skip,k", po::value<unsigned>()/*->value_name("NUMBER")*/,
                    "skip specified number of most recent records, default is 0."
                    " This option is not compatible with --since option.")
            ;

    po::options_description cmdline_options("Available options");
    cmdline_options.add_options()
            ("help,h", "print usage and exit")
            ("connection-string,c", po::value<std::string>()/*->value_name("CONN_STR")*/->default_value(coca::DBBase::defConnStr()),
                    "connection string for database (default: ATLAS offline Oracle RAC)")
            ;
    cmdline_options.add(dataopt).add(filteropt);


    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, cmdline_options), vm);

    if (vm.count("help")) {
        std::cout << "CoCa client application, reads data from CoCa database and dumps its contents.\n\n"
                << "Usage: " << argv[0] << " [options]\n\n"
                << cmdline_options << "\n";
        return 0;
    }

    po::notify(vm);

    // get option values
    std::string conn_str = vm["connection-string"].as<std::string>();
    std::string dataset = vm["dataset"].as<std::string>();
    std::string archive = vm["archive"].as<std::string>();
    std::string rel_path = vm["file"].as<std::string>();

    // what to list
    std::string w2list;
    if (vm.count("list")) {
        w2list = vm["list"].as<std::string>();
    }
    if (not w2list.empty() and w2list != "datasets" and w2list != "archives"
        and w2list != "files" and w2list != "locations") {
        Issues::CommandLineSyntax err(ERS_HERE, "bad argument for '-l' option");
        ers::error (err);
        return 1;
    }

    bool list_datasets = (w2list == "datasets");
    bool list_archives = (w2list == "archives");
    bool list_files = (w2list == "files");
    bool list_locations = (w2list == "locations");

    if (w2list.empty()) {
        if (not rel_path.empty() or not archive.empty()) {
            list_files = true;
        } else if (not dataset.empty()) {
            list_archives = true;
        } else {
            list_datasets = true;
        }
    }

    // check time and limits
    if ((vm.count("since") or vm.count("until")) and (vm.count("number") or vm.count("skip"))) {
        Issues::CommandLineSyntax err(ERS_HERE, "options --number and --skip are not compatible with --since and --until");
        ers::error (err);
        return 1;
    }

    system_clock::time_point since, until;
    if (vm.count("since")) since = parseTime(vm["since"].as<std::string>());
    if (vm.count("until")) since = parseTime(vm["until"].as<std::string>());

    unsigned limit = vm.count("number") ? vm["number"].as<unsigned>() : 100U;
    unsigned skip = vm.count("skip") ? vm["skip"].as<unsigned>() : 0U;

    /*
     * get info
     */

    coca::DBClient db (conn_str);

    // datasets
    if (list_datasets) {
        auto datasets =  db.datasets();
        for (const auto& ds: datasets) {
            std::cout << "dataset: " << ds << '\n';
        }
        return 0;
    }

    // archives
    if (list_archives) {
        std::vector<coca::RemoteArchive> archives;
        if (vm.count("since") or vm.count("until")) {
            archives = db.archives(dataset, archive, since, until);
        } else {
            archives = db.archives(dataset, archive, limit, skip);
        }
        for (const auto& ar: archives) {
            std::cout << "\n === \n" << ar;
        }
        std::cout << '\n';
        return 0;
    }

    // files
    if (list_files) {
        std::vector<coca::RemoteFile> files;
        if (vm.count("since") or vm.count("until")) {
            files = db.files(dataset, archive, rel_path, since, until);
        } else {
            files = db.files(dataset, archive, rel_path, limit, skip);
        }
        for (const auto& rf: files) {
            std::cout << "\n === \n" << rf;
        }
        std::cout << '\n';
        return 0;
    }

    // locations
    if (list_locations) {
        if (rel_path.empty()) {
          Issues::CommandLineSyntax err(ERS_HERE, "-l locations needs non-empty file name");
          ers::error (err);
          return 1;
        }
        std::vector<std::string> locations = db.fileLocations(rel_path, dataset);
        for (const auto& loc: locations) {
            std::cout << loc << '\n';
        }
        return 0;
    }

    return 0;

} catch (const po::error& ex) {
    std::cerr << ex.what() << "\n"
            "Use --help option for usage information.\n";
    return 1;
} catch (const std::exception& ex) {
    ers::error(Issues::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(Issues::UncaughtException(ERS_HERE));
    return 1;
}
