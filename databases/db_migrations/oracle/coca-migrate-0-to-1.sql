--
-- Migrate CoCa schema from version 0 to version 1
--

-- add new table for metadata
CREATE TABLE ATLAS_COCA.COCA_META (
    METAKEY VARCHAR2(4000) PRIMARY KEY,
    VALUE VARCHAR2(4000) NOT NULL
);

-- insert initial version number
INSERT INTO ATLAS_COCA.COCA_META (METAKEY, VALUE) VALUES ('VERSION', '1');

COMMIT;