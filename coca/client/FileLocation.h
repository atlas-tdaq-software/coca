#ifndef COCA_FILELOCATION_H
#define COCA_FILELOCATION_H
//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <iosfwd>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_client

/**
 *  @ingroup coca_client
 *  
 *  @brief Class representing location of the file (host plus path name).
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class FileLocation  {
public:

    /// Constructor takes host name and file path name.
    FileLocation (const std::string &server, const std::string &path) ;
    
    // Destructor
    ~FileLocation () throw ();

    /// Returns host name where file is located.
    const std::string& host() const { return m_host; }
    
    /// Returns path name on remote host.
    const std::string& path() const { return m_path; }
    
protected:

private:

    std::string m_host;  ///< Host name where file is located.
    std::string m_path;  ///< Path name of the file on remote host.
};

/// Standard stream insertion operator
std::ostream&
operator<<(std::ostream &str, const FileLocation& loc);

} // namespace coca
} // namespace daq

#endif // COCA_FILELOCATION_H
