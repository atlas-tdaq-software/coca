//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class Process...
//
// Author List:
//      Andy Salnikov
//
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/server/Process.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>
#include <signal.h>
#include <sys/wait.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {


    ERS_DECLARE_ISSUE ( errors, ErrnoError, 
       "call to `" << syscall << "' failed with error: " << ::strerror (errno), 
       ((std::string) syscall))
    
    /**
     *  Helper class to facilitate the construction of the string arguments 
     *  from C++ constructs usable by system calls.
     */
    class Vector2Exec {
    public:
        Vector2Exec (const std::vector<std::string> &commandLine);
        ~Vector2Exec () throw ();
        const char *executable () const {return m_argv[0];}
        int argc() const {return m_argc;}
        char* const* argv() const {return m_argv;}
    private:
        int m_argc;
        char** m_argv;
    };

#ifndef ERS_NO_DEBUG
    std::ostream&
    operator<<(std::ostream& out, const Vector2Exec& v2e) {
        for(int i = 0 ; i != v2e.argc() ; ++ i ) {
            if (i) out << ' ';
            if (strchr(v2e.argv()[i], ' ') != 0) {
                out << "'" << v2e.argv()[i] << "'";
            } else {
                out << v2e.argv()[i];
            }
        }
        return out;
    }
#endif
}

/*
 * Vector2Exec
 */

::Vector2Exec::Vector2Exec (const std::vector<std::string> &commandLine)
    : m_argc(int(commandLine.size()))
    , m_argv(0)
{
    ERS_PRECONDITION(not commandLine.empty());

    typedef char * str_t;
    m_argv = new str_t[m_argc + 1];
    
    for (int k = 0; k != m_argc; ++ k) {
        m_argv[k] = new char[commandLine[k].size() + 1];
        strcpy (m_argv[k], commandLine[k].c_str());
    }
    m_argv[m_argc] = 0;
}

::Vector2Exec::~Vector2Exec ()
    throw ()
{
    for (int k = 0; k < m_argc; k ++) {
        delete[] m_argv[k];
    }
    delete[] m_argv;
}


//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

namespace daq {
namespace coca {


Process::Process (const std::vector<std::string> &commandLine,
                  const std::string &name, 
                  bool background)
    : m_name (name)
    , m_childPid(0)
    , m_running(false)
    , m_background (background)
{
    ERS_PRECONDITION(not commandLine.empty());
    
    ::Vector2Exec l2e (commandLine);
    ERS_DEBUG (3, "Going to exec:" << l2e);
    m_childPid = fork ();
    if (m_childPid == 0) {
        // this is the child
        /*
          Warning: info libc 'POSIX Threads' 'Threads and Fork'
          Basically: do not use anything, especially any mutex (no ERS!)
          "exec" something as soon as possible :-)
        */
        if (m_background) {
            close (0);
        }
        execvp (l2e.executable(), l2e.argv());
        // Got here? Error!!
        std::string fatal ("echo \"pid[$$]: FATAL: cannot execute: ");
        fatal = fatal + l2e.executable() + "; error: " + strerror (errno)
            + "\"; false";
        execl ("/bin/sh", "sh", "-c", fatal.c_str(), "1>&2", NULL);
        // what? still here??
        abort ();
    } else if (m_childPid == -1) {
        // this is the server; the child was NOT created
        throw ProcessIssues::ForkError (ERS_HERE, ::errors::ErrnoError(ERS_HERE, "fork"));
    } else {
        // parent, child started OK
        m_running = true;
    }
    // this is the server; the child was created successfully
    ERS_DEBUG (2, "pid [" << m_childPid << "]: " << l2e);
}

Process::~Process ()
    throw ()
{
    if (m_running && ! m_background) {
        terminate ();
    }
}

bool
Process::isRunning (int& code) const
{
    if (! m_running) {
        return false;
    }
    
    int exit_status = -1;
    pid_t tmp = waitpid (m_childPid, &exit_status, WNOHANG);
    switch (tmp) {
    case 0:
        // still running
        return true;
    case -1:
        // "wait" failed
        ::errors::ErrnoError err (ERS_HERE, "wait");
        ers::fatal(err);
        ::abort();
    }
    ERS_ASSERT (tmp == m_childPid);
    ERS_ASSERT (! WIFSTOPPED (exit_status));
    // process exited
    m_running = false;
    if (WIFEXITED (exit_status)) {
        code = WEXITSTATUS (exit_status);
        return false;
    } else if (WIFSIGNALED (exit_status)) {
        throw ProcessIssues::Killed (ERS_HERE, m_name, m_childPid, WTERMSIG (exit_status));
    }
    ERS_ASSERT(false);
}

bool
Process::isRunning () const
{
    int exit_code = -1;
    if (isRunning (exit_code)) {
        return true;
    }
    if (exit_code != 0) {
        throw ProcessIssues::BadExitCode (ERS_HERE, m_name, m_childPid, exit_code);
    }
    return false;
}

void
Process::wait (const std::chrono::milliseconds& time2Wait,
    const std::chrono::milliseconds& pollTime) const
{
    std::chrono::milliseconds elapsed = std::chrono::milliseconds::zero();
    while (elapsed <= time2Wait) {
        try {
            if (not isRunning()) {
                return;
            }
        } catch (const ProcessIssues::ChildError& ex) {
            ERS_DEBUG (3, ex);
            return;
        }
        tsleep(pollTime);
        elapsed += pollTime;
    }
}

void
Process::kill (int signum)
{
    ERS_DEBUG (3, "sending signal " << signum  << " (" << ::strsignal (signum)
              << ") to process: " << m_name);
    int tmp = ::kill (m_childPid, signum);
    if (tmp != 0) {
        ::errors::ErrnoError err (ERS_HERE, "kill");
        ers::fatal(err);
        ::abort();
    }
}

void
Process::terminate ()
{
    try {
        if (isRunning()) {
            this->kill (SIGTERM);
            wait (std::chrono::seconds(2), std::chrono::milliseconds(500));
        } else {
            return;
        }
        if (isRunning()) {
            this->kill (SIGTERM);
            wait (std::chrono::seconds(2), std::chrono::milliseconds(500));
        } else {
            return;
        }
        if (isRunning()) {
            this->kill (SIGKILL);
            wait (std::chrono::seconds(2), std::chrono::milliseconds(500));
        }
    } catch (const ProcessIssues::ChildError& ex) {
        ERS_DEBUG (0, ex);
    }
}

void
Process::waitTimeout (int &exit_code, 
    const std::chrono::milliseconds& time2Wait,
    const std::chrono::milliseconds& pollTime)
{
    std::chrono::milliseconds elapsed = std::chrono::milliseconds::zero();
    while (elapsed <= time2Wait) {
        if (not isRunning (exit_code)) {
            return;
        }
        tsleep(pollTime);
        elapsed += pollTime;
    }
    terminate ();
    throw ProcessIssues::KilledTimeout (ERS_HERE, m_name, m_childPid);
}

void
Process::waitTimeout (const std::chrono::milliseconds& time2Wait,
    const std::chrono::milliseconds& pollTime)
{
    int exit_code = -1;
    waitTimeout (exit_code, time2Wait, pollTime);
    if (exit_code != 0) {
        throw ProcessIssues::BadExitCode (ERS_HERE, m_name, m_childPid, exit_code);
    }
}

// sleep for a PollTime period
void 
Process::tsleep (const std::chrono::milliseconds& pollTime)
{
    struct timespec tsrem = {pollTime.count()/1000, (pollTime.count()%1000)*1000000};
    while (true) {
        int rc = nanosleep(&tsrem, &tsrem);
        // check if sleep was interrupted by signal
        if (not (rc == -1 and errno == EINTR)) break;
    }
}

} // namespace coca
} // namespace daq
