#ifndef COCA_FILECHECK_H
#define COCA_FILECHECK_H

#include "coca/common/Size.h"
#include "ers/ers.h"


namespace daq::coca {

class FileSystemABC;

    // Issues declaration
    ERS_DECLARE_ISSUE(FileCheck, Exception, , )

    ERS_DECLARE_ISSUE_BASE (FileCheck, CheckFailed, Exception,
                            "filed to stat/open file: " << path,
                            , ((std::string) path))

    ERS_DECLARE_ISSUE_BASE (FileCheck, SizeMismatch, Exception,
                            "file: " << path
                            << ": expected size " << exp_size
                            << ", actual size " << act_size,
                            , ((std::string) path)
                            ((uint64_t) exp_size)
                            ((uint64_t) act_size))

    ERS_DECLARE_ISSUE_BASE (FileCheck, ChecksumMismatch, Exception,
                            "file: " << path
                            << ": expected checksum " << exp_chksm
                            << ", actual checksum " << act_chksm,
                            , ((std::string) path)
                            ((std::string) exp_chksm)
                            ((std::string) act_chksm))

/// @addtogroup coca_common

/**
 * @ingroup coca_common
 * @brief Namespace containing methods to validate file size and checksum.
 */
namespace FileCheck {

    /**
     *  @brief Checks the size and checksum of a file.
     *
     *  Finds the file, gets its size and checksum compares to expected values,
     *  throws an exception if they are different.
     *
     *  @param[in] path  Path name of the file.
     *  @param[in] size  Expected file size.
     *  @param[in] checksum Expected file checksum, can be empty to disable check.
     *  @param[in] fs  Filesystem abstraction.
     *
     *  @throw CheckFailed  If the file cannot be open.
     *  @throw SizeMismatch If the file size differs from expected size.
     *  @throw ChecksumMismatch If the file checksum differs from expected.
     */
    void check(const std::string &path, Size size, std::string const& checksum, FileSystemABC& fs);

    /**
     *  @brief Checks the size of a file with a megabyte precision.
     *
     *  This method should be used on old files that did not have exact byte
     *  size stored in a database.
     *
     *  @param[in] path  Path name of the file.
     *  @param[in] size  Expected file size.
     *  @param[in] fs  Filesystem abstraction.
     *
     *  @throw CheckFailed  If the file cannot be open.
     *  @throw SizeMismatch If the file size differs from expected size.
     */
    void checkMB(const std::string &path, Size size, FileSystemABC& fs);

} // namespace FileCheck

} // namespace daq::coca

#endif // COCA_FILECHECK_H
