//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class Transaction...
//
// Author List:
//      Andrey Salnikov,32 2-A22,+41227678094,
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/common/Transaction.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <RelationalAccess/ITransaction.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Issues.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::coca;

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

//----------------
// Constructors --
//----------------
Transaction::Transaction (const std::shared_ptr<coral::ISessionProxy>& session, Mode mode, CloseMode cmode)
    : m_session(session)
    , m_cmode(cmode)
{
    coral::ITransaction& trans = m_session->transaction();
    if ( trans.isActive() and mode != NoOpen ) {
        throw Issues::TransactionActive(ERS_HERE);
    }
    if ( mode == Read ) {
        trans.start(true);
    } else if ( mode == Update ) {
        trans.start(false);
    }
}

//--------------
// Destructor --
//--------------
Transaction::~Transaction () throw ()
{
    if ( m_cmode != NoClose ) {
        coral::ITransaction& trans = m_session->transaction();
        if (trans.isActive()) {
            if (m_cmode == Abort) {
                trans.rollback();
            } else {
                trans.commit();
            }
        }
    }
}

// close the transaction
void
Transaction::commit()
{
    coral::ITransaction& trans = m_session->transaction();
    if ( not trans.isActive() ) {
        throw Issues::TransactionInactive(ERS_HERE);
    }
    trans.commit();
    m_cmode = NoClose;
}

// close the transaction
void
Transaction::rollback()
{
    coral::ITransaction& trans = m_session->transaction();
    if ( not trans.isActive() ) {
        throw Issues::TransactionInactive(ERS_HERE);
    }
    trans.rollback();
    m_cmode = NoClose;
}

// get current transaction mode
Transaction::Mode
Transaction::mode()
{
    coral::ITransaction& trans = m_session->transaction();
    if (trans.isActive()) {
        if (trans.isReadOnly()) {
            return Read;
        } else {
            return Update;
        }
    } else {
        return NoOpen;
    }
}
