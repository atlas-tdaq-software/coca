#ifndef COCA_LOCALFILE_H
#define COCA_LOCALFILE_H

//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class LocalFile.
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Size.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_common

/**
 *  @ingroup coca_common
 *  @brief Class representing local data file on CoCa server size.
 *
 *  Information about server-size copy of the files include file name,
 *  directory, file size, and dataset name.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

class LocalFile {
public:

    /**
     *   Constructor.
     *
     *   @param[in] serverDir  Directory on coca server host.
     *   @param[in] relPath    File name relative to the clientDir.
     *   @param[in] dataset    CoCa dataset name.
     *   @param[in] size       Size of the file, bytes.
     */
    LocalFile (const std::string& serverDir,
               const std::string& relPath,
               const std::string& dataset,
               Size size,
               const std::string& checksum);

    // Destructor
    ~LocalFile () throw ();

    /// Returns directory on coca server host.
    const std::string& serverDir() const {return m_serverDir;}

    /// Returns relative file name.
    const std::string& relPath () const {return m_relPath;}

    /// Returns path name on coca server.
    std::string serverPath () const ;

    /// Returns CoCa dataset name.
    const std::string& dataset() const {return m_dataset;}

    /// Returns size of the file in MBytes
    Size size () const {return m_size;}

    /// Returns file checksum, could be empty string.
    const std::string& checksum() const {return m_checksum;}

protected:

private:

    // Data members
    std::string m_serverDir;    ///< Directory on coca server host
    std::string m_relPath;      ///< Relative path (w.r.t. m_serverDir)
    std::string m_dataset;      ///< CoCa dataset name
    Size m_size;            ///< File size
    std::string m_checksum;     ///< File checksum

};


/// Stream insertion operator to print object contents
std::ostream&
operator<<(std::ostream& out, const LocalFile& f);

} // namespace coca
} // namespace daq

#endif // COCA_LOCALFILE_H
