#define BOOST_TEST_MODULE coca_test_nextid
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "coca/common/Issues.h"
#include "coca/common/NextId.h"
#include "coca/common/Transaction.h"
#include "coca_test_util.h"

using namespace daq::coca;
using namespace daq::coca::test;
namespace tt = boost::test_tools;


BOOST_FIXTURE_TEST_CASE(test_nextid, EmptyDBFixture)
{
    NextId nextid("COCA_NEXTID");
    {
        Transaction tr(session, Transaction::Update);
        int schema_version = 0;
        nextid.createTable(schema_version, tr);
    }

    struct {
        const char* seq;
        unsigned seqId;
        unsigned id;
    } tests[] = {
        {"A", 0, 1},
        {"A", 0, 2},
        {"A", 0, 3},
        {"A", 1, 1},
        {"A", 1, 2},
        {"B", 0, 1},
        {"C", 0, 1},
        {"A", 0, 4},
    };

    for (auto& test: tests) {
        Transaction tr(session, Transaction::Update);
        unsigned id = nextid.next(test.seq, test.seqId, tr);
        BOOST_TEST(id == test.id);
    }
}
