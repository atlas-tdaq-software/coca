//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/common/FileCheck.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/filesystem.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/FileSystem.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::coca;
namespace fs = boost::filesystem;


void
FileCheck::check(const std::string &path, Size size, std::string const& checksum, FileSystemABC& fs)
{
    // find actual size
    Size actual_size;
    try {
        actual_size = fs.file_size(path);
    } catch (const ers::Issue &ex) {
        throw CheckFailed(ERS_HERE, path, ex);
    }
    if (actual_size != size) {
        throw SizeMismatch (ERS_HERE, path, size.bytes(), actual_size.bytes());
    }

    if (not checksum.empty()) {
        std::string actual_checksum;
        try {
            actual_checksum = fs.checksum(path);
        } catch (const ers::Issue &ex) {
            throw CheckFailed(ERS_HERE, path, ex);
        }
        if (actual_checksum != checksum) {
            throw ChecksumMismatch(ERS_HERE, path, checksum, actual_checksum);
        }
    }
}

void
FileCheck::checkMB(const std::string &path, Size size, FileSystemABC& fs)
{
    // find actual size
    Size actual_size;
    try {
        actual_size = fs.file_size(path);
    } catch (const ers::Issue &ex) {
        throw CheckFailed(ERS_HERE, path, ex);
    }
    if (actual_size.megabytes() != size.megabytes()) {
        throw SizeMismatch (ERS_HERE, path, size.bytes(), actual_size.bytes());
    }
}
