#ifndef COCA_CHECKSTORAGE_H
#define COCA_CHECKSTORAGE_H
//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/server/NotifyFlag.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class OWLSemaphore;
namespace daq {
namespace coca {
    class DBServer;
}
}

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_server

/**
 *  @ingroup coca_server
 * 
 *  @brief Threads which watches that archives are copied to CDR.
 * 
 *  This class represents a thread which watches CDR space and
 *  marks the files as archived after they disappear from 
 *  CDR directory.
 */

class CheckStorage {
public:

    /**
     *  Constructor.
     *  
     *  @param[in] db         Database object.
     *  @param[in] cdrSuffix  File extension that CDR attaches to files.  
     *  @param[in] notifyUp   Communication channel from upstream thread.
     *  @param[in] semaphore  Semaphore to be raised when this thread exits.
     *  
     *  @throw boost::thread_resource_error if thread fails to launch.
     */
    CheckStorage (DBServer& db, const std::string& cdrSuffix, const NotifyFlag& notifyUp,
            OWLSemaphore& semaphore);

    /// operator called by thread
    void operator()();

private:
    
    DBServer& m_db;
    std::string m_cdrSuffix;
    NotifyFlag m_notifyUp;
    OWLSemaphore& m_semaphore;
    
};

} // namespace coca
} // namespace daq

#endif // COCA_CHECKSTORAGE_H
