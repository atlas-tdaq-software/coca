//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/client/DBStat.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <CoralBase/AttributeList.h>
#include <CoralBase/TimeStamp.h>
#include <RelationalAccess/ConnectionServiceException.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/coral-helpers.h"
#include "coca/common/Issues.h"
#include "coca/common/Transaction.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

#ifndef ERS_NO_DEBUG
    std::string
    sec2str(daq::coca::DBStat::time_point tp)
    {
        time_t t = std::chrono::system_clock::to_time_t(tp);
        struct tm tm;
        localtime_r(&t, &tm);
        char buf[64];
        strftime(buf, sizeof buf, "%Y-%m-%d %H:%M:%S%z", &tm);
        return std::string(buf);
    }
#endif

}



//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace coca {

//----------------
// Constructors --
//----------------
DBStat::DBStat (const std::string& connStr)
    : m_impl()
{
    m_impl.reset(new DBBase(connStr));
}

//--------------
// Destructor --
//--------------
DBStat::~DBStat() throw()
{
}

// Returns per-time interval statistics
void
DBStat::intervalStat(std::map<time_point, StatFiles>& stat,
                     const time_point& begin,
                     const time_point& end,
                     Interval interval)
try {

    // expression for grouping
    const char* truncFmt = "";
    switch (interval) {
    case Hour:
        truncFmt = "HH";
        break;
    case Day:
        truncFmt = "DDD";
        break;
    case Month:
        truncFmt = "MONTH";
        break;
    case Year:
        truncFmt = "YEAR";
        break;
    }
    std::string truncExpr = "TRUNC(CA.REGISTERED_AT, '";
    truncExpr += truncFmt;
    truncExpr += "')";

    // start read transaction
    Transaction tran(m_impl->session());

    // build query
    std::unique_ptr<coral::IQuery> query (tran.session().nominalSchema().newQuery());

    // What
    query->addToTableList(m_impl->cacheTableName(), "CA");

    coral_helpers::defineOutput<uint32_t>(*query, "COUNT(*)", "NFILES");
    coral_helpers::defineOutput<uint32_t>(*query, "SUM(CA.SIZE_MB)", "SIZE_MB");
    coral_helpers::defineOutput<coral::TimeStamp>(*query, truncExpr, "IBEGIN");
    if (m_impl->schemaVersion() >= 3) {
        // starting with v3 we store size oin bytes, but some or all values can be NULL
        coral_helpers::defineOutput<Size>(*query, "SUM(COALESCE(CA.SIZE_BYTES, 0))", "SIZE_BYTES");
    }

    // selection
    coral::AttributeList condData;
    std::string where;
    if (begin != time_point()) {
        std::chrono::nanoseconds chrono_ns = begin.time_since_epoch();
        coral::TimeStamp::ValueType ns = chrono_ns.count();
        coral::TimeStamp coral_begin(ns);
        if ( not where.empty() ) where += " AND ";
        where += "CA.REGISTERED_AT >= :BEGIN";
        coral_helpers::addAttr(condData, "BEGIN", coral_begin);
    }
    if (end != time_point()) {
      std::chrono::nanoseconds chrono_ns = end.time_since_epoch();
      coral::TimeStamp::ValueType ns = chrono_ns.count();
        coral::TimeStamp coral_end(ns);
        if ( not where.empty() ) where += " AND ";
        where += "CA.REGISTERED_AT <= :END";
        coral_helpers::addAttr(condData, "END", coral_end);
    }
    if (not where.empty()) {
        query->setCondition(where, condData);
    }

    // group
    query->groupBy(truncExpr);

    // order
    query->addToOrderList(truncExpr);

    // execute
    coral::ICursor& cursor = query->execute();
    while (cursor.next()) {

        const coral::AttributeList& row = cursor.currentRow();

        uint32_t nfiles = row[0].data<uint32_t>();
        Size size = Size::MB(row[1].data<uint32_t>());
        const coral::TimeStamp& ibegin = row[2].data<coral::TimeStamp>();
        if (m_impl->schemaVersion() >= 3) {
            size = coral_helpers::getAttr(row[3], size);
        }

        std::chrono::seconds seconds(ibegin.total_nanoseconds()/1000000000);
        time_point tp((time_point::duration(seconds)));
        stat.insert(std::make_pair(tp, StatFiles(nfiles, size)));

        ERS_DEBUG(2,"DBStat::intervalStat: time=" << sec2str(tp)
                << " nfiles=" << nfiles << " size=" << size);
    }

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

/// Returns per-dataset statistics
void
DBStat::datasetStat(std::map<std::string, StatFiles>& stat, bool cached)
try {

    // start read transaction
    Transaction tran(m_impl->session());

    // build query
    std::unique_ptr<coral::IQuery> query (tran.session().nominalSchema().newQuery());

    // What
    query->addToTableList(m_impl->cacheTableName(), "CA");

    coral_helpers::defineOutput<uint32_t>(*query, "DATASET", "DS_ID");
    coral_helpers::defineOutput<uint32_t>(*query, "COUNT(*)", "NFILES");
    coral_helpers::defineOutput<uint32_t>(*query, "SUM(CA.SIZE_MB)", "SIZE_MB");
    if (m_impl->schemaVersion() >= 3) {
        // starting with v3 we store size oin bytes, but some or all values can be NULL
        coral_helpers::defineOutput<Size>(*query, "SUM(COALESCE(CA.SIZE_BYTES, 0))", "SIZE_BYTES");
    }

    // selection
    if (cached) {
        coral::AttributeList condData;
        std::string where = "CA.IN_CACHE = :YES";
        coral_helpers::addAttr(condData, "YES", true);
        query->setCondition(where, condData);
    }

    // group
    query->groupBy("DATASET");

    // order
    query->addToOrderList("DATASET");

    // execute
    coral::ICursor& cursor = query->execute();
    while (cursor.next()) {

        const coral::AttributeList& row = cursor.currentRow();

        const std::string &dataset = m_impl->getStr (row[0].data<uint32_t>(), tran);
        uint32_t nfiles = row[1].data<uint32_t>();
        Size size = Size::MB(row[2].data<uint32_t>());
        if (m_impl->schemaVersion() >= 3) {
            size = coral_helpers::getAttr(row[3], size);
        }

        stat.insert(std::make_pair(dataset, StatFiles(nfiles, size)));

        ERS_DEBUG(2, "DBStat::intervalStat: dataset=" << dataset << " nfiles=" << nfiles
                << " size=" << size);
    }

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

} // namespace coca
} // namespace daq
