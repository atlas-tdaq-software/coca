// $Id$
// $Name$


//-----------------
// C/C++ Headers --
//-----------------
#include <boost/program_options.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/coca.h"
#include "ers/ers.h"
#include "ipc/core.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------
namespace po = boost::program_options;
using namespace daq::coca;

namespace {

    ERS_DECLARE_ISSUE( errors, UncaughtException, "Uncaught exception, terminating.", )

}

int
main(int argc, char **argv)
try {
    /*
     * command line
     */
    po::options_description cmdline_options("Available options");
    cmdline_options.add_options()
            ("help,h", "print usage and exit")
            ("partition,p", po::value<std::string>()/*->value_name("STRING")*/->default_value("initial"),
                    "partition to search for CoCa server")
            ("server,s", po::value<std::string>()/*->value_name("STRING")*/->required(),
                    "CoCa server name")
            ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, cmdline_options), vm);

    if (vm.count("help")) {
        std::cout << "CoCa (Collection and Cache service) server configuration dump\n\n"
                << "Usage: " << argv[0] << " [options]\n\n"
                << cmdline_options << "\n";
        return 0;
    }
    po::notify(vm);

    std::string partition = vm["partition"].as<std::string>();
    std::string server = vm["server"].as<std::string>();

    ERS_DEBUG (0, "'" << partition << "' '" << server << "'");

    /*
     * IPC initialization
     */

    IPCCore::init (argc, argv);

    /*
     * dumop config
     */

    Register coca (server, partition);

    ERS_INFO ("\n\n" << coca.configuration());
    return 0;

} catch (const po::error& ex) {
    std::cerr << ex.what() << "\n"
            "Use --help option for usage information.\n";
    return 1;
} catch (const std::exception& ex) {
    ers::error(errors::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(errors::UncaughtException(ERS_HERE));
    return 1;
}
