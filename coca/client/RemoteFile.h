#ifndef COCA_REMOTEFILE_H
#define COCA_REMOTEFILE_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <vector>
#include <iosfwd>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/client/FileLocation.h"
#include "coca/common/Size.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_client

/**
 *  @ingroup coca_client
 *
 *  @brief Class containing complete information about remote CoCa file.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class RemoteFile  {
public:

    typedef std::vector<FileLocation> FileLocations;

    /**
     *  @brief Constructor
     */
    RemoteFile (const std::string& dataset,
                const std::string& relPath,
                const FileLocations& locations,
                const std::string& archive,
                Size size,
                const std::string& checksum) ;

    // Destructor
    ~RemoteFile () throw ();

    /// Returns dataset name.
    const std::string& dataset () const { return m_dataset; }

    /// Returns relative path name.
    const std::string& relPath () const  { return m_relPath; }

    /// Returns list of the file locations.
    const FileLocations& locations() const { return m_locations; }

    /// Returns corresponding archive name.
    const std::string& archive () const { return m_archive; }

    /// Returns file size in MBytes.
    Size size () const { return m_size; }

    /// Returns file checksum, could be empty string.
    const std::string& checksum() const { return m_checksum; }

protected:

private:

    // Data members
    std::string m_dataset;
    std::string m_relPath;
    FileLocations m_locations;
    std::string m_archive;
    Size m_size;
    std::string m_checksum;
};

/// Standard stream insertion operator
std::ostream&
operator<<(std::ostream &str, const RemoteFile& file);

} // namespace coca
} // namespace daq

#endif // COCA_REMOTEFILE_H
