//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/server/ServerInfo.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Str2Id.h"
#include "coca/common/Transaction.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------
using namespace daq::coca;

/*
 * ServerInfo
 */

ServerInfo::ServerInfo (const std::shared_ptr<coral::ISessionProxy>& session,
                        Str2Id &s2i,
                        const std::string &serverIPC,
                        const std::string &serverHost,
                        const std::string &topDir,
                        const std::string &CDRHost,
                        const std::string &CDRDirOnCDRHost,
                        const std::string &archive_dir)
{
    // start transaction
    Transaction tran(session, Transaction::Update);

    m_server_ipc = s2i.insert (serverIPC, tran);
    m_server_host = s2i.insert (serverHost, tran);
    m_top_dir = topDir;
    m_cdr_host =  s2i.insert (CDRHost, tran);
    m_cdr_dir_on_cdr_host = CDRDirOnCDRHost;
    m_archive_dir = archive_dir;

    ERS_ASSERT (m_server_ipc && m_server_host && ! m_top_dir.empty()
                && m_cdr_host && ! m_cdr_dir_on_cdr_host.empty()
                && ! m_archive_dir.empty());
}

ServerInfo::~ServerInfo ()
    throw ()
{
}
