
#include "coca/common/Checksum.h"

#include <cerrno>
#include <cerrno>
#include <cstring>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iterator>
#include <boost/algorithm/hex.hpp>
#include <boost/uuid/detail/md5.hpp>


namespace fs = boost::filesystem;
using boost::uuids::detail::md5;

namespace {

// make hex-string out of md5 digest, from stackoverflow
std::string toString(const md5::digest_type& digest)
{
    auto digest_begin = &digest[0];
    int count = sizeof(md5::digest_type) / sizeof(digest[0]);
    std::string result;
    boost::algorithm::hex(digest_begin, digest_begin + count, std::back_inserter(result));
    return result;
}

} // namespace

namespace daq::coca {

ERS_DECLARE_ISSUE_BASE(Checksum, FileNotFound, Exception,
    "failed to open file \"" << name << "\" (error=" << errno << ", " << strerror(errno) << ")", ,
    ((std::string) name))

ERS_DECLARE_ISSUE_BASE(Checksum, ReadError, Exception,
    "failed to read file \"" << name << "\" (error=" << errno << ", " << strerror(errno) << ")", ,
    ((std::string) name))


namespace Checksum  {

std::string md5_string(const std::string& string)
{
    boost::uuids::detail::md5 hash;
    hash.process_bytes(string.data(), string.size());
    md5::digest_type digest;
    hash.get_digest(digest);

    return "md5:" + ::toString(digest);
}

std::string md5_file(const boost::filesystem::path& path)
{
    int fd = ::open(path.string().c_str(), O_RDONLY);
    if (fd < 0) {
        throw FileNotFound(ERS_HERE, path.string());
    }

    const unsigned buf_size = 1024*1024;
    // depends on large stack size
    char buf[buf_size];

    boost::uuids::detail::md5 hash;

    while (true) {
        auto len = ::read(fd, buf, buf_size);
        if (len == 0) {
            // EOF
            ::close(fd);
            break;
        } else if (len < 0) {
            if (errno == EINTR) {
                // retry
                continue;
            } else {
                ::close(fd);
                throw ReadError(ERS_HERE, path.string());
            }
        }
        hash.process_bytes(buf, len);
    }

    md5::digest_type digest;
    hash.get_digest(digest);

    return "md5:" + ::toString(digest);
}

} // namespace Checksum
} // namespace daq::coca
