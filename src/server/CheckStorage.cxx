//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Description:
//      Class CheckStorage...
//
// Author List:
//      Andy Salnikov
//
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/server/CheckStorage.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/date_time.hpp>
#include <boost/filesystem.hpp>
#include <boost/thread.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Issues.h"
#include "coca/server/DBServer.h"
#include "ers/ers.h"
#include "owl/semaphore.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace fs = boost::filesystem;
namespace ptime = boost::posix_time;

namespace {

    ERS_DECLARE_ISSUE(error, UnhandledException,
            "CheckStorage: Unhandled '" << name << "' exception was thrown",
            ((const char *)name) )

    template <typename Lockable>
    struct scoped_unlock {
     scoped_unlock(Lockable& lock) : m_lock(lock) { m_lock.unlock(); }
     ~scoped_unlock() { m_lock.lock(); }
     Lockable& m_lock;
    };
}


//              ----------------------------------------
//              -- Public Function Member Definitions --
//              ----------------------------------------

namespace daq {
namespace coca {

CheckStorage::CheckStorage(DBServer& db, const std::string& cdrSuffix,
    const NotifyFlag& notifyUp, OWLSemaphore& semaphore)
    : m_db (db)
    , m_cdrSuffix (cdrSuffix)
    , m_notifyUp(notifyUp)
    , m_semaphore(semaphore)
{
}

void
CheckStorage::operator()()
try {

    const ptime::time_duration long_wait = ptime::minutes(5);
    const ptime::time_duration short_wait = ptime::seconds(3);
    
    ptime::time_duration wait_time = ptime::seconds(0);
    while (true) {

        if (wait_time.total_seconds()) {
            m_notifyUp.wait(wait_time);
        }
        wait_time = long_wait;
        ERS_DEBUG (0, "CheckStorage: waken up");
        
        // if somebody wakes us up wait few seconds and let CDR do its job
        boost::this_thread::sleep(short_wait);

        try {

            // get the list of files that were copied to CDR directory
            const std::vector<std::string>& archives = m_db.archivesNotArchived();

            ERS_DEBUG (0, "CheckStorage: new iteration, found " << archives.size() << " files to watch");

            // if we don't mark any file as archived then sleep for 10 seconds and retry
            if (not archives.empty()) wait_time = ptime::seconds(10);

            typedef std::vector<std::string>::const_iterator it_t;
            for (it_t it = archives.begin(); it != archives.end()
                     && ! boost::this_thread::interruption_requested() ; ++ it) {
                bool copied = false;
                if (fs::is_regular_file(*it + m_cdrSuffix)) {
                    ERS_DEBUG (0, "CheckStorage: found mark file: " << (*it + m_cdrSuffix));
                    copied = true;
                }
                if (not fs::is_regular_file(*it)) {
                    ERS_DEBUG (0, "CheckStorage: file was removed: " << *it);
                    copied = true;
                }
                if (copied) {
                    // mark the file as archived
                    m_db.setArchived(*it);
                } else {
                    // has not been copied yet, go to next iteration with a short wait
                    wait_time = ptime::seconds(0);
                }
            }

        } catch (const Issues::CoralException& ex) {

            // This can potentially be any CORAL problem, but most likely this is
            // due to connectivity issues, retry again later
            ers::error(Issues::WaitForDatabase(ERS_HERE, ex));
            wait_time = ptime::minutes(1);

        }

    }

    m_semaphore.post();

} catch (const boost::thread_interrupted& ex) {
    // we just stop happily
    m_semaphore.post();
} catch( ers::Issue & ex ) {
    ers::fatal(::error::UnhandledException(ERS_HERE, ex.get_class_name(), ex));
    m_semaphore.post();
} catch (const std::exception &ex) {
    ers::fatal(::error::UnhandledException(ERS_HERE, "standard", ex));
    m_semaphore.post();
} catch (...) {
    ers::fatal(::error::UnhandledException(ERS_HERE, "unknown"));
    m_semaphore.post();
}

} // namespace coca
} // namespace daq
