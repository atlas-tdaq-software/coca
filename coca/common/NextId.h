#ifndef COCA_NEXTID_H
#define COCA_NEXTID_H
//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Description:
//      Class NextId.
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <RelationalAccess/ISessionProxy.h>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq {
    namespace coca {
        class Transaction;
    }
}

//              ---------------------
//              -- Class Interface --
//              ---------------------


namespace daq {
namespace coca {

/// @addtogroup coca_common

/**
 * @ingroup coca_common
 *
 * @brief Generator of the unique ID used for table keys.
 *
 * CoCa database needs sequences of the unique IDs used
 * for example for archive names, string mappings, etc.
 * This class provides a facility for generation of such
 * unique IDs. Every sequence is identified by the sequence
 * name and additional integer index which client should
 * provide when calling generator method.
 *
 * Implementation is based on the database table which keeps
 * track of all sequence names and their corresponding IDs.
 */


class NextId {
public:

    /**
     * @brief Standard constructor, initializes generator instance.
     *
     * @param[in] tablename  Name of database table storing generator data.
     */
    NextId (const std::string& tablename) ;

    // non-copyable
    NextId(const NextId&) = delete;
    NextId& operator=(const NextId&) = delete;

    /// Destructor
    ~NextId () throw() ;

    /// Get table name.
    const std::string& tableName () const { return m_name; }

    /**
     * @brief Generator method.
     *
     * Returns next ID in a sequence for the specified sequence name
     * and index.
     *
     * @param[in]  seq   Sequence name, non-empty string.
     * @param[in]  seqIdx  Sequence index, a number.
     * @param[in]  tran  Transaction context.
     *
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    unsigned next(const std::string& seq, unsigned seqIdx, Transaction& tran);

    /**
     *  Create database table.
     *
     *  The table must not exist prior to this call.
     *
     *  @param schemaVersion Database schema version number.
     *  @param tran Transaction context.
     */
    void createTable(int schemaVersion, Transaction& tran);

private:

    // data members
    std::string m_name;               ///< Database table name.
};

} // namespace coca
} // namespace daq

#endif // COCA_NEXTID_H

