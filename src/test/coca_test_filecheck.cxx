#define BOOST_TEST_MODULE coca_test_filecheck
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "coca/common/FileCheck.h"
#include "coca/common/System.h"
#include "coca_test_mockfs.h"

using namespace daq::coca;
using namespace daq::coca::test;
namespace fs = boost::filesystem;


BOOST_AUTO_TEST_CASE(test_FileCheck)
{
    MockFileSystem fs(Size::MB(1));

    std::string tmp_file("/tmp/file.data");
    fs.add_file(tmp_file, Size(1024), "md5:checksum");

    BOOST_CHECK_NO_THROW(FileCheck::check(tmp_file, Size(1024), "md5:checksum", fs));
    BOOST_CHECK_NO_THROW(FileCheck::check(tmp_file, Size(1024), "", fs));

    BOOST_CHECK_THROW(FileCheck::check("/tmp/no-file", Size(1024), "", fs), FileCheck::CheckFailed);
    BOOST_CHECK_THROW(FileCheck::check(tmp_file, Size(1025), "", fs), FileCheck::SizeMismatch);
    BOOST_CHECK_THROW(FileCheck::check(tmp_file, Size(1025), "md5:bad-checksum", fs), FileCheck::SizeMismatch);
    BOOST_CHECK_THROW(FileCheck::check(tmp_file, Size(1025), "md5:checksum", fs), FileCheck::SizeMismatch);
    BOOST_CHECK_THROW(FileCheck::check(tmp_file, Size(1024), "md5:bad-checksum?", fs), FileCheck::ChecksumMismatch);

    // MB precision
    BOOST_CHECK_NO_THROW(FileCheck::checkMB(tmp_file, Size(1024), fs));
    BOOST_CHECK_NO_THROW(FileCheck::checkMB(tmp_file, Size(1), fs));

    BOOST_CHECK_THROW(FileCheck::checkMB("/tmp/no-file", Size(1024), fs), FileCheck::CheckFailed);
    BOOST_CHECK_THROW(FileCheck::checkMB(tmp_file, Size(0), fs), FileCheck::SizeMismatch);
    BOOST_CHECK_THROW(FileCheck::checkMB(tmp_file, Size(10000000), fs), FileCheck::SizeMismatch);
}
