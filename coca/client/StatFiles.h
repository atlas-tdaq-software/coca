#ifndef COCA_STATFILES_H
#define COCA_STATFILES_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Size.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_client

/**
 *  @ingroup coca_client
 *
 *  @brief Class that collects statistics about size and number of files.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class StatFiles  {
public:

  /// Constructor takes number of files and their total size
  StatFiles (unsigned count, Size size)
      : m_count(count), m_size(size) {}

  /// Returns number of saved files
  unsigned count() const { return m_count; }

  /// Returns size of saved files
  Size size() const { return m_size; }

protected:

private:

  unsigned m_count;
  Size m_size;

};

/// Standard stream insertion operator
inline
std::ostream&
operator<<(std::ostream &str, const StatFiles& stat)
{
  return str << "StatFiles(count=" << stat.count()
      << ", size=" << stat.size() << ")" ;
}

} // namespace coca
} // namespace daq

#endif // COCA_STATFILES_H
