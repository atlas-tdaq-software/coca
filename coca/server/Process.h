#ifndef COCA_PROCESS_H
#define COCA_PROCESS_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class Process.
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>
#include <string>
#include <chrono>
#include <sys/types.h>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/server/ServerIssues.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_server

/**
 *  @ingroup coca_server
 *
 *  @brief Class representing a child process running a command.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class Process  {
public:

    /**
     *  @brief Constructor creates new child process. 
     *  
     *  The process may run in background, the background process will not be 
     *  terminated when destructor is called.
     *  
     *  @param[in] commandLine  Command line (argv), first element is executable name.
     *  @param[in] name         Command name, arbitrary string, for identification only.
     *  @param[in] background   If false then child will be stopped in destructor.
     *  
     *  @throw ProcessIssues::ForkError Thrown when fork system call fails.
     */
    Process (const std::vector<std::string>& commandLine,
             const std::string& name, 
             bool background = false);
    
    /// Destructor terminates non-background process if still running
    ~Process () throw ();

    /// Returns process ID of the child
    pid_t pid() const { return m_childPid; }
    
    /**
     *  @brief Returns true if the child process is still running. 
     *  
     *  If it has finished then its exit code is returned too or exception is 
     *  thrown if process was killed by a signal. The exception is thrown only 
     *  once for this method call.
     *  
     *  @param[out] code  Return status of the process if it has exited already
     *  @return True if child is still running, false if finished.
     *  
     *  @throw ProcessIssues::Killed Thrown if child process was terminated with a signal
     */
    bool isRunning(int& code) const;
    
    /**
     *  @brief Returns true if the child process is still running. 
     *  
     *  If child has finished with non-zero exit code or was killed by a signal 
     *  then exception is generated.
     *  
     *  @return True if child is still running, false if finished.
     *  
     *  @throw ProcessIssues::Killed Thrown if child process was terminated with a signal.
     *  @throw ProcessIssues::BadExitCode Thrown if child process finished with non-zero exit code.
     */
    bool isRunning() const;

    /**
     *  @brief Forces stop of the child process. 
     *  
     *  Sends sequence of signals [TERM, TERM, KILL] to the child waiting max 2 seconds 
     *  after each signal for child to stop.
     */
    void terminate();
    
    /**
     *  @brief Wait for child process to finish. 
     *  
     *  If it does not stop after specified timeout then terminates child process 
     *  calling terminate().
     *  
     *  @param[out] code  Return status of the process if it has exited.
     *  @param[in] time2Wait  Timeout value.
     *  @param[in] pollTime  Polling interval.
     *  
     *  @throw ProcessIssues::Killed Thrown if child process was terminated with a signal.
     *  @throw ProcessIssues::KilledTimeout Thrown if child process had to be terminated.
     */
    void waitTimeout (int &code, 
        const std::chrono::milliseconds& time2Wait,
        const std::chrono::milliseconds& pollTime = std::chrono::milliseconds(500));
    
    /**
     *  @brief Wait for child process to finish. 
     *  
     *  If it does not stop after specified timeout then terminates child process 
     *  calling terminate().
     *  
     *  @param[in] time2Wait  Timeout value.
     *  @param[in] pollTime  Polling interval.
     *  
     *  @throw ProcessIssues::Killed Thrown if child process was terminated with a signal.
     *  @throw ProcessIssues::BadExitCode Thrown if child process finished with non-zero exit code.
     *  @throw ProcessIssues::KilledTimeout Thrown if child process had to be terminated.
     */
    void waitTimeout (const std::chrono::milliseconds& time2Wait,
        const std::chrono::milliseconds& pollTime = std::chrono::milliseconds(500));

    /// Returns name of the process which was supplied in the constructor.
    const std::string& name () const {return m_name;}
    
protected:

private:

    /**
     *  @brief Wait until process stops or timeout expires. 
     *  
     *  @param[in] time2Wait  Timeout value.
     *  @param[in] pollTime  Polling interval.
     */
    void wait (const std::chrono::milliseconds& time2Wait,
        const std::chrono::milliseconds& pollTime) const;
    
    /**
     *  @brief Sends a signal to child process.
     *  
     *  @param[in] signum Signal number.
     */
    void kill (int signum);

    // Data members
    std::string m_name;      ///< Process name, for identification only
    pid_t m_childPid;        ///< Child process ID
    mutable bool m_running;  ///< True if process is running
    bool m_background;       ///< If true then do not kill child process in destructor

//------------------
// Static Members --
//------------------
public:
    
    /// Delay execution for a given period of time.
    static void tsleep (const std::chrono::milliseconds& pollTime);

};

} // namespace coca
} // namespace daq

#endif // COCA_PROCESS_H
