//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class TransientFile...
//
// Author List:
//      Andrey Salnikov,32 2-A22,+41227678094,
//
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/common/TransientFile.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/FileSystem.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

namespace daq {
namespace coca {

//----------------
// Constructors --
//----------------
TransientFile::TransientFile (const std::string& host,
                              const std::string& clientDir,
                              const std::string& relPath,
                              const std::string& serverDir,
                              const std::string& dataset,
                              Size size,
                              const std::string& checksum)
    : m_host(host)
    , m_clientDir(clientDir)
    , m_relPath(relPath)
    , m_serverDir(serverDir)
    , m_dataset(dataset)
    , m_size(size)
    , m_checksum(checksum)
{
}

//--------------
// Destructor --
//--------------
TransientFile::~TransientFile () throw ()
{
}

/// Returns path name on client host.
std::string
TransientFile::clientPath () const
{
    return FileSystem::join(m_clientDir, m_relPath);
}

/// Returns path name on coca server.
std::string
TransientFile::serverPath () const
{
    return FileSystem::join(m_serverDir, m_relPath);
}

/// Stream insertion operator to print object contents
std::ostream&
operator<<(std::ostream& out, const TransientFile& f)
{
    out << "RelPath(" << f.relPath() << ") Size(" << f.size() << ")"
        << " DirServer(" << f.serverDir() << ") Dataset(" << f.dataset() << ")"
        << " Host(" << f.host() << ") DirClient(" << f.clientDir() << ")"
        << " Checksum(" << f.checksum() << ")";
    return out;
}

} // namespace coca
} // namespace daq
