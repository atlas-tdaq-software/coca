#include <string>
#include <time.h>

#define BOOST_TEST_MODULE coca_test_dbserver
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include "coca/client/ClientIssues.h"
#include "coca/client/DBClient.h"
#include "coca/server/DBServer.h"
#include "coca/common/Dataset.h"
#include "coca/common/IncomingFile.h"
#include "coca/common/LocalFile.h"
#include "coca/common/TransientFile.h"
#include "coca_test_util.h"

using namespace daq::coca;
using namespace daq::coca::test;
namespace tt = boost::test_tools;
namespace bdata = boost::unit_test::data;


struct MakeServerFixture : DBFixture {

    std::unique_ptr<DBServer> make_server(std::string const& uri) const {
        return std::make_unique<DBServer>(
            uri, "server_ipc", "server-host", "/top/server/dir", "cdr-host", "/cdr/dir", "/archive/dir");
    }

};

using Fixture = PopulateBDFixture<MakeServerFixture>;


BOOST_DATA_TEST_CASE_F(Fixture, test_valid_version, bdata::make({0, 1, 2, 3, 4}), version)
{
    std::string const uri = makeSchema(version);
    auto dbsrv = this->make_server(uri);
    MockFileSystem fs(Size::MB(1024));
    this->populateDB(*dbsrv, "file", fs, "/top/server/dir");

    DBClient db(uri);

    auto datasets = db.datasets();
    std::sort(datasets.begin(), datasets.end());
    BOOST_TEST(datasets == this->dataset_names, tt::per_element());

    auto now = std::chrono::system_clock::now();
    auto delta = std::chrono::hours(1);

    // test for non-paged archives() method
    auto archives = db.archives();
    BOOST_TEST(archives.size() == this->dataset_names.size() * files_per_dataset);
    archives = db.archives(this->dataset_names[0]);
    BOOST_TEST(archives.size() == files_per_dataset);
    archives = db.archives("", "*file-0.root");
    BOOST_TEST(archives.size() == 2);
    archives = db.archives(this->dataset_names[0], "*file-0.root");
    BOOST_TEST(archives.size() == 1);
    archives = db.archives(this->dataset_names[0], "", now - delta, now + delta);
    BOOST_TEST(archives.size() == files_per_dataset);
    archives = db.archives(this->dataset_names[0], "", DBClient::time_point(), now - delta);
    BOOST_TEST(archives.empty());
    archives = db.archives(this->dataset_names[0], "", now + delta);
    BOOST_TEST(archives.empty());
    BOOST_REQUIRE_THROW(db.archives("NotADataset"), DBClientIssues::DatasetNotFound);

    // test for paged archives() method
    archives = db.archives("", "", files_per_dataset/2);
    BOOST_TEST(archives.size() == files_per_dataset/2);
    archives = db.archives(this->dataset_names[0], "", 1);
    BOOST_TEST(archives.size() == 1);
    archives = db.archives(this->dataset_names[0], "", 1000000);
    BOOST_TEST(archives.size() == files_per_dataset);
    archives = db.archives(this->dataset_names[0], "", files_per_dataset, files_per_dataset/2);
    BOOST_TEST(archives.size() == files_per_dataset/2);
    BOOST_REQUIRE_THROW(db.archives("NotADataset", "", 1), DBClientIssues::DatasetNotFound);

    // test for countFiles
    auto count = db.countFiles();
    BOOST_TEST(count == this->dataset_names.size() * files_per_dataset);
    count = db.countFiles(this->dataset_names[0]);
    BOOST_TEST(count == files_per_dataset);
    BOOST_REQUIRE_THROW(db.countFiles("NotADataset"), DBClientIssues::DatasetNotFound);
    count = db.countFiles("", "*-file-?.root");
    BOOST_TEST(count == 20);
    count = db.countFiles(this->dataset_names[0], "*-file-1.root");
    BOOST_TEST(count == 1);
    count = db.countFiles(this->dataset_names[0], "", "dataset1-file-0.root");
    BOOST_TEST(count == 1);
    count = db.countFiles(this->dataset_names[0], "*-file-1.root", "dataset1-file-0.root");
    BOOST_TEST(count == 0);

    // test for non-paged files() method
    auto files = db.files();
    BOOST_TEST(files.size() == this->dataset_names.size() * files_per_dataset);
    // locations() only include locations in cache (and on sender host if still there)
    BOOST_TEST(files[0].locations().size() == 1);
    BOOST_TEST_MESSAGE("files[0].locations()[0]: " << files[0].locations()[0]);
    BOOST_REQUIRE_THROW(db.files("NotADataset"), DBClientIssues::DatasetNotFound);
    files = db.files(this->dataset_names[0]);
    BOOST_TEST(files.size() == files_per_dataset);
    files = db.files("", "*file-0.root");
    BOOST_TEST(files.size() == 2);
    files = db.files("", "", "dataset?-file-0.root");
    BOOST_TEST(files.size() == 2);
    files = db.files(this->dataset_names[0], "*file-0.root");
    BOOST_TEST(files.size() == 1);
    BOOST_REQUIRE_THROW(db.files(this->dataset_names[0], "*dataset2-file-1.root", "dataset2-file-0.root"),
                        DBClientIssues::FileNotFound);
    files = db.files(this->dataset_names[0], "", "", now - delta, now + delta);
    BOOST_TEST(files.size() == files_per_dataset);
    files = db.files(this->dataset_names[0], "", "", DBClient::time_point(), now - delta);
    BOOST_TEST(files.empty());
    files = db.files(this->dataset_names[0], "", "", now + delta);
    BOOST_TEST(files.empty());

    // test for paged files() method
    files = db.files("", "", "", files_per_dataset/2);
    BOOST_TEST(files.size() == files_per_dataset/2);
    files = db.files(this->dataset_names[0], "", "", 1);
    BOOST_TEST(files.size() == 1);
    files = db.files(this->dataset_names[0], "", "", 1000000);
    BOOST_TEST(files.size() == files_per_dataset);
    files = db.files(this->dataset_names[0], "", "", files_per_dataset, files_per_dataset/2);
    BOOST_TEST(files.size() == files_per_dataset/2);
    BOOST_REQUIRE_THROW(db.files("NotADataset", "", "", 1), DBClientIssues::DatasetNotFound);
    BOOST_REQUIRE_THROW(db.files(this->dataset_names[0], "*dataset2-file-1.root", "dataset2-file-0.root", 1),
                        DBClientIssues::FileNotFound);

    // test for fileLocations method
    auto dataset_name = this->dataset_names[0];
    auto fname = file_name(dataset_name, "file", 0);
    BOOST_REQUIRE_THROW(db.fileLocations(fname, "NotADataset"), DBClientIssues::DatasetNotFound);
    auto locations = db.fileLocations("not-a-file.root", "");
    BOOST_TEST(locations.empty());
    locations = db.fileLocations(fname, "");
    BOOST_TEST(locations.size() == 2);
    locations = db.fileLocations(fname, dataset_name);
    BOOST_TEST(locations.size() == 2);
    locations = db.fileLocations(fname, dataset_name, IDBClient::LocAll);
    BOOST_TEST(locations.size() == 2);
    locations = db.fileLocations(fname, dataset_name, IDBClient::LocCache);
    BOOST_TEST(locations.size() == 1);
    locations = db.fileLocations(fname, dataset_name, IDBClient::LocArchive);
    BOOST_TEST(locations.size() == 1);

    // remove one file from cache
    dbsrv->removeFromCache(dataset_name, fname);
    locations = db.fileLocations(fname, dataset_name, IDBClient::LocAll);
    BOOST_TEST(locations.size() == 1);
    locations = db.fileLocations(fname, dataset_name, IDBClient::LocCache);
    BOOST_TEST(locations.empty());
    locations = db.fileLocations(fname, dataset_name, IDBClient::LocArchive);
    BOOST_TEST(locations.size() == 1);

    // remove all archives from a dataset
    for (auto archive: db.archives(dataset_name)) {
        dbsrv->setRemoved(archive.name());
    }
    archives = db.archives();
    BOOST_TEST(archives.size() == (this->dataset_names.size() - 1) * files_per_dataset);
    archives = db.archives("", "", this->dataset_names.size() * files_per_dataset);
    BOOST_TEST(archives.size() == (this->dataset_names.size() - 1) * files_per_dataset);

    // files counted only if they have a non-removed archive, even if cache is till there
    count = db.countFiles();
    BOOST_TEST(count == (this->dataset_names.size() - 1) * files_per_dataset);
    files = db.files();
    BOOST_TEST(files.size() == (this->dataset_names.size() - 1) * files_per_dataset);
    files = db.files(dataset_name);

    locations = db.fileLocations(fname, dataset_name, IDBClient::LocAll);
    BOOST_TEST(locations.empty());

    fname = file_name(dataset_name, "file", 1);
    locations = db.fileLocations(fname, dataset_name, IDBClient::LocAll);
    BOOST_TEST(locations.size() == 1);
    locations = db.fileLocations(fname, dataset_name, IDBClient::LocCache);
    BOOST_TEST(locations.size() == 1);
    locations = db.fileLocations(fname, dataset_name, IDBClient::LocArchive);
    BOOST_TEST(locations.empty());
}

BOOST_FIXTURE_TEST_CASE(test_wrong_version, DBFixture)
{
    // make schema version which will never exist
    std::string const uri = makeSchema(999);

    BOOST_REQUIRE_THROW(
        DBClient db(uri),
        Issues::UnsupportedSchemaVersionError
    );
}

BOOST_FIXTURE_TEST_CASE(test_metadata, DBFixture)
{
    std::string const uri = makeSchema();

    DBClient db(uri);
    auto metadata = db.metadata();

    // initially it will contain only VERSION.
    BOOST_TEST(metadata.contains("VERSION"));
    BOOST_TEST(not metadata.contains("key"));
    auto items = metadata.items();
    BOOST_TEST(items.size() == 1);
    auto value = metadata.get("key", "missing");
    BOOST_TEST(value == "missing");

    metadata.set("key", "value");
    BOOST_TEST(metadata.items().size() == 2);
    value = metadata.get("key", "missing");
    BOOST_TEST(value == "value");

    metadata.remove("key");
    BOOST_TEST(metadata.items().size() == 1);
    value = metadata.get("key", "missing");
    BOOST_TEST(value == "missing");
}

namespace {

std::string p1_rules(R"(
[
    {
        "match": "root://eosatlas.cern.ch//archive/dir/",
        "replace": "root://pc-tdq-mon-01.cern.ch:2099//",
        "keep_original": true
    },
    {
        "match": "root://anonymous@server-host",
        "replace": "root://pc-tdq-mon-05.cern.ch"
    }
]
)");

}

BOOST_FIXTURE_TEST_CASE(test_location_override, Fixture)
{
    // Test that configured overrides in metadata work with fileLocations()

    std::string const uri = makeSchema();
    auto dbsrv = this->make_server(uri);
    MockFileSystem fs(Size::MB(1024));
    this->populateDB(*dbsrv, "file", fs, "/top/server/dir");

    // Use default environment, set metadata overrides for p1
    DBClient db(uri, "default");
    auto&& metadata = db.metadata();
    metadata.set("location-rules-p1", p1_rules);

    auto dataset_name = this->dataset_names[0];
    auto fname = file_name(dataset_name, "file", 0);

    time_t t = time(0);
    struct tm tm;
    localtime_r(&t, &tm);
    auto year_str = std::to_string(tm.tm_year + 1900);

    using V = std::vector<std::string>;
    auto locations = db.fileLocations(fname, "");
    BOOST_TEST(locations == V({
        std::string("root://anonymous@server-host//top/server/dir/dataset1/dataset1-file-0.root"),
        "root://eosatlas.cern.ch//archive/dir/" + year_str + "/dataset1/dataset1-file-0.root"
    }),
    tt::per_element()
    );

    // switch to p1 env.
    DBClient db1(uri, "p1");
    using V = std::vector<std::string>;
    locations = db1.fileLocations(fname, "");
    BOOST_TEST(locations == V({
        std::string("root://pc-tdq-mon-05.cern.ch//top/server/dir/dataset1/dataset1-file-0.root"),
        "root://eosatlas.cern.ch//archive/dir/" + year_str + "/dataset1/dataset1-file-0.root",
        "root://pc-tdq-mon-01.cern.ch:2099//" + year_str + "/dataset1/dataset1-file-0.root"
    }),
    tt::per_element()
    );
}
