from __future__ import annotations

from typing import ClassVar, overload

defConnStr: str = ...


class FileLocation:
    def __init__(self, host:str, path: str): ...
    def host(self) -> str: ...
    def path(self) -> str: ...


class RemoteArchive:
    def dataset(self) -> str: ...
    def name(self) -> str: ...
    def locations(self) -> list[FileLocation]: ...
    def archivePath(self) -> str: ...


class RemoteFile:
    def dataset(self) -> str: ...
    def relPath(self) -> str: ...
    def locations(self) -> list[FileLocation]: ...
    def size(self) -> int: ...
    def archive(self) -> str: ...
    def checksum(self) -> str: ...


class Metadata:
      def contains(self, key: str) -> bool: ...
      def get(self, key: str, defVal: str = "") -> str: ...
      def items(self, ) -> dict[str, str]: ...
      def set(self, key: str, value: str) -> None: ...
      def remove(self, key: str) -> None: ...


class DBClient:

    class Location:
        LocAll: ClassVar[DBClient.Location] = ...
        LocArchive: ClassVar[DBClient.Location] = ...
        LocCache: ClassVar[DBClient.Location] = ...

    LocAll: ClassVar[Location] = Location.LocAll
    LocArchive: ClassVar[Location] = Location.LocArchive
    LocCache: ClassVar[Location] = Location.LocCache

    def __init__(self, connString: str = defConnStr): ...
    def datasets(self) -> list[str]: ...
    @overload
    def archives(
        self, dataset: str = "", archive: str = "", *, since: int, until: int
    ) -> list[RemoteArchive]: ...
    @overload
    def archives(
        self, dataset: str = "", archive: str = "", *, count: int = 100, skip: int = 0
    ) -> list[RemoteArchive]: ...
    def countFiles(self, dataset: str = "", archive: str = "", file: str = "") -> int: ...

    @overload
    def files(
        self, dataset: str = "", archive: str = "", file: str = "",  *, since: int, until: int
    ) -> list[RemoteFile]: ...
    @overload
    def files(
        self, dataset: str = "", archive: str = "", file: str = "",  *, count: int = 100, skip: int = 0
    ) -> list[RemoteFile]: ...
    def fileLocations(self, file: str, dataset: str, location: Location = LocAll) -> list[str]: ...
    def metadata(self) -> Metadata: ...


class Register:

    def __init__(self, server: str, partition: str): ...
    def configuration(self) -> str: ...
    def registerFile(self, dataset: str, file: str, prio: int, dtcSec: int, baseDir: str) -> str: ...


class StatFiles:
    def __init__(self, count: int, size: int): ...
    def count(self) -> int: ...
    def size(self) -> int: ...


class DBStat:

    class Interval:
        Hour: ClassVar[DBStat.Interval] = ...
        Day: ClassVar[DBStat.Interval] = ...
        Month: ClassVar[DBStat.Interval] = ...
        Year: ClassVar[DBStat.Interval] = ...

    def __init__(self, connString: str = defConnStr): ...
    def intervalStat(self, begin: float, end: float, interval: Interval) -> dict[float, StatFiles]: ...
    def datasetStat(self, cached: bool) -> dict[str, StatFiles]: ...
