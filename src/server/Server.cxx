//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Description:
//      Class Server...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/server/Server.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>
#include <sstream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/IncomingFile.h"
#include "coca/common/Issues.h"
#include "coca/common/Size.h"
#include "coca/server/Config.h"
#include "coca/server/DBServer.h"
#include "ers/ers.h"
#include "owl/semaphore.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

    /**
     *  @brief Special deleter method to be used with shared pointer.
     *
     *  IPC server cannot be destroyed by delete (even though it must be created
     *  with new), _destroy method must be called instead.
     */
    void serverDeleter(daq::coca::Server* srv) { srv->_destroy(true); }

    ERS_DECLARE_ISSUE(error, DatabaseError,
            "Generic database error while registering file '" << file << "'",
            ((std::string)file) )

    ERS_DECLARE_ISSUE(error, DatabaseConnectionError,
            "Database connection failure while registering file '" << file << "'",
            ((std::string)file) )

}

//              ------------------------------
//              -- Class Member Definitions --
//              ------------------------------

namespace daq {
namespace coca {

/**
 *  @brief Factory method which returns instance of the server class.
 *
 *  This method takes the same set of argument as the constructor.
 */
std::shared_ptr<Server>
Server::makeServer(IPCPartition& partition,
                   const std::string& name,
                   DBServer& db,
                   const Config& config,
                   const NotifyFlag& notifyDown,
                   OWLSemaphore& semaphore)
{
    Server* srv = new Server(partition, name, db, config, notifyDown, semaphore);
    return std::shared_ptr<Server>(srv, &::serverDeleter);
}

/*
 * Server
 */

Server::Server (IPCPartition& partition,
                const std::string& name,
                DBServer& db,
                const Config& config,
                const NotifyFlag& notifyDown,
                OWLSemaphore& semaphore)
    : IPCNamedObject<POA_cocaIPC::Server> (partition, name)
    , m_db(db)
    , m_config(config)
    , m_notifyDown(notifyDown)
    , m_semaphore(semaphore)
    , m_configStr()
    , m_disabled(false)
{
    // dump config and store it as string
    std::ostringstream cfgstr;
    cfgstr << config;
    m_configStr = cfgstr.str();

    // start running
    this->publish();
}

Server::Server::~Server() throw ()
{
    this->withdraw();
}

cocaIPC::Server::RegStatus
Server::registerFile (const cocaIPC::NewFile &file,
    const char *dataset, CORBA::String_out archive)
{
    // check disabled flag
    if (disabled()) {
        return cocaIPC::Server::ErrRegistrationDisabled;
    }

    int priority = std::min(int(file.prio), 100);
    IncomingFile remoteFile ((const char *) file.machine,
            (const char *) file.baseDir,
            (const char *) file.relPath,
            Size(file.size),
            priority,
            file.dtcSec,
            (const char *) file.checksum);
    ERS_LOG("New file: " << remoteFile);

    // get archive name
    std::string archiveStr;
    cocaIPC::Server::RegStatus error = cocaIPC::Server::ErrNoError;
    try {
        const Dataset &ds = m_config.dataset(dataset);

        // check total size of registration
        Size addsize = remoteFile.size ();
        if (addsize > ds.maxArchiveSize()) {
            ERS_LOG("entry size exceeds limit: " << addsize);
            error = cocaIPC::Server::ErrMaxArchiveSizeExceeded;
        } else {
            // open (new) archive
            archiveStr = m_db.addNewFile(remoteFile, ds);
            error = cocaIPC::Server::ErrNoError;
            ERS_LOG("entry archive name: " << archiveStr);
        }
    } catch (const Issues::DBConnectionFailure& ex) {
        // Failed to connect to database
        ers::error(::error::DatabaseConnectionError(ERS_HERE, remoteFile.relPath(), ex));
        error = cocaIPC::Server::ErrDBDisconnected;
    } catch (const Issues::CoralException& ex) {
        // Generic database error, likely due to connectivity too
        ers::error(::error::DatabaseError(ERS_HERE, remoteFile.relPath(), ex));
        error = cocaIPC::Server::ErrDBDisconnected;
    } catch (const ConfigIssues::DatasetNotAllowed& ex) {
        ERS_LOG("dataset name is not allowed: " << ex);
        error = cocaIPC::Server::ErrInvalidDataset;
    } catch (const Issues::FileAlreadyRegistered& ex) {
        ERS_LOG("file already registered: " << ex);
        error = cocaIPC::Server::ErrFileAlreadyRegistered;
    }

    if (error == cocaIPC::Server::ErrNoError) {
        // new file(s) in; wakeup GetFiles
        m_notifyDown.notify();
    }

    // return archive name
    archive = CORBA::string_dup (archiveStr.c_str());

    return error;
}

char *
Server::configuration ()
{
    return CORBA::string_dup (m_configStr.c_str());
}

void
Server::disable ()
{
    m_disabled = true;
}

bool
Server::disabled () const
{
    bool disabled = m_disabled;
    if (disabled) {
        ERS_LOG("New registrations are disabled");
    }
    return disabled;
}

// this is required by IPC
void
Server::shutdown()
{
    // raise global semaphore
    m_semaphore.post();
}

} // namespace coca
} // namespace daq
