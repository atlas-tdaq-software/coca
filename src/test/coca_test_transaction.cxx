#include "RelationalAccess/ConnectionService.h"
#include "RelationalAccess/ITransaction.h"

#define BOOST_TEST_MODULE coca_test_transaction
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "coca/common/Issues.h"
#include "coca/common/Transaction.h"
#include "coca_test_util.h"

using namespace daq::coca;
using namespace daq::coca::test;
namespace tt = boost::test_tools;


BOOST_FIXTURE_TEST_CASE(test_transaction, EmptyDBFixture)
{
    BOOST_TEST(session.get() != nullptr);
    BOOST_TEST(not session->transaction().isActive());

    {
        Transaction tran1(session, Transaction::NoOpen, Transaction::NoClose);
        BOOST_TEST(tran1.mode() == Transaction::NoOpen);
        BOOST_TEST(not session->transaction().isActive());
    }

    {
        Transaction tran1(session, Transaction::Read);
        BOOST_TEST(tran1.mode() == Transaction::Read);
        BOOST_TEST(session->transaction().isActive());
    }
    BOOST_TEST(not session->transaction().isActive());

    {
        Transaction tran1(session, Transaction::Update);
        BOOST_TEST(tran1.mode() == Transaction::Update);
        BOOST_TEST(session->transaction().isActive());
    }
    BOOST_TEST(not session->transaction().isActive());

    {
        Transaction tran1(session, Transaction::Read, Transaction::NoClose);
        BOOST_TEST(tran1.mode() == Transaction::Read);
        BOOST_TEST(session->transaction().isActive());
    }
    BOOST_TEST(session->transaction().isActive());

    {
        Transaction tran1(session, Transaction::NoOpen);
        BOOST_TEST(tran1.mode() == Transaction::Read);
        BOOST_TEST(session->transaction().isActive());
    }
    BOOST_TEST(not session->transaction().isActive());

    // test for explicit commit
    {
        Transaction tran1(session, Transaction::Read);
        BOOST_TEST(tran1.mode() == Transaction::Read);
        BOOST_TEST(session->transaction().isActive());
        tran1.commit();
        BOOST_TEST(tran1.mode() == Transaction::NoOpen);
        BOOST_TEST(not session->transaction().isActive());
    }
    BOOST_TEST(not session->transaction().isActive());

    // test for explicit abort
    {
        Transaction tran1(session, Transaction::Read);
        BOOST_TEST(tran1.mode() == Transaction::Read);
        BOOST_TEST(session->transaction().isActive());
        tran1.rollback();
        BOOST_TEST(tran1.mode() == Transaction::NoOpen);
        BOOST_TEST(not session->transaction().isActive());
    }
    BOOST_TEST(not session->transaction().isActive());
}

BOOST_FIXTURE_TEST_CASE(test_transaction_errors1, EmptyDBFixture)
{
    {
        Transaction tran1(session, Transaction::Read, Transaction::NoClose);
        BOOST_TEST(tran1.mode() == Transaction::Read);
        BOOST_TEST(session->transaction().isActive());
    }
    BOOST_TEST(session->transaction().isActive());

    BOOST_REQUIRE_THROW(Transaction(session, Transaction::Read), Issues::TransactionActive);
}

BOOST_FIXTURE_TEST_CASE(test_transaction_errors2, EmptyDBFixture)
{
    {
        Transaction tran1(session, Transaction::Read, Transaction::NoClose);
        BOOST_TEST(tran1.mode() == Transaction::Read);
        BOOST_TEST(session->transaction().isActive());
    }
    BOOST_TEST(session->transaction().isActive());

    BOOST_REQUIRE_THROW(Transaction(session, Transaction::Read), Issues::TransactionActive);

    {
        Transaction tran1(session, Transaction::NoOpen);
        BOOST_TEST(tran1.mode() == Transaction::Read);
    }
    BOOST_TEST(not session->transaction().isActive());

    {
        Transaction tran1(session, Transaction::NoOpen);
        BOOST_TEST(tran1.mode() == Transaction::NoOpen);
        BOOST_REQUIRE_THROW(tran1.commit(), Issues::TransactionInactive);
    }
    BOOST_TEST(not session->transaction().isActive());

    {
        Transaction tran1(session, Transaction::NoOpen);
        BOOST_TEST(tran1.mode() == Transaction::NoOpen);
        BOOST_REQUIRE_THROW(tran1.rollback(), Issues::TransactionInactive);
    }
    BOOST_TEST(not session->transaction().isActive());
}
