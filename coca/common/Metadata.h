#ifndef COCA_METADATA_H
#define COCA_METADATA_H

//-----------------
// C/C++ Headers --
//-----------------
#include <map>
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/MetaTable.h"
#include "RelationalAccess/ISessionProxy.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_common

/**
 *  @ingroup coca_common
 *
 *  @brief Class providing client API for metadata operations.
 */

class Metadata {
public:

    /**
     *  @brief Standard constructor.
     *
     *  @param[in] connStr  CORAL connection string.
     *  @param[in] metaTable Reference to MetaTable instance, copy will be made.
     */
    Metadata(const std::string& connStr, const MetaTable& metaTable)
        : m_connStr(connStr), m_metaTable(metaTable) {}

    /**
     *  Check existence of a key in the metadata.
     *
     *  Returns true if key exists.
     */
    bool contains(const std::string& key);

    /**
     *  Return value corresponding to given key.
     *
     *  If key does not exist then defVal is returned.
     */
    std::string get(const std::string& key, const std::string& defVal=std::string());

    /**
     *  Return all records from META table as mapping from key to value.
     *
     *  If key does not exist then empty map is returned.
     */
    std::map<std::string, std::string> items();

    /**
     *  Store new record in META table.
     *
     *  If given key already exists then its value will be replaced.
     */
    void set(const std::string& key, const std::string& value);

    /**
     *  Remove record from META table.
     *
     *  If key does not exist nothing happens.
     */
    void remove(const std::string& key);

protected:

    std::shared_ptr<coral::ISessionProxy> _session(bool update=false);

private:

    std::string m_connStr;
    MetaTable m_metaTable;
};

} // namespace coca
} // namespace daq

#endif // COCA_METADATA_H
