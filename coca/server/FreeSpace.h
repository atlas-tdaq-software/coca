#ifndef COCA_FREESPACE_H
#define COCA_FREESPACE_H
//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/server/ServerIssues.h"
#include "coca/common/Size.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq {
namespace coca {
class DBServer;
class FileSystemABC;
}
}

namespace daq {
namespace coca {

/// @addtogroup coca_server

/**
 *  @ingroup coca_server
 *
 *  @brief Namespace for functions that manage free space on cache filesystem.
 */
class FreeSpace {
public:

    /**
     *  Make new instance.
     *
     *  @param[in] db              Database object.
     *  @param[in] freePercentGoal How many percent of disk space to try to keep free.
     *  @param[in] filesystem      Filesystem interface.
     */
    FreeSpace(DBServer& db, unsigned freePercentGoal, FileSystemABC& filesystem);

    /**
     *  @brief The method that makes sure that the requested space is available.
     *
     *  If there is not enough free space available in the directory then this
     *  method will obtain the list of files that can be deleted from the database.
     *  It will delete files until there is enough free space. If it cannot free
     *  enough space then false is returned. The directory argument to this method
     *  must be on the same filestystem as the cache directory.
     *
     *  @param[in] goal  Minimum free space requested.
     *  @param[in] dir   Location that needs that free space.
     *  @return  True if there is enough free space, false otherwise.
     *
     *  @throw Issues::CoralException Thrown in case of database error
     *  @throw FreeSpaceIssues::FileSystemError If directory operations fail.
     */
    bool makeAvailable(Size goal, const std::string& dir);

private:

    DBServer& m_db;
    unsigned m_freePercentGoal;
    FileSystemABC& m_fs;
};

} // namespace coca
} // namespace daq

#endif // COCA_FREESPACE_H
