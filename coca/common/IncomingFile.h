#ifndef COCA_INCOMINGFILE_H
#define COCA_INCOMINGFILE_H

//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class IncomingFile.
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <iosfwd>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Size.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace coca {


/// @addtogroup coca_common

/**
 *  @ingroup coca_common
 *  @brief Class representing new data file for CoCa.
 *
 *  This is a counterpart for CORBA's class NewFile (cocaIPC::NewFile).
 *  It contains all the same data but uses regular C++ strings instead
 *  of CORBA strings. The data about file include client host name, path
 *  name (on the client host), file size, and caching options.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 */

class IncomingFile  {
public:

    /**
    *   Constructor.
    *
    *   @param[in] host       Name of the client host.
    *   @param[in] clientDir  Directory name on the client host.
    *   @param[in] relPath    File name relative to the clientDir.
    *   @param[in] size       Size of the file in bytes.
    *   @param[in] prio       Priority value for caching.
    *   @param[in] dtcSec     Desired Time in Cache in seconds.
    *   @param[in] checksum   File checksum ("md5:hex-string" or similar) or
    *                         empty string.
    */
    IncomingFile (const std::string& host,
                  const std::string& clientDir,
                  const std::string& relPath,
                  Size size,
                  int prio,
                  unsigned dtcSec,
                  const std::string& checksum);

    // Destructor
    ~IncomingFile () throw ();

    /// Returns client host name.
    const std::string& host() const {return m_host;}

    /// Returns directory name on the client host.
    const std::string& clientDir() const {return m_clientDir;}

    /// Returns relative file name.
    const std::string& relPath () const {return m_relPath;}

    /// Returns path name on client host.
    std::string clientPath () const ;

    /// Returns size of the file in MBytes
    Size size () const {return m_size;}

    /// Returns priority value for caching
    int prio () const {return m_prio;}

    /// Returns Desired Time in Cache in seconds
    unsigned dtcSec () const {return m_dtcSec;}

    /// Returns file checksum, could be empty string.
    const std::string& checksum() const {return m_checksum;}

protected:

private:

    // Data members
    std::string m_host;         ///< Client host name
    std::string m_clientDir;    ///< Directory on client host
    std::string m_relPath;      ///< Relative path (w.r.t. m_clientDir)
    Size m_size;                ///< File size
    int m_prio;                 ///< Priority for cache
    unsigned m_dtcSec;          ///< Desired Time in Cache (in seconds)
    std::string m_checksum;     ///< File checksum

};

/// Stream insertion operator to print object contents
std::ostream&
operator<<(std::ostream& out, const IncomingFile& f);

} // namespace coca
} // namespace daq

#endif // COCA_INCOMINGFILE_H
