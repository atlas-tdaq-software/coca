//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Description:
//      Class Str2Id...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/common/Str2Id.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <ctype.h>
#include <CoralBase/Attribute.h>
#include <CoralBase/AttributeList.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/SchemaException.h>
#include <RelationalAccess/TableDescription.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Issues.h"
#include "coca/common/NextId.h"
#include "coca/common/Transaction.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

    // add one more attribute to the list and set its value in one call
    template <typename T>
    void
    addAttr(coral::AttributeList& data, const std::string& name, const T& value) {
        const size_t idx = data.size() ;
        data.extend<T>(name);
        data[idx].setValue(value);
    }

}

namespace daq {
namespace coca {

/*
 * Constructor
 */
Str2Id::Str2Id (const std::string& tablename,
                NextId& nextid,
                bool preFillCache)
    : m_tablename (tablename)
    , m_nextid (nextid)
    , m_preFillCache(preFillCache)
{
}

/*
 * Destructor
 */
Str2Id::~Str2Id()
    throw ()
{
}

unsigned
Str2Id::insert (const std::string &str, Transaction& tran)
{
    try {
        // check if there is a string already
        return getId (str, tran);
    } catch (const Issues::NoSuchRecord &ex) {
        ERS_DEBUG (0, ex);
    }
    // new entry
    std::string safe = safeStr (str);

    // try to insert new ID into database
    unsigned id = m_nextid.next ("STR2ID", 0, tran);
    try {

        // get table handle
        coral::ISchema& schema = tran.session().nominalSchema();
        coral::ITable& table = schema.tableHandle(m_tablename);

        coral::AttributeList ins;
        ::addAttr(ins, "STR", str);
        ::addAttr(ins, "ID", id);
        table.dataEditor().insertRow(ins);

    } catch (const coral::DuplicateEntryInUniqueKeyException &ex) {

        // such record already exists
        id = 0;

    }

    if (id == 0) {
        // retry get
        ERS_DEBUG (3, "string entered in the meanwhile");
        id = getId (str, tran);
    } else {
        // cache it
        insertCache (safe, id);
        ERS_DEBUG (3, safe << " -> " << id);
    }

    return id;
}


/*
  str -> id
*/

unsigned
Str2Id::getId (const std::string &str, Transaction& tran)
{
    if (str.empty()) {
        return 0;
    }
    std::string safe = safeStr (str);

    // fill cache first time around
    if (m_preFillCache) {
        initCache(tran);
    }

    // check cache first
    std::map<std::string, unsigned>::iterator it = m_cache_str2id.find (str);
    if (it != m_cache_str2id.end()) {
        ERS_DEBUG (3, safe << " -> " << it->second);
        return it->second;
    }

    // database
    unsigned id = getIdDB (safe, tran);
    if (id == 0) {
        // not found
        Issues::NoSuchRecord err (ERS_HERE, "STR = " + str);
        ERS_DEBUG (0, err);
        throw err;
    }

    insertCache (safe, id);
    ERS_DEBUG (3, safe << " -> " << id);
    return id;
}

/*
  id -> str
*/

std::string
Str2Id::getStr (unsigned id, Transaction& tran)
{
    if (id == 0) {
        ERS_DEBUG (3, id << " -> \"\"");
        return std::string();
    }

    // fill cache first time around
    if (m_preFillCache) {
        initCache(tran);
    }

    // check cache first
    std::map<unsigned, std::string>::iterator it = m_cache_id2str.find (id);
    if (it != m_cache_id2str.end()) {
        ERS_DEBUG (3, id << " -> " << it->second);
        return it->second;
    }

    // database
    std::string str = getStrDB (id, tran);
    if (str.empty()) {
        // not found
        std::ostringstream buf;
        buf << "ID = " << id;
        Issues::NoSuchRecord err (ERS_HERE, buf.str());
        ERS_DEBUG (0, err);
        throw err;
    }

    insertCache (str, id);
    ERS_DEBUG (3, id << " -> " << str);
    return str;
}

/*
  non ASCII characters
*/

std::string
Str2Id::safeStr (const std::string& unsafe)
{
    std::string safe = unsafe;
    for (std::string::iterator it = safe.begin(); it != safe.end(); ++ it) {
        if (! isascii (*it)) {
            *it = '_';
            //ERS_DEBUG (2, "non ASCII character " << int (*it) << " substituted in: " << unsafe);
        }
    }
    return safe;
}

/*
  cache
*/

void
Str2Id::insertCache (const std::string &str, unsigned id)
{
    ERS_DEBUG (3, "inserting: '" << str << "' -> " << id);
    ERS_ASSERT (! str.empty());
    ERS_ASSERT (id > 0);
    ERS_ASSERT (m_cache_str2id[str] == 0);
    m_cache_str2id[str] = id;
    ERS_ASSERT (m_cache_id2str[id].empty());
    m_cache_id2str[id] = str;
}

/*
  database
*/

unsigned
Str2Id::getIdDB (const std::string &str, Transaction& tran)
{
    unsigned id = 0;

    // attribute list for selection
    coral::AttributeList sel;
    ::addAttr(sel, "STR", str);

    // new query
    coral::ISchema& schema = tran.session().nominalSchema();
    coral::ITable& table = schema.tableHandle(m_tablename);
    std::unique_ptr<coral::IQuery> query (table.newQuery());

    query->addToOutputList ("ID");
    query->defineOutputType("ID", coral::AttributeSpecification::typeNameForType<uint32_t>());
    std::string where = "STR = :STR";
    query->setCondition (where, sel);

    // execute
    coral::ICursor &cursor = query->execute();
    if (cursor.next()) {
        id = cursor.currentRow()[0].data<uint32_t>();
        ERS_DEBUG (3, "'" << str << "' -> " << id);
        ERS_ASSERT (id > 0);
        ERS_ASSERT (! cursor.next());
    } else {
        ERS_DEBUG (3, "'" << str << "' not found");
    }

    return id;
}

std::string
Str2Id::getStrDB (unsigned id, Transaction& tran)
{
    // new query
    coral::ISchema& schema = tran.session().nominalSchema();
    coral::ITable& table = schema.tableHandle(m_tablename);
    std::unique_ptr<coral::IQuery> query (table.newQuery());

    // What
    query->addToOutputList ("STR");

    // attribute list for selection
    coral::AttributeList sel;
    ::addAttr(sel, "ID", id);
    query->setCondition ("ID = :ID", sel);

    // execute
    coral::ICursor &cursor = query->execute();
    std::string str;
    if (cursor.next()) {
        str = cursor.currentRow()[0].data<std::string>();
        ERS_DEBUG (3, "'" << str << "' -> " << id);
        ERS_ASSERT (! str.empty());
        ERS_ASSERT (! cursor.next());
    } else {
        ERS_DEBUG (3, "id " << id << " not found");
    }

    return str;
}


// fill cache with data from database
void
Str2Id::initCache(Transaction& tr)
{
    ERS_DEBUG (0, "going to fill cache ...");

    // new query
    coral::ISchema& schema = tr.session().nominalSchema();
    coral::ITable& table = schema.tableHandle(m_tablename);
    std::unique_ptr<coral::IQuery> query (table.newQuery());

    // What
    query->addToOutputList("ID");
    query->defineOutputType("ID", coral::AttributeSpecification::typeNameForType<uint32_t>());
    query->addToOutputList("STR");

    // Where
    coral::AttributeList condData;
    ::addAttr( condData, "ZERO", uint32_t(0));
    query->setCondition ("ID != :ZERO", condData);

    // adjust cache params for performance
    query->setRowCacheSize(100000);
    query->setMemoryCacheSize(10);

    // execute
    coral::ICursor &cursor = query->execute();
    while (cursor.next()) {
        const coral::AttributeList &row = cursor.currentRow();
        unsigned id = row[0].data<uint32_t>();
        const std::string &str = row[1].data<std::string>();
        insertCache (str, id);
    }
    ERS_DEBUG (0, "cache filled with " << m_cache_str2id.size() << " entries");

    m_preFillCache = false;
}

void
Str2Id::createTable(int schemaVersion, Transaction& tran)
{
    auto& schema = tran.session().nominalSchema();

    coral::TableDescription td("Str2Id::createTable");
    td.setName(m_tablename);
    td.insertColumn("STR", "string");
    td.insertColumn("ID", "unsigned");
    td.setPrimaryKey("STR");
    td.setNotNullConstraint("ID");
    // td.setUniqueConstraint("ID", "COCA_STR2ID_ID_UNIQ");
    td.createIndex("COCA_STR2ID_ID_UNIQ", "ID", true);
    // td.createIndex("COCA_STR2ID_PK", "STR", true);
    schema.createTable(td);
}

} // namespace coca
} // namespace daq
