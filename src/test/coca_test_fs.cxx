//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Test suite case for the coca::FileSystem.
//
//------------------------------------------------------------------------

//---------------
// C++ Headers --
//---------------

#define BOOST_TEST_MODULE coca_test_fs
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/FileSystem.h"
#include "coca/common/System.h"
#include "coca_test_mockfs.h"

using namespace daq::coca;
using namespace daq::coca::test;
namespace fs = boost::filesystem;

/**
 * Simple test suite for module coca_test_fs.
 * See http://www.boost.org/doc/libs/1_36_0/libs/test/doc/html/index.html
 */

// ==============================================================

BOOST_AUTO_TEST_CASE( test_join )
{
    BOOST_CHECK_EQUAL(FileSystem::join("a", "b"), "a/b");
    BOOST_CHECK_EQUAL(FileSystem::join("a", "/b"), "a/b");
    BOOST_CHECK_EQUAL(FileSystem::join("/a", "b"), "/a/b");
    BOOST_CHECK_EQUAL(FileSystem::join("/a", "/b"), "/a/b");
    BOOST_CHECK_EQUAL(FileSystem::join("/a", "////b"), "/a/b");
    BOOST_CHECK_EQUAL(FileSystem::join("a/", "/b/"), "a/b");
    BOOST_CHECK_EQUAL(FileSystem::join("a///", "/b///"), "a/b");
    BOOST_CHECK_EQUAL(FileSystem::join("/a/b////c/", "/d///e///f"), "/a/b/c/d/e/f");
}

// ==============================================================

BOOST_AUTO_TEST_CASE( test_split )
{
    std::pair<std::string, std::string> res;

    // check with absolute path and empty basedir
    res = FileSystem::split("/a/b/c/d/e", "");
    BOOST_CHECK_EQUAL(res.first, "/a/b/c/d");
    BOOST_CHECK_EQUAL(res.second, "e");

    // check with relative path and empty basedir
    res = FileSystem::split("d/e", "", "/a/b/c/");
    BOOST_CHECK_EQUAL(res.first, "/a/b/c/d");
    BOOST_CHECK_EQUAL(res.second, "e");

    // check with absolute path and basedir
    res = FileSystem::split("/a/b/c/d/e", "/a/b/c/");
    BOOST_CHECK_EQUAL(res.first, "/a/b/c");
    BOOST_CHECK_EQUAL(res.second, "d/e");

    // check with relative path and basedir
    res = FileSystem::split("e", "/a/b/c", "/a/b/c/d");
    BOOST_CHECK_EQUAL(res.first, "/a/b/c");
    BOOST_CHECK_EQUAL(res.second, "d/e");

    // This should throw because basedir is different from path
    BOOST_CHECK_THROW(res = FileSystem::split("/a/b/c/d/e", "/a/b/C"), FileSystem::BasedirMismatch);

    // This should throw because basedir is longer than path
    BOOST_CHECK_THROW(res = FileSystem::split("/a/b", "/a/b/c"), FileSystem::BasedirMismatch);

}

BOOST_AUTO_TEST_CASE(test_StdFileSystem_errors)
{
    // some tests may need initialized System instance
    System::initialize();
    StdFileSystem fs;
    std::string missing_file("/not-a-file");
    std::string missing_file2("/not-a-file-too");

    BOOST_TEST(fs.status(missing_file).type() == fs::file_not_found);
    BOOST_TEST(not fs.is_directory(missing_file));
    BOOST_TEST(not fs.is_regular_file(missing_file));
    BOOST_TEST(not fs.remove(missing_file));
    BOOST_CHECK_THROW(fs.remove("/tmp"), FileSystem::Exception);
    BOOST_CHECK_THROW(fs.rename(missing_file, missing_file2), FileSystem::Exception);
    BOOST_CHECK_THROW(fs.file_size(missing_file), FileSystem::Exception);
    // disable this test, looks like in some cases it may actually create a folder
    // BOOST_CHECK_THROW(fs.create_dirs(missing_file), FileSystem::Exception);
}

// Test for a mock class to check that it's functioning
BOOST_AUTO_TEST_CASE(test_MockFileSystem)
{
    MockFileSystem fs(Size::MB(1));

    std::string tmp("/tmp");
    std::string tmp_file("/tmp/file.data");

    fs.add_dir(tmp);
    fs.add_file(tmp_file, Size(1024), "md5:checksum");

    auto space = fs.space(tmp);
    BOOST_TEST(space.capacity == 1024*1024);
    BOOST_TEST(space.free == 1019*1024);
    BOOST_TEST(space.available == 1019*1024);

    BOOST_TEST(fs.status(tmp).type() == fs::directory_file);
    BOOST_TEST(fs.status(tmp_file).type() == fs::regular_file);
    BOOST_TEST(fs.is_directory(tmp));
    BOOST_TEST(not fs.is_regular_file(tmp));
    BOOST_TEST(fs.is_regular_file(tmp_file));
    BOOST_TEST(not fs.is_directory(tmp_file));
    BOOST_TEST(fs.file_size(tmp_file) == Size(1024));
    BOOST_TEST(fs.checksum(tmp_file) == "md5:checksum");

    std::string missing_file("/not-a-file");
    std::string missing_file2("/not-a-file-too");

    BOOST_TEST(fs.status(missing_file).type() == fs::file_not_found);
    BOOST_TEST(not fs.is_directory(missing_file));
    BOOST_TEST(not fs.is_regular_file(missing_file));
    fs.rename(tmp_file, "/tmp/file2.dat");
    BOOST_TEST(not fs.remove(missing_file));
    BOOST_TEST(fs.remove("/tmp/file2.dat"));
    BOOST_CHECK_THROW(fs.remove("/tmp"), FileSystem::Exception);
    BOOST_CHECK_THROW(fs.rename(missing_file, missing_file2), FileSystem::Exception);
    BOOST_CHECK_THROW(fs.file_size(missing_file), FileSystem::Exception);
    BOOST_CHECK_THROW(fs.file_size(tmp), FileSystem::Exception);
}
