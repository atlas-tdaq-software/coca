#ifndef COCA_DB_BASE
#define COCA_DB_BASE
//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <string>
#include <vector>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ISessionProxy.h>
#include <RelationalAccess/ITable.h>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Metadata.h"
#include "coca/common/MetaTable.h"
#include "coca/common/NextId.h"
#include "coca/common/Str2Id.h"
#include "coca/common/Transaction.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq {
namespace coca {
    class Transaction;
}
}

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_common

/**
 *  @ingroup coca_common
 *  @brief Class implementing common operations on CoCa database.
 *
 *  Despite its name this class is not supposed to be a base class
 *  for anything, as it does not define any public interface.
 *  Its purpose is to keep common parts of the implementation so
 *  that other database classes can be implemented using the code
 *  from this class. It is OK to use this class as private base
 *  class to express "implemented in terms of" relation, it can
 *  also be used as a member of other classes.
 */

class DBBase {
public:

    /**
     *  @brief Constructor takes CORAL connection string.
     *
     *  Instantiates read-only instance. Database schema has to exist.
     *
     *  @param[in] connStr  CORAL connection string.
     */
    explicit DBBase (const std::string& connStr);

    /**
     *  @brief Constructor takes CORAL connection string and boolean update flag.
     *
     *  Database schema has to exist.
     *
     *  @param[in] connStr  CORAL connection string.
     *  @param[in] update   If true then create modifiable instance.
     */
    DBBase (const std::string& connStr, bool update);

    // noncopyable
    DBBase(const DBBase&) = delete;
    DBBase& operator=(const DBBase&) = delete;

    // Destructor
    ~DBBase () throw ();

    /// Get database technology as obtained from a connection string
    const std::string& technology() const { return m_technology; }

    /// Get database schema version number
    int schemaVersion() const { return m_schemaVersion; }

    /// Get the name of the archive database table
    const std::string& archiveTableName() const { return m_arName; }

    /// Get the name of the cache database table
    const std::string& cacheTableName() const { return m_caName; }

    /**
     * @brief Get the handle for archive database table
     *
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    coral::ITable& archiveTable(Transaction& tran) const;

    /**
     *  @brief Get the handle for cache database table
     *
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    coral::ITable& cacheTable(Transaction& tran) const;

    /// Access STR2ID mapping object
    Str2Id& s2i() { return m_s2i; }

    /**
     *  @brief Generate next ID for given sequence name and index (see NextId)
     *
     * @param[in]  seq   Sequence name, non-empty string.
     * @param[in]  seqIdx  Sequence index, a number.
     * @param[in]  tran  Transaction context.
     *
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    unsigned nextId (const std::string& seq, unsigned seqIdx, Transaction& tran) {
        return m_nextid.next(seq, seqIdx, tran);
    }

    /**
     *  @brief Find a string for an ID (wraps Str2Id::getStr).
     *
     *  @param[in] id  ID for which we need to find a string.
     *  @param[in] tran  Transaction context (see Transaction).
     *  @return  String corresponding to given ID, empty string is returned for zero ID.
     *
     *  @throw Issues::NoSuchRecord Thrown if there is no such record exist.
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    std::string getStr (unsigned id, Transaction& tran);

    /**
     *  @brief Find an ID for a string (wraps Str2Id::getId).
     *
     *  @param[in] str  String for which we need to find ID.
     *  @param[in] tran  Transaction context (see Transaction).
     *  @return  ID corresponding to given string, zero returned for empty string.
     *
     *  @throw Issues::NoSuchRecord Thrown if there is no such record exist.
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    unsigned getId (const std::string &str, Transaction& tran);

    /**
     *  @brief Create new ID for a string or return existing one (wraps Str2Id::insert).
     *
     *  If string contains control characters they will be replaces with
     *  underscores first (there is a potential for name clash). Empty
     *  strings are not stored in database, instead we return zero ID.
     *
     *  @param[in] str  String for which we need to find or create ID.
     *  @param[in] tran  Transaction context (see Transaction).
     *  @return  ID corresponding to given string, zero returned for empty string.
     *
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    unsigned insStr (const std::string &str, Transaction& tran);

    /**
     *  @brief Returns CORAL session object.
     *
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    std::shared_ptr<coral::ISessionProxy> session();

    /**
     *  Returns default CORAL connection string for CoCa database.
     */
    static std::string defConnStr();

    /**
     * @brief Create database schema.
     *
     * Creates database schema from scratch, COCA tables should not exist
     * prior to this call.
     *
     * Note that CORAL does not support specification of default values for
     * columns, and we use defaults. This method cannot create an exact schema
     * for some versions (2 and 3, maybe later). For production service in
     * Oracle one should use migration scripts to upgrade schema (in
     * databases/db_migrations/oracle folder). For unit tests that run with
     * sqlite we have a bunch of fixup scripts to adjust schema after this
     * method is executed, they are in databases/db_migrations/sqlite).
     *
     *  @param[in] connStr  CORAL connection string.
     *  @param[in] version  Schema version number, -1 means use latest known
     *  version.
     */
    static void createSchema(const std::string& connStr, int version = -1);

    /**
     * @brief Return object providing metadata access.
     */
    Metadata metadata();

private:

    /**
     *  @brief Constructor takes CORAL connection string and boolean update flag.
     *
     *  Database schema does not need to exist if
     *
     *  @param[in] connStr  CORAL connection string.
     *  @param[in] update   If true then create modifiable instance.
     *  @param[in] check_version If true then check schema version.
     */
    DBBase(const std::string& connStr, bool update, bool check_version);

    /**
     * @brief Create database schema.
     *
     * Creates database schema from scratch, COCA tables should not exist
     * prior to this call.
     *
     *  @param[in] version  Schema version number, -1 means use latest known version.
     */
    void _createSchema(int version = -1);


    // data members
    const std::string m_connStr;  ///< CORAL connection string
    const std::string m_technology; ///< technology part of the CORAL connection string
    MetaTable m_metaTable;        ///< Metadata access
    NextId m_nextid;              ///< ID generator object
    Str2Id m_s2i;                 ///< String to ID mapping
    const std::string m_arName;   ///< Archive table name
    const std::string m_caName;   ///< Cache table name
    const bool m_update;          ///< If true then create update sessions
    int m_schemaVersion = -1;     ///< Actual database schema version

    static int s_latestVersion;
    static std::vector<int> s_supportedVersions;
};

} // namespace coca
} // namespace daq

#endif // COCA_DB_BASE
