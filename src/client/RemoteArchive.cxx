//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/client/RemoteArchive.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace coca {

//----------------
// Constructors --
//----------------
RemoteArchive::RemoteArchive (const std::string& dataset, 
                              const std::string& name,
                              const FileLocations& locations,
                              const std::string& archivePath)
    : m_dataset(dataset)
    , m_name(name)
    , m_locations(locations)
    , m_archivePath(archivePath)
{
}

//--------------
// Destructor --
//--------------
RemoteArchive::~RemoteArchive () throw ()
{
}

/// Standard stream insertion operator
std::ostream&
operator<<(std::ostream& str, const RemoteArchive& archive)
{
    str << "Dataset: " << archive.dataset()
        << "\nArchive: " << archive.name();
    const RemoteArchive::FileLocations& locations = archive.locations();
    if (! locations.empty()) {
        ERS_ASSERT (locations.size() == 1);
        str << '\n' << locations.front();
    }
    if (! archive.archivePath().empty()) {
        str << "\nArchive path: " << archive.archivePath();
    }
    return str;
}

} // namespace coca
} // namespace daq
