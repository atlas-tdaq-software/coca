#include <string>

#define BOOST_TEST_MODULE coca_test_dbbase
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include "coca/common/DBBase.h"
#include "coca/common/Issues.h"
#include "coca/common/Transaction.h"
#include "coca_test_util.h"

using namespace daq::coca;
using namespace daq::coca::test;
namespace tt = boost::test_tools;
namespace bdata = boost::unit_test::data;

struct Fixture : DBFixture {

    void test_nextid(DBBase& db) {
        struct {
            const char* seq;
            unsigned seqId;
            unsigned id;
        } tests_nextid[] = {
            {"A", 0, 1},
            {"A", 0, 2},
            {"A", 0, 3},
            {"A", 1, 1},
            {"A", 1, 2},
            {"B", 0, 1},
            {"C", 0, 1},
            {"A", 0, 4},
        };

        for (auto& test: tests_nextid) {
            Transaction tr(db.session(), Transaction::Update);
            unsigned id = db.nextId(test.seq, test.seqId, tr);
            BOOST_TEST(id == test.id);
        }
    }

    void test_str2id(DBBase& db) {

        struct {
            const char* str;
            unsigned id;
        } tests[] = {
            {"", 0},
            {"A", 1},
            {"B", 2},
            {"C", 3},
        };

        for (auto& test: tests) {
            Transaction tr(db.session(), Transaction::Update);
            unsigned id = db.insStr(test.str, tr);
            BOOST_TEST(id == test.id);
        }


        for (auto& test: tests) {
            Transaction tr(db.session(), Transaction::Read);
            unsigned id = db.getId(test.str, tr);
            BOOST_TEST(id == test.id);
        }

        for (auto& test: tests) {
            Transaction tr(db.session(), Transaction::Read);
            auto str = db.getStr(test.id, tr);
            BOOST_TEST(str == test.str);
        }

    }
};


BOOST_DATA_TEST_CASE_F(Fixture, test_valid_version, bdata::make({0, 1, 2, 3, 4}), version)
{
    std::string const uri = makeSchema(version);

    bool update = true;
    DBBase db(uri, update);

    BOOST_TEST(db.technology() == "sqlite");

    BOOST_TEST(db.schemaVersion() == version);

    test_nextid(db);
    test_str2id(db);
}

BOOST_FIXTURE_TEST_CASE(test_latest_version, Fixture)
{
    std::string const uri = makeSchema();

    bool update = true;
    DBBase db(uri, update);

    BOOST_TEST(db.technology() == "sqlite");

    BOOST_TEST(db.schemaVersion() == 4);
}

BOOST_FIXTURE_TEST_CASE(test_wrong_version, DBFixture)
{
    // make schema version which will never exist
    std::string const uri = makeSchema(999);

    BOOST_REQUIRE_THROW(DBBase(uri, true), Issues::UnsupportedSchemaVersionError);
}
