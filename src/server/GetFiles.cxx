//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Description:
//      Class GetFiles...
//
// Author List:
//      Andy Salnikov
//
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/server/GetFiles.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/filesystem.hpp>
#include <boost/date_time.hpp>
#include <boost/thread.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/FileCheck.h"
#include "coca/common/FileSystem.h"
#include "coca/common/Issues.h"
#include "coca/common/TransientFile.h"
#include "coca/server/DBServer.h"
#include "coca/server/FreeSpace.h"
#include "coca/server/Process.h"
#include "coca/server/SSHProcess.h"
#include "ers/ers.h"
#include "owl/semaphore.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace fs = boost::filesystem;
namespace ptime = boost::posix_time;

namespace {

    ERS_DECLARE_ISSUE(error, UnhandledException,
            "GetFiles: Unhandled '" << name << "' exception was thrown",
            ((const char *)name) )

    ERS_DECLARE_ISSUE(message, Interrupted,
     "thread " << thread_name  << " interrupted while " << action,
     ((std::string) thread_name) ((std::string) action))

}


namespace daq {
namespace coca {



GetFiles::GetFiles(DBServer& db, const NotifyFlag& notifyUp, const NotifyFlag& notifyDown,
                const FreeSpace& freeSpace, OWLSemaphore& semaphore, int retries,
                FileSystemABC& filesystem)
    : m_db(db)
    , m_notifyUp(notifyUp)
    , m_notifyDown(notifyDown)
    , m_freeSpace(freeSpace)
    , m_semaphore(semaphore)
    , m_retries(retries)
    , m_filesystem(filesystem)
    , m_attemptCounts()
{
}

bool
GetFiles::FreeSpace4 (const TransientFile &file)
{
    return m_freeSpace.makeAvailable(2 * file.size(), file.serverDir());
}

void
GetFiles::GetRemoteFile (const TransientFile &file)
{
    std::string machine = file.host();
    std::string rem_path =  file.clientPath();
    std::string loc_path = file.serverPath();

    std::vector<std::string> cmdline;
    cmdline.push_back ("/usr/bin/rsync");
    cmdline.push_back ("-aqS");
    cmdline.push_back ("-e");
    // options for ssh to enable non-interactive logon
    const char * ssh4rsync = "/usr/bin/ssh -o PasswordAuthentication=no"
        " -o StrictHostKeyChecking=no -o ForwardX11=no";
    cmdline.push_back (ssh4rsync);
    cmdline.push_back (machine + ":" + rem_path);
    cmdline.push_back (loc_path);
    Process rsync (cmdline, "rsync");
    while (rsync.isRunning()) {

        try {
            boost::this_thread::sleep(boost::posix_time::seconds(1));
        } catch(const boost::thread_interrupted& ex) {
            // terminate process, complain, and rethrow
            rsync.terminate ();
            ers::warning(::message::Interrupted (ERS_HERE, "GetFiles",
                    "copying file " + rem_path + " from host " + machine));
            throw;
        }
    }

    // check file size
    try {
        if (file.checksum().empty()) {
            // If checksum is empty then this is likely a file which was
            // created with old schema which only had MB precision
            FileCheck::checkMB(loc_path, file.size(), m_filesystem);
        } else {
            FileCheck::check(loc_path, file.size(), file.checksum(), m_filesystem);
        }
    } catch (const ers::Issue &ex) {
        // file size mismatch, to avoid further mess remove local copy
        ERS_LOG("File size or checksum mismatch, will remove local copy: " << loc_path);
        m_filesystem.remove(loc_path, true);
        throw;
    }

    // all check passed
    ERS_LOG ("file copied from client: " << loc_path);
    {
        m_db.addToCache (file.dataset(), file.relPath(), file.size());
        m_notifyDown.notify();
    }
    try {
        SSHProcess::removeRemote (machine, rem_path);
        ERS_LOG ("file removed on machine " << machine << ": " << rem_path);
        m_db.removeFromClient (file.dataset(), file.relPath());
    }
    catch (const ProcessIssues::ChildError &ex) {
        // we don't care too much if the file was not removed ...
        ers::warning(ex);
    }
}

void
GetFiles::operator()()
try {

    // first time around do not wait
    ptime::time_duration wait_time = ptime::seconds(0);
    while (not boost::this_thread::interruption_requested()) {

        if (wait_time.total_seconds()) {
            m_notifyUp.wait(wait_time);
        }
        wait_time = ptime::minutes(10);

        try {

            ERS_LOG("GetFiles: new iteration");

            const std::vector<TransientFile>& files = m_db.filesToRetrieve();
            ERS_LOG("GetFiles: file list size: " << files.size());

            for (std::vector<TransientFile>::const_iterator it = files.begin(); it != files.end(); ++ it) {

                // give it a chance to break out of the loop
                boost::this_thread::interruption_point();

                ERS_LOG ("GetFiles: next file - " << *it);

                // create directory for files, if can't then it must be an error
                // in configuration, so we terminate thread and hope main thread
                // will notice it.
                std::string loc_path = it->serverPath();
                m_filesystem.create_dirs(fs::path(loc_path).parent_path());

                // Make sure we have free space
                if (! FreeSpace4 (*it)) {
                    // emergency! all archived files were removed,
                    // still need some space
                    m_db.closeAllArchives();
                    // all archives were closed; let's wakeup MkZip and
                    // wait for some more files to be archived
                    ers::warning(GetFilesIssues::SpaceExhausted (ERS_HERE));
                    m_notifyDown.notify();
                    wait_time = ptime::seconds(10);
                    break;
                }

                try {
                    // copy remote file to cache
                    GetRemoteFile (*it);

                } catch (const ers::Issue &ex) {

                    ers::warning(GetFilesIssues::CopyFailed(ERS_HERE, it->relPath(), ex));

                    // m_retries is the number of _additional_ re-tries beyond first attempt,
                    // it can be set to negative number for infinite retry
                    if (m_retries >= 0) {
                        // Count failures, ignore file if too many failures.
                        DatasetAndRelPath key(it->dataset(), it->relPath());
                        if (++ m_attemptCounts[key] > m_retries) {
                            ers::error(GetFilesIssues::CopyRetryExceeded(ERS_HERE, it->relPath(), m_retries, ex));

                            // mark it as missing from a client, should stop retries
                            m_db.removeFromClient(it->dataset(), it->relPath());

                            // also clean up the count map to avoid wasting memory
                            m_attemptCounts.erase(key);
                        } else {
                            // don't wait whole cycle, retry sooner
                             wait_time = ptime::minutes(1);
                        }
                    }
                }

            }

        } catch (const Issues::CoralException& ex) {

            // This can potentially be any CORAL problem, but most likely this is
            // due to connectivity issues, retry again later
            ers::error(Issues::WaitForDatabase(ERS_HERE, ex));
            wait_time = ptime::minutes(1);

        }

    }

    m_semaphore.post();

} catch (const boost::thread_interrupted& ex) {
    // we just stop happily
    m_semaphore.post();
} catch( ers::Issue & ex ) {
    ers::fatal(::error::UnhandledException(ERS_HERE, ex.get_class_name(), ex));
    m_semaphore.post();
} catch (const std::exception &ex) {
    ers::fatal(::error::UnhandledException(ERS_HERE, "standard", ex));
    m_semaphore.post();
} catch (...) {
    ers::fatal(::error::UnhandledException(ERS_HERE, "unknown"));
    m_semaphore.post();
}

} // namespace coca
} // namespace daq
