#ifndef COCA_SERVER_H
#define COCA_SERVER_H
//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <atomic>
#include <memory>

//----------------------
// Base Class Headers --
//----------------------
#include "ipc/object.h"

class OWLSemaphore;

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "cocaIPC/cocaIPC.hh"
#include "coca/server/NotifyFlag.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq {
namespace coca {
    class Config;
    class DBServer;
}
}

namespace daq {
namespace coca {

/// @addtogroup coca_server

/** 
 * @ingroup coca_server
 * 
 * @brief Class implementing CoCa CORBA interface.
 * 
 * 
 */
class Server : public IPCNamedObject<POA_cocaIPC::Server> {
public:

    /**
     *  @brief Factory method which returns instance of the server class.
     *  
     *  This method takes the same set of argument as the constructor
     *  (which is not accessible directly).
     */
    static std::shared_ptr<Server> makeServer(IPCPartition& partition,
                        const std::string& name,
                        DBServer& db,
                        const Config& config,
                        const NotifyFlag& notifyDown,
                        OWLSemaphore& semaphore);

    /**
     *  @brief register new file with CoCa.
     *
     *  This method takes the list of the NewFile objects and CoCa dataset name.
     *  It returns registration status, ErrNoError means registration was successful,
     *  any other value means failure.
     *
     *  @param[in]  file  File to be registered
     *  @param[in]  dataset Name of the Coca dataset.
     *  @param[out] archive Archive name, undefined if status is non-zero.
     *  @return     Status code, ErrNoError for success.
     */
    cocaIPC::Server::RegStatus registerFile(const cocaIPC::NewFile &file,
        const char *dataset, CORBA::String_out archive);

    /**
     *  @brief Method called by CORBA to get server configuration data.
     * 
     *  @return Server configuration string supplied in the constructor.
     */
    char* configuration();

    /**
     *  @brief Disable new registrations.
     *
     *  After call to this method any call to registerFile will return ErrRegistrationDisabled.
     */
    void disable();

    /// Method called by IPC library to stop server.
    void shutdown();
    
protected:

    /**
     *  @brief Constructor creates IPC server handling CORBA requests.
     *
     *  @param[in] partition  IPC partition object.
     *  @param[in] name       Server name.
     *  @param[in] db         Database object.
     *  @param[in] config     Server configuration data.
     *  @param[in] notifyDown Communication channel to downstream thread.
     *  @param[in] semaphore  Semaphore to be raised when server is finishing.
     *
     *  @throw  daq::ipc::InvalidPartition
     *  @throw  daq::ipc::InvalidObjectName
     */
    Server (IPCPartition& partition,
            const std::string& name,
            DBServer& db,
            const Config& config,
            const NotifyFlag& notifyDown,
            OWLSemaphore& semaphore);

    /// Destuctor is protected, it must not be called directly. 
    ~Server() throw ();

    /// Returns true if new registrations have been disabled
    bool disabled() const;

private:

    // Data members
    DBServer& m_db;             ///< Database object.
    const Config& m_config;     ///< Server configuration data.
    NotifyFlag m_notifyDown;    ///< Communication channel to downstream thread
    OWLSemaphore& m_semaphore;  ///< global semaphore we need to raise when terminating
    std::string m_configStr;    ///< Server configuration as string.

    std::atomic<bool> m_disabled;  ///< Stop accepting registrations if true
};

} // namespace coca
} // namespace daq

#endif // COCA_SERVER_H
