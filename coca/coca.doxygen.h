/**

\page coca Design and Implementation of CoCa server and CoCa database

\tableofcontents


\section overview Overview

CoCa (name is meant to be an acronym for Collection and Caching Service) is a
collection of software components which perform several tasks all related
to archiving of the data produced by ATLAS TDAQ components:
- receiving the files to be archived
- optionally bundling these files in larger archives
- sending files or bundles to long-term storage
- keeping tracks of all archived files
- providing interface for finding and accessing archived files

This page provides information on design and implementation of CoCa components.
More user-oriented information is available on
<A HREF="https://twiki.cern.ch/twiki/bin/view/Atlas/CoCaDesignAndImplementation">CoCa TWiki page</A>.


\section architecture Overall Architecture

Here is the diagram representing the whole architecture of the CoCa:

\dot
digraph coca {
  rankdir=LR;
  node [fontname=Helvetica];
  edge [fontname=Helvetica];
  subgraph cluster_writer {
    label="Writer node at P1";
    graph[style="filled,rounded", fillcolor="#EEEEEE"];
    subgraph {
      rank = same;
      writer [label="Client (writer)", shape=box, style=filled, fillcolor="#3399FF"];
      writer_disk [label="Writer disk", shape=oval, style=filled, fillcolor="#33CC99"];
    }
  }
  subgraph cluster_server {
    label="CoCa server node at P1";
    graph[style="filled,rounded", fillcolor="#EEEEEE"];
    subgraph {
      rank = same;
      server [label="CoCa server", shape=box, style=filled, fillcolor="#CC9966"];
      cache [label="Cache", shape=oval, style=filled, fillcolor="#33CC99"];
      stage [label="Stage-in", shape=oval, style=filled, fillcolor="#33CC99"];
    }
    xrootd [label="xrootd", shape=box, style=filled, fillcolor="#CC9966"];
  }
  db [label="Database", shape=octagon, style=filled, fillcolor="#FFCC66"];
  EOS [label="LTS (EOS)", shape=oval, style=filled, fillcolor="#339966"];
  reader [label="Client (reader)", shape=box, style=filled, fillcolor="#3399FF"];

  writer -> server [color="#3333FF"];
  writer -> writer_disk [style=bold, color="#993366"];
  writer_disk -> cache [label="via ssh", style=bold, color="#993366"];
  server -> db [dir="both", color="#3333FF"];
  server -> cache [dir="both", color="#3333FF"];
  cache -> stage [style=bold, color="#993366"];
  cache -> xrootd [style=bold, color="#993366"];
  server -> stage [dir="both", color="#3333FF"];
  stage -> EOS [label="via CDR", style=bold, color="#993366"];
  EOS -> reader [style=bold, color="#993366"];
  db -> reader [color="#3333FF"];
  xrootd -> reader [label="P1 only", style="dashed,bold", color="#993366"];

  xrootd -> EOS [style=invis];
  xrootd -> db [style=invis];
}
\enddot


Brief description of individual components:

<DL>
<DT>Client (writer)</DT>
<DD>
This is the client application which creates new data files (though data can be produced by
something else) and registers files with a CoCa server.
</DD>

<DT>Writer disk</DT>
<DD>
Data are not sent by writer to CoCa server directly, instead files are stored on client
machine and CoCa server retrieves data after registration using ssh connection to writer host.
</DD>

<DT>CoCa server</DT>
<DD>
Special server application which is responsible for managing actual data movement and
database updates. Typically there is only one instance running at a time (or one instance
per active TDAQ release) in the initial partition.
</DD>

<DT>Cache</DT>
<DD>
Special disk area local to the CoCa server host. Server moves newly registered files to
cache. Files are kept in cache as long as there is available free disk space to accommodate
new files of to create new archives.
</DD>

<DT>xrootd</DT>
<DD>
Server that serves files from cache, it is accessible from Point1 only.
</DD>

<DT>Stage-in area</DT>
<DD>
Disk area used to create archives to be copied to EOS via separate CDR system. Usually stage-in
area shares the same disk with cache.
</DD>

<DT>LTS</DT>
<DD>
Long-Term Storage for archives produced by CoCa, currently this is EOS.
</DD>

<DT>Database</DT>
<DD>
This is the heart of the whole system. It keeps track of all registered files, created archives,
location of those, and current state of the whole system. It is updated by CoCa server and is
queried by clients.
</DD>

<DT>Client (reader)</DT>
<DD>
Client applications that need to read archived data, they usually query CoCa database to find
location of the archived data either in EOS or at Point1 and open the data file directly or
copy data locally. Clients can run anywhere where there is an access to database and EOS (or
xrootd server).
</DD>
</DL>


\section concepts Main Concepts

There are three main types of items that CoCa deals with - datasets, files and archives.

Datasets are a way of grouping of related files, datasets are identified by their name.
All datasets are defined in CoCa server configuration (in OKS), dataset properties determine
how files belonging to the dataset are handled. This include various life-time parameters,
maximum possible size of files and archives in a dataset, etc.

File is the main and smallest unit of work in CoCa. Every file belongs to exactly one dataset,
files are identified by the name of the file and the name of the dataset, the combination of
dataset name and file name must be unique. Clients (writers) specify dataset name for new file
when they register file. File names can include slashes (directories).

Archives represent collections of files stored in long-term storage (EOS presently or CASTOR
in earlier days). If there is more than one file in archive then these files are bundled
together inside ZIP archive with a unique name (something like <tt><dataset>_<unique_number>.zip</tt>).
If there is only one file in the archive then this file is stored as-is but its name can be
transformed to make it unique for storing in LTS. Archives, just like files, are associated
with the datasets, multi-file bundle can only contain files from the same dataset.

\subsection bundling Bundling policies

Original CoCa implementation used CASTOR as long-term storage. CASTOR was mostly
tape-oriented storage and did not handle large number of small files well. As a result
CoCa was designed to always try to bundle small files into archives. Bundling reduced
number of files in LTS but it also caused delays in files being available outside P1
(until CoCa collects several files to form a bundle). After EOS had replaced CASTOR some
limitations disappeared, it became possible to store smaller files in LTS. With CoCa
redesign several possible bundling policies were introduced:

<DL>
<DT>SingleFile</DT>
<DD>
This is the new default policy, with this policy files are not bundled (there is just one
file per archive), archive differs from file name only by prefix <tt><year>/<dataset>/</tt>
(that is files are stored in sub-directories, per-year and per-dataset to reduce number of
files in one directory). Because files are not bundled they are sent to LTS immediately
after registration which makes them available almost immediately for off-line applications.
</DD>

<DT>ZipBundle</DT>
<DD>
This is the original default policy. Large files (exact size limit is in CoCa configuration)
without slashes in the name (without directories) are archived without bundling, but their
names are prefixed with <tt><dataset>_</tt>. Smaller files are bundled into ZIP archives,
archives have names <tt><dataset>_<unique_number>.zip</tt>. All archived files end up on
the same LTS directory which lead to a large number of files stored over few years.
</DD>

<DT>ZipBundleDir</DT>
<DD>
This is a modification of ZipBundle policy which alleviates problem of too many files in
one directory by introducing sub-directories in LTS. Large files (with or without slashes
in names) are stored in LTS with prefix <tt><year>/<dataset>/</tt>, bundled files are
stored in archives which have names <tt><year>/<dataset>/<dataset>_<unique_number>.zip</tt>.
</DD>
</DL>


\section database CoCa database

At the heart of the whole system is the database which contains information about anything
that ever happened in a system. It records:
- all registered files
- processing state for each registered file
- all created archives
- state and location of the archives

CoCa database contains two main tables - COCA1_CACHE and  COCA1_ARCHIVES - and two auxiliary
tables - COCA_NEXTID and  COCA_STR2ID. Here is a brief description of each table.

\subsection str2id COCA_STR2ID table

This table provides mapping between strings an integer numbers. This could be used to more
efficiently store repeating strings in the tables. Instead of storing a string in a table
column one can instead use the ID from this table.


COCA_STR2ID columns:
Column  | Type        | Keys    | Description
--------|-------------|---------|--------------------------
STR     | VARCHAR     | PRI     | Value of the string
ID      | INTEGER     | UNI     | Corresponding integer ID


\subsection nextid_table COCA_NEXTID Table

This auxiliary table is used to keep track of the "next ID" for archive numbers and other
sorts of unique IDs. It implements a number of unique ID sequences, each providing sequential
ID numbers. This could probably be implemented more efficiently using native ORACLE sequences,
but performance is not critical in CoCa.

COCA_NEXTID columns:
Column  | Type        | Keys    | Description
--------|-------------|---------|---------------------------------------
WHAT    | VARCHAR     | PRI     | Name of the sequence
SPEC    | INTEGER     | PRI     | Additional sequence index
NEXTID  | INTEGER     |         | Next available ID for this sequence


\subsection cache_table COCA1_CACHE Table

Table containing records of all data files that were ever registered (not only those in cache
as the name may imply).

COCA1_CACHE columns:
Column          | Type        | Keys    | Description
----------------|-------------|---------|---------------------------------------------------------
DATASET         | INTEGER     | PRI, FK | ID of the dataset (use COCA_STR2ID to map ID to name)
REL_PATH        | VARCHAR     | PRI     | File name (relative path, may contain slashes)
SIZE_MB         | INTEGER     |         | File size in MB (very approximate)
CLIENT_HOST     | INTEGER     |         | ID of the host where file originated (ID in COCA_STR2ID table)
DIR_CLIENT      | INTEGER     |         | ID of the directory on client host (ID in COCA_STR2ID table)
PRIO            | INTEGER     |         | File priority for caching
REGISTERED_AT   | TIMESTAMP   |         | Time when the file was registered
TRY2KEEP_TIL    | TIMESTAMP   |         | Time until when to keep it in cache
ARCHIVE         | INTEGER     | FK      | Archive number
ON_CLIENT       | BOOLEAN     |         | If true then file is sill on client host. When file is registered it is set to true, set to false once file is removed from client.
IN_CACHE        | BOOLEAN     |         | If true then in the CoCa cache
IN_CACHE_AT     | TIMESTAMP   |         | Time when the file appeared in cache
REMOVED         | BOOLEAN     |         | Currently unused
REMOVED_AT      | TIMESTAMP   |         | Currently unused

Combination of (DATASET, ARCHIVE) is a foreign key into the COCA1_ARCHIVES table which links
file record with its corresponding archive.


\subsection archive_table COCA1_ARCHIVES Table

Table containing records of all archives ever created by CoCa.

COCA1_ARCHIVES columns:
Column          | Type        | Keys    | Description
----------------|-------------|---------|-------------------------------------------------------------
DATASET         | INTEGER     | PRI     | ID of the dataset (ID in COCA_STR2ID table)
ARCHIVE         | INTEGER     | PRI     | Archive number
NAME            | VARCHAR     | UNI     | Archive name
BOOKED_AT       | TIMESTAMP   | UNI     | Time when archive record was created (not when archive file was created)
SERVER_IPC      | INTEGER     |         | Unique ID string of the server used to separate multiple CoCa servers. Unique server name usually includes release name and application name (ID in COCA_STR2ID table)
SERVER_HOST     | INTEGER     |         | Host name where server is running (ID in COCA_STR2ID table)
DIR_SERVER      | INTEGER     |         | Server directory where archive is located (ID in COCA_STR2ID table)
OPEN_IF_0       | INTEGER     |         | If 0 then this archive is in open state meaning that more files can be added
CLOSED_AT       | TIMESTAMP   |         | Time when archive was closed
ON_CDR          | BOOLEAN     |         | If true then archive was moved to CDR stage-in area
STORED          | BOOLEAN     |         | If true then archive was moved to LTS (this is detected as disappearance of archive from CDR stage-in area)
STORED_AT       | TIMESTAMP   |         | Time when archive was moved to LTS
STOR_PATH       | VARCHAR     |         | Full archive path in LTS
REMOVED         | BOOLEAN     |         | If true then archive was removed from LTS
REMOVED_AT      | TIMESTAMP   |         | Time when archive was removed from LTS


\section lifetime Lifetime Cycle

Files and archives go through the the series of states which are reflected in the database in
their corresponding tables. Here is the sequence of events that happen to files and archives
together with the summary of the changes that happen in database.

Files:
-# File is created in the writer client host (this is not reflected in CoCa)
-# File is registered in CoCa by writer client
   - new record is created in COCA1_CACHE table
   - ON_CLIENT is set to TRUE
   - ARCHIVE set to corresponding archive ID (see below)
-# CoCa server copies the file from writer client host to CoCa cache
   - IN_CACHE is set to TRUE
-# CoCa server removes the file from writer client host
   - if removal succeeds then ON_CLIENT is set to FALSE
-# CoCa server archives file (with or without bundling into ZIP files) to LTS
   - file record does not change, only archive record
-# CoCa server at some point can remove file from cache
   - IN_CACHE is set to FALSE

Archives are created when files are registered with CoCa server, some files may be added to
existing open archive depending on the bundling policy explained above:
-# When new file is registered CoCa server finds corresponding archive for it
   - if file has to be stored in a separate archive (e.g. its size is too large or
     policy is set to SingleFile) then new archive is "allocated" with the next
     available ID and a name built from file name as explained above. For single-file
     archives OPEN_IF_0 is set to non-zero
   - if file needs to be bundled then CoCa server tries to find existing open
     archive (with OPEN_IF_0 = 0), if it cannot find any open archive it creates
     new archive. For newly created bundle-archives OPEN_IF_0 is set to 0.
-# CoCa closes open archives when either archive size limit or timeout is reached
   - OPEN_IF_0 is set to non-zero (same as ARCHIVE column value)
-# closed archives are coped to stage-in area for CDR
   - non-bundled files are copied from CoCa cache to stage-in area
   - bundles are created at this point by adding all files in archive to one ZIP file
   - archives are placed into stage-in area and ON_CDR is set to TRUE
-# CoCa server checks regularly that archives are copied to LTS
   - currently this is triggered by disappearance of archive from CDR stage-in area
   - STORED is set to TRUE (ON_CDR is not changed)
-# potentially archives can be removed from LTS but this was never done and there is
   no implementation of the mechanism in CoCa server
   - if someone deletes archive from LTS then database is supposed to be updated by
     setting COCA1_ARCHIVES.REMOVED to TRUE (and optionally COCA1_CACHE.REMOVED to TRUE)


\section cache_mgmt Cache Management

CoCa server manages lifetime of all files residing in a cache. Original implementation
removed files from cache when there was not enough free space on disk to store new
incoming files.

When CoCa server decides that it needs to free some space it does that based on the
"cost" of each file. Cost function is defined as:

\code

  # 'file' is a record of type COCA1_CACHE

  time_in_cache(file) -> seconds :
      now() - file.REGISTERED_AT

  expired(file) -> int (0/1) :
      int(file.TRY2KEEP_TIL > now())

  cost(file) -> int :
      # day := 24*3600  (seconds in 1 day)
      time_in_cache(file) - file.PRIO * day + file.SIZE_MB/1024 * day + expired(file) * 2^32

\endcode

The longer file stays in cache the higher is its cost. One unit of priority "buys"
one extra day, and one GB of file size costs 1 day. Files that stay in cache beyond
TRY2KEEP_TIL are "expired" and get huge additional cost, this is done so that expired
files are always removed from cache first. Expired files are still ordered by their
cost when they are removed. TRY2KEEP_TIL is calculated at file registration time as
a sum of REGISTERED_AT plus "Desired Time in Cache" (DTC) which is provided by client
at registration time (typical DTC is one day).

CoCa sorts all files in cache (or expected to be in cache) according to their cost and
removes files from disk starting with the most "expensive" until it reaches goal for
necessary free space on disk. Usually necessary space is twice the size of the next
file to be copied from client or to CDR.

CoCa server assumes that CDR stage-in area is located on the same volume as CoCa cache
and they compete for disk space. Whenever CoCa server needs to create new archive (by
copying non-bundled file or creating new ZIP bundle) it will try to make sure that disk
has enough free space to hold new archive by removing some files from cache. Archives
are removed from CDR stage-in area by CDR service after they are moved to LTS.

For Run2 the algorithm was adjusted to keep larger fraction of disk space free, this
was done to reduce possibility of a disk becoming full in situations when the same
physical disk could be shared between different clients. With the new implementation
CoCa server tries to keep a pre-configured amount (typically 20%) of the disk space
free removing "expired" files only. If this goal cannot be achieved then it switches
back to old algorithm by freeing enough space to store next incoming file.

*/
