#ifndef COCA_CACHEDFILE_H
#define COCA_CACHEDFILE_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <iosfwd>
#include <string>
#include <ctime>
#include <boost/filesystem.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Size.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_server

/**
 *  @ingroup coca_server
 *
 *  @brief Class representing file in coca cache (or COCA_CACHE1 table in
 *  coca database).
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Andy Salnikov
 */

class CachedFile {
public:

    /// Type for caching cost
    typedef int32_t cost_t;

    /**
     *   Constructor.
     *
     *   @param[in] serverDir  Directory on coca server host.
     *   @param[in] relPath    File name relative to the clientDir.
     *   @param[in] dataset    CoCa dataset name.
     *   @param[in] serverIPC  ID of the server which created the file.
     *   @param[in] size       Size of the file in bytes.
     *   @param[in] prio       Caching priority.
     *   @param[in] registeredAt  File registration time.
     *   @param[in] keepUntil  File cache expiration time.
     *   @param[in] checksum   File checksum ("md5:hex-string" or similar) or
     *                         empty string.
     */
    CachedFile(const boost::filesystem::path& serverDir,
               const boost::filesystem::path& relPath,
               const std::string& dataset,
               const std::string& serverIPC,
               Size size,
               int prio,
               time_t registeredAt,
               time_t keepUntil,
               const std::string& checksum);

    /// Returns relative file name.
    const boost::filesystem::path& relPath() const { return m_relPath; }

    /// Returns directory on coca server host.
    const boost::filesystem::path& serverDir() const { return m_serverDir; }

    /// Returns path name on coca server.
    boost::filesystem::path serverPath() const;

    /// Returns CoCa dataset name.
    const std::string& dataset() const { return m_dataset; }

    /// Returns ID of the server which created the file.
    const std::string& serverIPC() const { return m_serverIPC; }

    /// Returns size of the file in bytes
    Size size() const { return m_size; }

    /// Returns caching priority
    int prio() const { return m_prio; }

    /// Returns time when file was registered
    time_t registeredAt() const { return m_registeredAt; }

    /// Returns true if given time is past file expiration
    bool expired(time_t time) const;

    /// Returns file checksum, could be empty string.
    const std::string& checksum() const {return m_checksum;}

    /// Returns file caching "cost" at specific time.
    cost_t cacheCost(time_t time) const;

protected:

private:

    boost::filesystem::path m_serverDir;    ///< Directory on coca server host
    boost::filesystem::path m_relPath;      ///< Relative path (w.r.t. m_serverDir)
    std::string m_dataset;      ///< CoCa dataset name
    std::string m_serverIPC;    ///< ID of the server which created the file
    Size m_size;                ///< File size
    int m_prio;                 ///< Caching priority
    time_t m_registeredAt;      ///< Time when file was registered
    time_t m_keepUntil;         ///< Expiration time
    std::string m_checksum;     ///< File checksum
};

/// Stream insertion operator to print object contents
std::ostream&
operator<<(std::ostream& out, const CachedFile& f);

} // namespace coca
} // namespace daq

#endif // COCA_CACHEDFILE_H
