
#include <fstream>
#include <boost/program_options.hpp>
#include <sqlite3.h>

#include "coca/common/DBBase.h"
#include "ers/ers.h"

namespace po = boost::program_options;
using namespace daq::coca;

namespace {

    ERS_DECLARE_ISSUE(LclIssues, UncaughtException, "Uncaught exception, terminating.", )
}


int
main(int argc, char **argv)
try {

    po::options_description cmdline_options("Available options");
    cmdline_options.add_options()
            ("help,h", "print usage and exit")
            ("schema-version", po::value<int>()->default_value(-1),
                    "Schema version number")
            ("fixup-path", po::value<std::string>()->default_value(""),
                    "Fixup folder location, required for sqlite database")
            ("database,c", po::value<std::string>()->default_value("coca"),
                    "CoCa database connection string, e.g. sqlite_file:coca.sqlite3")
            ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, cmdline_options), vm);

    if (vm.count("help")) {
        std::cout << "Generate database schema for CoCa database.\n\n"
                << "Usage: " << argv[0] << " [options] -c databaseURI\n\n"
                << cmdline_options << "\n";
        return 0;
    }

    po::notify(vm);

    std::string connStr = vm["database"].as<std::string>();
    int version = vm["schema-version"].as<int>();

    DBBase::createSchema(connStr, version);

    std::string technology;
    int schemaVersion = 0;
    {
        DBBase db(connStr);
        technology = db.technology();
        schemaVersion = db.schemaVersion();
    }
    if (technology == "sqlite") {
        // apply fixup, need actual version
        std::string const fixup_folder = vm["fixup-path"].as<std::string>();
        if (fixup_folder.empty()) {
            std::cerr << "Fixup path is required for sqlite, resulting schema is incomplete.\n";
            return 1;
        }

        std::string const fixup_script = fixup_folder + "/coca-sqlite-fixup-" +
                std::to_string(schemaVersion) + ".sql";

        // We could use `sqlite3` CLI to run it but PATH is not usually set
        // right but we can use C API to do the same.

        std::string sql;
        std::ifstream sqlstream(fixup_script);
        std::getline(sqlstream, sql, '\0');

        // for sqlite we usually specify patch after colon, there is no host name
        auto doubleColonPosition = connStr.find(":");
        if (doubleColonPosition == std::string::npos) {
            std::cerr << "Failed to parse connection string, missing colon\n";
            return 1;
        }

        std::string const dbpath(connStr, doubleColonPosition+1);
        sqlite3 *pDb = nullptr;
        auto rc = sqlite3_open(dbpath.c_str(), &pDb);
        if (rc != SQLITE_OK) {
            std::cerr << "Failed to open SQLite database, file = " << dbpath << "\n";
            return 1;
        }

        // std::cout << "SQLite fixup script: " << sql;
        rc = sqlite3_exec(pDb, sql.c_str(), nullptr, nullptr, nullptr);
        if (rc != SQLITE_OK) {
            std::cerr << "Failed to execute SQLite fixup script.\n";
            return 1;
        }

        rc = sqlite3_close(pDb);
        if (rc != SQLITE_OK) {
            std::cerr << "Failed to close SQLite database.\n";
            return 1;
        }
    }


} catch (const po::error& ex) {
    std::cerr << ex.what() << "\n"
            "Use --help option for usage information.\n";
    return 1;
} catch (const std::exception& ex) {
    ers::error(LclIssues::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(LclIssues::UncaughtException(ERS_HERE));
    return 1;
}
