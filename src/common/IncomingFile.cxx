//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class IncomingFile...
//
// Author List:
//      Andy Salnikov
//
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/common/IncomingFile.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/FileSystem.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

namespace daq {
namespace coca {

//----------------
// Constructors --
//----------------
IncomingFile::IncomingFile (const std::string& host,
                            const std::string& clientDir,
                            const std::string& relPath,
                            Size size,
                            int prio,
                            unsigned dtcSec,
                            const std::string& checksum)
    : m_host(host)
    , m_clientDir(clientDir)
    , m_relPath(relPath)
    , m_size(size)
    , m_prio(prio)
    , m_dtcSec(dtcSec)
    , m_checksum(checksum)
{
}

//--------------
// Destructor --
//--------------
IncomingFile::~IncomingFile () throw ()
{
}

/// Returns path name on client host.
std::string
IncomingFile::clientPath () const
{
    return FileSystem::join(m_clientDir, m_relPath);
}

/// Stream insertion operator to print object contents
std::ostream&
operator<<(std::ostream& out, const IncomingFile& f)
{
    out << "RelPath(" << f.relPath() << ") Size(" << f.size() << ")"
        << " PRIO(" << f.prio() << ") DTC(" << f.dtcSec() << "sec)"
        << " Host(" << f.host() << ") DirClient(" << f.clientDir() << ")"
        << " Checksum(" << f.checksum() << ")";
    return out;
}

} // namespace coca
} // namespace daq
