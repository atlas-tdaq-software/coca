#ifndef COCA_CHECKSUM_H
#define COCA_CHECKSUM_H

#include <string>
#include <boost/filesystem.hpp>

#include "ers/ers.h"


namespace daq::coca {

    /**
     *  @class Checksum::Exception
     *  @brief Base class for the exception-type issues generated by Checksum methods.
     */
    ERS_DECLARE_ISSUE (Checksum, Exception, , )

/// @addtogroup coca_common

/**
 * @ingroup coca_common
 *
 *  @brief Helper functions for calculating checksums.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 */

namespace Checksum {

    /**
     * @brief Calculate MD5 of a string.
     *
     * @param[in] string String with data to digest.
     * @return String in a format "md5:hex-string"
     *
     * @throw Checksum:Exception Thrown for any errors.
     */
    std::string md5_string(const std::string& string);

    /**
     * @brief Calculate MD5 of a file.
     *
     * @param[in] path Path name of an existing file.
     * @return String in a format "md5:hex-string"
     *
     * @throw Checksum:Exception Thrown for any errors.
     */
    std::string md5_file(const boost::filesystem::path& path);

} // namespace Checksum

} // namespace daq::coca

#endif // COCA_CHECKSUM_H
