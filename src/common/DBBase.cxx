
#include "coca/common/DBBase.h"

#include <CoralBase/Exception.h>
#include <RelationalAccess/ConnectionService.h>
#include <RelationalAccess/IConnectionServiceConfiguration.h>
#include <RelationalAccess/TableDescription.h>

#include "coca/common/Issues.h"
#include "ers/ers.h"


using namespace daq::coca;

namespace {

// Extract technology name from URI, return empty string if it ails to parse.
// Stolen from CORAL URIParser class
std::string get_tech(const std::string& connStr) {
    std::string tech;

    auto doubleColonPosition = connStr.find(":");
    if (doubleColonPosition == std::string::npos) {
        return tech;
    }

    tech = connStr.substr(0, doubleColonPosition);

    // Separate the technology from the protocol
    auto underscorePosition = tech.find("_");
    if (underscorePosition != std::string::npos) {
        tech.erase(underscorePosition);
    }

    return tech;
}

}


int DBBase::s_latestVersion = 4;
std::vector<int> DBBase::s_supportedVersions = {0, 1, 2, 3, 4};

/*
 * DBBase
 */

DBBase::DBBase (const std::string& connStr)
    : DBBase(connStr, false, true)
{
}

DBBase::DBBase (const std::string& connStr, bool update)
    : DBBase(connStr, update, true)
{
}

DBBase::DBBase(const std::string& connStr, bool update, bool check_version)
    : m_connStr(connStr)
    , m_technology(::get_tech(connStr))
    , m_metaTable("COCA_META")
    , m_nextid("COCA_NEXTID")
    , m_s2i("COCA_STR2ID", m_nextid)
    , m_arName("COCA1_ARCHIVES")
    , m_caName("COCA1_CACHE")
    , m_update(update)
{
    if (check_version) {
        Transaction tr(session());
        m_schemaVersion = m_metaTable.dbSchemaVersion(tr);
        if (m_schemaVersion < 0) {
            // missing META table means version 0
            m_schemaVersion = 0;
        }

        if (std::count(s_supportedVersions.begin(), s_supportedVersions.end(), m_schemaVersion) == 0) {
            throw Issues::UnsupportedSchemaVersionError(ERS_HERE, m_schemaVersion);
        }
    }
}

DBBase::~DBBase ()
    throw ()
{
}

/// Get the handle for archive database table
coral::ITable&
DBBase::archiveTable(Transaction& tran) const
{
    return tran.session().nominalSchema().tableHandle(m_arName);
}

/// Get the handle for cache database table
coral::ITable&
DBBase::cacheTable(Transaction& tran) const
{
    return tran.session().nominalSchema().tableHandle(m_caName);
}


std::string
DBBase::getStr (unsigned id, Transaction& tran)
{
    return m_s2i.getStr (id, tran);
}

unsigned
DBBase::getId (const std::string &str, Transaction& tran)
{
    return m_s2i.getId (str, tran);
}

unsigned
DBBase::insStr (const std::string &str, Transaction& tran)
{
    return m_s2i.insert (str, tran);
}

std::shared_ptr<coral::ISessionProxy>
DBBase::session()
{
    coral::ConnectionService conn_serv;

    static bool doDebug = getenv("COCA_CORAL_DEBUG") != nullptr;
    if (doDebug) {
        conn_serv.setMessageVerbosityLevel(coral::Debug);
    } else {
        conn_serv.setMessageVerbosityLevel(coral::Warning);
    }

    // set connection parameter so that connection failures happen sooner
    // and replicas are not excluded for too long
    coral::IConnectionServiceConfiguration& config = conn_serv.configuration();
    config.setMissingConnectionExclusionTime(0);
    config.setConnectionRetrialPeriod(2);
    config.setConnectionRetrialTimeOut(2);

    ERS_DEBUG (0, "database connection: \"" << m_connStr << '"');
    auto accessMode = m_update ? coral::Update : coral::ReadOnly;
    coral::ISessionProxy* session = conn_serv.connect (m_connStr, accessMode);

    return std::shared_ptr<coral::ISessionProxy>(session);

}

// Returns default CORAL connection string for CoCa database.
std::string
DBBase::defConnStr()
{
  static const std::string connStr("COCA");
  return connStr;
}

void
DBBase::createSchema(const std::string& connStr, int version)
{
    DBBase db(connStr, true, false);

    db._createSchema(version);
}

Metadata
DBBase::metadata()
{
    return Metadata(m_connStr, m_metaTable);
}


void
DBBase::_createSchema(int version)
{
    if (version < 0) {
        // use latest one
        version = s_latestVersion;
    }

    Transaction tr(session(), Transaction::Update);
    if (version > 0) {
        // Meta table only exists for version 1 and above
        m_metaTable.createTable(version, tr);
    }

    m_nextid.createTable(version, tr);
    m_s2i.createTable(version, tr);

    auto& schema = tr.session().nominalSchema();

    coral::TableDescription tda("DBBase::_createSchema");
    tda.setName("COCA1_ARCHIVES");
    tda.insertColumn("DATASET", "unsigned");
    tda.insertColumn("ARCHIVE", "unsigned");
    tda.insertColumn("NAME", "string");
    tda.insertColumn("BOOKED_AT", "time stamp");
    tda.insertColumn("CLOSE_AT", "time stamp");
    tda.insertColumn("DELETE_AT", "time stamp");
    tda.insertColumn("SERVER_IPC", "unsigned");
    tda.insertColumn("SERVER_HOST", "unsigned");
    tda.insertColumn("DIR_SERVER", "unsigned");
    tda.insertColumn("TOTSIZE_MB", "unsigned");
    tda.insertColumn("TOTSIZE_MB_CACHED", "unsigned");
    tda.insertColumn("OPEN_IF_0", "unsigned");
    tda.insertColumn("CLOSED_AT", "time stamp");
    tda.insertColumn("ON_CDR", "bool");
    tda.insertColumn("ON_CDR_AT", "time stamp");
    tda.insertColumn("CDR_HOST", "unsigned");
    tda.insertColumn("CDR_PATH", "string");
    tda.insertColumn("STORED", "bool");
    tda.insertColumn("STORED_AT", "time stamp");
    tda.insertColumn("STOR_PATH", "string");
    if (version < 4) {
        // Starting with version 2 all DDM* columns have default values, but CORAL
        // does not support default value specification.
        // DDM columns were dropped in version 4.
        tda.insertColumn("DDM", "bool");
        tda.insertColumn("DDM_AT", "time stamp");
        tda.insertColumn("DDM_GUID", "string");
        tda.insertColumn("DDM_LFN", "string");
    }
    tda.insertColumn("REMOVED", "bool");
    tda.insertColumn("REMOVED_AT", "time stamp");
    if (version > 2) {
        // These three columns have DEFAULT NULL but CORAL does not support defaults
        // We use "long", not "unsigned long" to avoid issue with signed 64 bit types
        tda.insertColumn("TOTSIZE_BYTES", "long");
        tda.insertColumn("ARCHIVE_SIZE_BYTES", "long");
        tda.insertColumn("CHECKSUM", "string");
    }
    tda.setPrimaryKey(std::vector<std::string>({"DATASET", "ARCHIVE"}));
    // tda.setUniqueConstraint("NAME", "COCA1_ARCHIVES_NAME_UNIQ");
    // tda.setUniqueConstraint("CDR_PATH", "COCA1_ARCHIVES_CDR_PATH_UNIQ");
    // tda.setUniqueConstraint("STOR_PATH", "COCA1_ARCHIVES_STOR_PATH_UNIQ");
    // tda.setUniqueConstraint("DDM_GUID", "COCA1_ARCHIVES_DDM_GUID_UNIQ");
    // tda.setUniqueConstraint("DDM_LFN", "COCA1_ARCHIVES_DDM_LFN_UNIQ");
    tda.setNotNullConstraint("DATASET", true);
    tda.setNotNullConstraint("ARCHIVE", true);
    tda.setNotNullConstraint("NAME", true);
    tda.setNotNullConstraint("BOOKED_AT", true);
    tda.setNotNullConstraint("CLOSE_AT", true);
    tda.setNotNullConstraint("DELETE_AT", true);
    tda.setNotNullConstraint("SERVER_IPC", true);
    tda.setNotNullConstraint("SERVER_HOST", true);
    tda.setNotNullConstraint("DIR_SERVER", true);
    tda.setNotNullConstraint("TOTSIZE_MB", true);
    tda.setNotNullConstraint("TOTSIZE_MB_CACHED", true);
    tda.setNotNullConstraint("OPEN_IF_0", true);
    tda.setNotNullConstraint("ON_CDR", true);
    tda.setNotNullConstraint("CDR_HOST", true);
    tda.setNotNullConstraint("STORED", true);
    if (version < 4) {
        tda.setNotNullConstraint("DDM", true);
    }
    tda.setNotNullConstraint("REMOVED", true);
    tda.createForeignKey("COCA1_ARCHIVES_CDR_HOST_STR", "CDR_HOST", "COCA_STR2ID", "ID");
    tda.createForeignKey("COCA1_ARCHIVES_DATASET_STR", "DATASET", "COCA_STR2ID", "ID");
    tda.createForeignKey("COCA1_ARCHIVES_DIR_SERVER_STR", "DIR_SERVER", "COCA_STR2ID", "ID");
    tda.createForeignKey("COCA1_ARCHIVES_SERVER_HOST_STR", "SERVER_HOST", "COCA_STR2ID", "ID");
    tda.createForeignKey("COCA1_ARCHIVES_SERVER_IPC_STR", "SERVER_IPC", "COCA_STR2ID", "ID");
    // tda.createIndex("COCA1_ARCHIVES_PK", std::vector<std::string>({"DATASET", "ARCHIVE"}), true);
    tda.createIndex("COCA1_ARCHIVES_CDR_PATH_UNIQ", "CDR_PATH", true);
    if (version < 4) {
        tda.createIndex("COCA1_ARCHIVES_DDM_GUID_UNIQ", "DDM_GUID", true);
        tda.createIndex("COCA1_ARCHIVES_DDM_LFN_UNIQ", "DDM_LFN", true);
    }
    tda.createIndex("COCA1_ARCHIVES_NAME_UNIQ", "NAME", true);
    tda.createIndex("COCA1_ARCHIVES_STOR_PATH_UNIQ", "STOR_PATH", true);
    tda.createIndex("COCA1_ARCHIVES_SERVER_CLOSED", {"DATASET", "SERVER_IPC", "OPEN_IF_0"}, true);
    tda.createIndex("COCA1_ARCHIVES_CLOSED_DELAY", std::vector<std::string>({"OPEN_IF_0", "CLOSE_AT"}));
    if (version < 4) {
        tda.createIndex("COCA1_ARCHIVES_DDM_REGISTR", {"STORED", "REMOVED", "DDM"});
    }
    tda.createIndex("COCA1_ARCHIVES_GET_FILES", {"DATASET", "ARCHIVE", "SERVER_IPC", "OPEN_IF_0", "ON_CDR"});
    schema.createTable(tda);

    coral::TableDescription tdc("DBBase::_createSchema");
    tdc.setName("COCA1_CACHE");
    tdc.insertColumn("DATASET", "unsigned");
    tdc.insertColumn("REL_PATH", "string");
    tdc.insertColumn("SIZE_MB", "unsigned");
    tdc.insertColumn("CLIENT_HOST", "unsigned");
    tdc.insertColumn("DIR_CLIENT", "unsigned");
    tdc.insertColumn("PRIO", "unsigned");
    tdc.insertColumn("TRY2KEEP_TIL", "time stamp");
    tdc.insertColumn("REGISTERED_AT", "time stamp");
    tdc.insertColumn("ARCHIVE", "unsigned");
    tdc.insertColumn("ON_CLIENT", "bool");
    tdc.insertColumn("IN_CACHE", "bool");
    tdc.insertColumn("IN_CACHE_AT", "time stamp");
    tdc.insertColumn("REMOVED", "bool");
    tdc.insertColumn("REMOVED_AT", "time stamp");
    if (version > 2) {
        // These two columns have DEFAULT NULL but CORAL does not support defaults
        // We use "long", not "unsigned long" to avoid issue with signed 64 bit types
        tdc.insertColumn("SIZE_BYTES", "long");
        tdc.insertColumn("CHECKSUM", "string");
    }
    tdc.setPrimaryKey(std::vector<std::string>({"DATASET", "REL_PATH"}));
    tdc.setNotNullConstraint("DATASET", true);
    tdc.setNotNullConstraint("REL_PATH", true);
    tdc.setNotNullConstraint("SIZE_MB", true);
    tdc.setNotNullConstraint("CLIENT_HOST", true);
    tdc.setNotNullConstraint("DIR_CLIENT", true);
    tdc.setNotNullConstraint("PRIO", true);
    tdc.setNotNullConstraint("TRY2KEEP_TIL", true);
    tdc.setNotNullConstraint("REGISTERED_AT", true);
    tdc.setNotNullConstraint("ARCHIVE", true);
    tdc.setNotNullConstraint("ON_CLIENT", true);
    tdc.setNotNullConstraint("IN_CACHE", true);
    tdc.setNotNullConstraint("REMOVED", true);
    tdc.createForeignKey("COCA1_CACHE_2_DS_AR", std::vector<std::string>({"DATASET", "ARCHIVE"}),
                         "COCA1_ARCHIVES", std::vector<std::string>({"DATASET", "ARCHIVE"}));
    tdc.createForeignKey("COCA1_CACHE_CLIENT_HOST_STR", "CLIENT_HOST", "COCA_STR2ID", "ID");
    tdc.createForeignKey("COCA1_CACHE_DIR_CLIENT_STR", "DIR_CLIENT", "COCA_STR2ID", "ID");
    // tdc.createIndex("COCA1_CACHE_PK", std::vector<std::string>({"DATASET", "REL_PATH"}), true);
    tdc.createIndex("COCA1_CACHE_DS_AR", std::vector<std::string>({"DATASET", "ARCHIVE"}));
    schema.createTable(tdc);
}
