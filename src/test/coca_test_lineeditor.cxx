
#define BOOST_TEST_MODULE coca_test_lineeditor
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "coca/common/LineEditor.h"
#include "coca/common/Issues.h"

using namespace daq::coca;

std::string rules(R"(
[
    {
        "match": "root://atlaseos/",
        "replace": "root://atlaseos.cern.ch/"
    },
    {
        "match": "^rfio:",
        "discard": true
    },
    {
        "match": "root://atlaseos.cern.ch/",
        "replace": "root://pc-tdq-mon-01.cern.ch:2099/",
        "keep_original": true,
        "stop": true
    }
]
)");

std::string rules_multiply(R"(
[
    {
        "match": "blah",
        "replace": "bloody-blah",
        "keep_original": true
    },
    {
        "match": "blah",
        "replace": "bloody-blah",
        "keep_original": true
    },
    {
        "match": "blah",
        "replace": "bloody-blah",
        "keep_original": true
    }
]
)");

// not JSON
std::string bad_rules_1("[}]");

// not a list.
std::string bad_rules_2(R"(
{
    "match": "match_this",
    "replace": "replacement"
}
)");

BOOST_AUTO_TEST_CASE(test_exceptions)
{
    BOOST_REQUIRE_THROW((LineEditor(bad_rules_1)), Issues::LineEditorRulesError);
    BOOST_REQUIRE_THROW((LineEditor(bad_rules_2)), Issues::LineEditorRulesError);
}

BOOST_AUTO_TEST_CASE(test_rules)
{
    LineEditor editor(rules);

    auto const& rules = editor.rules();
    BOOST_TEST(rules.size() == 3);

    using V = std::vector<std::string>;
    std::pair<std::vector<std::string>, bool> result;

    auto rule = rules[0];
    result = rule.apply("not a match");
    BOOST_TEST(result.first == V{"not a match"});
    BOOST_TEST(result.second == false);
    result = rule.apply("root://atlaseos/blah.root");
    BOOST_TEST(result.first == V{"root://atlaseos.cern.ch/blah.root"});
    BOOST_TEST(result.second == false);

    rule = rules[1];
    result = rule.apply("root://rfio:1099/blah.root");
    BOOST_TEST(result.first == V{"root://rfio:1099/blah.root"});
    BOOST_TEST(result.second == false);
    result = rule.apply("rfio://rfio:1099/blah.root");
    BOOST_TEST(result.first.empty());
    BOOST_TEST(result.second == true);

    rule = rules[2];
    result = rule.apply("root://atlaseos/blah.root");
    BOOST_TEST(result.first == V{"root://atlaseos/blah.root"});
    BOOST_TEST(result.second == false);
    result = rule.apply("root://atlaseos.cern.ch/blah.root");
    BOOST_TEST(
        result.first == V({"root://atlaseos.cern.ch/blah.root", "root://pc-tdq-mon-01.cern.ch:2099/blah.root"})
    );
    BOOST_TEST(result.second == true);
}

BOOST_AUTO_TEST_CASE(test_editor)
{
    LineEditor editor(rules);

    using V = std::vector<std::string>;

    std::vector<std::string> lines({
        "root://pc-tdq-mon-01.cern.ch/blah.root",
        "root://atlaseos/blah.root",
        "rfio://somewhere/blah.root",
        "http://atlas.cern.ch/blah.root"
    });
    lines = editor.apply(lines);
    BOOST_TEST(
        lines == V({
            "root://pc-tdq-mon-01.cern.ch/blah.root",
            "root://atlaseos.cern.ch/blah.root",
            "root://pc-tdq-mon-01.cern.ch:2099/blah.root",
            "http://atlas.cern.ch/blah.root"
        })
    );
}

BOOST_AUTO_TEST_CASE(test_multiply)
{
    LineEditor editor(rules_multiply);

    using V = std::vector<std::string>;

    std::vector<std::string> lines{"blah"};
    lines = editor.apply(lines);
    BOOST_TEST(
        lines == V({
            "blah",
            "bloody-blah",
            "bloody-blah",
            "bloody-bloody-blah",
            "bloody-blah",
            "bloody-bloody-blah",
            "bloody-bloody-blah",
            "bloody-bloody-bloody-blah",
        })
    );
}