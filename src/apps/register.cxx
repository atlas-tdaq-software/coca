// $Id$
// $Name$

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/program_options.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/coca.h"
#include "ipc/core.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------
namespace po = boost::program_options;
using namespace daq::coca;

namespace {
    ERS_DECLARE_ISSUE(LclIssues, UnexpectedNegative, 
     var_descr << ": expected positive value, got: " << act_value,
     ((std::string) var_descr) ((int64_t) act_value))

    ERS_DECLARE_ISSUE(LclIssues, CommandLineSyntax, 
     "bad command line syntax: " << error, ((std::string) error))

    ERS_DECLARE_ISSUE(LclIssues, RegistrationFailed, "File registration failed", )

    ERS_DECLARE_ISSUE(LclIssues, UncaughtException, "Uncaught exception, terminating.", )
}


int
main(int argc, char **argv)
try {
    /*
     * command line
     */

    po::options_description cmdline_options("Available options");
    cmdline_options.add_options()
            ("help,h", "print usage and exit")
            ("partition,p", po::value<std::string>()/*->value_name("STRING")*/->default_value("initial"),
                    "partition to search for CoCa server")
            ("server,s", po::value<std::string>()/*->value_name("STRING")*/->default_value("coca"),
                    "CoCa server name")
            ;

    po::options_description dataopt("Data specification options");
    dataopt.add_options()
            ("dataset,d", po::value<std::string>()/*->value_name("STRING")*/->required(), "CoCa dataset name")
            ("PRIO,P", po::value<int>()/*->value_name("NUMBER")*/->default_value(10), "specify archive name")
            ("DTC,D", po::value<unsigned>()/*->value_name("SECONDS")*/->default_value(86400U), "desired time (seconds) in CoCa local cache")
            ("basedir,b", po::value<std::string>()/*->value_name("PATH")*/->default_value(""),
                    "base directory, will be stripped from file names to obtain relative name, "
                    "if not specified then basename is used for relative name")
            ("files,f", po::value<std::vector<std::string> >()/*->value_name("PATHS")*/->required()->multitoken(), "file list")
            ;

    cmdline_options.add(dataopt);

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, cmdline_options), vm);

    if (vm.count("help")) {
        std::cout << "Register file with the CoCa server.\n\n"
                << "Usage: " << argv[0] << " [options] -d dataset -f file ...\n\n"
                << cmdline_options << "\n";
        return 0;
    }

    po::notify(vm);

    std::string partition = vm["partition"].as<std::string>();
    std::string server = vm["server"].as<std::string>();
    std::string dataset = vm["dataset"].as<std::string>();
    std::string basedir = vm["basedir"].as<std::string>();
    int prio = vm["PRIO"].as<int>();
    unsigned dtc_s = vm["DTC"].as<unsigned>();

    ERS_DEBUG (0, "partition='" << partition << "' server='" << server << "' dataset='" << dataset
              << "' PRIO=" << prio << " DTC=" << dtc_s);

    /*
     * IPC initialization
     */

    IPCCore::init (argc, argv);

    /*
     * registration
     */

    Register coca (server, partition);

    std::vector<std::string> files = vm["files"].as<std::vector<std::string> >();
    for (const std::string& file: files) {
        ERS_DEBUG (0, "file: " << file);
        std::string archive = coca.registerFile (dataset, file, prio, dtc_s, basedir);
        ERS_INFO ("archive: " << archive);
    }

    return 0;

} catch (const RegisterIssues::Exception& ex) {
    ers::error(LclIssues::RegistrationFailed(ERS_HERE, ex));
    return 1;
} catch (const po::error& ex) {
    std::cerr << ex.what() << "\n"
            "Use --help option for usage information.\n";
    return 1;
} catch (const std::exception& ex) {
    ers::error(LclIssues::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(LclIssues::UncaughtException(ERS_HERE));
    return 1;
}
