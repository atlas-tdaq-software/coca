#ifndef COCA_THREAD_H
#define COCA_THREAD_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <boost/thread.hpp>

//----------------------
// Base Class Headers --
//----------------------


//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_server

/**
 *  @ingroup coca_server
 *
 *  @brief Thread helper class.
 *
 *  This is a thin wrapper class for boost::thread which does graceful shutdown
 *  of the thread in a destructor. This class is non-copyable.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class Thread {
public:

    /// Start thread running given functor
    template <typename F>
    Thread (F f) : m_thread (f) {}

    /// Non-copyable
    Thread(const Thread&) = delete;
    /// Non-assignable
    Thread& operator=(const Thread&) = delete;

    /// Destructor
    ~Thread () {
        m_thread.interrupt();
        // wait until it actually finishes
        m_thread.join();
    }

protected:

private:

    boost::thread m_thread;

};

} // namespace coca
} // namespace daq

#endif // COCA_THREAD_H
