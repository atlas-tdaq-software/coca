#ifndef COCA_DBCLIENT_H
#define COCA_DBCLIENT_H
//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <memory>
#include <chrono>

//----------------------
// Base Class Headers --
//----------------------
#include "coca/client/IDBClient.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/client/ClientIssues.h"
#include "coca/client/RemoteArchive.h"
#include "coca/client/RemoteFile.h"
#include "coca/common/DBBase.h"
#include "coca/common/LineEditor.h"
#include "ers/ers.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_client

/**
 *  @ingroup coca_client
 *
 *  @brief Class providing client API for CoCa database.
 *
 *  This class accesses CoCa database directly and avoids CoCa server
 *  interaction.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */
class DBClient : public IDBClient{
public:

    /**
     *  @brief Constructor which takes CORAL connection string.
     *
     *  @param[in] connStr  CORAL connection string.
     *  @param[in] env      Name of the execution environment, e.g. "p1". "auto"
     *                      will try to guess the environment automatically.
     */
    explicit DBClient(const std::string& connStr = DBBase::defConnStr(), const std::string& env = "auto");

    // non-copyable
    DBClient(const DBClient&) = delete;
    DBClient& operator=(const DBClient&) = delete;

    // Destructor
    virtual ~DBClient() override = default;

    // See base class documentation
    std::vector<std::string> datasets() override;

    // See base class documentation
    std::vector<RemoteArchive> archives(const std::string& dataset = std::string(),
                                        const std::string& archive = std::string(),
                                        const time_point& since = time_point(),
                                        const time_point& until = time_point()) override;

    // See base class documentation
    std::vector<RemoteArchive> archives(const std::string& dataset,
                                        const std::string& archive,
                                        unsigned count,
                                        unsigned skip = 0) override;

    // See base class documentation
    unsigned countFiles(const std::string& dataset = std::string(),
                        const std::string& archive = std::string(),
                        const std::string& file = std::string()) override;

    // See base class documentation
    std::vector<RemoteFile> files(const std::string& dataset = std::string(),
                                  const std::string& archive = std::string(),
                                  const std::string& file = std::string(),
                                  const time_point& since = time_point(),
                                  const time_point& until = time_point()) override;

    // See base class documentation
    std::vector<RemoteFile> files(const std::string& dataset,
                                  const std::string& archive,
                                  const std::string& file,
                                  unsigned count,
                                  unsigned skip = 0) override;

    // See base class documentation
    std::vector<std::string> fileLocations(const std::string& file,
                                           const std::string& dataset,
                                           Location location) override;

    // See base class documentation
    std::vector<std::string> fileLocations(const std::string& file,
                                           const std::string& dataset) override;

    // See base class documentation
    Metadata metadata() override;

private:


    // common implementation of the two files() methods
    std::vector<RemoteFile> _files(const std::string& dataset,
                                   const std::string& archive,
                                   const std::string& file,
                                   const unsigned* count,
                                   const unsigned* skip,
                                   const time_point* since,
                                   const time_point* until);

    // common implementation of the two archives() methods
    std::vector<RemoteArchive> _archives(const std::string& dataset,
                                         const std::string& archive,
                                         const unsigned* count,
                                         const unsigned* skip,
                                         const time_point* since,
                                         const time_point* until);

    // Return line editor, make it if needed first.
    const LineEditor& _line_editor();

    std::unique_ptr<DBBase> m_impl;
    const std::string m_env;
    std::unique_ptr<LineEditor> m_line_editor;

};

} // namespace coca
} // namespace daq

#endif // COCA_DBCLIENT_H
