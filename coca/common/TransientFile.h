#ifndef COCA_TRANSIENTFILE_H
#define COCA_TRANSIENTFILE_H

//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class TransientFile.
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Size.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_common

/**
 *  @ingroup coca_common
 *  @brief Class representing file being copied from client to server.
 *
 *  This class contains information about location and characteristics
 *  of the file both on client side and on Coca server side.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */

class TransientFile  {
public:

    // Default constructor
    TransientFile (const std::string& host,
                   const std::string& clientDir,
                   const std::string& relPath,
                   const std::string& serverDir,
                   const std::string& dataset,
                   Size size,
                   const std::string& checksum);

    // Destructor
    ~TransientFile () throw ();

    /// Returns client host name.
    const std::string& host() const {return m_host;}

    /// Returns directory name on the client host.
    const std::string& clientDir() const {return m_clientDir;}

    /// Returns relative file name.
    const std::string& relPath () const {return m_relPath;}

    /// Returns directory on coca server host.
    const std::string& serverDir() const {return m_serverDir;}

    /// Returns path name on client host.
    std::string clientPath () const ;

    /// Returns path name on coca server.
    std::string serverPath () const ;

    /// Returns CoCa dataset name.
    const std::string& dataset() const {return m_dataset;}

    /// Returns size of the file in MBytes
    Size size () const {return m_size;}

    /// Returns file checksum, could be empty string.
    const std::string& checksum() const {return m_checksum;}

protected:

private:

    // Data members
    std::string m_host;         ///< Client host name
    std::string m_clientDir;    ///< Directory on client host
    std::string m_relPath;      ///< Relative path (w.r.t. m_serverDir)
    std::string m_serverDir;    ///< Directory on coca server host
    std::string m_dataset;      ///< CoCa dataset name
    Size m_size;            ///< File size, bytes
    std::string m_checksum;     ///< File checksum

};

/// Stream insertion operator to print object contents
std::ostream&
operator<<(std::ostream& out, const TransientFile& f);

} // namespace coca
} // namespace daq

#endif // COCA_TRANSIENTFILE_H
