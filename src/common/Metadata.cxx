
//-----------------------
// This Class's Header --
//-----------------------
#include "coca/common/Metadata.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Transaction.h"
#include "RelationalAccess/ConnectionService.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using namespace daq::coca;

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

bool
Metadata::contains(const std::string& key)
{
    Transaction tran(_session());
    return m_metaTable.hasKey(key, tran);
}

std::string
Metadata::get(const std::string& key, const std::string& defVal)
{
    Transaction tran(_session());
    return m_metaTable.get(key, tran, defVal);
}

std::map<std::string, std::string>
Metadata::items()
{
    Transaction tran(_session());
    return m_metaTable.getAll(tran);
}

void
Metadata::set(const std::string& key, const std::string& value)
{
    Transaction tran(_session(true), Transaction::Update);
    m_metaTable.put(key, value, tran);
}

void
Metadata::remove(const std::string& key)
{
    Transaction tran(_session(true), Transaction::Update);
    m_metaTable.remove(key, tran);
}

std::shared_ptr<coral::ISessionProxy>
Metadata::_session(bool update)
{
    coral::ConnectionService conn_serv;
    auto accessMode = update ? coral::Update : coral::ReadOnly;
    coral::ISessionProxy* session = conn_serv.connect (m_connStr, accessMode);
    return std::shared_ptr<coral::ISessionProxy>(session);
}
