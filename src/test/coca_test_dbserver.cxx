#include <string>

#define BOOST_TEST_MODULE coca_test_dbserver
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include "coca/server/CachedFile.h"
#include "coca/server/DBServer.h"
#include "coca/common/Dataset.h"
#include "coca/common/IncomingFile.h"
#include "coca/common/LocalFile.h"
#include "coca/common/TransientFile.h"
#include "coca_test_util.h"

using namespace daq::coca;
using namespace daq::coca::test;
namespace tt = boost::test_tools;
namespace bdata = boost::unit_test::data;


struct SimpleFixture : DBFixture {

    std::string const dataset_name = "Dataset";

    std::string file_name(unsigned i) const { return "file" + std::to_string(i) + ".root"; }

    Dataset dataset() const {
        std::string name = dataset_name;
        unsigned quotaMB = 1;
        unsigned ttlSec = 3600;
        Size minArchiveSize = Size::MB(1);
        Size maxArchiveSize = Size::MB(1000000);
        unsigned maxDelaySec = 300;
        Dataset::BundlePolicy bundlePolicy = Dataset::SingleFile;
        return daq::coca::Dataset(name, quotaMB, ttlSec, minArchiveSize, maxArchiveSize, maxDelaySec, bundlePolicy);
    }

    IncomingFile incoming(int i) const {
        std::string host = "host";
        std::string clientDir = "/client/dir";
        std::string relPath = file_name(i);
        Size size = Size::MB(1);
        int prio = 10;
        unsigned dtcSec = 3600;
        // Use file name as file data
        auto checksum = Checksum::md5_string(relPath);
        BOOST_TEST_MESSAGE("Incoming file " << relPath);
        return IncomingFile(host, clientDir, relPath, size, prio, dtcSec, checksum);
    }
};

template <typename F>
struct MakeServerFixture : F {

    std::unique_ptr<DBServer> make_server(
        std::string const& uri, std::string const& server_ipc,
        std::string const& server_host, std::string const& top_dir) const
    {
        return std::make_unique<DBServer>(
            uri, server_ipc, server_host, top_dir, "cdr-host", "/cdr/dir", "/archive/dir");
    }

};

using Fixture1 = MakeServerFixture<SimpleFixture>;
using Fixture2 = PopulateBDFixture<MakeServerFixture<DBFixture>>;


BOOST_DATA_TEST_CASE_F(Fixture1, test_valid_version, bdata::make({0, 1, 2, 3}), version)
{
    std::string const uri = makeSchema(version);
    auto db = make_server(uri, "server_ipc", "host", "/top/dir");

    auto ds = this->dataset();

    // add a bunch of files
    unsigned const N = 10;
    for (unsigned i = 0; i < N; ++ i) {
        auto file = this->incoming(i);
        db->addNewFile(file, ds);
    }

    // all of them have to be retrieved.
    BOOST_TEST(db->filesToRetrieve().size() == N);

    // mark few of them as retrieved
    for (unsigned i = 0; i < N/2; ++ i) {
        db->removeFromClient(dataset_name, file_name(i));
    }
    BOOST_TEST(db->filesToRetrieve().size() == (N + 1)/2);

    // mark remaining as retrieved
    for (unsigned i = N/2; i < N; ++ i) {
        db->removeFromClient(dataset_name, file_name(i));
    }
    BOOST_TEST(db->filesToRetrieve().empty());

    // none is marked as in cache yet
    BOOST_TEST(db->archivesToCDR().empty());

    // add all to cache
    for (unsigned i = 0; i < N; ++ i) {
        db->addToCache(dataset_name, file_name(i), Size::MB(1));
    }

    std::vector<std::string> archives;
    for (auto&& pair: db->archivesToCDR()) {
        archives.push_back(pair.first);
    }
    BOOST_TEST(archives.size() == N);

    // send some of the files to CDR
    for (unsigned i = 0; i < N / 2; ++ i) {
        db->setArchiveOnCDR(archives[i]);
    }
    BOOST_TEST(db->archivesToCDR().size() == N/2);
    BOOST_TEST(db->archivesNotArchived().size() == N / 2);
    BOOST_TEST(db->filesToDelete().size() == N / 2);

    // mark them as archived
    for (auto&& archive: db->archivesNotArchived()) {
        db->setArchived(archive);
    }
    BOOST_TEST(db->filesToDelete().size() == N / 2);

    // send remaining to CDR
    for (unsigned i = N / 2; i < N; ++ i) {
        db->setArchiveOnCDR(archives[i]);
    }
    BOOST_TEST(db->archivesToCDR().empty());
    BOOST_TEST(db->archivesNotArchived().size() == N / 2);
    BOOST_TEST(db->filesToDelete().size() == N);

    // mark as archived
    for (auto&& archive: db->archivesNotArchived()) {
        db->setArchived(archive);
    }
    BOOST_TEST(db->archivesNotArchived().empty());
    BOOST_TEST(db->filesToDelete().size() == N);

    // mark all as deleted
    for (auto&& archive: archives) {
        db->setRemoved(archive);
    }

    // remove all from cache
    for (unsigned i = 0; i < N; ++ i) {
        db->removeFromCache(dataset_name, file_name(i));
    }
}

BOOST_DATA_TEST_CASE_F(Fixture1, test_duplicate_file, bdata::make({0, 1, 2, 3}), version)
{
    std::string const uri = makeSchema(version);
    auto db = make_server(uri, "server_ipc", "host", "/top/dir");

    auto ds = this->dataset();

    // first registration is OK
    auto file = this->incoming(0);
    db->addNewFile(file, ds);

    // second is not
    BOOST_REQUIRE_THROW(db->addNewFile(file, ds), Issues::FileAlreadyRegistered);
}

BOOST_FIXTURE_TEST_CASE(test_wrong_version, DBFixture)
{
    // make schema version which will never exist
    std::string const uri = makeSchema(999);

    BOOST_REQUIRE_THROW(
        DBServer db(uri, "server_ipc", "host", "/top/dir", "cdr_host", "/cdr/dir", "/archive/dir"),
        Issues::UnsupportedSchemaVersionError
    );
}

// Test that cached files can be cleared by servers that share the cache
BOOST_DATA_TEST_CASE_F(Fixture2, test_shared_cache, bdata::make({0, 1, 2, 3}), version)
{
    std::string const uri = makeSchema(version);
    MockFileSystem fs(Size::MB(1024));
    std::string topCacheDir = "/data/coca/cache";

    auto server1 = make_server(uri, "tdaq-08-03-01/coca", "server_host1", topCacheDir);
    auto server2 = make_server(uri, "tdaq-09-03-00/coca", "server_host1", topCacheDir);
    auto server3 = make_server(uri, "tdaq-09-03-00/coca", "server_host2", topCacheDir);

    this->populateDB(*server1, "file1", fs, topCacheDir);

    auto files = server1->filesToDelete(true);
    BOOST_TEST(files.size() == this->expectedNumFiles());
    files = server1->filesToDelete(false);
    BOOST_TEST(files.size() == this->expectedNumFiles());

    this->populateDB(*server2, "file2", fs, topCacheDir);

    // server1 and server2 are on the same host
    files = server1->filesToDelete(true);
    BOOST_TEST(files.size() == this->expectedNumFiles());
    files = server1->filesToDelete(false);
    BOOST_TEST(files.size() == 2 * this->expectedNumFiles());
    files = server2->filesToDelete(true);
    BOOST_TEST(files.size() == this->expectedNumFiles());
    files = server2->filesToDelete(false);
    BOOST_TEST(files.size() == 2 * this->expectedNumFiles());

    this->populateDB(*server3, "file3", fs, topCacheDir);

    // server3 is on a different host
    files = server1->filesToDelete(true);
    BOOST_TEST(files.size() == this->expectedNumFiles());
    files = server1->filesToDelete(false);
    BOOST_TEST(files.size() == 2 * this->expectedNumFiles());
    files = server2->filesToDelete(true);
    BOOST_TEST(files.size() == this->expectedNumFiles());
    files = server2->filesToDelete(false);
    BOOST_TEST(files.size() == 2 * this->expectedNumFiles());
    files = server3->filesToDelete(true);
    BOOST_TEST(files.size() == this->expectedNumFiles());
    files = server3->filesToDelete(false);
    BOOST_TEST(files.size() == this->expectedNumFiles());
}
