#define BOOST_TEST_MODULE coca_test_str2id
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "coca/common/Issues.h"
#include "coca/common/NextId.h"
#include "coca/common/Str2Id.h"
#include "coca/common/Transaction.h"
#include "coca_test_util.h"

using namespace daq::coca;
using namespace daq::coca::test;
namespace tt = boost::test_tools;


BOOST_FIXTURE_TEST_CASE(test_strid, EmptyDBFixture)
{
    NextId nextid("COCA_NEXTID");
    Str2Id str2id("COCA_STR2ID", nextid);
    {
        Transaction tr(session, Transaction::Update);
        int schema_version = 0;
        nextid.createTable(schema_version, tr);
        str2id.createTable(schema_version, tr);
    }

    struct {
        const char* str;
        unsigned id;
    } tests_insert[] = {
        {"", 0},
        {"A", 1},
        {"A", 1},
        {"B", 2},
        {"C", 3},
    };

    for (auto& test: tests_insert) {
        Transaction tr(session, Transaction::Update);
        unsigned id = str2id.insert(test.str, tr);
        BOOST_TEST(id == test.id);
    }

    struct {
        const char* str;
        unsigned id;
    } tests_getid[] = {
        {"", 0},
        {"A", 1},
        {"B", 2},
        {"C", 3},
    };

    for (auto& test: tests_getid) {
        Transaction tr(session, Transaction::Read);
        unsigned id = str2id.getId(test.str, tr);
        BOOST_TEST(id == test.id);
    }

    {
        Transaction tr(session, Transaction::Read);
        BOOST_REQUIRE_THROW(str2id.getId("XYZ", tr), Issues::NoSuchRecord);
    }

    for (auto& test: tests_getid) {
        Transaction tr(session, Transaction::Read);
        auto str = str2id.getStr(test.id, tr);
        BOOST_TEST(str == test.str);
    }

    {
        Transaction tr(session, Transaction::Read);
        BOOST_REQUIRE_THROW(str2id.getStr(123, tr), Issues::NoSuchRecord);
    }

    {
        // Another instance with false for prefill flag
        Str2Id str2id2("COCA_STR2ID", nextid, false);

        Transaction tr(session, Transaction::Read);

        for (auto& test: tests_getid) {
            unsigned id = str2id2.getId(test.str, tr);
            BOOST_TEST(id == test.id);
        }

        for (auto& test: tests_getid) {
            auto str = str2id2.getStr(test.id, tr);
            BOOST_TEST(str == test.str);
        }
    }

    {
        // Another instance with true for prefill flag
        Str2Id str2id2("COCA_STR2ID", nextid, true);

        Transaction tr(session, Transaction::Read);

        for (auto& test: tests_getid) {
            unsigned id = str2id2.getId(test.str, tr);
            BOOST_TEST(id == test.id);
        }

        for (auto& test: tests_getid) {
            auto str = str2id2.getStr(test.id, tr);
            BOOST_TEST(str == test.str);
        }
    }
}
