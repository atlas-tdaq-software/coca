
#include <map>
#include <string>
#include <boost/filesystem.hpp>

#include "coca/common/Checksum.h"
#include "coca/common/FileSystem.h"


namespace daq {
namespace coca {
namespace test {

/**
 * @ingroup coca_common
 *
 * @brief Implementation of FileSystemABC using standard filesystem operation.
 */
class MockFileSystem : public FileSystemABC {
public:

    MockFileSystem(Size capacity) : m_capacity(capacity) {}
    MockFileSystem(const MockFileSystem&) = delete;
    MockFileSystem& operator=(const MockFileSystem&) = delete;

    ~MockFileSystem() {}

    void add_file(const boost::filesystem::path& path, Size size, const std::string& checksum) {
        create_dirs(path.parent_path());
        if (Size(space(path).available) < size) {
            throw FileSystem::Exception(ERS_HERE);
        }
        m_paths[path] = {boost::filesystem::regular_file, size, checksum};
    }

    void add_dir(const boost::filesystem::path& path) {
        Size size(4096);
        if (Size(space(path).available) < size) {
            throw FileSystem::Exception(ERS_HERE);
        }
        m_paths[path] = {boost::filesystem::directory_file, size, std::string()};
    }

    // See base class for documentation
    boost::filesystem::file_status status(const boost::filesystem::path& path) override {
        boost::filesystem::file_type type = boost::filesystem::file_not_found;
        auto iter = m_paths.find(path);
        if (iter != m_paths.end()) {
            type = iter->second.type;
        }
        return boost::filesystem::file_status(type);
    }

    // See base class for documentation
    bool is_directory(const boost::filesystem::path& path) override {
        auto iter = m_paths.find(path);
        if (iter != m_paths.end()) {
            return iter->second.type == boost::filesystem::directory_file;
        }
        return false;
    }

    // See base class for documentation
    bool is_regular_file(const boost::filesystem::path& path) override {
        auto iter = m_paths.find(path);
        if (iter != m_paths.end()) {
            return iter->second.type == boost::filesystem::regular_file;
        }
        return false;
    }

    // See base class for documentation
    bool remove(const boost::filesystem::path& path, bool nothrow=false) override {
        auto iter = m_paths.find(path);
        if (iter != m_paths.end()) {
            if (iter->second.type == boost::filesystem::regular_file) {
                m_paths.erase(iter);
                return true;
            }
            if (nothrow) {
                return false;
            } else {
                throw FileSystem::Exception(ERS_HERE);
            }
        }
        return false;
    }

    // See base class for documentation
    void rename(const boost::filesystem::path& path, const boost::filesystem::path& newPath) override {
        auto iter = m_paths.find(path);
        if (iter != m_paths.end()) {
            auto fileData = iter->second;
            m_paths.erase(iter);
            m_paths[newPath] = fileData;
            return;
        }
        throw FileSystem::Exception(ERS_HERE);
    }

    // See base class for documentation
    Size file_size(const boost::filesystem::path& path) override {
        auto iter = m_paths.find(path);
        if (iter != m_paths.end()) {
            if (iter->second.type == boost::filesystem::regular_file) {
                return iter->second.file_size;
            }
        }
        throw FileSystem::Exception(ERS_HERE);
    }

    // See base class for documentation
    boost::filesystem::space_info space(const boost::filesystem::path& path) override {
        Size::value_type available = m_capacity.bytes();
        for (auto& entry: m_paths) {
            available -= entry.second.file_size.bytes();
        }
        return {m_capacity.bytes(), available, available};
    }

    // See base class for documentation
    dev_t device_id(const boost::filesystem::path& path) override {
        return dev_t(1);
    }

    // See base class for documentation
    bool create_dirs(const boost::filesystem::path& path) override {
        if (path.empty()) {
            return false;
        }
        auto iter = m_paths.find(path);
        if (iter != m_paths.end()) {
            return false;
        }
        // all all parent dirs recursively
        create_dirs(path.parent_path());
        add_dir(path);
        return true;
    }

    // See base class for documentation
    std::string checksum(const boost::filesystem::path& path) override {
        auto iter = m_paths.find(path);
        if (iter != m_paths.end()) {
            if (iter->second.type == boost::filesystem::regular_file) {
                return iter->second.checksum;
            }
        }
        throw FileSystem::Exception(ERS_HERE);
    }

    size_t count_files() const {
        return std::count_if(m_paths.begin(), m_paths.end(), [](auto& entry) {
            return entry.second.type == boost::filesystem::regular_file;
        });
    }

private:

    struct FileData {
        boost::filesystem::file_type type;
        Size file_size;
        std::string checksum;
    };

    std::map<boost::filesystem::path, FileData> m_paths;
    Size m_capacity;

};

}}} // namespace daq::coca::test
