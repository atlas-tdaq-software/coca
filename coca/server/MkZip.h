#ifndef COCA_MKZIP_H
#define COCA_MKZIP_H
//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/LocalFile.h"
#include "coca/server/FreeSpace.h"
#include "coca/server/NotifyFlag.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class OWLSemaphore;
namespace daq {
namespace coca {
class DBServer;
class FileSystemABC;
}
}


namespace daq {
namespace coca {

/// @addtogroup coca_server

/**
 *  @ingroup coca_server
 *
 *  @brief Thread that creates archives from cached files.
 *
 *  This class represents a thread which watches for the state
 *  of the archives and packs cached files into archive when archive
 *  is closed.
 */
class MkZip {
public:

    /**
     *  Constructor starts the thread.
     *
     *  @param[in] db       Database object.
     *  @param[in] dirCDR   Location of the CDR directory.
     *  @param[in] notifyUp   Communication channel from upstream thread.
     *  @param[in] notifyDown Communication channel from downstream thread.
     *  @param[in] freeSpace  Free space manager object.
     *  @param[in] semaphore  Semaphore to be raised when this thread exits.
     *  @param[in] filesystem Filesystem interface.
     *
     *  @throw boost::thread_resource_error if thread fails to launch.
     */
    MkZip (DBServer& db, const std::string& dirCDR, const NotifyFlag& notifyUp,
            const NotifyFlag& notifyDown, const FreeSpace& freeSpace,
            OWLSemaphore& semaphore, FileSystemABC& filesystem);

    /// operator called by thread
    void operator()();

private:

    void copyFile (const std::string& file,
                   const std::string& archive);
    void mkZip (const std::vector<LocalFile>& files,
                const std::string& archive);

    // typedef for map which counts unsuccessful attempts to locate a file
    typedef std::pair<std::string, std::string> DatasetAndRelPath;
    typedef std::map<DatasetAndRelPath, int> AttemptCount;

    DBServer& m_db;
    std::string m_dirCDR;
    NotifyFlag m_notifyUp;
    NotifyFlag m_notifyDown;
    FreeSpace m_freeSpace;
    OWLSemaphore& m_semaphore;
    FileSystemABC& m_filesystem;
    const int m_retries;
    AttemptCount m_attemptCounts;
};

} // namespace coca
} // namespace daq

#endif // COCA_MKZIP_H
