#ifndef COCA_GETFILES_H
#define COCA_GETFILES_H
//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/server/NotifyFlag.h"
#include "coca/server/FreeSpace.h"
#include "coca/server/ServerIssues.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
class OWLSemaphore;
namespace daq {
namespace coca {
class DBServer;
class FileSystemABC;
class TransientFile;
}
}

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace coca {

 /// @addtogroup coca_server

 /**
  *  @ingroup coca_server
  *
  *  @brief Threads which moves files from clients to server.
  *
  *  This class represents a thread which watches new requests to copy
  *  the registered files and then moves the files fromclient host to
  *  server cache directory using rsync over ssh.
  */

class GetFiles {
public:

    /**
     *  Constructor starts the thread.
     *
     *  @param[in] db         Database object.
     *  @param[in] notifyUp   Communication channel from upstream thread.
     *  @param[in] notifyDown Communication channel from downstream thread.
     *  @param[in] freeSpace  Free space manager object.
     *  @param[in] semaphore  Semaphore to be raised when this thread exits.
     *  @param[in] retries    Number of retries for a file before declaring
     *                        file inaccessible.
     *  @param[in] filesystem Filesystem interface.
     *
     *  @throw boost::thread_resource_error if thread fails to launch.
     */
    GetFiles (DBServer& db, const NotifyFlag& notifyUp, const NotifyFlag& notifyDown,
              const FreeSpace& freeSpace, OWLSemaphore& semaphore, int retries,
              FileSystemABC& filesystem);

    /// operator called by thread
    void operator()();

private:

    bool FreeSpace4 (const TransientFile& file);
    void GetRemoteFile (const TransientFile& file);

    // typedef for map which counts unsuccessful copy attempts
    typedef std::pair<std::string, std::string> DatasetAndRelPath;
    typedef std::map<DatasetAndRelPath, int> CopyAttemptCount;

    DBServer& m_db;
    NotifyFlag m_notifyUp;
    NotifyFlag m_notifyDown;
    FreeSpace m_freeSpace;
    OWLSemaphore& m_semaphore;
    const int m_retries;
    FileSystemABC& m_filesystem;
    CopyAttemptCount m_attemptCounts;
};

} // namespace coca
} // namespace daq

#endif // COCA_GETFILES_H
