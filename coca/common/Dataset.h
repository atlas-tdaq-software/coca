#ifndef COCA_DATASET_H
#define COCA_DATASET_H
//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <iosfwd>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Size.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_common

/**
 * @ingroup coca_common
 * @brief Class describing single CoCa dataset.
 */

class Dataset {
public:

    /// Enumeration for possible bundling policy values
    enum BundlePolicy {
        SingleFile,    /**< SingleFile: archive files individually in subdirectories
                            year/dataset/file-name.ext */
        ZipBundle,     /**< large files without slashes in names are archived
                            individually with names coca_dataset_file-name.ext, smaller
                            files are packed into ZIP archives with name coca_dataset_NNNN.zip */
        ZipBundleDir   /**< small files are packed into ZIP archives with
                            names year/dataset/coca_dataset_NNNN.zip, large files are
                            archived individually with names year/dataset/file-name.ext */
    };

    /**
     *  @brief Constructor takes all dataset attributes.
     */
    Dataset (const std::string& name,
             unsigned quotaMB,
             unsigned ttlSec,
             Size minArchiveSize,
             Size maxArchiveSize,
             unsigned maxDelaySec,
             BundlePolicy bundlePolicy);

    /**
     *  @brief Get the name of this dataset.
     */
    const std::string& name () const {return m_name;}

    /**
     *  @brief Get dataset quota in MBytes.
     */
    unsigned quotaMB () const {return m_quotaMB;}

    /**
     *  @brief Get time-to-live in seconds for the files in dataset.
     */
    unsigned ttlSec () const {return m_ttlSec;}

    /**
     *  @brief Get the minimum size of the dataset in MBytes.
     *
     *  Archives below this size will not be sent to CDR unless
     *  maxDelay is exceeded.
     */
    Size minArchiveSize () const {return m_minArchiveSize;}

    /**
     *  @brief Get the maximum size of the dataset in MBytes.
     *
     *  Archives above this size will be sent to CDR immediately,
     *  individual files above this size will not be packed but saved
     *  individually.
     */
    Size maxArchiveSize () const {return m_maxArchiveSize;}

    /**
     *  @brief Get maximum delay for files to be sent to CDR.
     *
     *  Files that sit in cache longer than this delay will be packed and
     *  sent to CDR even if archive size is below limit.
     */
    unsigned maxDelaySec () const { return m_maxDelaySec; }

    /**
     *  @brief Get bundling policy for a dataset.
     */
    BundlePolicy bundlePolicy() const { return m_bundlePolicy; }

private:

    std::string m_name;          ///< Dataset name
    unsigned m_quotaMB;          ///< Quota on this server
    unsigned m_ttlSec;           ///< Time To Live (sec)
    Size m_minArchiveSize;       ///< Minimum archive size
    Size m_maxArchiveSize;       ///< Maximum archive size
    unsigned m_maxDelaySec;      ///< Maximum delay (sec) before archives being pushed to CDR
    BundlePolicy m_bundlePolicy; ///< Defines bundling policy for dataset
};

// stream insertion/extraction
std::ostream&
operator<<(std::ostream& str, Dataset::BundlePolicy pol);
std::istream&
operator>>(std::istream& str, Dataset::BundlePolicy &pol);

} // namespace coca
} // namespace daq

#endif // COCA_DATASET_H
