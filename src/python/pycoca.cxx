//
// @file $Id$
//
// @author Andy Salnikov
//
// @brief Boost.Python interface to the mda library class
//

// C++ includes
#include <set>
#include <string>
#include <sstream>
#include <time.h>
#include <boost/python.hpp>
#include <boost/python/raw_function.hpp>
#include <stdlib.h>

// Related classes
#include "coca/coca.h"
#include "coca/client/DBStat.h"
#include "ers/Issue.h"
#include "ipc/core.h"

using namespace std::chrono;
using namespace daq;
using namespace daq::coca;
using namespace boost::python;
namespace boopy = boost::python;

namespace {

// Wrapper for RemoteFile to avoid exposing Size class to Python
class PyRemoteFile {
public:

    typedef std::vector<FileLocation> FileLocations;

    /**
     *  @brief Constructor
     */
    PyRemoteFile(const RemoteFile& remoteFile) : m_remoteFile(remoteFile) {}

    /// Returns dataset name.
    const std::string& dataset () const { return m_remoteFile.dataset(); }

    /// Returns relative path name.
    const std::string& relPath () const  { return m_remoteFile.relPath(); }

    /// Returns list of the file locations.
    const FileLocations& locations() const { return m_remoteFile.locations(); }

    /// Returns corresponding archive name.
    const std::string& archive () const { return m_remoteFile.archive(); }

    /// Returns file size in MBytes.
    uint64_t size () const { return m_remoteFile.size().bytes(); }

    /// Returns file checksum, could be empty string.
    const std::string& checksum() const { return m_remoteFile.checksum(); }

protected:

private:

    RemoteFile m_remoteFile;
};


// ================================================================
//    Class PyDBClient
// ================================================================
//
// Boost.Python-friendly wrapper for DBRead class.
//
class PyDBClient {
public:

  typedef system_clock::time_point time_point;

  // constructor
  PyDBClient(const std::string& connStr = DBBase::defConnStr(), const std::string& env = "auto") ;

  /// Get the list of datasets in the database, returns a list of strings.
  boopy::list datasets () ;

  // Get the list of archives, this is a special Python-like method that accepts
  // python objects instead of converted values as we want to do keyword parsing
  // ourselves
  static boopy::list archives(tuple args, dict kw);

  // Get the list of archives
  boopy::list archives_(const std::string& dataset,
          const std::string& archive,
          const time_point& since,
          const time_point& until) ;

  // Get the list of archives
  boopy::list archives_(const std::string& dataset,
          const std::string& archive,
          unsigned count,
          unsigned skip) ;

  // Get the number of files.
  unsigned countFiles(const std::string& dataset = std::string(),
          const std::string& archive = std::string(),
          const std::string& file = std::string());

  // Get the list of files, this is a special Python-like method that accepts
  // python objects instead of converted values as we want to do keyword parsing
  // ourselves
  static boopy::list files(tuple args, dict kw);

  // Get the list of files
  boopy::list files_(const std::string& dataset,
          const std::string& archive,
          const std::string& file,
          const time_point& since,
          const time_point& until) ;

  // Get the list of files
  boopy::list files_(const std::string& dataset,
          const std::string& archive,
          const std::string& file,
          unsigned count,
          unsigned skip) ;

  // Get the list of known file locations.
  boopy::list fileLocations(const std::string& file,
                            const std::string& dataset,
                            DBClient::Location loc = DBClient::LocAll);

  Metadata metadata() { return m_client.metadata(); }

private:

  DBClient m_client;

};

// constructor
PyDBClient::PyDBClient(const std::string& connStr, const std::string& env)
  : m_client(connStr, env)
{
}

/// Get the list of datasets in the database, returns a list of strings.
boopy::list
PyDBClient::datasets ()
{
  auto datasets = m_client.datasets();
  boopy::list res ;
  for (const auto& str: datasets) {
    res.append(boopy::str(str.c_str()));
  }
  return res;
}


boopy::list
PyDBClient::archives(tuple args, dict kw)
{
    if (boopy::len(args) > 3) {
        PyErr_SetString(PyExc_ValueError, "too many arguments");
        boost::python::throw_error_already_set();
    }

    boopy::object self = args[0];
    PyDBClient& this_ = boopy::extract<PyDBClient&>(args[0]);

    // positional args
    std::string dataset, archive;
    if (boopy::len(args) > 1) dataset = boopy::extract<std::string>(args[1]);
    if (boopy::len(args) > 2) archive = boopy::extract<std::string>(args[2]);

    // check kw args
    if ((kw.has_key("since") or kw.has_key("until")) and (kw.has_key("count") or kw.has_key("skip"))) {
        PyErr_SetString(PyExc_ValueError, "kw args 'number' and 'skip' are not compatible with 'since' and 'until'");
        boost::python::throw_error_already_set();
    }

    if (kw.has_key("since") or kw.has_key("until")) {
        // these kw args hold time in seconds, convert them to Time
        time_point since, until;
        if (kw.has_key("since")) {
            unsigned long sec = boopy::extract<unsigned long>(kw.get("since"));
            since = system_clock::from_time_t(sec);
        }
        if (kw.has_key("until")) {
            unsigned long sec = boopy::extract<unsigned long>(kw.get("until"));
            until = system_clock::from_time_t(sec);
        }
        return this_.archives_(dataset, archive, since, until);
    } else {
        unsigned count=100, skip=0;
        if (kw.has_key("count")) count = boopy::extract<unsigned>(kw.get("count"));
        if (kw.has_key("skip")) skip = boopy::extract<unsigned>(kw.get("skip"));
        return this_.archives_(dataset, archive, count, skip);
    }
}


// Get the list of archives
boopy::list
PyDBClient::archives_(const std::string& dataset,
                      const std::string& archive,
                      const time_point& since,
                      const time_point& until)
{
  auto archives = m_client.archives(dataset, archive, since, until);

  boopy::list res ;
  for (const auto& ar: archives) {
    res.append(ar);
  }
  return res;
}

boopy::list
PyDBClient::archives_(const std::string& dataset,
                      const std::string& archive,
                      unsigned count,
                      unsigned skip)
{
  auto archives = m_client.archives(dataset, archive, count, skip);

  boopy::list res ;
  for (const auto& ar: archives) {
    res.append(ar);
  }
  return res;
}

unsigned
PyDBClient::countFiles(const std::string& dataset,
        const std::string& archive,
        const std::string& file)
{
    return m_client.countFiles(dataset, archive, file);
}

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(countFiles_overloads, countFiles, 0, 3)

boopy::list
PyDBClient::files(tuple args, dict kw)
{
    if (boopy::len(args) > 4) {
        PyErr_SetString(PyExc_ValueError, "too many arguments");
        boost::python::throw_error_already_set();
    }

    boopy::object self = args[0];
    PyDBClient& this_ = boopy::extract<PyDBClient&>(args[0]);

    // positional args
    std::string dataset, archive, file;
    if (boopy::len(args) > 1) dataset = boopy::extract<std::string>(args[1]);
    if (boopy::len(args) > 2) archive = boopy::extract<std::string>(args[2]);
    if (boopy::len(args) > 3) file = boopy::extract<std::string>(args[3]);

    // check kw args
    if ((kw.has_key("since") or kw.has_key("until")) and (kw.has_key("count") or kw.has_key("skip"))) {
        PyErr_SetString(PyExc_ValueError, "kw args 'number' and 'skip' are not compatible with 'since' and 'until'");
        boost::python::throw_error_already_set();
    }

    if (kw.has_key("since") or kw.has_key("until")) {
        // these kw args hold time in seconds, convert them to Time
        time_point since, until;
        if (kw.has_key("since")) {
            unsigned long sec = boopy::extract<unsigned long>(kw.get("since"));
            since = system_clock::from_time_t(sec);
        }
        if (kw.has_key("until")) {
            unsigned long sec = boopy::extract<unsigned long>(kw.get("until"));
            until = system_clock::from_time_t(sec);
        }
        return this_.files_(dataset, archive, file, since, until);
    } else {
        unsigned count=100, skip=0;
        if (kw.has_key("count")) count = boopy::extract<unsigned>(kw.get("count"));
        if (kw.has_key("skip")) skip = boopy::extract<unsigned>(kw.get("skip"));
        return this_.files_(dataset, archive, file, count, skip);
    }
}



// Get the list of files
boopy::list
PyDBClient::files_(const std::string& dataset,
                   const std::string& archive,
                   const std::string& file,
                   const time_point& since,
                   const time_point& until)
{
  auto files = m_client.files(dataset, archive, file, since, until);

  boopy::list res ;
  for (const auto& file: files) {
    res.append(PyRemoteFile(file));
  }
  return res;
}

// Get the list of files
boopy::list
PyDBClient::files_(const std::string& dataset,
                   const std::string& archive,
                   const std::string& file,
                   unsigned count,
                   unsigned skip)
{
  auto files = m_client.files(dataset, archive, file, count, skip);

  boopy::list res ;
  for (const auto& file: files) {
    res.append(PyRemoteFile(file));
  }
  return res;
}

// Get the list of known file locations.
boopy::list
PyDBClient::fileLocations(const std::string& file,
                          const std::string& dataset,
                          DBClient::Location loc)
{
    auto locations = m_client.fileLocations(file, dataset, loc);

    boopy::list res ;
    for (const auto& str: locations) {
      res.append(str);
    }
    return res;
}

// generate all overloads for default arguments
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(fileLocations_overloads, fileLocations, 2, 3)

// ================================================================
//    Class PyRegister
// ================================================================

//
// Boost.Python-friendly wrapper for Register class.
//
class PyRegister {
public:

  // constructor
  PyRegister(const std::string& server, const std::string& partition);

  /**
   *  @brief Returns server configuration as arbitrary formatted string.
   *
   *  @throw RegisterIssues::IPCException Thrown for server communication errors.
   */
  std::string configuration() { return m_reg->configuration(); }

  /**
   *  @brief Register new file with CoCa.
   *
   *  @param[in] dataset  Name of dataset.
   *  @param[in] file     File name to register.
   *  @param[in] prio     Priority value, accepted range is 0-100
   *  @param[in] dtcSec   Desired Time in Cache in seconds.
   *  @param[in] baseDir  Directory on client side where file is located.
   *  @return The name of the archive for the newly registered file.
   *
   *  @throw RegisterIssues::MissingFile         Thrown if file is missing or inaccessible.
   *  @throw RegisterIssues::RegistrationRefused Thrown if server does not accept new registrations.
   *  @throw RegisterIssues::RegistrationError   Thrown if server fails to register it.
   *  @throw RegisterIssues::IPCException        Thrown for server communication errors.
   */
  std::string registerFile(const std::string& dataset,
                           const std::string& file,
                           int prio,
                           unsigned dtcSec,
                           const std::string& baseDir)
  {
    // call C++ methods
    return m_reg->registerFile(dataset, file, prio, dtcSec, baseDir);
  }


private:

  Register* m_reg;

};


PyRegister::PyRegister(const std::string& server, const std::string& partition)
  : m_reg(0)
{
  // initialize IPC if not initialized
  if (not IPCCore::isInitialised()) {

    // workaround for exception generated in ipcproxyicp
    putenv((char*)"TDAQ_IPC_CLIENT_INTERCEPTORS=ipcclicp");

    char* argv[] = { (char*)"pycoca", 0 };
    int argc = 1;

    IPCCore::init(argc, argv);
  }

  m_reg = new Register(server, partition);
}

// ================================================================
//    Class PyDBStat
// ================================================================

//
// Boost.Python-friendly wrapper for StatFiles class.
//
class PyStatFiles {
public:

  /// Constructor takes number of files and their total size
  PyStatFiles (unsigned count, uint64_t size)
      : m_count(count), m_size(size) {}

  /// Returns number of saved files
  unsigned count() const { return m_count; }

  /// Returns size of saved files
  uint64_t size() const { return m_size; }

protected:

private:

  unsigned m_count;
  uint64_t m_size;

};



inline
std::ostream&
operator<<(std::ostream &str, const PyStatFiles& stat)
{
  return str << "StatFiles(count=" << stat.count()
      << ", size=" << stat.size() << ")" ;
}

//
// Boost.Python-friendly wrapper for DBStat class.
//
class PyDBStat {
public:
  // constructor
  PyDBStat(const std::string& connStr = DBBase::defConnStr()) ;

  /**
   *  @brief Returns per-time interval statistics
   *
   *  @param[in] begin  Start of the time interval
   *  @param[in] end    End of the time interval
   *  @param[in] interval  Interval period,
   *  @return Dictionary, the key is timestamp (beginning of interval) the value
   *          is StatFiles objects.
   */
  boopy::dict intervalStat(double begin, double end, DBStat::Interval interval);

  /**
   *  @brief Returns per-dataset statistics
   *
   *  @param[in] cached If true then information about cached files only will be
   *                  returned, otherwise all files.
   *  @return Dictionary, mapping with the name of dataset as a key and
   *          StatFiles objects for values.
   */
  boopy::dict datasetStat(bool cached);

private:

  DBStat m_db;

};


PyDBStat::PyDBStat(const std::string& connStr)
  : m_db(connStr)
{
}

boopy::dict
PyDBStat::intervalStat(double begin, double end, DBStat::Interval interval)
{
  // convert time
  DBStat::time_point b = std::chrono::system_clock::from_time_t((time_t(begin)));
  DBStat::time_point e = std::chrono::system_clock::from_time_t((time_t(end)));

  // call C++ method
  std::map<DBStat::time_point, StatFiles> stat;
  m_db.intervalStat(stat, b, e, interval);

  // convert the result to Python dict
  boopy::dict result;
  for (const auto& pair: stat) {
    double ts = std::chrono::system_clock::to_time_t(pair.first);
    result[ts] = PyStatFiles(pair.second.count(), pair.second.size().bytes());
  }
  return result;
}

boopy::dict
PyDBStat::datasetStat(bool cached)
{
  // call C++ method
  std::map<std::string, StatFiles> stat;
  m_db.datasetStat(stat, cached);

  // convert the result to Python dict
  boopy::dict result;
  for (const auto& pair: stat) {
    result[pair.first] = PyStatFiles(pair.second.count(), pair.second.size().bytes());
  }
  return result;
}


// ================================================================

//
void exception_translator(const std::exception& exc)
{
  PyErr_SetString(PyExc_RuntimeError, exc.what());
}

void issue_translator(const ers::Issue& exc)
{
    std::string msg = exc.message();
    for (auto cause = exc.cause(); cause != nullptr; cause = cause->cause()) {
        msg += "\n    caused by: ";
        msg += cause->message();
    }
    PyErr_SetString(PyExc_RuntimeError, msg.c_str());
}


// special converter object for C++ vector
template<class T>
struct VectorToListConverter {

  static PyObject* convert(const std::vector<T>& seq)
  {
    boopy::list res ;
    for (const auto& v: seq) {
      res.append(v);
    }
    return incref(res.ptr());
  }

  static PyTypeObject const* get_pytype()
  {
      return &PyList_Type;
  }
};

std::string FileLocation_str(const FileLocation& f)
{
    std::ostringstream str;
    str << "FileLocation('" << f.host() << ":" << f.path() << "')";
    return str.str();
}

std::string RemoteArchive_str(const RemoteArchive& ar)
{
    std::ostringstream str;
    str << "RemoteArchive(dataset='" << ar.dataset() << "', name='" << ar.name() << "')";
    return str.str();
}

std::string PyRemoteFile_str(const PyRemoteFile& f)
{
    std::ostringstream str;
    str << "RemoteFile(dataset='" << f.dataset() << "', path='" << f.relPath() << "')";
    return str.str();
}

boopy::dict metadata_items(Metadata& m)
{
    boopy::dict items;
    for (auto&& pair: m.items()) {
      items[pair.first] = pair.second;
    }
    return items;
}

} // namespace

BOOST_PYTHON_MODULE(libpycoca)
{
  // handle exceptions, order is important
  register_exception_translator<std::exception>(exception_translator);
  register_exception_translator<ers::Issue>(issue_translator);

  // register special converter
  to_python_converter<std::vector<FileLocation>, VectorToListConverter<FileLocation>, true>();

  // constants
  scope().attr("defConnStr") = DBBase::defConnStr();

  // wrapper for Location class
  class_<FileLocation>("FileLocation", init<std::string, std::string>(args("host", "path")))
      .def("host", &FileLocation::host, return_value_policy<copy_const_reference>())
      .def("path", &FileLocation::path, return_value_policy<copy_const_reference>())
      .def("__str__", &FileLocation_str)
      .def("__repr__", &FileLocation_str)
      ;

  // wrapper for RemoteArchive class
  class_<RemoteArchive>("RemoteArchive", no_init)
      .def("dataset", &RemoteArchive::dataset, return_value_policy<copy_const_reference>())
      .def("locations", &RemoteArchive::locations, return_value_policy<copy_const_reference>(),
           "Returns the list of FileLocation objects.")
      .def("name", &RemoteArchive::name, return_value_policy<copy_const_reference>())
      .def("archivePath", &RemoteArchive::archivePath, return_value_policy<copy_const_reference>())
      .def("__str__", &RemoteArchive_str)
      .def("__repr__", &RemoteArchive_str)
      ;

  // wrapper for RemoteFile class
  class_<PyRemoteFile>("RemoteFile", no_init)
      .def("dataset", &PyRemoteFile::dataset, return_value_policy<copy_const_reference>())
      .def("locations", &PyRemoteFile::locations, return_value_policy<copy_const_reference>(),
           "Returns the list of FileLocation objects.")
      .def("relPath", &PyRemoteFile::relPath, return_value_policy<copy_const_reference>())
      .def("size", &PyRemoteFile::size)
      .def("archive", &PyRemoteFile::archive, return_value_policy<copy_const_reference>())
      .def("checksum", &PyRemoteFile::checksum, return_value_policy<copy_const_reference>())
      .def("__str__", &PyRemoteFile_str)
      .def("__repr__", &PyRemoteFile_str)
      ;

  class_<Metadata>("Metadata", no_init)
      .def("contains", &Metadata::contains, arg("key"), "Check for key existence.")
      .def("get", &Metadata::get, (arg("key"), arg("defVal")=""), "Return value for a key.")
      .def("items", &metadata_items, "Return mapping of keys to values.")
      .def("set", &Metadata::set, (arg("key"), arg("value")), "Add or replace metadata item.")
      .def("remove", &Metadata::remove, arg("key"), "Remove metadata item given its key.")
      ;

  {
    // wrapper for DBClient class
    scope in_PyDBClient = class_<PyDBClient, boost::noncopyable>(
            "DBClient",
            init<optional<std::string, std::string>>(
                (arg("connString")=DBBase::defConnStr(), arg("env")="auto")
            )
        )
        .def("datasets", &PyDBClient::datasets,
             "Get the list of datasets in the database, returns a list of strings.")
        .def("archives", boopy::raw_function(&PyDBClient::archives),
             "Get the list of archives, optionally specify dataset and archive name.\n"
             "Optional keyword arguments are 'since', 'until', 'count', and 'skip'.")
        .def("countFiles", &PyDBClient::countFiles,
             countFiles_overloads((arg("dataset"), arg("archive"), arg("file")),
                 "Returns total number of files which satisfy selection criteria set by the method arguments."))
        .def("files", boopy::raw_function(&PyDBClient::files),
             "Get the list of files, optionally specify dataset, archive and file name.\n"
             "Optional keyword arguments are 'since', 'until', 'count', and 'skip'.")
        .def("fileLocations", &PyDBClient::fileLocations,
             fileLocations_overloads((arg("file"), arg("dataset"), arg("location")),
                 "Finds and returns the list of all known file locations for a given "
                 "dataset and file name. File location is returned as a string, "
                 "starts with the \"file://host/path\" for file in cache, \"rfio:/...\" "
                 "for file in CASTOR, or \"root://...\" for file in EOS or other locations "
                 "served by xrootd. It is not guaranteed that file can be open "
                 "from the cache location even if that location is returned, the file "
                 "can be purged from cache at any moment. For CASTOR/EOS locations, if "
                 "the file is in archive then location will look like "
                 "\"root://archive-path.zip#file-name.ext\"; if the file is stored "
                 "individually then location looks like \"root://file-path.ext\". "
                 "Locations returned are limited by location argument, if LocCache is "
                 "specified then only Point1 cache location is returned (this is likely "
                 "not accessible outside P1), if LocArchive is specified then locations "
                 "in CASTOR/EOS archives are returned, if LocAll is specified (default) "
                 "then all possible location are returned."))
        .def("metadata", &PyDBClient::metadata)
        ;

    // define enum in PyDBClient scope
    enum_<DBClient::Location>("Location")
        .value("LocCache", DBClient::LocCache)
        .value("LocArchive", DBClient::LocArchive)
        .value("LocAll", DBClient::LocAll)
        .export_values()
        ;

  } // back to global scope

  // wrapper for Register class
  class_<PyRegister, boost::noncopyable>("Register", init<std::string, std::string>(args("server", "partition")))
      .def("configuration", &PyRegister::configuration,
           "Returns server configuration as arbitrarily formatted string.")
      .def("registerFile", &PyRegister::registerFile,
           (arg("dataset"), arg("file"), arg("prio"), arg("dtcSec"), arg("baseDir")),
           "Register new file with CoCa.")
      ;

  // wrapper for StatFiles class
  class_<PyStatFiles>("StatFiles", init<unsigned, uint64_t>(args("count", "size")))
      .def("count", &PyStatFiles::count)
      .def("size", &PyStatFiles::size)
      .def(self_ns::str(self))
      .def(self_ns::repr(self))
      ;

  {
    // wrapper for DBStat class, not this changes global scope
    scope scopeDBStat =
        class_<PyDBStat, boost::noncopyable>("DBStat",
            init< optional<std::string> >(arg("connString")=DBBase::defConnStr()))
        .def("intervalStat", &PyDBStat::intervalStat,
             (arg("begin"), arg("end"), arg("interval")),
             "Returns per-time interval statistics as dictionary, the key is timestamp\n"
             "(beginning of interval), the value is StatFiles object")
        .def("datasetStat", &PyDBStat::datasetStat, (arg("cached")),
             "Returns per-dataset interval statistics as dictionary, the key is dataset name,\n"
             "the value is StatFiles object")
        ;

    // define enum in the above scope
    enum_<DBStat::Interval>("Interval")
        .value("Hour", DBStat::Hour)
        .value("Day", DBStat::Day)
        .value("Month", DBStat::Month)
        .value("Year", DBStat::Year)
    ;

  } // back to global scope
}
