#ifndef COCA_SERVERINFO_H
#define COCA_SERVERINFO_H
//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <string>
#include <RelationalAccess/ISessionProxy.h>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq {
namespace coca {
    class Str2Id;
}
}


//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_server

/** 
 * @ingroup coca_server
 * @brief Class providing access to various server-side parameters.
 */

class ServerInfo {
public:
    
    /**
     *  @brief Initialize the object.
     *  
     *  Standard constructor needs access to the database to store
     *  some strings in the Str2ID map. 
     *
     *  @throw Issues::CoralException Thrown in case of the database access errors.
     */
    ServerInfo (const std::shared_ptr<coral::ISessionProxy>& session,
                Str2Id &s2i,
                const std::string &serverIPC,
                const std::string &serverHost,
                const std::string &topDir,
                const std::string &cdrHost,
                const std::string &cdrDirOnCDRHost,
                const std::string &archive_dir);

    ~ServerInfo () throw ();
    
    /// Returns the database ID of the server IPC string
    unsigned getServerIPC () const {return m_server_ipc;}
    
    /// Returns the database ID of the server host name
    unsigned getServerHost () const {return m_server_host;}

    /// Returns the name of the top directory
    const std::string& getTopDir () const {return m_top_dir;}
    
    /// Returns the database ID of the CDR host name
    unsigned getCDRHost () const {return m_cdr_host;}
    
    /// Returns the location of CDR directory on CDR host
    const std::string& getCDRDirOnCDRHost () const {return m_cdr_dir_on_cdr_host;}
    
    /// Returns archive directory name
    const std::string& getArchiveDir () const {return m_archive_dir;}

private:

    unsigned m_server_ipc;      ///< Server IPC string
    unsigned m_server_host;     ///< Name of the server host.
    std::string m_top_dir;      ///< Top directory name.
    unsigned m_cdr_host;        ///< Name of the CDR host.
    std::string m_cdr_dir_on_cdr_host;  ///< Location of CDR directory on CDR host.
    std::string m_archive_dir;   ///< Final archiving directory (or location).
    
};

} // namespace coca
} // namespace daq

#endif // COCA_SERVERINFO_H
