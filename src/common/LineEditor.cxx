//-----------------------
// This Class's Header --
//-----------------------
#include "coca/common/LineEditor.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <nlohmann/json.hpp>
#include <stdexcept>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Issues.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------
using json = nlohmann::json;

//              ------------------------------
//              -- Class Member Definitions --
//              ------------------------------

namespace daq::coca {

ERS_DECLARE_ISSUE_BASE(
    Issues,
    JsonParseError,
    LineEditorRulesError,
    "Failed to parse JSON: " << json,
    ,
    ((std::string) json)
)

ERS_DECLARE_ISSUE_BASE(
    Issues,
    JsonConfigError,
    LineEditorRulesError,
    message << ": " << json,
    ,
    ((std::string) message)
    ((std::string) json)
)


LineEditor::Rule::Rule(const std::regex& match,
    const std::string fmt,
    bool keep_original,
    bool discard,
    bool stop)
    : m_match(match)
    , m_fmt(fmt)
    , m_keep_original(keep_original)
    , m_discard(discard)
    , m_stop(stop)
{
}

std::pair<std::vector<std::string>, bool>
LineEditor::Rule::apply(const std::string& line) const
{
    std::vector<std::string> lines;
    if (not std::regex_search(line, m_match)) {
        // Return without changes.
        lines.push_back(line);
        return std::make_pair(lines, false);
    }
    if (m_discard) {
        // Also say it has to stop as there is nothing else to do.
        return std::make_pair(lines, true);
    }
    if (m_keep_original) {
        lines.push_back(line);
    }
    try {
        auto new_line = std::regex_replace(line, m_match, m_fmt);
        lines.push_back(new_line);
    } catch (const std::regex_error& exc) {
        throw std::invalid_argument("invalid replacement format string: " + m_fmt + "(" + exc.what() + ")");
    }
    return std::make_pair(lines, m_stop);
}


LineEditor::LineEditor(const std::string& rules)
{
    json rules_json;
    try {
        rules_json = json::parse(rules);
    } catch (json::parse_error& exc) {
        throw Issues::JsonParseError(ERS_HERE, rules);
    }
    if (not rules_json.is_array()) {
        throw Issues::JsonConfigError(ERS_HERE, "rules string is not a JSON array", rules);
    }

    for (auto rule_json: rules_json) {
        if (not rule_json.is_object()) {
            throw Issues::JsonConfigError(ERS_HERE, "rules array member is not a JSON object", rules);
        }
        std::string match = rule_json.value("match", "");
        if (match.empty()) {
            throw Issues::JsonConfigError(ERS_HERE, "No 'match' in a rule", rules);
        }
        bool keep_original = rule_json.value("keep_original", false);
        bool discard = rule_json.value("discard", false);
        bool stop = rule_json.value("stop", false);
        std::string replace = rule_json.value("replace", "");
        if (replace.empty() and not discard) {
            throw Issues::JsonConfigError(ERS_HERE, "'replace' cannot be empty if 'discard' is false", rules);
        }
        try {
            auto match_re = std::regex(match);
            m_rules.emplace_back(match_re, replace, keep_original, discard, stop);
        } catch (const std::regex_error& exc) {
            throw Issues::JsonConfigError(ERS_HERE, "invalid regular expression in rules", match);
        }
    }
}

std::vector<std::string>
LineEditor::apply(const std::vector<std::string>& lines) const
{
    std::vector<std::string> result;

    for (auto& line: lines) {
        // Lines can multiply after each rule, and some rules can request
        // stopping of the further processing of some lines, so we need
        // come care.
        std::vector<std::string> rule_inputs{line};
        for (auto& rule: m_rules) {
            std::vector<std::string> rule_outputs;
            for (auto& rule_input: rule_inputs) {
                auto rule_output = rule.apply(rule_input);
                if (rule_output.second) {
                    // Stop  further processing, copy them to final list.
                    result.insert(result.end(), rule_output.first.begin(), rule_output.first.end());
                } else {
                    // Pass to next rule
                    rule_outputs.insert(rule_outputs.end(), rule_output.first.begin(), rule_output.first.end());
                }
            }
            // Outputs of this rule go to the next one.
            rule_inputs = std::move(rule_outputs);
        }
        // Copy to the final result after all rules are done.
        result.insert(result.end(), rule_inputs.begin(), rule_inputs.end());
    }

    return result;
}

} // namespace daq::coca
