#ifndef COCA_SSHPROCESS_H
#define COCA_SSHPROCESS_H

//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class SSHProcess.
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>
#include <string>
#include <chrono>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/server/Process.h"
#include "coca/server/ServerIssues.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_server

/**
 *  @ingroup coca_server
 *
 *  @brief Class representing a child process running a remote command via ssh.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class SSHProcess  {
public:

    /**
     *  @brief Constructor starts process on remote host.
     *  
     *  @param[in] machine     Remote host name.
     *  @param[in] commandLine Command line (argv), first element is executable name.
     *  @param[in] name        Command name, arbitrary string, for identification only.
     *  
     *  @throw ProcessIssues::ForkError Thrown when fork system call fails.
     */
    SSHProcess (const std::string &machine,
                const std::vector<std::string> &commandLine,
                const std::string &name);
    
    /// Destructor will stop the SSH process if it is still running.
    ~SSHProcess () throw ();

    /**
     *  @brief Returns true if the child process is still running. 
     *  
     *  If it has finished then its exit code is returned too or exception is 
     *  thrown if process was killed by a signal. The exception is thrown only 
     *  once for this method call.
     *  
     *  @param[out] code  Return status of the process if it has exited already
     *  @return True if child is still running, false if finished.
     *  
     *  @throw ProcessIssues::Killed Thrown if SSH process was terminated with a signal.
     *  @throw ProcessIssues::SSHFailure Thrown if SSH cannot start remote process.
     */
    bool isRunning (int& code) const;
    
    /**
     *  @brief Returns true if the child process is still running. 
     *  
     *  If child has finished with non-zero exit code or was killed by a signal 
     *  then exception is generated.
     *  
     *  @return True if child is still running, false if finished.
     *  
     *  @throw ProcessIssues::Killed Thrown if SSH process was terminated with a signal.
     *  @throw ProcessIssues::SSHFailure Thrown if SSH cannot start remote process.
     *  @throw ProcessIssues::BadExitCodeRemote Thrown if remote process finished with non-zero exit code.
     */
    bool isRunning () const;
    
    /**
     *  @brief Forces stop of the SSH process. 
     *  
     *  Sends sequence of signals [TERM, TERM, KILL] to the child waiting max 2 seconds 
     *  after each signal for child to stop.
     */
    void terminate ();
    
    /**
     *  @brief Wait for child process to finish. 
     *  
     *  If it does not stop after specified timeout then terminates child process 
     *  calling terminate().
     *  
     *  @param[out] code  Return status of the process if it has exited.
     *  @param[in] time2Wait  Timeout value.
     *  @param[in] pollTime  Polling interval.
     *  
     *  @throw ProcessIssues::Killed Thrown if SSH process was terminated with a signal.
     *  @throw ProcessIssues::SSHFailure Thrown if SSH cannot start remote process.
     *  @throw ProcessIssues::KilledTimeout Thrown if SSH process had to be terminated.
     */
    void waitTimeout (int& code, 
        const std::chrono::milliseconds& time2Wait,
        const std::chrono::milliseconds& pollTime = std::chrono::milliseconds(500));
    
    /**
     *  @brief Wait for child process to finish. 
     *  
     *  If it does not stop after specified timeout then terminates child process 
     *  calling terminate().
     *  
     *  @param[in] time2Wait  Timeout value.
     *  @param[in] pollTime  Polling interval.
     *  
     *  @throw ProcessIssues::Killed Thrown if child process was terminated with a signal.
     *  @throw ProcessIssues::SSHFailure Thrown if SSH cannot start remote process.
     *  @throw ProcessIssues::BadExitCodeRemote Thrown if child process finished with non-zero exit code.
     *  @throw ProcessIssues::KilledTimeout Thrown if child process had to be terminated.
     */
    void waitTimeout (const std::chrono::milliseconds& time2Wait,
        const std::chrono::milliseconds& pollTime = std::chrono::milliseconds(500));
    
    /// Returns name of the process which was supplied in the constructor.
    const std::string& name () const {return m_name;}

protected:

private:

    // Data members
    std::string m_name;    ///< Process name, for identification only. 
    Process *m_process;    ///< Child process running SSH.
    std::string m_machine; ///< Remote host name.

//------------------
// Static Members --
//------------------
public:

    /**
     *  @brief Call SSH to remove a file on a remote host.
     *  
     *  This method will generate an exception if anything goes wrong. 
     *  
     *  @param[in] machine  Remote host name.
     *  @param[in] path     Absolute path name of the file to delete.
     *  
     *  @throw ProcessIssues::ForkError Thrown when fork system call fails.
     *  @throw ProcessIssues::Killed Thrown if child process was terminated with a signal.
     *  @throw ProcessIssues::SSHFailure Thrown if SSH cannot start remote process.
     *  @throw ProcessIssues::BadExitCode Thrown if child process finished with non-zero exit code.
     *  @throw ProcessIssues::KilledTimeout Thrown if child process had to be terminated.
     */
    static void removeRemote (const std::string &machine,
                              const std::string &path);


};

} // namespace coca
} // namespace daq

#endif // COCA_SSHPROCESS_H
