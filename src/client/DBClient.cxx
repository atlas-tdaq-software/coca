//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/client/DBClient.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <cstring>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <CoralBase/AttributeList.h>
#include <CoralBase/TimeStamp.h>
#include <RelationalAccess/ConnectionServiceException.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITable.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/coral-helpers.h"
#include "coca/common/Transaction.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace fs = boost::filesystem;

namespace {

    // regular and FQDN prefix for archive names in EOS
    const char eos_pfx[] = "root://eosatlas/";
    const char eos_pfx_fqdn[] = "root://eosatlas.cern.ch/";

    std::string default_location_rules(R"(
    [
        {
            "match": "root://eosatlas/",
            "replace": "root://eosatlas.cern.ch/"
        }
    ]
    )");

    // converts file-match wildcard string to SQL match expression,
    // if there is no wildcard characters (*?) in the string then
    // do nothing and return false, otherwise replace those with
    // %_, quote original %_ and return true
    bool wc2sql(std::string& str, char escape)
    {
        bool wc = str.find_first_of("*?") != std::string::npos;
        if (wc) {
            for (unsigned i = 0; i != str.size(); ++ i) {
                if (str[i] == '_' or str[i] == '%') {
                    // quote it
                    str.insert(i, 1, escape);
                    ++ i;
                } else if (str[i] == '*') {
                    str[i] = '%';
                } else if (str[i] == '?') {
                    str[i] = '_';
                }
            }
        }
        return wc;
    }

    // method which guesses whether this binary runs at Point1.
    // Thanks Sergei for idea
    bool runningAtP1()
    {
        const char * instpath = getenv("TDAQ_INST_PATH");
        if (not instpath) {
            return false;
        }

        // file that should exist only at P1
        std::string filename = instpath;
        filename += "/com/ipc_reference_location";
        return ::access(filename.c_str(), R_OK) == 0;
    }

    // Determine execution environment, returns "default" if no specific
    // environment (such as "p1") can be guessed.
    std::string _guess_env(const std::string& env) {
        if (env != "auto") {
            return env;
        }
        if (runningAtP1()) {
            return "p1";
        }
        return "default";
    }

}  // namespace

//              ------------------------------
//              -- Class Member Definitions --
//              ------------------------------

namespace daq {
namespace coca {

DBClient::DBClient (const std::string& connStr, const std::string& env)
    : m_impl(new DBBase(connStr)), m_env(::_guess_env(env))
{
}

/*
 * datasets
 */
std::vector<std::string>
DBClient::datasets()
try {

    // start read transaction
    Transaction tran(m_impl->session());

    // build query
    coral::ITable& table = m_impl->archiveTable(tran);
    std::unique_ptr<coral::IQuery> query (table.newQuery());

    // What
    coral_helpers::defineOutput<uint32_t>(*query, "DATASET");

    // how
    query->setDistinct();

    // run query
    coral::ICursor& cursor = query->execute();

    std::vector<std::string> datasets;
    while ( cursor.next() ) {
        // get current value
        unsigned dsId = cursor.currentRow()[0].data<uint32_t>();
        datasets.push_back (m_impl->getStr (dsId, tran));
    }
    return datasets;

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

/*
 * archives
 */

std::vector<RemoteArchive>
DBClient::archives(const std::string &dataset,
                   const std::string &archive,
                   const time_point &since,
                   const time_point &until)
{
    return _archives(dataset, archive, nullptr, nullptr, &since, &until);
}

std::vector<RemoteArchive>
DBClient::archives(const std::string &dataset,
                   const std::string &archive,
                   unsigned count,
                   unsigned skip)
{
    return _archives(dataset, archive, &count, &skip, nullptr, nullptr);
}

// Get the number of files in archive.
unsigned
DBClient::countFiles(const std::string& dataset,
                     const std::string& archive,
                     const std::string& file)
try {

    // start read transaction
    Transaction tran(m_impl->session());

    // build query
    std::unique_ptr<coral::IQuery> query (tran.session().nominalSchema().newQuery());

    // What
    query->addToTableList(m_impl->archiveTableName(), "AR");
    query->addToTableList(m_impl->cacheTableName(), "CA");

    coral_helpers::defineOutput<uint32_t>(*query, "COUNT(*)", "COUNT");

    // selection
    coral::AttributeList condData;
    std::string where = "CA.DATASET=AR.DATASET AND CA.ARCHIVE=AR.ARCHIVE AND AR.REMOVED = :NO";
    coral_helpers::addAttr(condData, "NO", false);

    if (not file.empty()) {
        std::string wcfile = file;
        if (wc2sql(wcfile, '@')) {
            where += " AND CA.REL_PATH LIKE :REL_PATH ESCAPE '@'";
        } else {
            where += " AND CA.REL_PATH=:REL_PATH";
        }
        coral_helpers::addAttr(condData, "REL_PATH", wcfile);
    }

    if (not dataset.empty()) {
        try {
            where += " AND CA.DATASET=:DATASET";
            coral_helpers::addAttr(condData, "DATASET", m_impl->getId(dataset, tran));
        } catch (const Issues::NoSuchRecord &ex) {
            ERS_DEBUG (0, ex);
            throw DBClientIssues::DatasetNotFound (ERS_HERE, dataset);
        }
    }

    if (not archive.empty()) {
        std::string wcarchive = archive;
        if (wc2sql(wcarchive, '@')) {
            where += " AND AR.NAME LIKE :ARCHIVE_NAME ESCAPE '@'";
        } else {
            where += " AND AR.NAME=:ARCHIVE_NAME";
        }
        coral_helpers::addAttr(condData, "ARCHIVE_NAME", wcarchive);
    }

    query->setCondition(where, condData);

    // execute
    coral::ICursor& cursor = query->execute();
    unsigned count = 0;
    while (cursor.next()) {
        const coral::AttributeList& row = cursor.currentRow();
        count = row[0].data<uint32_t>();
    }

    return count;

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

/*
 * files
 */

std::vector<RemoteFile>
DBClient::files(const std::string& dataset,
                const std::string& archive,
                const std::string& file,
                const time_point& since,
                const time_point& until)
{
    return _files(dataset, archive, file, nullptr, nullptr, &since, &until);
}

std::vector<RemoteFile>
DBClient::files(const std::string& dataset,
                const std::string& archive,
                const std::string& file,
                unsigned count,
                unsigned skip)
{
    return _files(dataset, archive, file, &count, &skip, nullptr, nullptr);
}

// Get the list of known file locations.
std::vector<std::string>
DBClient::fileLocations(const std::string& fileName, const std::string& dataset, Location location)
{
    // most files exist in one or two locations
    std::vector<std::string> result;
    result.reserve(2);
    if (fileName.empty()) {
      return result;
    }

    std::string archiveLoc;

    // first find files which are in coca cache
    try {

        // start read transaction
        Transaction tran(m_impl->session());

        // build query
        std::unique_ptr<coral::IQuery> query (tran.session().nominalSchema().newQuery());

        // What
        query->addToTableList(m_impl->archiveTableName(), "AR");
        query->addToTableList(m_impl->cacheTableName(), "CA");

        coral_helpers::defineOutput<bool>(*query, "CA.IN_CACHE", "IN_CACHE");
        coral_helpers::defineOutput<uint32_t>(*query, "AR.SERVER_HOST", "SERVER_HOST");
        coral_helpers::defineOutput<uint32_t>(*query, "AR.DIR_SERVER", "DIR_SERVER");
        coral_helpers::defineOutput<bool>(*query, "AR.STORED", "STORED");
        coral_helpers::defineOutput<bool>(*query, "AR.REMOVED", "REMOVED");
        coral_helpers::defineOutput<std::string>(*query, "AR.STOR_PATH", "STOR_PATH");

        // selection
        coral::AttributeList condData;
        std::string where = "CA.DATASET=AR.DATASET AND CA.ARCHIVE=AR.ARCHIVE AND CA.REL_PATH=:REL_PATH";
        coral_helpers::addAttr(condData, "REL_PATH", fileName);

        if (not dataset.empty()) {
            try {
                where += " AND CA.DATASET=:DATASET";
                unsigned dsId = m_impl->getId(dataset, tran);
                coral_helpers::addAttr(condData, "DATASET", dsId);
                ERS_DEBUG (2, "DBClient::fileLocations: dataset=" << dataset << " dsId=" << dsId);
            } catch (const Issues::NoSuchRecord &ex) {
                ERS_DEBUG (0, ex);
                throw DBClientIssues::DatasetNotFound (ERS_HERE, dataset);
            }
        }

        query->setCondition(where, condData);


        // execute
        coral::ICursor& cursor = query->execute();
        while (cursor.next()) {

            ERS_DEBUG (2, "DBClient::fileLocations: after cursor.next()");

            const coral::AttributeList& row = cursor.currentRow();

            if (location & LocArchive and archiveLoc.empty() and row["STORED"].data<bool>()
                    and not row["REMOVED"].data<bool>()) {
                archiveLoc = row["STOR_PATH"].data<std::string>();
            }
            if (location & LocCache and row["IN_CACHE"].data<bool>()) {
                // xrootd server on coca server host serves directory /data/coca/cache,
                // so we need to strip that directory from the path
                std::string dirServer = m_impl->getStr(row["DIR_SERVER"].data<uint32_t>(), tran);
                static const std::string xrootdPfx = "/data/coca/cache/";
                if (boost::algorithm::starts_with(dirServer, xrootdPfx)) {
                    dirServer = std::string(dirServer, xrootdPfx.size()-1);
                }
                // URL should look like "root://anonymous@server//path/to/file.root"
                // (note double slash after server name)
                std::string path = "root://anonymous@";
                path += m_impl->getStr(row["SERVER_HOST"].data<uint32_t>(), tran);
                path += "/";
                path += dirServer;
                path += "/";
                path += fileName;
                result.push_back(path);
            }

        }

    } catch (const coral::ConnectionNotAvailableException& ex) {
        throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
    } catch (const coral::Exception& ex) {
        throw Issues::CoralException(ERS_HERE, ex.what(), ex);
    }


    // next archived location
    if (not archiveLoc.empty()) {

        // Convoluted logic here, needed to account for some historical stuff
        // and for the period of transition from CASTOR to EOS.
        if (boost::algorithm::starts_with(archiveLoc, "/castor")) {
            // file is apparently on castor
            archiveLoc.insert(0, "rfio:");
        } else if (boost::algorithm::starts_with(archiveLoc, ::eos_pfx)) {
            // for EOS paths starting with root://eosatlas/ replace host name
            // with FQDN so that outside users can have access
            archiveLoc.replace(0, std::strlen(::eos_pfx), ::eos_pfx_fqdn);
        } else if (not boost::algorithm::starts_with(archiveLoc, "root:")) {
            // if it does not start with root: already then assume it is
            // on EOS (and path should start with /eos/atlas/...)
            archiveLoc.insert(0, ::eos_pfx_fqdn);
        }
        if (boost::algorithm::ends_with(archiveLoc, ".zip")) {
            // add file name inside ZIP archive
            archiveLoc += "#";
            archiveLoc += fileName;
        }
        result.push_back(archiveLoc);
        ERS_DEBUG (2, "DBClient::fileLocations: archiveLoc=" << archiveLoc);
    }

    // And apply configured overrides.
    result = _line_editor().apply(result);
    return result;
}

// Get the list of known file locations.
std::vector<std::string>
DBClient::fileLocations(const std::string& fileName, const std::string& dataset)
{
    return fileLocations(fileName, dataset, LocAll);
}

Metadata
DBClient::metadata()
{
    return m_impl->metadata();
}

std::vector<RemoteFile>
DBClient::_files(const std::string& dataset,
                 const std::string& archive,
                 const std::string& file,
                 const unsigned* count,
                 const unsigned* skip,
                 const time_point* since,
                 const time_point* until)
try {

    // start read transaction
    Transaction tran(m_impl->session());

    // build query
    std::unique_ptr<coral::IQuery> query (tran.session().nominalSchema().newQuery());

    // What
    query->addToTableList(m_impl->archiveTableName(), "AR");
    query->addToTableList(m_impl->cacheTableName(), "CA");

    coral_helpers::defineOutput<uint32_t>(*query, "CA.DATASET", "DATASET");
    coral_helpers::defineOutput<std::string>(*query, "CA.REL_PATH", "REL_PATH");
    coral_helpers::defineOutput<uint32_t>(*query, "CA.SIZE_MB", "SIZE_MB");
    coral_helpers::defineOutput<uint32_t>(*query, "CA.CLIENT_HOST", "CLIENT_HOST");
    coral_helpers::defineOutput<uint32_t>(*query, "CA.DIR_CLIENT", "DIR_CLIENT");
    coral_helpers::defineOutput<bool>(*query, "CA.ON_CLIENT", "ON_CLIENT");
    coral_helpers::defineOutput<bool>(*query, "CA.IN_CACHE", "IN_CACHE");
    coral_helpers::defineOutput<std::string>(*query, "AR.NAME", "NAME");
    coral_helpers::defineOutput<uint32_t>(*query, "AR.SERVER_HOST", "SERVER_HOST");
    coral_helpers::defineOutput<uint32_t>(*query, "AR.DIR_SERVER", "DIR_SERVER");
    if (m_impl->schemaVersion() >= 3) {
        coral_helpers::defineOutput<Size>(*query, "CA.SIZE_BYTES", "SIZE_BYTES");
        coral_helpers::defineOutput<std::string>(*query, "CA.CHECKSUM", "CHECKSUM");
    }

    // selection
    coral::AttributeList condData;
    std::string where = "CA.DATASET=AR.DATASET AND CA.ARCHIVE=AR.ARCHIVE AND AR.REMOVED = :NO";
    coral_helpers::addAttr(condData, "NO", false);

    if (not file.empty()) {
        std::string wcfile = file;
        if (wc2sql(wcfile, '@')) {
            where += " AND CA.REL_PATH LIKE :REL_PATH ESCAPE '@'";
        } else {
            where += " AND CA.REL_PATH=:REL_PATH";
        }
        coral_helpers::addAttr(condData, "REL_PATH", wcfile);
    }

    if (not dataset.empty()) {
        try {
            where += " AND CA.DATASET=:DATASET";
            coral_helpers::addAttr(condData, "DATASET", m_impl->getId(dataset, tran));
        } catch (const Issues::NoSuchRecord &ex) {
            ERS_DEBUG (0, ex);
            throw DBClientIssues::DatasetNotFound (ERS_HERE, dataset);
        }
    }

    if (not archive.empty()) {
        std::string wcarchive = archive;
        if (wc2sql(wcarchive, '@')) {
            where += " AND AR.NAME LIKE :ARCHIVE_NAME ESCAPE '@'";
        } else {
            where += " AND AR.NAME=:ARCHIVE_NAME";
        }
        coral_helpers::addAttr(condData, "ARCHIVE_NAME", wcarchive);
    }

    if (since and *since != time_point()) {
        std::chrono::nanoseconds chrono_ns = since->time_since_epoch();
        coral::TimeStamp::ValueType cts_tmp = chrono_ns.count();
        coral::TimeStamp coral_since (cts_tmp);

        where += " AND CA.REGISTERED_AT>=:SINCE";
        coral_helpers::addAttr(condData, "SINCE", coral_since);
    }

    if (until and *until != time_point()) {
        std::chrono::nanoseconds chrono_ns = until->time_since_epoch();
        coral::TimeStamp::ValueType cts_tmp = chrono_ns.count();
        coral::TimeStamp coral_until(cts_tmp);

        where += " AND CA.REGISTERED_AT<=:UNTIL";
        coral_helpers::addAttr(condData, "UNTIL", coral_until);
    }

    query->setCondition(where, condData);

    if (count) {
        query->addToOrderList("CA.REGISTERED_AT DESC");
        query->limitReturnedRows(*count, *skip);
    }

    query->addToOrderList("CA.DATASET ASC");
    query->addToOrderList("CA.ARCHIVE ASC");
    query->addToOrderList("CA.REL_PATH ASC");


    // execute
    std::vector<RemoteFile> files;
    coral::ICursor& cursor = query->execute();
    while (cursor.next()) {

        const coral::AttributeList& row = cursor.currentRow();

        std::vector<FileLocation> locations;
        if (row["IN_CACHE"].data<bool>()) {
            fs::path path = m_impl->getStr (row["DIR_SERVER"].data<uint32_t>(), tran);
            path /= row["REL_PATH"].data<std::string>();
            locations.emplace_back(m_impl->getStr (row["SERVER_HOST"].data<uint32_t>(), tran), path.string());
        }
        if (row["ON_CLIENT"].data<bool>()) {
            fs::path path = m_impl->getStr (row["DIR_CLIENT"].data<uint32_t>(), tran);
            path /= row["REL_PATH"].data<std::string>();
            locations.emplace_back(m_impl->getStr (row["CLIENT_HOST"].data<uint32_t>(), tran), path.string());
        }
        Size size = Size::MB(row["SIZE_MB"].data<uint32_t>());
        std::string checksum;
        if (m_impl->schemaVersion() >= 3) {
            size = coral_helpers::getAttr(row["SIZE_BYTES"], Size(0));
            checksum = coral_helpers::getAttr(row["CHECKSUM"], checksum);
        }
        files.emplace_back(m_impl->getStr (row["DATASET"].data<uint32_t>(), tran),
                          row["REL_PATH"].data<std::string>(),
                          locations,
                          row["NAME"].data<std::string>(),
                          size,
                          checksum);
    }
    if (files.empty() and not file.empty()) {
        throw DBClientIssues::FileNotFound (ERS_HERE, dataset, file);
    }
    return files;
} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

std::vector<RemoteArchive>
DBClient::_archives(const std::string &dataset,
                    const std::string &archive,
                    const unsigned* count,
                    const unsigned* skip,
                    const time_point* since,
                    const time_point* until)
try {

    // start read transaction
    Transaction tran(m_impl->session());

    // build query
    coral::ITable& table = m_impl->archiveTable(tran);
    std::unique_ptr<coral::IQuery> query (table.newQuery());

    // What
    coral_helpers::defineOutput<std::string>(*query, "NAME");
    coral_helpers::defineOutput<uint32_t>(*query, "DATASET");
    coral_helpers::defineOutput<bool>(*query, "ON_CDR");
    coral_helpers::defineOutput<uint32_t>(*query, "CDR_HOST");
    coral_helpers::defineOutput<std::string>(*query, "CDR_PATH");
    coral_helpers::defineOutput<bool>(*query, "STORED");
    coral_helpers::defineOutput<std::string>(*query, "STOR_PATH");

    // selection
    coral::AttributeList condData;
    std::string where = "REMOVED = :NO";
    coral_helpers::addAttr(condData, "NO", false);
    if (not archive.empty()) {
        where += " AND ";
        std::string wcarchive = archive;
        if (wc2sql(wcarchive, '@')) {
            where += "NAME LIKE :NAME ESCAPE '@'";
        } else {
            where += "NAME = :NAME";
        }
        coral_helpers::addAttr(condData, "NAME", wcarchive);
    }

    if (not dataset.empty()) {
        try {
            where += " AND DATASET = :DATASET";
            coral_helpers::addAttr(condData, "DATASET", m_impl->getId(dataset, tran));
        } catch (const Issues::NoSuchRecord &ex) {
            ERS_DEBUG (0, ex);
            throw DBClientIssues::DatasetNotFound (ERS_HERE, dataset);
        }
    }

    if (since and *since != time_point()) {
        std::chrono::nanoseconds chrono_ns = since->time_since_epoch();
        coral::TimeStamp::ValueType ns = chrono_ns.count();
        coral::TimeStamp booked (ns);
        where += " AND BOOKED_AT>=:SINCE";
        coral_helpers::addAttr(condData, "SINCE", booked);
    }

    if (until and *until != time_point()) {
        std::chrono::nanoseconds chrono_ns = until->time_since_epoch();
        coral::TimeStamp::ValueType ns = chrono_ns.count();
        coral::TimeStamp booked (ns);
        where += " AND BOOKED_AT<=:UNTIL";
        coral_helpers::addAttr(condData, "UNTIL", booked);
    }

    query->setCondition(where, condData);

    if (count) {
        query->addToOrderList("BOOKED_AT DESC");
        query->limitReturnedRows(*count, *skip);
    }

    query->addToOrderList("NAME ASC");
    query->addToOrderList("DATASET ASC");

    // execute
    std::vector<RemoteArchive> archives;
    coral::ICursor& cursor = query->execute();
    while (cursor.next()) {

        const coral::AttributeList& row = cursor.currentRow();

        const std::string &dataset = m_impl->getStr (row["DATASET"].data<uint32_t>(), tran);
        std::vector<FileLocation> locations;
        if (row["ON_CDR"].data<bool>()) {
            locations.emplace_back(m_impl->getStr (row["CDR_HOST"].data<uint32_t>(), tran),
                                   row["CDR_PATH"].data<std::string>());
        }
        std::string stor_path;
        if (row["STORED"].data<bool>()) {
            stor_path = row["STOR_PATH"].data<std::string>();
            if (boost::algorithm::starts_with(stor_path, ::eos_pfx)) {
                // for EOS paths starting with root://eosatlas/ replace host name
                // with FQDN so that outside users can have access
                stor_path.replace(0, std::strlen(::eos_pfx), ::eos_pfx_fqdn);
            }
        }
        const std::string& name = row["NAME"].data<std::string>();
        archives.emplace_back(dataset, name, locations, stor_path);
    }
    if (archives.empty() and not archive.empty()) {
        throw DBClientIssues::ArchiveNotFound (ERS_HERE, archive);
    }
    return archives;
} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}

const LineEditor&
DBClient::_line_editor()
try {

    if (m_line_editor == nullptr) {
        // Read rules from database and make instance
        std::string metadata_key = "location-rules-" + m_env;
        auto rules = metadata().get(metadata_key);
        if (rules.empty()) {
            rules = ::default_location_rules;
        }
        m_line_editor.reset(new LineEditor(rules));
    }
    return *m_line_editor;

} catch (const coral::ConnectionNotAvailableException& ex) {
    throw Issues::DBConnectionFailure(ERS_HERE, ex.what(), ex);
} catch (const coral::Exception& ex) {
    throw Issues::CoralException(ERS_HERE, ex.what(), ex);
}


} // namespace coca
} // namespace daq
