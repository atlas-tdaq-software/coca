tdaq_package()

add_definitions(-DTDAQ_RELEASE_NAME=\"${TDAQ_PROJECT_NAME}\")

tdaq_generate_dal(databases/coca/schema/coca.schema.xml
  NAMESPACE   daq::coca_dal
  INCLUDE     coca_dal
  INCLUDE_DIRECTORIES dal
  CPP_OUTPUT  dal_cpp_srcs
  JAVA_OUTPUT dal_java_srcs)

tdaq_add_jar(cocadal
  DAL
  ${dal_java_srcs}
  INCLUDE_JARS dal/dal.jar DFConfiguration/DFdal.jar config/config.jar Jers/ers.jar TDAQExtJars/external.jar)

tdaq_add_library(coca_dal DAL
  ${dal_cpp_srcs}
  LINK_LIBRARIES daq-core-dal tdaq-common::ers)

tdaq_add_library(coca_common
  idl/cocaIPC.idl
  src/common/*.cxx
  IDL_DEPENDS ipc
  LINK_LIBRARIES coca_dal ipc CORAL::RelationalAccess CORAL::CoralCommon CORAL::CoralKernel CORAL::CoralBase tdaq-common::ers Boost::filesystem
  )

tdaq_add_library(coca_client
  src/client/*.cxx
  LINK_LIBRARIES coca_common osw)

tdaq_add_library(coca_srvlib
  src/server/*.cxx
  LINK_LIBRARIES coca_common coca_dal osw Boost::thread Boost::filesystem rt)

tdaq_add_executable(coca_server
  src/apps/coca_server.cxx
  LINK_LIBRARIES coca_srvlib pmgsync osw Boost::program_options)

tdaq_add_executable(coca_cached_file_cost
  src/apps/cached_file_cost.cxx
  LINK_LIBRARIES coca_srvlib Boost::program_options)

tdaq_add_executable(coca_register
  src/apps/register.cxx
  LINK_LIBRARIES coca_client coca_common coca_dal Boost::program_options)

tdaq_add_executable(coca_dump_config
  src/apps/dump_config.cxx
  LINK_LIBRARIES coca_client coca_common coca_dal Boost::program_options)

tdaq_add_executable(coca_get_info
  src/apps/get_info.cxx
  LINK_LIBRARIES coca_client coca_common coca_dal Boost::program_options)

tdaq_add_executable(coca_make_schema
  src/apps/make_schema.cxx
  LINK_LIBRARIES coca_common Boost::program_options SQLite)

tdaq_add_library(pycoca MODULE
  src/python/pycoca.cxx
  LINK_LIBRARIES coca_client Boost::python Python::Development)

tdaq_add_python_package(coca)
tdaq_add_python_files( python/coca/*.pyi python/coca/py.typed DESTINATION coca/ )

tdaq_add_schema(databases/coca/schema/coca.schema.xml)

tdaq_add_data(databases/coca/COCA_test_segment.data.xml DESTINATION examples)

tdaq_add_scripts(src/scripts/coca_get_files)
tdaq_add_scripts(src/scripts/coca-cli)

# unit tests
foreach(unit_test
  coca_test_fs
  coca_test_dbbase
  coca_test_nextid
  coca_test_str2id
  coca_test_transaction
  coca_test_checksum
  coca_test_lineeditor
  coca_test_filecheck)
    tdaq_add_executable(${unit_test} NOINSTALL
      src/test/${unit_test}.cxx
      LINK_LIBRARIES coca_common coca_dal Boost::unit_test_framework SQLite)
    add_test(NAME ${unit_test}
             COMMAND ${unit_test} -- ${CMAKE_CURRENT_SOURCE_DIR}/databases/db_migrations/sqlite)
endforeach(unit_test)

foreach(unit_test
  coca_test_dbserver
  coca_test_freespace)
    tdaq_add_executable(${unit_test} NOINSTALL
      src/test/${unit_test}.cxx
      LINK_LIBRARIES coca_srvlib Boost::unit_test_framework SQLite)
    add_test(NAME ${unit_test}
             COMMAND ${unit_test} -- ${CMAKE_CURRENT_SOURCE_DIR}/databases/db_migrations/sqlite)
endforeach(unit_test)

foreach(unit_test
  coca_test_dbclient)
    tdaq_add_executable(${unit_test} NOINSTALL
      src/test/${unit_test}.cxx
      LINK_LIBRARIES coca_client coca_srvlib Boost::unit_test_framework SQLite)
    add_test(NAME ${unit_test}
             COMMAND ${unit_test} -- ${CMAKE_CURRENT_SOURCE_DIR}/databases/db_migrations/sqlite)
endforeach(unit_test)

tdaq_add_test(
  NAME mypy
  COMMAND tdaq_python -m mypy
  POST_INSTALL
  WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)
