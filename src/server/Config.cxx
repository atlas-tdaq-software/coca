// $Id$
// $Name$

#include "coca/server/Config.h"

#include <iostream>
#include <stdlib.h>
#include <boost/lexical_cast.hpp>

#include "coca/common/System.h"
#include "coca_dal/CoCaServer.h"
#include "coca_dal/CoCaDatasetQuota.h"
#include "coca_dal/CoCaDataset.h"
#include "dal/util.h"
#include "system/Host.h"

namespace {

    const unsigned secsInWeek = 3600*24*7;

}

namespace daq {
namespace coca {


Config::Config(const std::string &serverIPC)
try
    : m_server_ipc()
    , m_partition()
    , m_db_conn_str()
    , m_top_dir()
    , m_CDR_dir()
    , m_CDR_host()
    , m_CDR_dir_on_host()
    , m_archive_dir()
    , m_def_Quota_MB(0)
    , m_def_TTL_s(0)
    , m_def_MinArchiveSize_MB(0)
    , m_def_MaxArchiveSize_MB(0)
    , m_def_MaxDelay_s(0)
    , m_datasets()
    , m_any_dataset_configured(false)
    , m_defBundlePolicy(Dataset::SingleFile)
    , m_freePercentGoal(0)
{
    // IPC name (provided by the controller via the "-n" option
    m_server_ipc = serverIPC;

    // partition name from environment
    if ( const char* var = getenv("TDAQ_PARTITION") ) {
        m_partition = var;
    } else {
        throw ConfigIssues::TDAQPartitionNotDefined (ERS_HERE);
    }

    // substitute variables; needs partition object
    Configuration confdb ("");
    ERS_ASSERT (confdb.loaded());
    const daq::core::Partition *partition_dal =
        daq::core::get_partition (confdb, m_partition);
    if (partition_dal) {
        auto sv = new daq::core::SubstituteVariables(*partition_dal);
        confdb.register_converter (sv);
    } else {
        throw ConfigIssues::PartitionNotFound(ERS_HERE, m_partition);
    }

    const coca_dal::CoCaServer *conf;
    conf = confdb.get<coca_dal::CoCaServer> (m_server_ipc, true);
    if (! conf) {
        throw ConfigIssues::ServerNotFound (ERS_HERE, m_server_ipc);
    }

    m_db_conn_str = conf->get_Database();
    m_top_dir = conf->get_CacheDir();
    m_CDR_dir = conf->get_CDRDir();
    m_CDR_host = conf->get_CDRHost();
    m_CDR_dir_on_host = conf->get_CDRDir_on_CDRHost();
    m_archive_dir = conf->get_CASTOR_dir();
    m_def_TTL_s = conf->get_DefaultTTL_week() * ::secsInWeek;
    m_def_MinArchiveSize_MB = conf->get_DefaultMinArchiveSize_MB();
    m_def_MaxArchiveSize_MB = conf->get_DefaultMaxArchiveSize_MB();
    m_def_MaxDelay_s = conf->get_DefaultMaxDelay_s();
    ERS_DEBUG(0, "Config: DefaultBundlePolicy: " << conf->get_DefaultBundlePolicy());
    m_defBundlePolicy = boost::lexical_cast<Dataset::BundlePolicy>(conf->get_DefaultBundlePolicy());
    m_freePercentGoal = conf->get_FreePercentGoal();
    m_maxCopyRetries = conf->get_MaxCopyRetries();

    try {
        System::initialize (*(conf->get_UnixProperties()));
    } catch (const SystemIssues::GroupNotFound& ex) {
        throw ConfigIssues::GroupNotFound (ERS_HERE,
                             conf->get_UnixProperties()->get_NewDirGroup(), ex);
    }

    const std::vector<const coca_dal::CoCaDatasetQuota*>& datasets =
        conf->get_Datasets();
    typedef std::vector<const coca_dal::CoCaDatasetQuota*>::const_iterator
        it_t;
    for (it_t it = datasets.begin(); it != datasets.end(); ++ it) {
        const coca_dal::CoCaDataset *ds = (*it)->get_Dataset();
        std::string name = ds->UID();

        Dataset::BundlePolicy bundlePolicy = m_defBundlePolicy;
        ERS_DEBUG(0, "Config: Dataset " << name << " BundlePolicy: " << ds->get_BundlePolicy());
        if (ds->get_BundlePolicy() != "Default") {
            bundlePolicy = boost::lexical_cast<Dataset::BundlePolicy>(ds->get_BundlePolicy());
        }

        Dataset tmp(name,
                    (*it)->get_Quota_MB(),
                    ds->get_TTL_week() * ::secsInWeek,
                    Size::MB(ds->get_MinArchiveSize_MB()),
                    Size::MB(ds->get_MaxArchiveSize_MB()),
                    ds->get_MaxDelay_s(),
                    bundlePolicy);
        m_datasets.insert(DSMap::value_type(name, tmp));
    }
    m_any_dataset_configured = ! m_datasets.empty();

} catch (const daq::config::Exception& ex) {
    throw ConfigIssues::ConfigError(ERS_HERE, ex);
} catch (const std::exception& ex) {
    throw ConfigIssues::ConfigError(ERS_HERE, ex);
}

Config::~Config()
    throw ()
{
}

const daq::coca::Dataset&
Config::dataset (const std::string &name) const
{
    DSMap::const_iterator it = m_datasets.find (name);
    if (it != m_datasets.end()) {
        return it->second;
    }
    // dataset not found
    if (m_any_dataset_configured) {
        throw ConfigIssues::DatasetNotAllowed (ERS_HERE, name);
    }
    // every database is allowed; fill with default
    Dataset tmp(name,
                m_def_Quota_MB,
                m_def_TTL_s,
                Size::MB(m_def_MinArchiveSize_MB),
                Size::MB(m_def_MaxArchiveSize_MB),
                m_def_MaxDelay_s,
                m_defBundlePolicy);
    it = m_datasets.insert(DSMap::value_type(name, tmp)).first;
    return it->second;
}

/// Dump configuration info to a stream
void
Config::print (std::ostream& out) const
{
    System::instance().print(out);

    out << "\n"
        "\nconfiguration for CoCaServer " << m_server_ipc << ":"
        "\n db_conn_str = " << m_db_conn_str << ";"
        "\n hostname = " << ::System::LocalHost::local_name() << ";"
        "\n top_dir = " << m_top_dir << ";"
        "\n CDR_dir = " << m_CDR_dir << ";"
        "\n CDR_host = " << m_CDR_host << ";"
        "\n CDR_dir_on_host = " << m_CDR_dir_on_host << ";"
        "\n archive_dir = " << m_archive_dir << ";"
        "\n def_TTL_s = " << m_def_TTL_s << ";"
        "\n def_MinArchiveSize_MB = " << m_def_MinArchiveSize_MB << ";"
        "\n def_MaxArchiveSize_MB = " << m_def_MaxArchiveSize_MB << ";"
        "\n def_MaxDelay_s = " << m_def_MaxDelay_s << ";"
        "\n FreePercentGoal = " << m_freePercentGoal << ";"
        "\n MaxCopyRetries = " << m_maxCopyRetries;

    for (DSMap::const_iterator it = m_datasets.begin(); it != m_datasets.end(); ++ it) {
        const std::string& name = it->first;
        const Dataset& ds = it->second;
        out << "\n"
            "\nconfiguration for Dataset = " << name << ";"
            "\n Quota_MB = " << ds.quotaMB() << ";"
            "\n TTL_s = " << ds.ttlSec() << ";"
            "\n MinArchiveSize_bytes = " << ds.minArchiveSize() << ";"
            "\n MaxArchiveSize_bytes = " << ds.maxArchiveSize() << ";"
            "\n MaxDelay_s = " << ds.maxDelaySec() << ";"
            "\n BundlePolicy = " << ds.bundlePolicy();
    }

}

/// Stream insertion operator to print object contents
std::ostream&
operator<<(std::ostream& out, const Config& cfg)
{
    cfg.print(out);
    return out;
}

} // namespace coca
} // namespace daq
