#include <string>

#define BOOST_TEST_MODULE coca_test_freespace
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/test/data/test_case.hpp>

#include "coca/server/CachedFile.h"
#include "coca/server/DBServer.h"
#include "coca/server/FreeSpace.h"
#include "coca/common/Dataset.h"
#include "coca/common/LocalFile.h"
#include "coca/common/TransientFile.h"
#include "coca_test_util.h"

using namespace daq::coca;
using namespace daq::coca::test;
namespace tt = boost::test_tools;
namespace bdata = boost::unit_test::data;


template <typename F>
struct MakeServerFixture : F {

    std::unique_ptr<DBServer> make_server(
        std::string const& uri, std::string const& server_ipc,
        std::string const& server_host, std::string const& top_dir) const
    {
        return std::make_unique<DBServer>(
            uri, server_ipc, server_host, top_dir, "cdr-host", "/cdr/dir", "/archive/dir");
    }

};

using Fixture = PopulateBDFixture<MakeServerFixture<DBFixture>>;

// Test the case when there is only one server controlling the cache
BOOST_DATA_TEST_CASE_F(Fixture, test_freespace_one_server, bdata::make({0, 1, 2, 3}), version)
{
    // make cache size big enough to fit one batch of files (each file is 1MB)
    Size MB = Size::MB(1);
    Size cache_size = (this->expectedNumFiles() + 1) * MB;
    MockFileSystem fs(cache_size);

    std::string const uri = makeSchema(version);
    std::string topCacheDir = "/data/coca/cache";

    auto server = make_server(uri, "tdaq-09-03-00/coca", "server_host1", topCacheDir);

    unsigned freePercentGoal = 20;
    FreeSpace freespace(*server, freePercentGoal, fs);

    this->populateDB(*server, "file1", fs, topCacheDir);
    BOOST_TEST(fs.count_files() == this->expectedNumFiles());

    // this should remove one file
    Size goal = MB;
    BOOST_TEST(freespace.makeAvailable(goal, topCacheDir));
    BOOST_TEST(fs.count_files() == this->expectedNumFiles() - 1);

    // and this should remove everything
    goal = this->expectedNumFiles() * MB;
    BOOST_TEST(freespace.makeAvailable(goal, topCacheDir));
    BOOST_TEST(fs.count_files() == 0);
}

// Test the case with two servers sharing the cache
BOOST_DATA_TEST_CASE_F(Fixture, test_freespace_two_servers, bdata::make({0, 1, 2, 3}), version)
{
    // make cache size big enough to fit one batch of files (each file is 1MB)
    Size MB = Size::MB(1);
    Size cache_size = (this->expectedNumFiles() * 2 + 1) * MB;
    MockFileSystem fs(cache_size);

    std::string const uri = makeSchema(version);
    std::string topCacheDir = "/data/coca/cache";

    auto server1 = make_server(uri, "tdaq-09-03-00/coca", "server_host1", topCacheDir);
    auto server2 = make_server(uri, "tdaq-09-03-xx/coca", "server_host1", topCacheDir);

    unsigned freePercentGoal = 20;
    FreeSpace freespace1(*server1, freePercentGoal, fs);
    FreeSpace freespace2(*server2, freePercentGoal, fs);

    this->populateDB(*server1, "file1", fs, topCacheDir);
    this->populateDB(*server2, "file2", fs, topCacheDir);
    BOOST_TEST(fs.count_files() == this->expectedNumFiles() * 2);

    // this should remove one file
    Size goal = MB;
    BOOST_TEST(freespace1.makeAvailable(goal, topCacheDir));
    BOOST_TEST(fs.count_files() == this->expectedNumFiles() * 2 - 1);

    // and this should remove half of all files
    goal = this->expectedNumFiles() * MB;
    BOOST_TEST(freespace1.makeAvailable(goal, topCacheDir));
    BOOST_TEST(fs.count_files() == this->expectedNumFiles());

    // and this should remove everything
    goal = this->expectedNumFiles() * 2 * MB;
    BOOST_TEST(freespace2.makeAvailable(goal, topCacheDir));
    BOOST_TEST(fs.count_files() == 0);
}
