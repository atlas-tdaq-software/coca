#ifndef COCA_NOTIFYFLAG_H
#define COCA_NOTIFYFLAG_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <memory>
#include <boost/date_time.hpp>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_server

/**
 *  @ingroup coca_server
 *
 *  @brief Class provides notification means for two separate threads.
 *
 *  This class implements a light-weight communication mechanism between
 *  two threads. Two threads communicate via shared instance of this class.
 *  When one thread wants to notify other about some event (e.g. after
 *  finishing its piece of job) it calls @c notify() method. Downstream
 *  thread waits for notifications with @c wait([timeout]) method. There
 *  is no data associated with the notification event. Multiple calls to
 *  @c notify() when nobody is waiting are equivalen to single @c notify()
 *  call.
 *
 *  Instances of this class can be copied, all copies share the same
 *  state and are equivalent.
 *
 *  This software was developed for the ATLAS project.  If you use all or 
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class NotifyFlag {
public:

    /**
     *  Constructor takes the name of the flag, name is arbitrary string and
     *  is used for  identification only when debugging output is produced.
     */
    NotifyFlag(const std::string& name);

    // destructor
    ~NotifyFlag();

    /**
     *  @brief Send notification to a partner thread.
     */
    void notify();

    /**
     *  @brief Wait for notification to arrive.
     *
     *  If timeout expires before notification is received then false is returned,
     *  otherwise returns true.
     */
    bool wait(const boost::posix_time::time_duration& timeout);
    
protected:

private:

    class NotifyFlagImpl;

    std::shared_ptr<NotifyFlagImpl> m_impl;
};

} // namespace coca
} // namespace daq

#endif // COCA_NOTIFYFLAG_H
