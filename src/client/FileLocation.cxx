//----------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/client/FileLocation.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <iostream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		------------------------------
// 		-- Class Member Definitions --
//		------------------------------

namespace daq {
namespace coca {

//----------------
// Constructors --
//----------------
FileLocation::FileLocation (const std::string &host, const std::string &path)
    : m_host(host)
    , m_path(path)
{
}

//--------------
// Destructor --
//--------------
FileLocation::~FileLocation () throw ()
{
}

/// Standard stream insertion operator
std::ostream&
operator<<(std::ostream &str, const FileLocation& loc)
{
    return str << "Location: " << loc.host() << ':' << loc.path();
}

} // namespace coca
} // namespace daq
