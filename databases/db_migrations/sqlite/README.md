# CoCa schema versions in sqlite

This folder has a bunch of scripts used by unit test that fix schema after it
is created by `DBBase::createSchema()` method. Reason why we need these fixups
is that CORAL does not allow specification of column default value and we use
defaults for backward/forward compatibility.


Each fixup script has a name `coca-sqlite-fixup-N.sql` where `N` is a version
number.
