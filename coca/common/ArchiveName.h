#ifndef COCA_ARCHIVENAME_H
#define COCA_ARCHIVENAME_H
//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>
#include <string>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Dataset.h"
#include "coca/common/IncomingFile.h"
#include "coca/common/LocalFile.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------


namespace daq {
namespace coca {

/// @addtogroup coca_common

/**
 * @ingroup coca_common
 * @brief Namespace containing several helper methods related to 
 * operations on archives and their names.
 */

namespace ArchiveName {
  
    /**
     *  @brief Check if file can be saved without packing.
     *  
     *  Returns true if the file is eligible to be saved as a single file without
     *  packing it into archive. File size must be larger than minimum size.
     *  
     *  @param[in] file     Archived file.
     *  @param[in] dataset  Dataset configuration.
     *  @return Boolean value.
     */
    bool isEligible4SingleFile (const IncomingFile& file,
            const Dataset& dataset);
    
    /**
     *  @brief Make archive name.
     *   
     *  Generate archive name out of dataset name and file name.
     *
     *  @param[in] dataset   Dataset configuration.
     *  @param[in] fileName  The file name, cannot contain slashes.
     *  @return Archive name as a string.
     */
    std::string archiveName (const Dataset& dataset,
                             const std::string& fileName);
    
    /**
     *  @brief Make archive name.
     *   
     *  Generate archive name out of dataset name and archive number.
     *
     *  @param[in] dataset   Dataset configuration.
     *  @param[in] archive   The archive index (integer number). 
     *  @return Archive name as a string.
     */
    std::string archiveName (const Dataset& dataset,
                             unsigned archive);

    /**
     *  @brief Check if file list was saved without packing.
     *  
     *  Returns true if the list of files is eligible to be saved
     *  as a single file (see isEligible4SingleFile()) and archive name 
     *  matches the one generated from dataset and file name. 
     *
     *  @param[in] files     List of files.
     *  @param[in] dataset   The name of the dataset. 
     *  @param[in] archive   The archive name.
     *  @return Boolean value.
     */
    bool wasEligible4SingleFile (const std::vector<LocalFile>& files,
                                 const std::string& dataset, 
                                 const std::string& archive);

} // namespace ArchiveName
} // namespace coca
} // namespace daq

#endif // COCA_ARCHIVENAME_H

