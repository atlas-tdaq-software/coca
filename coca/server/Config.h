#ifndef COCA_CONFIG_H
#define COCA_CONFIG_H
//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <map>
#include <iosfwd>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Dataset.h"
#include "coca/server/ServerIssues.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------


//--------------------
// Class DEfinition --
//--------------------

namespace daq {
namespace coca {

/// @addtogroup coca_server

/**
 * @ingroup coca_server
 * @brief Class holding CoCa server configuration.
 */

class Config {
public:
        
    /**
     *  Extracts configuration information from the current partition,
     *  environment variable TDAQ_PARTITION must be defined.
     * 
     *  @param[in] serverIPC  String containing server IPC name.
     * 
     *  @throw ConfigIssues::TDAQPartitionNotDefined
     *  @throw ConfigIssues::PartitionNotFound
     *  @throw ConfigIssues::ServerNotFound
     *  @throw ConfigIssues::GroupNotFound
     *  @throw ConfigIssues::ConfigError
     */
    explicit Config (const std::string& serverIPC);
    
    // Destructor
    ~Config () throw ();

    /// Returns the server IPC name.
    const std::string& serverIPC () const {return m_server_ipc;}

    /// Returns the partition name.
    const std::string& partition () const {return m_partition;}

    /// Returns the coca database CORAL connection string.
    const std::string& dbConnStr () const {return m_db_conn_str;}

    /// Returns top directory to store cached files.
    const std::string& topDir () const {return m_top_dir;}

    /// Returns name of the CDR directory.
    const std::string& cdrDir () const {return m_CDR_dir;}

    /// Returns host name of CDR server.
    const std::string& cdrHost () const {return m_CDR_host;}

    /// Returns name of the CDR directory on CDR server host.
    const std::string& cdrDirOnCDRHost () const {return m_CDR_dir_on_host;}

    /// Returns name of the archive directory to store archives.
    const std::string& archiveDir () const {return m_archive_dir;}

    /// Percent of disk space to try to keep free
    unsigned freePercentGoal() const { return m_freePercentGoal; }

    /// Max. number of retries when copying file from client
    int maxCopyRetries() const { return m_maxCopyRetries; }

    /**
     *  @brief  Get dataset object for given dataset name.
     *  
     *  @throw ConfigIssues::DatasetNotAllowed
     */
    const Dataset& dataset (const std::string &name) const;

    /// Dump configuration info to a stream
    void print (std::ostream& out) const;

private:

    typedef std::map<std::string, Dataset> DSMap;
    
    std::string m_server_ipc;
    std::string m_partition;
    std::string m_db_conn_str;
    std::string m_top_dir;
    std::string m_CDR_dir;
    std::string m_CDR_host;
    std::string m_CDR_dir_on_host;
    std::string m_archive_dir;
    unsigned m_def_Quota_MB;
    unsigned m_def_TTL_s;
    unsigned m_def_MinArchiveSize_MB;
    unsigned m_def_MaxArchiveSize_MB;
    unsigned m_def_MaxDelay_s;
    mutable DSMap m_datasets;
    bool m_any_dataset_configured;
    Dataset::BundlePolicy m_defBundlePolicy;
    unsigned m_freePercentGoal;
    int m_maxCopyRetries;
};

/// Stream insertion operator to print object contents
std::ostream& 
operator<<(std::ostream& out, const Config& cfg);

} // namespace coca
} // namespace daq

#endif // COCA_CONFIG_H
