//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Description:
//      CoCa server main function
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <sstream>
#include <utility>
#include <stdlib.h>
#include <signal.h>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "cocaIPC/cocaIPC.hh"
#include "coca/common/Issues.h"
#include "coca/common/FileSystem.h"
#include "coca/server/CheckStorage.h"
#include "coca/server/Config.h"
#include "coca/server/DBServer.h"
#include "coca/server/FreeSpace.h"
#include "coca/server/GetFiles.h"
#include "coca/server/MkZip.h"
#include "coca/server/NotifyFlag.h"
#include "coca/server/Server.h"
#include "coca/server/Thread.h"
#include "ers/ers.h"
#include "owl/semaphore.h"
#include "pmg/pmg_initSync.h"
#include "system/Host.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------
using namespace daq::coca;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

namespace {

    ERS_DECLARE_ISSUE (SrvIssues, CommandLine,
     "bad command line: " << error, ((std::string) error))

    ERS_DECLARE_ISSUE (SrvIssues, GotSignal,
     "got signal number: " << signum, ((int) signum))

    ERS_DECLARE_ISSUE (SrvIssues, ChildTerminated,
	 "child thread terminated unexpectedly: " << name, ((std::string) name))

    ERS_DECLARE_ISSUE(SrvIssues, UncaughtException, "Uncaught exception, terminating.", )


    // Initially this semaphore is at 0 and we pass it to anyone
    // who can decide to stop the whole shebang (like signal handler
    // or corba server)
    OWLSemaphore g_semaphore;

    // signal handler stuff
    int signalFired = 0;
    extern "C"
    void sighandler(int sig)
    {
        g_semaphore.post();
    	signalFired = sig;
    }

    /// raise exception if signal was fired
    void throwIfSignalled()
    {
    	if ( signalFired ) {
            SrvIssues::GotSignal exc (ERS_HERE, signalFired);
            ERS_DEBUG (0, exc);
            throw exc;
    	}
    }

}

// ------------
// --- Main ---
// ------------
int
main (int argc, char **argv)
try {
    // register signal handler for usual set of terminating signals
    signal(SIGTERM, ::sighandler);
    signal(SIGINT, ::sighandler);
    signal(SIGQUIT, ::sighandler);

    // init IPC stuff
    IPCCore::init (argc, argv);

    /*
     * command line
     */
    ::throwIfSignalled();

    po::options_description cmdline_options("Available options");
    cmdline_options.add_options()
            ("help,h", "print usage and exit")
            ("name,n", po::value<std::string>()/*->value_name("STRING")*/->default_value(""),
                    "application name, if not specified then $TDAQ_APPLICATION_NAME is used")
            ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, cmdline_options), vm);

    if (vm.count("help")) {
        std::cout << "CoCa server application.\n\n"
                << "Usage: " << argv[0] << " [options]\n\n"
                << cmdline_options << "\n";
        return 0;
    }

    po::notify(vm);

    std::string name = vm["name"].as<std::string>();
    if (name.empty()) {
        if (const char* var = getenv("TDAQ_APPLICATION_NAME")) {
            name = var;
        } else {
            SrvIssues::CommandLine err
                (ERS_HERE, "if variable TDAQ_APPLICATION_NAME is not set,"
                 " the option '-n' is mandatory");
            ers::error(err);
            return 1;
        }
    }

    ::throwIfSignalled();
    Config config (name);

    ::throwIfSignalled();

    // add TDAQ_RELEASE_NAME in directory path and in db, but not in IPC name
    std::string ipc_server = config.serverIPC();
    std::string unique_ipc_server = TDAQ_RELEASE_NAME "/" + ipc_server;

    fs::path ipc_path = config.topDir();
    ipc_path /= unique_ipc_server;
    std::unique_ptr<DBServer> db;
    try {
        db.reset(new DBServer(config.dbConnStr(),
                unique_ipc_server,
                System::LocalHost::full_local_name(),
                ipc_path.string(),
                config.cdrHost(),
                config.cdrDirOnCDRHost(),
                config.archiveDir()));
    } catch (const Issues::DBConnectionFailure& ex) {
        ers::error(ex);
        return 1;
    }

    ::throwIfSignalled();

    IPCPartition *partition = new IPCPartition (config.partition());
    if (!partition->isValid()) {
        ers::error(SrvIssues::CommandLine
                  (ERS_HERE, "invalid partition: " + config.partition()));
        return 1;
    }

    ::throwIfSignalled();

    // communication endpoints for threads
    NotifyFlag corba2getFiles("corba2getFiles");
    NotifyFlag getFiles2mkZip("getFiles2mkZip");
    NotifyFlag mkZip2checkStor("mkZip2checkStor");

    // Start all threads
    ERS_DEBUG (0, "creating server");
    std::shared_ptr<Server> server =
            Server::makeServer(*partition, ipc_server, *db, config, corba2getFiles, g_semaphore);

    StdFileSystem filesystem;
    FreeSpace free_space(*db, config.freePercentGoal(), filesystem);
    Thread get_files_thread(GetFiles(*db, corba2getFiles, getFiles2mkZip, free_space, g_semaphore,
                                     config.maxCopyRetries(), filesystem));
    Thread mk_zip_thread(MkZip(*db, config.cdrDir(), getFiles2mkZip, mkZip2checkStor, free_space,
                               g_semaphore, filesystem));
    Thread check_storage_thread(CheckStorage(*db, ".COPIED", mkZip2checkStor, g_semaphore));

    // ok, configuration completed
    pmg_initSync();
    ERS_LOG ("CoCa server '" << unique_ipc_server << "' created in partition '" << config.partition() << "'");

    // wait until someone decides to stop and raises semaphore for us
    g_semaphore.wait();

    // disable registration during wind-down period
    server->disable();

    // signal is a normal way to stop, just say we finished OK
    if (::signalFired) {
        ERS_LOG("stop due to signal number " << ::signalFired);
    } else {
        ERS_LOG("stop due to child exception or CORBA shutdown");
    }

    // All threads should be stopped correctly when their destructors are called

    return 0;

} catch (const po::error& ex) {
    std::cerr << ex.what() << "\n"
            "Use --help option for usage information.\n";
    return 1;
} catch (const std::exception& ex) {
    ers::error(SrvIssues::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(SrvIssues::UncaughtException(ERS_HERE));
    return 1;
}
