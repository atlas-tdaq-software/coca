#ifndef COCA_DBSERVER_H
#define COCA_DBSERVER_H
//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//----------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <vector>
#include <map>
#include <memory>
#include <boost/thread/mutex.hpp>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Size.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq {
namespace coca {
class CachedFile;
class Dataset;
class DBBase;
class IncomingFile;
class LocalFile;
class ServerInfo;
class TransientFile;
class Transaction;
}
}


//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_server

/**
 *  @ingroup coca_server
 *
 *  @brief Database API for CoCa server.
 *
 *  This class implements operations with database required by coca
 *  server.
 */

class DBServer {
public:

    /**
     *  @brief Standard constructor.
     *
     *  @param[in] connStr      CORAL connection string
     *  @param[in] serverIPC    Server IPC name
     *  @param[in] serverHost   Name of the host where server is running
     *  @param[in] topDir       Top directory with coca files
     *  @param[in] cdrHost      Host name where CDR server is running
     *  @param[in] cdrDirOnCDRHost Location of CDR directory on CDR host
     *  @param[in] archive_dir   archiving location
     */
    DBServer (const std::string& connStr,
              const std::string& serverIPC,
              const std::string& serverHost,
              const std::string& topDir,
              const std::string& cdrHost,
              const std::string& cdrDirOnCDRHost,
              const std::string& archive_dir);

    // noncopyable
    DBServer(const DBServer&) = delete;
    DBServer& operator=(const DBServer&) = delete;

    // destructor
    ~DBServer () throw ();

    /**
     *  @brief Add all files in the new registration entry.
     *
     *  Adds a file in the new registration request to the currently
     *  open dataset's archive (or new archive if current archive needs
     *  to be closed). Returns name of the archive.
     *
     *  @param[in] file    New file to be added
     *  @param[in] dataset CoCa dataset object
     *  @return  Name of the archive
     *
     *  @throw Issues::FileAlreadyRegistered  Thrown if any of the files
     *                                        has been registered before
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error
     */
    std::string addNewFile(const IncomingFile& file, const Dataset& dataset);

    /**
     *  @brief Get list of files to be retrieved from clients.
     *
     *  @return  List of files to retrieve.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error
     */
    std::vector<TransientFile> filesToRetrieve();

    /**
     *  @brief Get list of files that can be deleted from cache.
     *
     *  File can be deleted from cache when it was already archived, that it
     *  its corresponding archive object was moved to long term storage.
     *  Returned list is not ordered in any way, it's up to the caller to
     *  decide in which order to delete files.
     *
     *  @param[in] sameServer If true then return files that were created by
     *          the same server instance on the same host, otherwise return
     *          files from any server on the same host.
     *  @return  List of files to delete.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error
     */
    std::vector<CachedFile> filesToDelete(bool sameServer = true);

    /**
     *  @brief Mark file as copied into the cache.
     *
     *  @param[in] dataset  CoCa dataset name.
     *  @param[in] relPath  File's relative path name.
     *  @param[in] size     File size, bytes.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error.
     */
    void addToCache (const std::string& dataset,
                     const std::string& relPath,
                     Size size);

    /**
     *  @brief Mark file as removed from the cache.
     *
     *  @param[in] dataset  CoCa dataset name.
     *  @param[in] relPath  File's relative path name.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error.
     */
    void removeFromCache (const std::string& dataset,
                          const std::string& relPath);

    /**
     *  @brief Mark file was removed from the client machine.
     *
     *  @param[in] dataset  CoCa dataset name.
     *  @param[in] relPath  File's relative path name.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error.
     */
    void removeFromClient (const std::string& dataset,
                           const std::string& relPath);

    /**
     *  @brief Get list of archives to move to CDR.
     *
     *  Returns a map whose keys are archive names and values are the lists of
     *  files which belong to corresponding archive. Only returns archives that are
     *  ready to be moved to CDR, that is: closed, not in CDR yet, and all files
     *  are in cache.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error.
     */
    std::map<std::string, std::vector<LocalFile> > archivesToCDR();

    /**
     *  @brief Updsate archive record with new size and checksum.
     *
     *  @param[in] archive  Archive file name.
     *  @param[in] size     Size of the archive file.
     *  @param[in] checksum Checksum of the archive file.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error.
     */
    void setArchiveMeta(const std::string& archive, Size size, std::string const& checksum);

    /**
     *  @brief Mark archive as created in CDR directory.
     *
     *  @param[in] archive  Archive file name.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error.
     */
    void setArchiveOnCDR (const std::string& archive);

    /**
     *  @brief Close all archives that are currently open.
     *
     *  This method may be called when there is a need to free space in
     *  the cache directory by moving all files to archives.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error.
     */
    void closeAllArchives ();

    /**
     *  @brief Close all archives which lived past their max delay time.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error
     */
    void closeOldArchives ();

    /**
     *  @brief Get the list of archives not yet archived.
     *
     *  Returns the list of archive paths in CDR that have been sent to CDR
     *  already and which still may be in CDR.
     *
     *  @return List of archive paths.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error.
     */
    std::vector<std::string> archivesNotArchived();

    /**
     *  @brief Mark archive as copied to final archive location.
     *
     *  @param[in] archive  Archive file name.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error.
     */
    void setArchived (const std::string& archive);

    /**
     *  @brief Mark archive as removed.
     *
     *  Archive can be marked as removed at any moment, even if it is not been
     *  stored or sent to CDR yet. Removed archives are ignored by other queries,
     *  so this flag can be used to make archive as damaged, e.g. when archive
     *  cannot be created because files disappeared from cache.
     *
     *  @param[in] archive  Archive file name.
     *
     *  @throw Issues::DBConnectionFailure Thrown if database connection fails
     *  @throw Issues::CoralException Thrown in case of database error.
     */
    void setRemoved(const std::string& archive);

private:

    /**
     *  @brief Find ID (key in ARCHIVES table) of the archive for the file
     *  which has been registered previously.
     *
     *  This method does not do mutex locking, can only be called when
     *  then mutex is locked already by caller.
     *
     *  @param[in] dataset  CoCa dataset name.
     *  @param[in] relPath  File's relative path name.
     *  @return   Archive ID.
     *
     *  @throw Issues::NoSuchRecord Thrown if the file was not registered.
     *  @throw coral::Exception Thrown in case of database error.
     */
    unsigned findArchiveId (const std::string& dataset,
                            const std::string& relPath);

    /**
     *  @brief Close an archive given dataset ID and archive ID.
     *
     *  @param[in] dataset  CoCa dataset ID.
     *  @param[in] archive  Archive ID.
     *  @param[in] archiveSize  Archive size, if 0 then do not update archive size.
     *  @param[in] checksum  Archive checksum, if empty then do not set archive checksum.
     *
     *  @throw coral::Exception Thrown in case of database error.
     */
    void closeArchive (unsigned dataset, unsigned archive,
                       Size archiveSize=Size(0), std::string const& checksum=std::string());

    /**
     *  @brief Register new archive.
     *
     *  @param[in] dataset  CoCa dataset ID.
     *  @param[in] archive  Archive ID.
     *  @param[in] name     Archive name.
     *  @param[in] ttl_s    Time To Live in seconds.
     *  @param[in] max_delay_s Max delay until CDR in seconds.
     *
     *  @throw coral::Exception Thrown in case of database error.
     *  @throw Issues::FileAlreadyRegistered Thrown in case archive name is already known.
     */
    void newArchive (unsigned dataset,
                     unsigned archive,
                     const std::string& name,
                     unsigned ttl_s,
                     unsigned max_delay_s);

    /**
     *  @brief Find an open archive for a given dataset.
     *
     *  @param[in] dataset  CoCa dataset ID.
     *  @param[out] archive  Archive ID.
     *  @param[out] name     Archive name.
     *  @param[out] totsize  Current archive size in MBytes.
     *  @return  True if the archive was found, false if no archive is
     *           defined for this dataset.
     *
     *  @throw coral::Exception Thrown in case of database error.
     */
    bool findOpenArchive (unsigned dataset,
                          unsigned& archive,
                          std::string& name,
                          Size& totsize);

    /**
     *  @brief Increase archive size by the size of the new files.
     *
     *  @param[in] dataset   CoCa dataset ID.
     *  @param[in] archive   Archive ID.
     *  @param[in] addsize   Size of the new files to add.
     *  @param[in] tran      Transaction context.
     *
     *  @throw coral::Exception Thrown in case of database error.
     */
    void updateArchiveSize (unsigned dataset,
                            unsigned archive,
                            Size addsize,
                            Transaction& tran);

    /**
     *  @brief Add one more file to a specified archive.
     *
     *  @param[in] dataset   CoCa dataset ID.
     *  @param[in] archive   Archive ID.
     *  @param[in] file      File to add.
     *  @param[in] tran      Transaction context.
     *
     *  @throw Issues::FileAlreadyRegistered Thrown when file name is already known.
     *  @throw coral::Exception Thrown in case of database error.
     */
    void addFile (unsigned dataset,
                  unsigned archive,
                  const IncomingFile& file,
                  Transaction& tran);

    /**
     *  @brief Initialize ServerInfo object if not initialized yet
     *  and return a reference to it.
     *
     *  This method does not do mutex locking, can only be called when
     *  then mutex is locked already by caller.
     *
     *  @throw coral::Exception Thrown in case of database error.
     */
    const ServerInfo& serverInfo();

    // Data members
    std::unique_ptr<DBBase> m_impl;            ///< Common operations implementation object
    const std::string m_serverIPC;
    const std::string m_serverHost;
    const std::string m_topDir;
    const std::string m_cdrHost;
    const std::string m_cdrDirOnCDRHost;
    const std::string m_archive_dir;
    std::unique_ptr<ServerInfo> m_serverInfo;  ///< CoCa server information
    boost::mutex m_mutex;                    ///< Mutex for synchronization

};

} // namespace coca
} // namespace daq

#endif // COCA_DBSERVER_H
