from __future__ import annotations

__all__ = ["metadata_remove", "metadata_get", "metadata_set", "metadata_show"]

import json
import sys

from .. import DBClient


def metadata_get(connection_string: str, key: str) -> None:
    """Print value of the metadata item.

    Parameters
    ----------
    connection_string : `str`
        CORAL connection string for CoCa database.
    key : `str`
        Metadata key.
    """
    client = DBClient(connection_string)
    metadata = client.metadata()
    if not metadata.contains(key):
        raise KeyError(f"Metadata key {key!r} does not exist.")
    value = metadata.get(key)
    print(value)


def metadata_set(connection_string: str, key: str, value: str) -> None:
    """Add or update metadata item.

    Parameters
    ----------
    connection_string : `str`
        CORAL connection string for CoCa database.
    key : `str`
        Metadata key.
    value : `str`
        Metadata value.
    """
    client = DBClient(connection_string)
    metadata = client.metadata()
    metadata.set(key, value)


def metadata_show(connection_string: str, use_json: bool) -> None:
    """Show contents of APDB metadata table.

    Parameters
    ----------
    connection_string : `str`
        CORAL connection string for CoCa database.
    use_json : `bool`
        If True dump in JSON format.
    """
    client = DBClient(connection_string)
    metadata = client.metadata()
    if use_json:
        data = {key: value for key, value in metadata.items().items()}
        json.dump(data, sys.stdout, indent=2)
        print()
    else:
        data = metadata.items()
        for key, value in data.items():
            print(f"{key}: {value}")


def metadata_remove(connection_string: str, key: str) -> None:
    """Delete metadata key.

    Parameters
    ----------
    connection_string : `str`
        CORAL connection string for CoCa database.
    key : `str`
        Metadata key.
    """
    client = DBClient(connection_string)
    metadata = client.metadata()
    metadata.remove(key)
