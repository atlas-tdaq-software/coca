#ifndef COCA_CORAL_HELPERS_H
#define COCA_CORAL_HELPERS_H
//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <CoralBase/Attribute.h>
#include <CoralBase/AttributeList.h>
#include <RelationalAccess/IQuery.h>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Size.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//		---------------------
// 		-- Class Interface --
//		---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_common

/**
 *  @ingroup coca_common
 *
 *  @brief Few helper functions to simplify CORAL coding.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */
namespace coral_helpers {

    /**
     *  @brief Add an attribute to the CORAL attribute list and set its value.
     *
     *  @param data   CORAL attribute list.
     *  @param name   Attribute name.
     *  @param value  Attribute value.
     */
    template <typename T>
    void
    addAttr(coral::AttributeList& data, const std::string& name, const T& value) {
        const size_t idx = data.size() ;
        data.extend<T>(name);
        data[idx].data<T>() = value;
    }
    template <>
    inline void
    addAttr(coral::AttributeList& data, const std::string& name, const Size& value) {
        const size_t idx = data.size() ;
        data.extend<Size::value_type>(name);
        data[idx].data<Size::value_type>() = value.bytes();
    }

    /**
     *  @brief Add an attribute to the CORAL attribute list and make it NULL.
     *
     *  @param data   CORAL attribute list.
     *  @param name   Attribute name.
     */
    template <typename T>
    void
    addAttr(coral::AttributeList& data, const std::string& name) {
        const size_t idx = data.size() ;
        data.extend<T>(name);
        data[idx].setNull();
    }
    template <>
    inline void
    addAttr<Size>(coral::AttributeList& data, const std::string& name) {
        const size_t idx = data.size() ;
        data.extend<Size::value_type>(name);
        data[idx].setNull();
    }

    /**
     *  @brief Set a value of the attribute in a CORAL attribute list.
     *
     *  @param data   CORAL attribute list.
     *  @param name   Attribute name.
     *  @param value  Attribute value.
     */
    template <typename T>
    void
    setAttr(coral::AttributeList& data, const std::string& name, const T& value) {
        data[name].data<T>() = value;
    }
    template <>
    inline void
    setAttr(coral::AttributeList& data, const std::string& name, const Size& value) {
        data[name].data<Size::value_type>() = value.bytes();
    }

    /**
     *  @brief Define output variable for query.
     *
     *  @param query  CORAL query object.
     *  @param name   Attribute name.
     */
    template <typename T>
    void
    defineOutput(coral::IQuery& query, const std::string& name) {
        query.addToOutputList(name) ;
        query.defineOutputType(name, coral::AttributeSpecification::typeNameForType<T>()) ;
    }

    template <>
    inline void
    defineOutput<Size>(coral::IQuery& query, const std::string& name) {
        query.addToOutputList(name) ;
        query.defineOutputType(name, coral::AttributeSpecification::typeNameForType<Size::value_type>()) ;
    }

    /**
     *  @brief Define output variable for query.
     *
     *  @param query  CORAL query object.
     *  @param expr   SQL expression.
     *  @param name   Alias for expression.
     */
    template <typename T>
    void
    defineOutput(coral::IQuery& query, const std::string& expr, const std::string& name) {
        query.addToOutputList(expr, name) ;
        query.defineOutputType(name, coral::AttributeSpecification::typeNameForType<T>()) ;
    }
    template <>
    inline void
    defineOutput<Size>(coral::IQuery& query, const std::string& expr, const std::string& name) {
        query.addToOutputList(expr, name) ;
        query.defineOutputType(name, coral::AttributeSpecification::typeNameForType<Size::value_type>()) ;
    }

    /**
     *  @brief Get attribute value or default value if attribute is NULL.
     *
     *  @param attr     CORAL attribute object.
     *  @param defValue Default value to return if attribute is NULL.
     */
    template <typename T>
    T
    getAttr(const coral::Attribute& attr, T defValue) {
        if (attr.isNull()) {
            return defValue;
        } else {
            return attr.data<T>();
        }
    }

    /**
     *  @brief Specialization of the above method for Size type.
     *
     *  @param attr     CORAL attribute object.
     *  @param defValue Default value to return if attribute is NULL.
     */
    template <>
    inline Size
    getAttr(const coral::Attribute& attr, Size defValue) {
        if (attr.isNull()) {
            return defValue;
        } else {
            return Size(attr.data<Size::value_type>());
        }
    }

} // namespace coral_helpers
} // namespace coca
} // namespace daq

#endif // COCA_CORAL_HELPERS_H
