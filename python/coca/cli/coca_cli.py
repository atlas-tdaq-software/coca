from __future__ import annotations

__all__ = ["main"]

import argparse

from .. import scripts


def main() -> None:
    parser = argparse.ArgumentParser(description="CoCa command line interface")
    subparsers = parser.add_subparsers(title="available subcommands", required=True)
    _metadata_subcommand(subparsers)

    parsed_args = parser.parse_args()

    kwargs = vars(parsed_args)
    # Strip keywords not understood by scripts.
    method = kwargs.pop("method")
    method(**kwargs)


def _metadata_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser("metadata", help="Commands to query or update metadata.")
    subparsers = parser.add_subparsers(title="available subcommands", required=True)
    _metadata_show_subcommand(subparsers)
    _metadata_get_subcommand(subparsers)
    _metadata_set_subcommand(subparsers)
    _metadata_remove_subcommand(subparsers)


def _metadata_show_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser("show", help="Show contents of APDB metadata table.")
    parser.add_argument(
        "-j",
        "--json",
        dest="use_json",
        help="Dump metadata in JSON format.",
        default=False,
        action="store_true",
    )
    _arg_conn_str(parser)
    parser.set_defaults(method=scripts.metadata_show)


def _metadata_get_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser("get", help="Print value of the metadata item.")
    _arg_conn_str(parser)
    _arg_key(parser)
    parser.set_defaults(method=scripts.metadata_get)


def _metadata_set_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser("set", help="Add or update metadata item.")
    _arg_conn_str(parser)
    _arg_key(parser)
    _arg_value(parser)
    parser.set_defaults(method=scripts.metadata_set)


def _metadata_remove_subcommand(subparsers: argparse._SubParsersAction) -> None:
    parser = subparsers.add_parser("remove", help="Remove metadata item.")
    _arg_conn_str(parser)
    _arg_key(parser)
    parser.set_defaults(method=scripts.metadata_remove)


def _arg_conn_str(parser: argparse.ArgumentParser) -> None:
    parser.add_argument("connection_string", help="Connection string for CoCa database.")


def _arg_key(parser: argparse.ArgumentParser) -> None:
    parser.add_argument("key", help="Metadata key, arbitrary string.")


def _arg_value(parser: argparse.ArgumentParser) -> None:
    parser.add_argument("value", help="Corresponding metadata value.")
