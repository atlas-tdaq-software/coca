//----------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/client/Register.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>
#include <stdlib.h>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Checksum.h"
#include "coca/common/FileSystem.h"
#include "cocaIPC/cocaIPC.hh"
#include "ers/ers.h"
#include "ipc/object.h"
#include "system/Host.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace fs = boost::filesystem;

#define CATCH_CORBA_EXCEPTIONS \
    catch (const CORBA::SystemException& ex) { \
        std::string msg ("CORBA::SystemException: "); \
        const char* minor = ex.NP_minorString(); \
        msg += std::string (ex._name()) + "(" + (minor ? minor : "") + ")"; \
        ERS_DEBUG (0, msg); \
        throw daq::coca::RegisterIssues::IPCException (ERS_HERE, msg); \
    } \
    catch(const CORBA::Exception& ex) { \
        std::string msg ("CORBA::Exception: "); \
        msg += ex._name(); \
        ERS_DEBUG (0, msg); \
        throw daq::coca::RegisterIssues::IPCException (ERS_HERE, msg); \
    } \
    catch (const daq::ipc::Exception& ex) { \
        std::string msg ("daq::ipc::Exception: "); \
        msg += ex.what(); \
        ERS_DEBUG (0, msg); \
        throw daq::coca::RegisterIssues::IPCException (ERS_HERE, msg, ex); \
    }

namespace {


    // Maps cocaIPC::Server::RegStatus error code into string
    std::string
    ErrorStr(cocaIPC::Server::RegStatus status)
    {
        switch (status) {
        case cocaIPC::Server::ErrRegistrationDisabled:
            return "registration currently disabled";
        case cocaIPC::Server::ErrMaxArchiveSizeExceeded:
            return "maximum archive size exceeded";
        case cocaIPC::Server::ErrQuotaExceeded:
            return "quota exceeded";
        case cocaIPC::Server::ErrInvalidDataset:
            return "invalid dataset";
        case cocaIPC::Server::ErrFileAlreadyRegistered:
            return "file already registered";
        case cocaIPC::Server::ErrDBDisconnected:
            return "coca lost database connection";
        default:
            return "unexpected code: " + boost::lexical_cast<std::string>(int(status));
        }
    }

    // global partition/server string, this is a hack to do in-place
    // patching of the https://savannah.cern.ch/bugs/?92489
    // which relies on the fact that normal apps do not need
    // more than one coca connection
    std::string partitionHack, serverHack;

}


//              ------------------------------
//              -- Class Member Definitions --
//              ------------------------------

namespace daq {
namespace coca {

Register::Register(const std::string& server, const std::string& partition)
{
    ::partitionHack = partition;
    ::serverHack = server;
}

Register::~Register()
    throw ()
{
}

std::string
Register::registerFile(const std::string& dataset,
                       const std::string& file,
                       int prio,
                       unsigned dtcSec,
                       const std::string& baseDir)
{
    // if baseDir is not empty it mast be an absolute path
    ERS_PRECONDITION(baseDir.empty() || baseDir[0] == '/');

    if (CORBA::is_nil(m_server)) {
        try {

            IPCPartition part(::partitionHack);

            // _duplicate is called to make sure that the
            // object is not released when IPCCache is destroyed
            // otherwise this will crash the application if it was dlopen/dlclose this library
            m_server = cocaIPC::Server::_duplicate(part.lookup<cocaIPC::Server>(::serverHack));
        }
        CATCH_CORBA_EXCEPTIONS;
    }

    std::string archive;
    try {

        cocaIPC::NewFile newFile;
        std::string machine = System::LocalHost::local_name();

        ERS_ASSERT (not file.empty());

        // split file path into base directory and relative path
        std::pair<std::string, std::string> dirAndPath;
        try {
            dirAndPath = FileSystem::split(file, baseDir);
        } catch (const FileSystem::Exception& ex) {
            RegisterIssues::PathNameError err(ERS_HERE, baseDir, file, ex);
            ERS_DEBUG (0, err);
            throw err;
        }

        const std::string& dirName = dirAndPath.first;
        const std::string& relPath = dirAndPath.second;
        fs::path path = dirName;
        path /= relPath;

        // check that file indeed exists
        if (not fs::exists(path)) {
            RegisterIssues::MissingFile err (ERS_HERE, path.string());
            ERS_DEBUG (0, err);
            throw err;
        }

        // get the checksum
        auto checksum = Checksum::md5_file(path);

        ERS_DEBUG (1, path << " - " << relPath);
        newFile.relPath = relPath.c_str();
        newFile.size = get_fs().file_size(path).bytes();
        newFile.prio = std::min(prio, 100);
        newFile.dtcSec = dtcSec;
        newFile.machine = machine.c_str();
        newFile.baseDir = dirName.c_str();
        newFile.checksum = checksum.c_str();


        // Send registration request
        ERS_DEBUG (0, "registering files");
        CORBA::String_var archive_corba;
        cocaIPC::Server::RegStatus error = m_server->registerFile(newFile, dataset.c_str(), archive_corba);

        if (error == cocaIPC::Server::ErrNoError) {
            // got OK answer, stop here
            archive = (const char *) archive_corba;
        } else {
            // errors happened
            RegisterIssues::RegistrationError err(ERS_HERE, error, ErrorStr(error));
            ERS_DEBUG (0, err);
            throw err;
        }

    }
    CATCH_CORBA_EXCEPTIONS;

    ERS_DEBUG (0, "ARCHIVE: " << archive);
    return archive;
}

std::string
Register::configuration ()
{
    if (CORBA::is_nil(m_server)) {
        try {

            IPCPartition part(::partitionHack);

            // _duplicate is called to make sure that the
            // object is not released when IPCCache is destroyed
            // otherwise this will crash the application if it was dlopen/dlclose this library
            m_server = cocaIPC::Server::_duplicate(part.lookup<cocaIPC::Server>(::serverHack));
        }
        CATCH_CORBA_EXCEPTIONS;
    }

    try {
        CORBA::String_var config_str(m_server->configuration ());
        return std::string(config_str);
    }
    CATCH_CORBA_EXCEPTIONS;
}

FileSystemABC& Register::get_fs()
{
    static StdFileSystem fs;
    return fs;
}

} // namespace coca
} // namespace daq
