Connectivity Test for CoCa Server
=================================

This note describes steps needed to do simple network connectivity test
for coca server in standalone setup.

This is based on standalone test partition where coca server is running
agains Oracle INTR database (Database="oracle://INTR/ATLAS_COCA"). The idea
is to setup SSH tunnel from local machine to Oracle server and then
kill/restart ssh process to simulate network disconnect.

Step 1. Server host
-------------------

The name of the Oracle server is needed to setup tunnel, it is not enough
to look for INTR service in tnsnames.ora file because of the Oracle redirect.
Easiest is to run actual session on a machine and see which exact machine it
connects to. Something like:

    $ sqlplus ATLAS_COCA/%%%%%%%%@INTR

and then in other window run 

    $ lsof -p <PID> | gawk '/TCP/ {print $9}'
    lxplus0036.cern.ch:38620->itrac1312-v.cern.ch:10121

In that output `itrac1312-v.cern.ch` is the name of the actual server, and
10121 is the standard Orcale port number.

Step 2. Start tunnel
--------------------

Using the host name from previous step setup SSH port forwarding from local
host to that server:

    $ ssh -L '*:10121:itrac1312-v.cern.ch:10121' -x localhost "sleep 3600"

Step 3. Update tnsnames.ora
---------------------------

Copy standard tnsnames.ora file (and sqlnet.ora) to a new location and update
host name for INTR service:

    $ mkdir ./TNS_ADMIN
    $ cp $TND_ADMIN/sqlnet.ora ./TNS_ADMIN
    $ cp $TND_ADMIN/tnsnames.ora ./TNS_ADMIN
    $ vi ./TNS_ADMIN/tnsnames.ora

Find definition of INTR service which should look like:

    INTR=(
            DESCRIPTION=
            (ADDRESS= (PROTOCOL=TCP) (HOST=intr-s.cern.ch) (PORT=10121) )
            (LOAD_BALANCE=on)
            (ENABLE=BROKEN)
            (CONNECT_DATA=
                    (SERVER=DEDICATED)
                    (SERVICE_NAME=intr.cern.ch)
            )
    )

and change `HOST=` to be the host name where SSH tunnel is running.

Step 4. Update OKS
------------------

Coca server application (or the whole segment where it runs) has to use
modified tnsnames.ora file. For that add new environment variable to OKS
configuration with the name `TNS_ADMIN` pointing to a directory created
in previous step.

Step 5. Run whole thing
-----------------------

Start partition as usual and initialize to run coca server. Stop or restart
SSH process to simulate network disconnect and whatch what happens to coca
server.
