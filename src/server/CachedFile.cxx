//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/server/CachedFile.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/FileSystem.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

namespace daq {
namespace coca {

//----------------
// Constructors --
//----------------
CachedFile::CachedFile(const boost::filesystem::path& serverDir,
                       const boost::filesystem::path& relPath,
                       const std::string& dataset,
                       const std::string& serverIPC,
                       Size size,
                       int prio,
                       time_t registeredAt,
                       time_t keepUntil,
                       const std::string& checksum)
    : m_serverDir(serverDir)
    , m_relPath(relPath)
    , m_dataset(dataset)
    , m_serverIPC(serverIPC)
    , m_size(size)
    , m_prio(prio)
    , m_registeredAt(registeredAt)
    , m_keepUntil(keepUntil)
    , m_checksum(checksum)
{
}

// Returns path name on coca server.
boost::filesystem::path
CachedFile::serverPath () const
{
    return boost::filesystem::path(FileSystem::join(m_serverDir.string(), m_relPath.string()));
}

// Returns true if given time is past file expiration
bool
CachedFile::expired(time_t time) const
{
    return time > m_keepUntil;
}

/// Returns file caching "cost" at specific time.
CachedFile::cost_t
CachedFile::cacheCost(time_t time) const
{
    // Cost is calculated as:
    //   expired_cost + time_since_registration(days) - prio + file_size(GB)
    //
    // - expired_cost is a very large constant number for files which
    //   are expired, and 0 for non-expired.
    // - cost unit above is a "day" (one priority unit costs a day, and 1GB = 1day),
    //   actual calculations are done in units of seconds to avoid floating calculations

    // cost is proportional to time in seconds since registration
    cost_t cost = time - m_registeredAt;

    // one unit of priority adds one day, higher priority means lower cost
    const int sec_in_a_day = 24*3600;
    cost -= m_prio * sec_in_a_day;

    // at the same timestamp files with larger size cost more,
    // add size in GB multipled by seconds in a day
    cost += (m_size.megabytes() * sec_in_a_day) / 1024;

    // if file is expired then add large constant cost, constant here
    // is equivalent to ~10 years in cache
    if (expired(time)) {
        cost += 10 * 365 * 24 * 3600;
    }

    return cost;
}

/// Stream insertion operator to print object contents
std::ostream&
operator<<(std::ostream& out, const CachedFile& f)
{
    out << "CachedFile(RelPath=" << f.relPath() << ", Size=" << f.size() << ", "
        << "DirServer=" << f.serverDir() << ", Dataset=" << f.dataset()
        << ", ServerIPC=" << f.serverIPC() << ")";
    return out;
}

} // namespace coca
} // namespace daq
