//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Description:
//      Class NextId...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/common/NextId.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <memory>
#include <CoralBase/Attribute.h>
#include <CoralBase/AttributeList.h>
#include <CoralBase/Exception.h>
#include <RelationalAccess/ICursor.h>
#include <RelationalAccess/IQuery.h>
#include <RelationalAccess/ISchema.h>
#include <RelationalAccess/ITable.h>
#include <RelationalAccess/ITableDataEditor.h>
#include <RelationalAccess/TableDescription.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Issues.h"
#include "coca/common/Transaction.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------
namespace {

    // Helper function to simplify adding new elements to attribute lists
    template <typename T>
    void
    addAttr(coral::AttributeList& data, const std::string& name, const T& value) {
        // add one more attribute to the list
        const size_t idx = data.size() ;
        data.extend<T>(name);
        data[idx].setValue(value);
    }

}

namespace daq {
namespace coca {
/*
 * NextId
 */

NextId::NextId(const std::string& tablename)
    : m_name(tablename)
{
}

NextId::~NextId ()
    throw ()
{
}

unsigned
NextId::next (const std::string& seq, unsigned seqIdx, Transaction& tr)
{
    unsigned next_id = 0;

    coral::ISchema& schema =tr.session().nominalSchema();
    coral::ITable& table = schema.tableHandle(m_name);
    std::unique_ptr<coral::IQuery> query (table.newQuery());

    // What
    query->addToOutputList("NEXTID");
    query->defineOutputType("NEXTID", coral::AttributeSpecification::typeNameForType<uint32_t>());

    // Where
    const char* cond = "WHAT=:WHAT AND SPEC=:SPEC";
    coral::AttributeList condData;
    ::addAttr(condData, "WHAT", seq);
    ::addAttr(condData, "SPEC", seqIdx);
    query->setCondition(cond, condData);

    // lock it
    query->setForUpdate();

    // run query
    coral::ICursor& cursor = query->execute();
    if ( cursor.next() ) {

      // get current value
      next_id = cursor.currentRow()[0].data<uint32_t>();

      ERS_ASSERT(next_id >= 1);
      ERS_ASSERT(not cursor.next());

      // update value
      const char *setClause = "NEXTID = NEXTID + 1";
      table.dataEditor().updateRows(setClause, cond, condData);

    } else {

      // insert new row
      next_id = 1;
      coral::AttributeList insertData;
      ::addAttr(insertData, "WHAT", seq);
      ::addAttr(insertData, "SPEC", seqIdx);
      ::addAttr(insertData, "NEXTID", next_id+1);
      table.dataEditor().insertRow(insertData);

    }

    ERS_DEBUG (3, seq << '[' << seqIdx << "]: going to use id: " << next_id);
    return next_id;
}

void
NextId::createTable(int schemaVersion, Transaction& tran)
{
    auto& schema = tran.session().nominalSchema();

    coral::TableDescription td("NextId::createTable");
    td.setName(m_name);
    td.insertColumn("WHAT", "string");
    td.insertColumn("SPEC", "unsigned");
    td.insertColumn("NEXTID", "unsigned");
    td.setPrimaryKey(std::vector<std::string>({"WHAT", "SPEC"}));
    td.setNotNullConstraint("WHAT");
    td.setNotNullConstraint("SPEC");
    td.setNotNullConstraint("NEXTID");
    // td.createIndex("COCA_NEXTID_PK", std::vector<std::string>({"WHAT", "SPEC"}), true);
    schema.createTable(td);
}

} // namespace coca
} // namespace daq
