#ifndef COCA_REGISTER_H
#define COCA_REGISTER_H
//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <vector>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/client/ClientIssues.h"
#include "coca/common/FileSystem.h"
#include "cocaIPC/cocaIPC.hh"
#include "ers/ers.h"

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq::coca {
class FileSystemABC;
}

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_client

/**
 *  @ingroup coca_client
 *
 *  @brief Class providing client API for registering new files in CoCa.
 *
 *  This is a wrapper interface for interaction with CoCa server which
 *  hides all IPC details and simplifies interaction with the server to a
 *  single method call.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Federico Zema
 *  @author Andy Salnikov
 */
class Register {
public:

    /**
     *  @brief Constructor takes a location of the CoCa server.
     *
     *  @param[in] server    CoCa server name.
     *  @param[in] partition The name of the TDAQ partition which contains CoCa server.
     *
     *  @throw RegisterIssues::PartitionNotFound
     *  @throw RegisterIssues::ServerNotFound
     */
    Register (const std::string &server, const std::string &partition);

    // non-copyable
    Register(const Register&) = delete;
    Register& operator=(const Register&) = delete;

    // Destructor
    virtual ~Register() throw ();

    /**
     *  @brief Register new set of files with CoCa.
     *
     *  The method accepts a file name and base directory name which can be empty.
     *  File name starting with slash is treated as absolute otherwise it is considered
     *  relative to current directory. If base directory is empty then the file relative
     *  name will be its basename (last path component). If base directory is not empty
     *  then it must be absolute and it will be removed from file names to obtain
     *  relative names.
     *
     *  Here are few examples:
     *    @code
     *    File name         Base dir       Registered relative names
     *    -----------------------------------------------------------------
     *    /a/b/c.root       ""             c.root
     *    /e/f/g.root       ""             g.root
     *    /a/b/c.root       "/a"           b/c.root
     *    /a/e/f.root       "/a/"          e/f.root
     *    @endcode
     *
     *  @param[in] dataset  Name of dataset.
     *  @param[in] file     File name to register.
     *  @param[in] prio     Priority value, accepted range is 0-100
     *  @param[in] dtcSec   Desired Time in Cache in seconds.
     *  @param[in] baseDir  Directory on client side where file is located.
     *  @return The name of the archive for the newly registered file.
     *
     *  @throw RegisterIssues::PathNameError       Thrown if path names or baseDir have an error
     *  @throw RegisterIssues::MissingFile         Thrown if file is missing or inaccessible.
     *  @throw RegisterIssues::RegistrationRefused Thrown if server does not accept new registrations.
     *  @throw RegisterIssues::RegistrationError   Thrown if server fails to register it.
     *  @throw RegisterIssues::IPCException        Thrown for server communication errors.
     */
    std::string registerFile(const std::string& dataset,
                             const std::string& file,
                             int prio,
                             unsigned dtcSec,
                             const std::string& baseDir);


    /**
     *  @brief Returns server configuration as arbitrary formatted string.
     *
     *  @throw RegisterIssues::IPCException Thrown for server communication errors.
     */
    std::string configuration ();

private:

    virtual FileSystemABC& get_fs();

    cocaIPC::Server_var m_server; ///< CORBA reference for CoCa server.
};

} // namespace coca
} // namespace daq

#endif // COCA_REGISTER_H
