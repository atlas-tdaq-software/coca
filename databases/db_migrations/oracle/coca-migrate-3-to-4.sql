--
-- Migrate CoCa schema from version 3 to version 4
--

ALTER TABLE ATLAS_COCA.COCA1_ARCHIVES DROP (
    DDM,
    DDM_AT,
    DDM_GUID,
    DDM_LFN
);

-- update schema version number
UPDATE ATLAS_COCA.COCA_META SET VALUE = '4' WHERE METAKEY = 'VERSION';

COMMIT;
