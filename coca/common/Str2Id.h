#ifndef COCA_STR2ID_H
#define COCA_STR2ID_H
//--------------------------------------------------------------------------
// File and Version Information:
//      $Id$
//
// Description:
//      Class Str2Id.
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <map>
#include <RelationalAccess/ISessionProxy.h>

//----------------------
// Base Class Headers --
//----------------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------
namespace daq {
    namespace coca {
        class Transaction;
        class NextId;
    }
}

//              ---------------------
//              -- Class Interface --
//              ---------------------

namespace daq {
namespace coca {

/// @addtogroup coca_common

/**
 * @ingroup coca_common
 *
 * @brief Class representing one-to-one mapping between strings and IDs.
 *
 * The unique mapping between numbers and strings is stored as a
 * separate table in CoCa database. This class provides methods to
 * query and update that mapping table.
 */

class Str2Id {
public:

    /**
     *  @brief Standard constructor initializes the mapping object.
     *
     *  @param[in] tablename   The name of the corresponding table in the database.
     *  @param[in] nextid    The ID generator object (see NextId).
     *  @param[in] preFillCache  If true then cache will be filled first time data is accessed.
     */
    Str2Id (const std::string& tablename,
            NextId& nextid,
            bool preFillCache = true);

    // non-copyable
    Str2Id(const Str2Id&) = delete;
    Str2Id& operator=(const Str2Id&) = delete;

    /// Destructor.
    ~Str2Id () throw ();

    /**
     *  @brief Create new ID for a string or return existing one.
     *
     *  If string contains control characters they will be replaces with
     *  underscores first (there is a potential for name clash). Empty
     *  strings are not stored in database, instead we return zero ID.
     *
     *  @param[in] str  String for which we need to find or create ID.
     *  @param[in] tran  Transaction context (see Transaction).
     *  @return  ID corresponding to given string, zero returned for empty string.
     *
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    unsigned insert (const std::string& str, Transaction& tran);

    /**
     *  @brief Find an ID for a string.
     *
     *  @param[in] str  String for which we need to find ID.
     *  @param[in] tran  Transaction context (see Transaction).
     *  @return  ID corresponding to given string, zero returned for empty string.
     *
     *  @throw Issues::NoSuchRecord Thrown if there is no such record exist.
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    unsigned getId(const std::string& str, Transaction& tran);

    /**
     *  @brief Find a string for an ID.
     *
     *  @param[in] id  ID for which we need to find a string.
     *  @param[in] tran  Transaction context (see Transaction).
     *  @return  String corresponding to given ID, empty string is returned for zero ID.
     *
     *  @throw Issues::NoSuchRecord Thrown if there is no such record exist.
     *  @throw coral::Exception Thrown for databases-related conditions.
     */
    std::string getStr (unsigned id, Transaction& tran);

    /// Get table name
    const std::string& tableName () const {return m_tablename;}

    /**
     *  Create database table.
     *
     *  The table must not exist prior to this call.
     *
     *  @param schemaVersion Database schema version number.
     *  @param tran Transaction context.
     */
    void createTable(int schemaVersion, Transaction& tran);

private:

    /// Replace non ASCII characters with legal characters.
    static std::string safeStr (const std::string& unsafe);

    /// Insert one new mapping into cache, it must not exist yet.
    void insertCache (const std::string& str, unsigned id) ;

    /// Retrieve ID from the database, bypass cache.
    unsigned getIdDB (const std::string& str, Transaction& tran);

    /// Retrieve string from database, bypass cache.
    std::string getStrDB (unsigned id, Transaction& tran);

    // fill cache with data from database
    void initCache(Transaction& tr);

    // data members
    const std::string m_tablename;    ///< Name of the database table.
    NextId& m_nextid;                 ///< ID generator object.
    bool m_preFillCache;              ///< If true then cache will be filled first time data is accessed.

    // cache
    std::map<std::string, unsigned> m_cache_str2id;  ///< Cache map from name to ID.
    std::map<unsigned, std::string> m_cache_id2str;  ///< Cache map from ID to name.

};

} // namespace coca
} // namespace daq

#endif // COCA_STR2ID_H
