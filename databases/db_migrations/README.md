# CoCa database schema versions

This folder contains tools (SQL scripts) to upgrade CoCa database schema from
one version to another. SQL scrips in `oracle/` folder are written for Oracle
which is our production database. `sqlite/` folder contains some scripts used
by unit tests to fixup schema after it is created (CORAL cannot make exact
schema that CoCa needs).


## Known versions

### Version 0

This is the version that was used during Run1 and Run2 when we did not care
about schema versioning. This version can be identified by the absence of
`COCA_META` table in the schema.

### Version 1

Adds new table `COCA_META` with key-value records. Key `VERSION` now defines
CoCa schema version, which is set to string "1" with this version. Change is
forward-compatible, previous releases should be able to read/write.

Script: [`coca-migrate-0-to-1.sql`](./oracle/coca-migrate-0-to-1.sql)

### Version 2

Sets default values for `DDM*` columns so that they don't have to be specified
when new records are added to `COCA1_ARCHIVES` table (first step in
[ADHI-4852](https://its.cern.ch/jira/browse/ADHI-4852) implementation). Change
is forward-compatible, previous releases should be able to read/write.

Script: [`coca-migrate-1-to-2.sql`](./oracle/coca-migrate-1-to-2.sql)

### Version 3

Adds few new columns to tables, see
[ADHI-3891](https://its.cern.ch/jira/browse/ADHI-3891). Change is
forward-compatible, previous releases should be able to read/write (but new
columns are not updated if ADHI-3891 is not released).

Script: [`coca-migrate-2-to-3.sql`](./oracle/coca-migrate-2-to-3.sql)

### Version 4

Drops DDM, DDM_AT, DDM_GUID, DDM_LFN columns from `COCA1_ARCHIVES` table.
As usual upgrade script needs to be run only after all coca server instances
switch to new release. See [ADHI-4852](https://its.cern.ch/jira/browse/ADHI-4852).

Script: [`coca-migrate-2-to-3.sql`](./oracle/coca-migrate-3-to-4.sql)
