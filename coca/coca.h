// $Id$
// $Name$

#ifndef COCA_COCA
#define COCA_COCA

#include <coca/client/Register.h>
#include <coca/client/DBClient.h>

/**
 *  @defgroup coca Package coca
 *
 *  @{
 */ 

/**
 *  @defgroup coca_common Package coca_common
 *
 *  @brief Code which is shared by \ref coca_server and \ref coca_client packages.
 *  
 *  This package contains set of classes and namespaces which 
 *  is common to \ref coca_server and \ref coca_client. It contains  
 *  database access classes and set of common data type definitions.
 */

/**
 *  @defgroup coca_server Package coca_server
 *
 *  @brief Code which is used to build coca_server application.
 *  
 *  This package contains set of classes and namespaces which 
 *  together constitute coca_server application. There are additional 
 *  classes in \ref coca_common package which are used by both this 
 *  package and \ref coca_client.
 */

/**
 *  @defgroup coca_client Package coca_client
 *
 *  @brief Client-side library for CoCa.
 *  
 *  This package contains set of classes and namespaces which 
 *  together constitute CoCa client library. There are two distinct 
 *  pieces in the package:
 *   - database access library which does not need access to 
 *     running CoCa server (class DBClient)
 *   - file registration library which is a wrapper for CORBA 
 *     interface of CoCa server (class Register)
 */

/**
 *  @}
 */ 

#endif // COCA_COCA
