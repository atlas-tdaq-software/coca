//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id$
//
// Description:
//	Class FileSystem...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "coca/common/FileSystem.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <boost/filesystem.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "coca/common/Checksum.h"
#include "coca/common/System.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace fs = boost::filesystem;

namespace {

    ERS_DECLARE_ISSUE (warnings, ChgrpFailed,
            "failed to change group on directory " << name << ": " << strerror(errno),
            ((std::string) name))

    ERS_DECLARE_ISSUE (warnings, ChmodFailed,
            "failed to change permissions on directory " << name << ": " << strerror(errno),
            ((std::string) name))


    // remove extra slashes and dots from path name
    std::string normPath (const fs::path& path)
    {
        std::string result;
        for (fs::path::const_iterator it = path.begin(); it != path.end(); ++it) {
            if (*it != ".") {
                if(result.empty()) {
                    result += it->string();
                } else {
                    if(result != "/") result += '/';
                    result += it->string();
                }
            }
        }
        return result;
    }

}

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

namespace daq {
namespace coca {

/**
 * This function creates directory and all its parent directories,
 * and in addition sets UNIX group for new directories and group bit
 * if necessary.
 */
bool
FileSystem::create_dirs(const std::string& path)
{
    const fs::path ph(path);

    // it may exist already, throw if it is a regular file
    if (ph.empty() || exists(ph)) {
        if ( not ph.empty() and not fs::is_directory(ph) ) {
            throw MkdirFileExists(ERS_HERE, ph.string());
        }
        return false;
    }

    // First create parent, by calling ourself recursively
    create_dirs(ph.parent_path().string());

    // Now that parent's path exists, create the directory
    try {
        fs::create_directory(ph);
    } catch (const std::exception& ex) {
        throw MkdirFailed(ERS_HERE, ph.string(), ex);
    }

    // group
    gid_t group = System::instance().newDirGroupId();
    if ( group != (gid_t)-1 ) {
        if ( chown (path.c_str(), (uid_t)-1, group) < 0 ) {
            // it's not a total failure
            ers::warning(::warnings::ChgrpFailed(ERS_HERE, path));
        }
        ERS_DEBUG (0, "directory " << path << ": group set to " << group);
    }

    // permissions
    if (System::instance().newDirHasSetGid()) {

        struct stat buf;
        if (stat (path.c_str(), &buf) != 0) {
            // complain but do no stop
            ers::warning(::warnings::ChmodFailed(ERS_HERE, path));
        } else {
            mode_t mode = buf.st_mode & 07777;
            mode |= S_ISGID;
            if (chmod(path.c_str(), mode) != 0) {
                // complain but do no stop
                ers::warning(::warnings::ChmodFailed(ERS_HERE, path));
            }
        }

        ERS_DEBUG (0, "setgid bit enabled for directory " << path);
    }

    return true;
}

// join two components and remove extra slashes from file name
std::string
FileSystem::join(const std::string& path1, const std::string& path2)
{
    fs::path opath(path1);
    // NOTE: boost::filesystem and std::filesystem behave differently if
    // path2 looks like an absolute path.
    opath /= path2;
    return ::normPath(opath);
}


// Splits pathname into directory and relative path.
std::pair<std::string, std::string>
FileSystem::split(const std::string& apath, const std::string& baseDir, const std::string& cwd)
{
    fs::path path = ::normPath (fs::path(apath));
    fs::path rel_path;
    fs::path dirname;

    // If file name is relative prepend CWD.
    if (not path.is_absolute()) {
        fs::path wd = cwd.empty() ? fs::current_path() : fs::path(cwd);
        path = fs::absolute(path, ::normPath(wd));
    }

    if (baseDir.empty()) {

        // when basedir is empty we use basename as rel_path
        return std::make_pair(path.parent_path().string(), path.filename().string());

    } else {

        // If basedir is not empty then file name must start with the same
        // directory as basedir, strip basedir from file name.

        fs::path base_dir = ::normPath(baseDir);

        // make sure that number of components in path name is higher than in base name
        int ncBaseDir = std::distance(base_dir.begin(), base_dir.end());
        if (ncBaseDir >= std::distance(path.begin(), path.end())) {
            throw BasedirMismatch(ERS_HERE, base_dir.string(), path.string());
        }

        // check that path starts with baseDir
        if (not std::equal(base_dir.begin(), base_dir.end(), path.begin())) {
            throw BasedirMismatch(ERS_HERE, base_dir.string(), path.string());
        }

        // add remaining components to relative path
        fs::path rel_path;
        fs::path::iterator it = path.begin();
        std::advance(it, ncBaseDir);
        for (; it != path.end(); ++ it) {
            rel_path /= *it;
        }

        return std::make_pair(base_dir.string(), rel_path.string());

   }

}


boost::filesystem::file_status
StdFileSystem::status(const fs::path& path)
{
    try {
        return fs::status(path);
    } catch (const fs::filesystem_error &ex) {
        throw FileSystem::Exception(ERS_HERE, ex);
    }
}

bool
StdFileSystem::is_directory(const fs::path& path)
{
    try {
        return fs::is_directory(path);
    } catch (const fs::filesystem_error &ex) {
        throw FileSystem::Exception(ERS_HERE, ex);
    }
}

bool
StdFileSystem::is_regular_file(const fs::path& path)
{
    try {
        return fs::is_regular_file(path);
    } catch (const fs::filesystem_error &ex) {
        throw FileSystem::Exception(ERS_HERE, ex);
    }
}

bool
StdFileSystem::remove(const fs::path& path, bool nothrow)
{
    try {
        return fs::remove(path);
    } catch (const fs::filesystem_error &ex) {
        if (nothrow) {
            return false;
        } else {
            throw FileSystem::Exception(ERS_HERE, ex);
        }
    }
}

void
StdFileSystem::rename(const fs::path& path, const fs::path& newPath)
{
    try {
        return fs::rename(path, newPath);
    } catch (const fs::filesystem_error &ex) {
        throw FileSystem::Exception(ERS_HERE, ex);
    }
}

Size
StdFileSystem::file_size(const fs::path& path)
{
    try {
        return Size(fs::file_size(path));
    } catch (const fs::filesystem_error &ex) {
        throw FileSystem::Exception(ERS_HERE, ex);
    }
}

boost::filesystem::space_info
StdFileSystem::space(const fs::path& path)
{
    try {
        return fs::space(path);
    } catch (const fs::filesystem_error &ex) {
        throw FileSystem::Exception(ERS_HERE, ex);
    }
}

dev_t
StdFileSystem::device_id(const boost::filesystem::path& path)
{
    struct stat stat_buf;
    if (::stat(path.c_str(), &stat_buf) != 0) {
        // some error
        throw FileSystem::Exception(
            ERS_HERE, std::runtime_error("stat() failure for path " + path.string()));
    }
    return stat_buf.st_dev;
}

bool
StdFileSystem::create_dirs(const fs::path& path)
{
    return FileSystem::create_dirs(path.string());
}

std::string
StdFileSystem::checksum(const fs::path& path)
{
    try {
        return Checksum::md5_file(path);
    } catch (const std::exception& ex) {
        throw FileSystem::Exception(ERS_HERE, ex);
    }
}

} // namespace coca
} // namespace daq
