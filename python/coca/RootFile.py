"""File-like object representing read-only ROOT file.

This software was developed for the ATLAS project.  If you use all or
part of it, please give an appropriate acknowledgment.
"""

from __future__ import annotations

import ctypes
import errno
import os
import sys


class RootFile:
    """File-like object for ROOT files."""

    _kWhence: dict[int, int] | None = None

    def __init__(self, name: str):

        self._name = str(name)

        try:
            import ROOT
        except ImportError:
            try:
                rootsys = os.getenv('ROOTSYS')
                if not rootsys:
                    raise
                sys.path.append(os.path.join(rootsys, 'lib'))
                import ROOT
            except ImportError:
                print("Failed to import PyROOT, check your PYTHONPATH", file=sys.stderr)
                raise

        # open a file as raw ROOT file
        self._tfile = ROOT.TFile.Open(self._name + "?filetype=raw")
        if not self._tfile:
            # failed to open, ROOT probably printed something to stderr, just raise exception
            err = errno.ENOENT
            raise IOError(err, os.strerror(err), self._name)

        # Get file size, doing it in costructor means we do not support files which are
        # being resized. This is for performance reasons, otherwise we'd have to do this
        # on every read()
        self._size: int = self._tfile.GetSize()

        if not RootFile._kWhence:
            RootFile._kWhence = {os.SEEK_SET: ROOT.TFile.kBeg,
                                 os.SEEK_CUR: ROOT.TFile.kCur,
                                 os.SEEK_END: ROOT.TFile.kEnd}

    def __del__(self) -> None:
        if self._tfile:
            self._tfile.Close()

    def close(self) -> None:
        if self._tfile:
            if self._tfile.Close():
                err = self._tfile.GetErrno()
                raise IOError(err, os.strerror(err), self._name)
            self._tfile = None

    def flush(self) -> None:
        pass

    def fileno(self) -> int:
        return self._tfile.GetFd()

    def isatty(self) -> bool:
        return False

    def next(self) -> None:
        raise IOError(errno.ENOSYS, os.strerror(errno.ENOSYS), self._name)

    def read(self, size: int | None = None) -> bytes:
        # get current position
        off: int = self._tfile.GetRelOffset()

        # adjust the read size to not read beyond EOF, stupid ROOT cannot tell
        # us how many bytes did it actually read
        if size is None:
            size = self._size - off
        elif off + size > self._size:
            size = self._size - off

        if size == 0:
            return b""

        # read data into buffer
        buf = ctypes.create_string_buffer(int(size))
        stat = self._tfile.ReadBuffer(buf, size)
        if stat:
            err = self._tfile.GetErrno()
            raise IOError(err, os.strerror(err), self._name)
        # return bytes
        return buf[:size]  # type: ignore

    def readline(self, size: int | None = None) -> None:
        # not implemented
        raise IOError(errno.ENOSYS, os.strerror(errno.ENOSYS), self._name)

    def readlines(self, size: int | None = None) -> None:
        # not implemented
        raise IOError(errno.ENOSYS, os.strerror(errno.ENOSYS), self._name)

    def seek(self, offset: int, whence: int = os.SEEK_SET) -> None:

        if whence == os.SEEK_END:
            # ROOT does not support SEEK_END on xrootd
            whence = os.SEEK_SET
            offset = offset + self._size

        # translate to ROOT whence
        assert RootFile._kWhence is not None
        rwhence = RootFile._kWhence[whence]
        #logging.debug("root file seek: off=%s whence=%s rwhence=%s", offset, whence, rwhence)

        self._tfile.Seek(offset, rwhence)
        #logging.debug("root file seek: new off=%s", self._tfile.GetRelOffset())

    def tell(self) -> int:

        off = self._tfile.GetRelOffset()
        #logging.debug("root file tell: off=%s", off)
        return off
